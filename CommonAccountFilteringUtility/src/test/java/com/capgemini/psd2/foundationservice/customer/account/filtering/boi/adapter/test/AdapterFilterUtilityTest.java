package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdapterFilterUtilityTest {

	@InjectMocks
	AdapterFilterUtility adapterFilterUtility;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test(expected = AdapterException.class)
	public void testPrevalidateAccounts(){
		
		Accnts accGetResponse = new Accnts();
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNSC("test");
		accnt1.setAccountNumber("123");
		
		AccountMapping accountMapping = new AccountMapping();
		
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("test");
		accountDetails1.setAccountNumber("123");
		accGetResponse.getAccount().add(accnt1);
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		
		AccountMapping mapping = adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(mapping);
		
		accountDetails1.setAccountNSC("te");
		accountDetails1.setAccountNumber("12");
		adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
	}
}
