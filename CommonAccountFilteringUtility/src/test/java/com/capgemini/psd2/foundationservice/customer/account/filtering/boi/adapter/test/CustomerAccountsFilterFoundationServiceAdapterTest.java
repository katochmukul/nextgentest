/**
 * 
 */
package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client.CustomerAccountsFilterFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.logger.PSD2Constants;

/**
 * @author rchapega
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterFoundationServiceAdapterTest {

	@InjectMocks
	CustomerAccountsFilterFoundationServiceAdapter adapter;
	
	@Mock
	CustomerAccountsFilterFoundationServiceDelegate customerAccountsFilterFoundationServiceDelegate;
	
	@Mock
	CustomerAccountsFilterFoundationServiceClient customerAccountsFilterFoundationServiceClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	@Test(expected= AdapterException.class)
	public void testRetrieveCustomerAccountList(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(), anyObject())).thenReturn(accounts);
		
		Accnts accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		/*
		 * 
		 */
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "PISP");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		/*
		 * UserId Null
		 */
		accnts = adapter.retrieveCustomerAccountList(null, params);
	}
	
	@Test(expected= AdapterException.class)
	public void testRetrieveCustomerAccountListPISP(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(), anyObject())).thenReturn(accounts);
		
		Accnts accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		/*
		 * 
		 */
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "PISP");
		params.put(PSD2Constants.USER_ID, "test");
		params.put(PSD2Constants.CORRELATION_ID, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		/*
		 * UserId Null
		 */
		accnts = adapter.retrieveCustomerAccountList(null, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListParamsNull(){
		
		adapter.retrieveCustomerAccountList("test", null);
	}
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListAccountsAccountNull(){
		
		Map<String, String> params = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(), anyObject())).thenReturn(accounts);
		
		adapter.retrieveCustomerAccountList("test", params);
	}
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListAccountsNull(){
		
		Map<String, String> params = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(), anyObject())).thenReturn(null);
		
		adapter.retrieveCustomerAccountList("test", params);
		
	}
	
	

}
