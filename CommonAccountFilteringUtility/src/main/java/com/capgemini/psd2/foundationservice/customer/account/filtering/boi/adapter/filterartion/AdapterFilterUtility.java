package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;

@Component
public class AdapterFilterUtility {

	public AccountMapping prevalidateAccounts(Accnts accGetResponse, AccountMapping accountMapping) {

		List<Accnt> masterAccounts = (List<Accnt>) accGetResponse.getAccount();
		List<AccountDetails> accDetails = accountMapping.getAccountDetails();
		List<AccountDetails> filteredAccountMapping = new ArrayList<AccountDetails>();
		boolean prevalidateResult = false;

		for (AccountDetails accDetail : accDetails) {

			String nsc = accDetail.getAccountNSC();
			String num = accDetail.getAccountNumber();
			for (Accnt masterAcc : masterAccounts) {

				if (masterAcc.getAccountNumber().equals(num)
						&& masterAcc.getAccountNSC().equals(nsc)) {
					filteredAccountMapping.add(accDetail);
					prevalidateResult = true;
				}
			}

		}
		if (!prevalidateResult) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}
		
		accountMapping.setAccountDetails(filteredAccountMapping);
		return accountMapping;
	}

}