package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.List;
import java.util.Map;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;

public interface FilterationChain {
	public abstract void setNext(FilterationChain nextInChain);
	public abstract FilteredAccounts process(Accnts accounts,String consentFlowType, Map<String , Map<String,List<String>>> accountFiltering);	

}
