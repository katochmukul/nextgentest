package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;

public class JurisdictionFilter implements FilterationChain {
	
	private FilterationChain nextInChain;

	@Override
	public void setNext(FilterationChain next) {
		nextInChain = next;

	}

	@Override
	public FilteredAccounts process(Accnts accounts, String consentFlowType,
			Map<String , Map<String,List<String>>> accountFiltering 	) {
		
		FilteredAccounts filteredAccounts = new FilteredAccounts();
		if (accounts.getAccount().size() > 0) {
			Map<String, List<String>> jurisdictionList = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION);
			if(jurisdictionList != null && !jurisdictionList.isEmpty()){
				List<String> validJurisdictionForFlow = (ArrayList<String>) jurisdictionList.get(consentFlowType);
				Set<String> validJurisdictions = validJurisdictionForFlow.stream().map(u -> u.substring(0, u.indexOf("."))).collect(Collectors.toSet());
				
				List<Accnt> accountList = new ArrayList<>();
				
				for (Accnt accnt : accounts.getAccount()) {
					if (accnt.getJurisdiction() != null){
						if (validJurisdictions.contains(accnt.getJurisdiction().value())) {
							accountList.add(accnt);
						}
					}
				}
				filteredAccounts.setAccount(accountList);
				filteredAccounts.setCreditCardAccount(accounts.getCreditCardAccount());
				return nextInChain.process(filteredAccounts,consentFlowType,accountFiltering);
			}else{
				return nextInChain.process(accounts,consentFlowType,accountFiltering);
			}
		} 
		return filteredAccounts;
	}

	
}
