package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants;

public class CustomerAccountsFilterFoundationServiceConstants {
	
	/** The Constant for  CONSENT_FLOW_TYPE. */
	public static final String  CONSENT_FLOW_TYPE = "consentFlowType";
	public static final String SCHEMENAME="SchemeName";
	public static final String IDENTIFICATION ="Identification";
	public static final String SERVICERSCHEMENAME ="ServicerSchemeName";
	public static final String  SERVICERIDENTIFICATION="ServicerIdentification";
	public static final String ACCOUNTNUMBER ="AccountNumber";
	public static final String IBAN ="IBAN";
	public static final String ACCOUNTNSCNUMBER ="AccountNSCNumber";
	public static final String BIC ="BIC";
	public static final String JURISDICTION ="jurisdiction";
	public static final String PERMISSION ="permission";
	public static final String ACCOUNTTYPE ="accountType";
	public static final String BASEURL ="baseURL";
	public static final String AISP ="AISP";
	public static final String PISP ="PISP";
	
	
}

