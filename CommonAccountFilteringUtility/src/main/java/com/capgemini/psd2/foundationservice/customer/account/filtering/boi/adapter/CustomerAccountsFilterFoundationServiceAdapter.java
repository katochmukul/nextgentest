package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client.CustomerAccountsFilterFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * CustomerAccountProfileFoundationServiceAdapter This adapter is to get
 * Customer Account List
 */
@Component
public class CustomerAccountsFilterFoundationServiceAdapter {

	/* Customer Account Profile service Delegate */
	@Autowired
	private CustomerAccountsFilterFoundationServiceDelegate customerAccountProfileFoundationServiceDelegate;

	/* Customer Account Service Client */
	@Autowired
	private CustomerAccountsFilterFoundationServiceClient customerAccountProfileFoundationServiceClient;

	// @Override
	public Accnts retrieveCustomerAccountList(String userId, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		Accnts accnts = null;
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (CustomerAccountsFilterFoundationServiceConstants.AISP
				.equalsIgnoreCase(params.get(PSD2Constants.CONSENT_FLOW_TYPE))) {
			httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeadersAISP(params);
			requestInfo.setUrl(
					customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLAISP(params, userId));
			accnts = customerAccountProfileFoundationServiceClient.restTransportForCustomerAccountProfile(requestInfo,
					ChannelProfile.class, httpHeaders);

		} else if (CustomerAccountsFilterFoundationServiceConstants.PISP
				.equalsIgnoreCase(params.get(PSD2Constants.CONSENT_FLOW_TYPE))) {
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
					&& !NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
				httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeadersAISP(params);
				requestInfo.setUrl(
						customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLAISP(params, userId));
				accnts = customerAccountProfileFoundationServiceClient
						.restTransportForCustomerAccountProfile(requestInfo, ChannelProfile.class, httpHeaders);
			} else {
				httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeadersForPISP(params);
				requestInfo.setUrl(customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLPISP(
						params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER),
						params.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER),
						params.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER)));
				accnts = customerAccountProfileFoundationServiceClient
						.restTransportForSingleAccountProfile(requestInfo, Accounts.class, httpHeaders);
				
			}
		}
		if (accnts == null || accnts.getAccount() == null || accnts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND);
		}
		return customerAccountProfileFoundationServiceDelegate.getFilteredAccounts(accnts, params);
	}
}
