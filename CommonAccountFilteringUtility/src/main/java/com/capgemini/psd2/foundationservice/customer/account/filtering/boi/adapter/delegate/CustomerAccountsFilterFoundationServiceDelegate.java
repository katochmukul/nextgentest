package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.CustomerAccountsFilter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The class CustomerAccountProfileFoundationServiceDelegate
 *
 */
@Component
public class CustomerAccountsFilterFoundationServiceDelegate {

	/** To create Base URL basis on Channel Id */
	@Autowired
	private AdapterUtility adapterUtility;

	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The workstation in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-WORKSTATION}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-BOI-WORKSTATION}}")
	private String correlationReqHeader;

	/** The platform. */
	@Value("${app.platform}")
	private String platform;

	/* Customer Account Profile Foundation Service Transformer */
	@Autowired
	private CustomerAccountsFilter CustAccntProfileSerTrans;

	/* This method is giving URL */
	public String getFoundationServiceURLAISP(Map<String, String> params, String userId) {
		String baseURL = adapterUtility.retriveCustProfileFoundationServiceURL(params.get(PSD2Constants.CHANNEL_ID));
		return baseURL + "/" + userId + "/accounts";
	}

	/* This method is giving URL */
	public String getFoundationServiceURLPISP(String channelId, String accountNSC, String accountNumber) {
		if (NullCheckUtils.isNullOrEmpty(channelId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_CHANNEL_ID_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		String baseURL = adapterUtility.retriveFoundationServiceURL(channelId);
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNSC + "/" + accountNumber ;
	}

	/*
	 * This method is calling customer account profile transformer to convert
	 * object
	 */
	public Accnts getFilteredAccounts(Accnts accnts, Map<String, String> params) {
		return CustAccntProfileSerTrans.getFilteredAccounts(accnts, params);
	}

	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo
	 *            the request info
	 * @param accountMapping
	 *            the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeadersAISP(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_ID))
				|| NullCheckUtils.isNullOrEmpty(platform)
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(PSD2Constants.USER_ID));
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_ID));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		return httpHeaders;
	}

	public HttpHeaders createRequestHeadersForPISP(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
				|| NullCheckUtils.isNullOrEmpty(platform)
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		return httpHeaders;
	}
}