package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;

public class PermissionFilter implements FilterationChain {

	private FilterationChain nextInChain;

	@Override
	public void setNext(FilterationChain next) {
		nextInChain = next;

	}

	@Override
	public FilteredAccounts process(Accnts accounts, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {
		
		FilteredAccounts filteredAccounts = new FilteredAccounts();
		if (accounts.getAccount().size() > 0) {
			Map<String, List<String>> permissionList = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.PERMISSION);
			if(permissionList != null && !permissionList.isEmpty()){
			List<String> permissionForFlow = (ArrayList<String>) permissionList.get(consentFlowType);
			
			List<Accnt> accountList = new ArrayList<>();
			for (Accnt accnt : accounts.getAccount()) {
				if (accnt.getAccountPermission() != null){
					if (permissionForFlow.contains(accnt.getAccountPermission().trim())) {   
						accountList.add(accnt);
					}
				}
			}
			filteredAccounts.setAccount(accountList);
			filteredAccounts.setCreditCardAccount(accounts.getCreditCardAccount());
			return filteredAccounts;
			}
			else
			{
				filteredAccounts.setAccount(accounts.getAccount());
				filteredAccounts.setCreditCardAccount(accounts.getCreditCardAccount());
				return filteredAccounts;
			}
		}
		return filteredAccounts;

	
	}
}
