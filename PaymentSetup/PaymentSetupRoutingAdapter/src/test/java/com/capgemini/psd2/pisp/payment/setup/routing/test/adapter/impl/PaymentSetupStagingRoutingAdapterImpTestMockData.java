package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;

public class PaymentSetupStagingRoutingAdapterImpTestMockData {

	
	public static CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse;
	public static PaymentSetupStagingResponse paymentSetupStagingResponse;
	
	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponse(){
		paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		return paymentSetupPOSTResponse;
	}
	
	public static PaymentSetupStagingResponse getPaymentSetupStagingResponse(){
		paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		return paymentSetupStagingResponse;
	}
	
}
