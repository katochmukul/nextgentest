package com.capgemini.psd2.pisp.payment.setup.validation.routing.test.adapter.impl;

import java.util.Map;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public class PaymentSetupValidationTestRoutingAdapterImpl implements PaymentSetupValidationAdapter, PaymentAuthorizationValidationAdapter{

	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {
		return PaymentSetupValidationRoutingAdapterTestMockData.getPaymentSetupValidationResponse();
	}

	@Override
	public PaymentSetupValidationResponse validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest, Map<String, String> params) {
		return PaymentSetupValidationRoutingAdapterTestMockData.getPaymentSetupValidationResponse();
	}

	

}
