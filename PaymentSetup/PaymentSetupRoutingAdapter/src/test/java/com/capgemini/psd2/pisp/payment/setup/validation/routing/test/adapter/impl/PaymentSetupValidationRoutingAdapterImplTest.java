package com.capgemini.psd2.pisp.payment.setup.validation.routing.test.adapter.impl;

import static org.mockito.Matchers.anyObject;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.PaymentSetupValidationRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.PaymentSetupAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupValidationRoutingAdapterImplTest {
	
	@InjectMocks
	PaymentSetupValidationRoutingAdapterImpl adapter;
	
	@Mock
	private PaymentSetupAdapterFactory factory;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void validatePreStagePaymentSetupTest(){
		PaymentSetupValidationTestRoutingAdapterImpl paymentSetupValidationTestRoutingAdapterImpl = new PaymentSetupValidationTestRoutingAdapterImpl();
		Mockito.when(factory.getPaymentSetupValidationInstance(anyObject())).thenReturn(paymentSetupValidationTestRoutingAdapterImpl);
		adapter.validatePreStagePaymentSetup(new CustomPaymentSetupPOSTRequest(), new HashMap<>());
	}
	
	@Test
	public void preAuthorizationPaymentValidationTest(){
		
		PaymentSetupValidationTestRoutingAdapterImpl paymentSetupValidationTestRoutingAdapterImpl = new PaymentSetupValidationTestRoutingAdapterImpl();
		Mockito.when(factory.getPaymentAuthorizationValidationInstance(anyObject())).thenReturn(paymentSetupValidationTestRoutingAdapterImpl);
		adapter.preAuthorizationPaymentValidation(new CustomPaymentSetupPOSTResponse(), new HashMap<>());
	}
}
