package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.impl;

import java.util.Map;

import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;

public class PaymentSetupStagingTestRoutingAdapterImpl implements PaymentSetupStagingAdapter{

	@Override
	public CustomPaymentSetupPOSTResponse retrieveStagedPaymentSetup(String paymentId, Map<String, String> params) {
		return PaymentSetupStagingRoutingAdapterImpTestMockData.getPaymentSetupPOSTResponse();
	}

	@Override
	public PaymentSetupStagingResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest,
			Map<String, String> params) {
		return PaymentSetupStagingRoutingAdapterImpTestMockData.getPaymentSetupStagingResponse();
	}

	@Override
	public PaymentSetupStagingResponse updateStagedPaymentSetup(CustomPaymentSetupPOSTResponse paymentSetupBankResource,
			Map<String, String> params) {
		return PaymentSetupStagingRoutingAdapterImpTestMockData.getPaymentSetupStagingResponse();
	}

}
