package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.routing;

import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.PaymentSetupStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.PaymentSetupValidationRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.PaymentSetupCoreSystemAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupCoreSystemAdapterFactoryTest {

	@InjectMocks
	PaymentSetupCoreSystemAdapterFactory factory;
	
	@Mock
	private ApplicationContext applicationContext;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void getPaymentSetupStagingInstanceTest(){
		
		PaymentSetupStagingAdapter paymentSetupStagingAdapter = new PaymentSetupStagingRoutingAdapterImpl();
		
		Mockito.when(applicationContext.getBean(anyString())).thenReturn(paymentSetupStagingAdapter);
		factory.getPaymentSetupStagingInstance("adapterName");
	}
	
	@Test
	public void getPaymentSetupValidationInstanceTest(){
		
		PaymentSetupValidationAdapter paymentSetupValidationAdapter = new PaymentSetupValidationRoutingAdapterImpl();
		Mockito.when(applicationContext.getBean(anyString())).thenReturn(paymentSetupValidationAdapter);
		factory.getPaymentSetupValidationInstance("adapterName");
	}
	
	@Test
	public void getPaymentAuthorizationValidationInstanceTest(){
		
		PaymentAuthorizationValidationAdapter authorizationValidationAdapter = new PaymentSetupValidationRoutingAdapterImpl();
		Mockito.when(applicationContext.getBean(anyString())).thenReturn(authorizationValidationAdapter);
		factory.getPaymentAuthorizationValidationInstance("adapterName");
	}
	
	
}
