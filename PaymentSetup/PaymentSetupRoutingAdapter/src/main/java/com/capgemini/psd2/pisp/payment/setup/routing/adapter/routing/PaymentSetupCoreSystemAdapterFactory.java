
package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;

@Component
public class PaymentSetupCoreSystemAdapterFactory implements ApplicationContextAware, PaymentSetupAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;
		
	@Override
	public PaymentSetupStagingAdapter getPaymentSetupStagingInstance(String adapterName) {
		return (PaymentSetupStagingAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public PaymentSetupValidationAdapter getPaymentSetupValidationInstance(String adapterName) {
		return (PaymentSetupValidationAdapter) applicationContext.getBean(adapterName);
	}
	
	@Override
	public PaymentAuthorizationValidationAdapter getPaymentAuthorizationValidationInstance(String adapterName) {
		return (PaymentAuthorizationValidationAdapter) applicationContext.getBean(adapterName);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}

	
}
