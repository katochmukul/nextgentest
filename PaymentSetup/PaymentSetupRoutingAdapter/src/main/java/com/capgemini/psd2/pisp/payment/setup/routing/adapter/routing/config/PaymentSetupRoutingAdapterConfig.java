package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.PaymentSetupStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.PaymentSetupValidationRoutingAdapterImpl;

@Configuration
public class PaymentSetupRoutingAdapterConfig {

		
	@Bean(name = "paymentSetupValidationRoutingAdapter")
	public PaymentSetupValidationAdapter paymentSetupValidationRoutingAdapter() {
		return new PaymentSetupValidationRoutingAdapterImpl();
	}
	
	@Bean(name = "paymentSetupStagingRoutingAdapter")
	public PaymentSetupStagingAdapter paymentSetupStagingAdapter() {
		return new PaymentSetupStagingRoutingAdapterImpl();
	}
}
