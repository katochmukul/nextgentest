package com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.PaymentSetupAdapterFactory;

public class PaymentSetupValidationRoutingAdapterImpl implements PaymentSetupValidationAdapter,PaymentAuthorizationValidationAdapter{
	
	/** The factory. */
	@Autowired
	private PaymentSetupAdapterFactory factory;

	@Value("${app.paymentSetupValidationAdapter:#{null}}")
	private String paymentSetupValidationAdapter;
	
	@Value("${app.paymentAuthorizationValidationAdapter:#{null}}")
	private String paymentAuthorizationValidationAdapter;
	

	@Override
	public PaymentSetupValidationResponse validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest,
			Map<String, String> params) {
		return getPaymentSetupValidationRoutingAdapter().validatePreStagePaymentSetup(paymentSetupRequest, params);
	}
	
	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(
			CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {
		return getPaymentAuthorizationValidationRoutingAdapter().preAuthorizationPaymentValidation(paymentSetupResponse, params);
	}
	
	private PaymentAuthorizationValidationAdapter getPaymentAuthorizationValidationRoutingAdapter(){
		return factory.getPaymentAuthorizationValidationInstance(paymentAuthorizationValidationAdapter);
	}
	
	private PaymentSetupValidationAdapter getPaymentSetupValidationRoutingAdapter(){
		return factory.getPaymentSetupValidationInstance(paymentSetupValidationAdapter);
	}

	

}
