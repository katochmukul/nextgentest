package com.capgemini.psd2.pisp.payment.setup.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.payment.setup.controller.PaymentSetupController;
import com.capgemini.psd2.pisp.payment.setup.service.PaymentSetupService;
import com.capgemini.psd2.pisp.payment.setup.test.mock.data.PaymentSetupRequestResponseMockData;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupControllerTest {
	
	private MockMvc mockMvc;

	@Mock
	private PaymentSetupService service; 

	@Mock
	private PaymentValidator validator;
	
	@InjectMocks
	private PaymentSetupController controller;
	
	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}
	
	@Test
	public void testPaymentSetupPOSTRequestTestPositive() throws Exception {

		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockResponse();

		Mockito.when(validator.validatePaymentSetupRequest(paymentSetupPOSTRequest)).thenReturn(Boolean.TRUE);
		Mockito.when(service.createPaymentSetupResource(paymentSetupPOSTRequest, Boolean.TRUE)).thenReturn(paymentSetupPOSTResponse);
		this.mockMvc.perform(post("/payments").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSetupRequestResponseMockData.asJsonString(paymentSetupPOSTRequest)))
				.andExpect(status().isCreated());
	}
	
	@Test
	public void testPaymentSetupPOSTRequestTestNegative() throws Exception {
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		Mockito.when(validator.validatePaymentSetupRequest(paymentSetupPOSTRequest)).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		this.mockMvc.perform(post("/payments").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSetupRequestResponseMockData.asJsonString(paymentSetupPOSTRequest)))
		.andExpect(status().isBadRequest());
	}
	
	@Test(expected = Exception.class)
	public void testPaymentSetupPOSTRequestTestException() throws Exception {
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(validator.validatePaymentSetupRequest(paymentSetupPOSTRequest)).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		try{
			
			this.mockMvc.perform(post("/payments").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSetupRequestResponseMockData.asJsonString(paymentSetupPOSTRequest)));
		}catch(Exception e){
			assertEquals(true, e.getMessage().contains("IBAN Identification value of Creditor/Debtor Account is not valid."));
			throw e;
		}		
	}
	
	@Test(expected = Exception.class)
	public void testPaymentSetupPOSTRequestTestNegativeException() throws Exception {
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(validator.validatePaymentSetupRequest(paymentSetupPOSTRequest)).thenReturn(true);
		Mockito.when(service.createPaymentSetupResource(any(CustomPaymentSetupPOSTRequest.class), any(Boolean.class))).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		try{
			
			this.mockMvc.perform(post("/payments").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSetupRequestResponseMockData.asJsonString(paymentSetupPOSTRequest)));
		}catch(Exception e){
			assertEquals(true, e.getMessage().contains("IBAN Identification value of Creditor/Debtor Account is not valid."));
			throw e;
		}		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSetupGetRequestTestException() {
		String paymentId = "12345";
		PaymentRetrieveGetRequest request = new PaymentRetrieveGetRequest();
		request.setPaymentId(paymentId);
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(validator.validatePaymentRetrieveRequest(request)).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		controller.retrievePaymentSetupResource(request);		
	}
	
	@Test
	public void testPaymentSetupGetRequestTestNegative() {
		String paymentId = "12345";
		PaymentRetrieveGetRequest request = new PaymentRetrieveGetRequest();
		request.setPaymentId(paymentId);
		Mockito.when(validator.validatePaymentRetrieveRequest(request)).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		ResponseEntity<PaymentSetupPOSTResponse> response = controller.retrievePaymentSetupResource(request);
		assertTrue(HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
	}
	
	@Test
	public void testPaymentSetupGETRequestTestPositive() throws Exception {
		PaymentSetupPOSTResponse paymentSetupPOSTResponse = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockResponse();
		String paymentId = "12345";
		PaymentRetrieveGetRequest request = new PaymentRetrieveGetRequest();
		request.setPaymentId(paymentId);
		Mockito.when(validator.validatePaymentRetrieveRequest(request)).thenReturn(Boolean.TRUE);
		Mockito.when(service.retrievePaymentSetupResource(request)).thenReturn(paymentSetupPOSTResponse);
		
		ResponseEntity<PaymentSetupPOSTResponse> response =controller.retrievePaymentSetupResource(request);

		assertTrue(paymentSetupPOSTResponse.equals((PaymentSetupPOSTResponse)response.getBody()));
	}
	
	@After
	public void tearDown() throws Exception {
		controller = null;
		mockMvc = null;
		service = null; 
		validator = null;
		
	}

}
