package com.capgemini.psd2.pisp.payment.setup.test.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.comparator.PaymentSetupPayloadComparator;
import com.capgemini.psd2.pisp.validation.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupPayloadComparatorTest {

	@InjectMocks
	PaymentSetupPayloadComparator comparator;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void compareTest(){
		
		/*
		 * For Equality
		 */
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		
		CustomPaymentSetupPOSTResponse response1 = new CustomPaymentSetupPOSTResponse();
		response1.setData(data);
		response1.setRisk(risk);
	
		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());
		
		int result = comparator.compare(response, response1);
		assertEquals(0, result);
		
		
		/*
		 * For Non-Equality
		 */
		response1 = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data1 = new PaymentSetupResponse();
		response1.setData(data1);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk1 = new Risk();
		response1.setRisk(risk1);
		
		PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation1);
		
		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);
		
		CreditorAgent creditorAgent1 = new CreditorAgent();
		creditorAgent1.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		
		CreditorAccount creditorAccount1 = new CreditorAccount();
		creditorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setSecondaryIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);
		
		DebtorAgent debtorAgent1 = new DebtorAgent();
		debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent1.setIdentification("SC112800");
		initiation1.setDebtorAgent(debtorAgent1);
		
		DebtorAccount debtorAccount1 = new DebtorAccount();
		debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);
		
		RemittanceInformation remittanceInformation1 = new RemittanceInformation();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);
		
		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress1 = new RiskDeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);
		
		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");		
		deliveryAddress1.setAddressLine(addressLine1);
		
		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");
		
		List<String> countrySubDivision1 = new ArrayList<>();
		countrySubDivision1.add("ABCDABCDABCDABCDAB");
		countrySubDivision1.add("DEFG");
		deliveryAddress1.setCountrySubDivision(countrySubDivision1);
		
		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");
		
		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());
		
		result = comparator.compare(response, response1);
		assertEquals(1, result);

	}
	
	@Test
	public void comparePaymentDetailsTest(){
		
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		
		
		
		CustomPaymentSetupPOSTResponse response1 = new CustomPaymentSetupPOSTResponse();
		
		PaymentSetupResponse data1 = new PaymentSetupResponse();
		response1.setData(data1);
		data1.setPaymentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk1 = new Risk();
		response1.setRisk(risk1);
		
		PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
		data1.setInitiation(initiation1);
		
		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);
		
		CreditorAgent creditorAgent1 = new CreditorAgent();
		creditorAgent1.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		
		CreditorAccount creditorAccount1 = new CreditorAccount();
		creditorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setSecondaryIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);
		
		DebtorAgent debtorAgent1 = new DebtorAgent();
		debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent1.setIdentification("SC112800");
		initiation1.setDebtorAgent(debtorAgent1);
		
		DebtorAccount debtorAccount1 = new DebtorAccount();
		debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);
		
		RemittanceInformation remittanceInformation1 = new RemittanceInformation();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);
		
		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress1 = new RiskDeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);
		
		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");		
		deliveryAddress1.setAddressLine(addressLine1);
		
		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");
		
		List<String> countrySubDivision1 = new ArrayList<>();
		countrySubDivision1.add("ABCDABCDABCDABCDAB");
		countrySubDivision1.add("DEFG");
		deliveryAddress1.setCountrySubDivision(countrySubDivision1);
		
		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");
		
		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());
		
		PaymentSetupPlatformResource resource = new PaymentSetupPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(0, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//DebtorAgent null
		initiation.setDebtorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//DebtorAgent null
		initiation.setDebtorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1,     request, resource);
		assertEquals(1, result);
		
		//request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(0, result);
		
		//
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(0, result);
	}
	
	
	@Test
	public void comparePaymentDetailsTest2(){
		
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add(null);
				
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add(null);

		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		
		
		
		CustomPaymentSetupPOSTResponse response1 = new CustomPaymentSetupPOSTResponse();
		
		PaymentSetupResponse data1 = new PaymentSetupResponse();
		response1.setData(data1);
		data1.setPaymentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk1 = new Risk();
		response1.setRisk(risk1);
		
		PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
		data1.setInitiation(initiation1);
		
		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);
		
		CreditorAgent creditorAgent1 = new CreditorAgent();
		creditorAgent1.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		
		CreditorAccount creditorAccount1 = new CreditorAccount();
		creditorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setSecondaryIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);
		
		DebtorAgent debtorAgent1 = new DebtorAgent();
		debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent1.setIdentification("SC112800");
		initiation1.setDebtorAgent(debtorAgent1);
		
		DebtorAccount debtorAccount1 = new DebtorAccount();
		debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);
		
		RemittanceInformation remittanceInformation1 = new RemittanceInformation();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);
		
		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress1 = new RiskDeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);
		
		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add(null);
		
		
		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");
		
		List<String> countrySubDivision1 = new ArrayList<>();
		countrySubDivision1.add(null);
		
		
		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");
		
		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());
		
		PaymentSetupPlatformResource resource = new PaymentSetupPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		//assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//DebtorAgent null
		initiation.setDebtorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//DebtorAgent null
		initiation.setDebtorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		//assertEquals(1, result);
		
		//TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1,     request, resource);
		//assertEquals(1, result);
		
		//request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		//assertEquals(0, result);
		
		//
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}
}
