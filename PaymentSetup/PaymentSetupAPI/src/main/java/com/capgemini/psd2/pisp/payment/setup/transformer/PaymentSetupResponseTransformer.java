package com.capgemini.psd2.pisp.payment.setup.transformer;


import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;

public interface PaymentSetupResponseTransformer {
	public PaymentSetupPOSTResponse paymentSetupResponseTransformer(CustomPaymentSetupPOSTResponse paymentSetupResponse, PaymentSetupPlatformResource paymentSetupPlatformResource, String methodType);
}

