package com.capgemini.psd2.pisp.payment.setup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.payment.setup.comparator.PaymentSetupPayloadComparator;
import com.capgemini.psd2.pisp.payment.setup.service.PaymentSetupService;
import com.capgemini.psd2.pisp.payment.setup.transformer.PaymentSetupResponseTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class PaymentSetupServiceImpl implements PaymentSetupService {

	@Autowired
	private PaymentSetupResponseTransformer responseTransformer;
	@Autowired
	private PaymentSetupPayloadComparator paymentSetupComparator;	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;	
	@Autowired
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;		
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	private long idempotencyDuration;
	
	@Autowired
	public PaymentSetupServiceImpl(@Value("${app.idempotency.durationForPaymentSetup}") String idempotencyDuration){
		this.idempotencyDuration = PispUtilities.getMilliSeconds(idempotencyDuration);
	}

	@Override
	public PaymentSetupPOSTResponse createPaymentSetupResource(CustomPaymentSetupPOSTRequest paymentSetupRequest, boolean isValidPaymentSetupRequest) {
			
		if(! isValidPaymentSetupRequest){			
			createInvalidPaymentSetupResource(paymentSetupRequest);
			return null;
		}	
		
		PaymentSetupPOSTResponse paymentSetupResponse = performIdempotencyCheck(paymentSetupRequest);
		if(paymentSetupResponse != null && paymentSetupResponse.getData() != null)
			return paymentSetupResponse;	
		
		PaymentResponseInfo validationDetails = paymentSetupAdapterHelper.validatePreStagePaymentSetup(paymentSetupRequest);	
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = paymentSetupPlatformAdapter.createPaymentSetupResource(paymentSetupRequest, validationDetails);
		
		CustomPaymentSetupPOSTResponse paymentSetupFoundationResponse = createStagingPaymentSetup(paymentSetupRequest, paymentSetupPlatformResource);		
										
		return responseTransformer.paymentSetupResponseTransformer(paymentSetupFoundationResponse, paymentSetupPlatformResource, RequestMethod.POST.toString());
		
	}
	
	
	
	private CustomPaymentSetupPOSTResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest, PaymentSetupPlatformResource paymentSetupPlatformResource){
		
		paymentSetupRequest.setPaymentStatus(paymentSetupPlatformResource.getStatus());
		PaymentSetupStagingResponse stagingResponse = paymentSetupAdapterHelper.createStagingPaymentSetup(paymentSetupRequest);
		
		if(NullCheckUtils.isNullOrEmpty(stagingResponse.getPaymentId())){			
		
			paymentSetupPlatformResource.setStatus(PaymentStatusEnum.REJECTED.getStatusCode());			
			paymentSetupPlatformResource.setIdempotencyRequest(String.valueOf(Boolean.FALSE));
			paymentSetupPlatformAdapter.updatePaymentSetupResource(paymentSetupPlatformResource);
			
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_SETUP_CREATION_FAILED);
		}
		
		if(PaymentStatusEnum.PENDING.getStatusCode().equalsIgnoreCase(paymentSetupPlatformResource.getStatus()))						
			paymentSetupPlatformResource.setStatus(PaymentStatusEnum.ACCEPTEDTECHNICALVALIDATION.getStatusCode());					
		
		
		paymentSetupPlatformResource.setPaymentId(stagingResponse.getPaymentId());	
		paymentSetupPlatformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		
		CustomPaymentSetupPOSTResponse paymentSetupResponse = transformPaymentSetupRequestToResponse(paymentSetupRequest, paymentSetupPlatformResource);		
		
		/* 
		 * Date: 04-Aug-2017
		 * While updating final status of staged payment at bank end, if platform receives any exception from bank end
		 * then staged payment status of platform would not be rolled back and 
		 * error log for this exception will be maintained at Adapter end. 
		 * No exception will be thrown to PISP from here.
		 * Later platform will update the status at bank end.
		 * If payment setup validation's status is 'Rejected' then no need to send update call to FS
		 */
		if(! PaymentStatusEnum.REJECTED.getStatusCode().equalsIgnoreCase(paymentSetupResponse.getData().getStatus().toString()))
			paymentSetupAdapterHelper.updateStagedPaymentSetup(paymentSetupResponse);	
		
		verifyAndUpdatePaymentSetupResource(paymentSetupPlatformResource);  		
		
		return paymentSetupResponse;
	}
	
	/*
	 * If paymentId is already used for other resource then platform will throw the exception
	 */
	private void verifyAndUpdatePaymentSetupResource(PaymentSetupPlatformResource paymentPlatformResource){
		
		if(paymentSetupPlatformAdapter.retrievePaymentSetupResource(paymentPlatformResource.getPaymentId()) != null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DUPLICATE_PAYMENT_ID);
			
		paymentSetupPlatformAdapter.updatePaymentSetupResource(paymentPlatformResource);
	}
	
	private CustomPaymentSetupPOSTResponse transformPaymentSetupRequestToResponse(CustomPaymentSetupPOSTRequest paymentSetupRequest, PaymentSetupPlatformResource paymentSetupPlatformResponse){
		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
		CustomPaymentSetupPOSTResponse paymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentRequest, CustomPaymentSetupPOSTResponse.class);
		paymentSetupResponse.getData().setPaymentId(paymentSetupPlatformResponse.getPaymentId());
		paymentSetupResponse.getData().setStatus(StatusEnum.fromValue(paymentSetupPlatformResponse.getStatus()));
		paymentSetupResponse.setPaymentStatus(StatusEnum.fromValue(paymentSetupPlatformResponse.getStatus()).toString());
		paymentSetupResponse.getData().setCreationDateTime(paymentSetupPlatformResponse.getCreatedAt());
		return paymentSetupResponse;
	}
	

	private void createInvalidPaymentSetupResource(CustomPaymentSetupPOSTRequest paymentSetupRequest){
		PaymentResponseInfo info = new PaymentResponseInfo();
		info.setPaymentValidationStatus(PaymentStatusEnum.REJECTED.getStatusCode());		
		info.setIdempotencyRequest(String.valueOf(Boolean.FALSE));
		paymentSetupPlatformAdapter.createPaymentSetupResource(paymentSetupRequest, info);
	}
	
			
	@Override
	public PaymentSetupPOSTResponse retrievePaymentSetupResource(PaymentRetrieveGetRequest request) {

			
  		PaymentSetupPlatformResource paymentSetupPlatformResponse= paymentSetupPlatformAdapter.retrievePaymentSetupResource(request.getPaymentId().trim());			
		
		if(paymentSetupPlatformResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_ID_FOUND_IN_SYSTEM);			
		
		if(! paymentSetupPlatformResponse.getTppCID().equalsIgnoreCase(reqHeaderAtrributes.getTppCID()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_RESOURCE_FOUND_AGAINST_TPP);
				
		CustomPaymentSetupPOSTResponse paymentSetupFoundationResponse = paymentSetupAdapterHelper.retrieveStagedPaymentSetup(paymentSetupPlatformResponse.getPaymentId());
		return responseTransformer.paymentSetupResponseTransformer(paymentSetupFoundationResponse, paymentSetupPlatformResponse, RequestMethod.GET.toString());		
	}
	
	
	private PaymentSetupPOSTResponse performIdempotencyCheck(CustomPaymentSetupPOSTRequest paymentSetupRequest){
		
		PaymentSetupPlatformResource paymentSetupPlatformResponse = paymentSetupPlatformAdapter.getIdempotentPaymentSetupResource(idempotencyDuration);
		
		if(paymentSetupPlatformResponse == null)
			return null;
				
		CustomPaymentSetupPOSTResponse paymentSetupFoundationResponse;
		
		if(PaymentStatusEnum.PENDING.getStatusCode().equalsIgnoreCase(paymentSetupPlatformResponse.getStatus()) && 
									 NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResponse.getPaymentId())){
			/*
			 * Platform will not do any payload comparison or debtor flag validation. 
			 * It will send the request directly to stage payment insertion. 
			 * FS has to do payload validation for possible any change.
			 * Date: 03-August-2017
			 */
		
			paymentSetupFoundationResponse = createStagingPaymentSetup(paymentSetupRequest, paymentSetupPlatformResponse);
		}else{
			paymentSetupFoundationResponse = paymentSetupAdapterHelper.retrieveStagedPaymentSetup(paymentSetupPlatformResponse.getPaymentId());	
			if (paymentSetupComparator.comparePaymentDetails(paymentSetupFoundationResponse, paymentSetupRequest, paymentSetupPlatformResponse) > 0)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_PAYLOAD_COMPARISON_FAILED);
		}
		
		return responseTransformer.paymentSetupResponseTransformer(paymentSetupFoundationResponse, paymentSetupPlatformResponse, RequestMethod.POST.toString());
	}
		
}
