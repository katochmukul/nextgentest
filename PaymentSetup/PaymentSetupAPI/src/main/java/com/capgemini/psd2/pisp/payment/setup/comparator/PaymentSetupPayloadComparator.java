package com.capgemini.psd2.pisp.payment.setup.comparator;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PaymentSetupPayloadComparator implements Comparator<PaymentSetupPOSTResponse>{

	@Override
	public int compare(PaymentSetupPOSTResponse paymentResponse, PaymentSetupPOSTResponse adaptedPaymentResponse) {		
		
		int value = 1;			
		if(paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation()) && 
		   paymentResponse.getRisk().equals(adaptedPaymentResponse.getRisk()))
			value = 0;
	
		return value;
	}
	
	public int comparePaymentDetails(CustomPaymentSetupPOSTResponse response, CustomPaymentSetupPOSTRequest request, PaymentSetupPlatformResource paymentSetupPlatformResource){
		
		String strPaymentSetupRequest = JSONUtilities.getJSONOutPutFromObject(request);
		CustomPaymentSetupPOSTResponse adaptedPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), strPaymentSetupRequest, CustomPaymentSetupPOSTResponse.class);
		
		if(! validateDebtorDetails(response.getData().getInitiation(), adaptedPaymentSetupResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;
		
		compareAmount(response.getData().getInitiation().getInstructedAmount(), adaptedPaymentSetupResponse.getData().getInitiation().getInstructedAmount());
		/*
		 * Date: 13-Feb-2018
		 * Changes are made for CR-10
		 */
		checkAndModifyEmptyFields(adaptedPaymentSetupResponse);
		int returnValue = compare(response,adaptedPaymentSetupResponse);
		
		response.getData().getInitiation().setRemittanceInformation(request.getData().getInitiation().getRemittanceInformation());
		response.getRisk().setDeliveryAddress(request.getRisk().getDeliveryAddress());
		
		//End of CR-10
		return returnValue;
		
	}
	
	private void checkAndModifyEmptyFields(PaymentSetupPOSTResponse adaptedPaymentResponse) {

		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation());
		if(adaptedPaymentResponse.getRisk().getDeliveryAddress() != null){
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress());
			checkAndModifyCountrySubDivision(adaptedPaymentResponse.getRisk().getDeliveryAddress());
		}
		
	}
	private void checkAndModifyRemittanceInformation(PaymentSetupResponseInitiation responseInitiation){
		
		RemittanceInformation remittanceInformation = responseInitiation.getRemittanceInformation();
		if(remittanceInformation != null && NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference()) && 
											NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())){			
		
			responseInitiation.setRemittanceInformation(null);
		}
	}
	
	private void checkAndModifyAddressLine(RiskDeliveryAddress riskDeliveryAddress){
		
		if(riskDeliveryAddress.getAddressLine() != null){
		   List<String> addressLineList = new ArrayList<>();
		   for(String addressLine : riskDeliveryAddress.getAddressLine()){
			   if(NullCheckUtils.isNullOrEmpty(addressLine))
				   continue;
			   addressLineList.add(addressLine);			   
		   }
		   if(addressLineList.isEmpty())
			   addressLineList = null;
		   
		   riskDeliveryAddress.setAddressLine(addressLineList);
			
		}
	}
	
	private void checkAndModifyCountrySubDivision(RiskDeliveryAddress riskDeliveryAddress){
		if(riskDeliveryAddress.getCountrySubDivision() != null){
			List<String> countrySubDivisionList = new ArrayList<>();
			   for(String countrySubDivision : riskDeliveryAddress.getCountrySubDivision()){
				   if(NullCheckUtils.isNullOrEmpty(countrySubDivision))
					   continue;
				   countrySubDivisionList.add(countrySubDivision);			   
			   }
			   if(countrySubDivisionList.isEmpty())
				   countrySubDivisionList = null;
			   
			   riskDeliveryAddress.setCountrySubDivision(countrySubDivisionList);
				
		}
	}
	
	private boolean validateDebtorDetails(PaymentSetupResponseInitiation responseInitiation, PaymentSetupResponseInitiation requestInitiation, PaymentSetupPlatformResource paymentSetupPlatformResource){
		
		
		if(Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())){
			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then name will participate in comparison 
			 * else product will set foundation response DebtorAccount.Name as null so that it can be passed in comparison.
			 * Both fields would have the value as 'Null'.
			 */
			if( ! Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				responseInitiation.getDebtorAccount().setName(null);
			
			return Objects.equals(responseInitiation.getDebtorAgent(), requestInitiation.getDebtorAgent()) &&  
			         Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());			
		}
		if(! Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()) && requestInitiation.getDebtorAccount() != null){
			return Boolean.FALSE;  
		}
		if(! Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()) && responseInitiation.getDebtorAccount() != null){
			responseInitiation.setDebtorAgent(null);
			responseInitiation.setDebtorAccount(null);
			return Boolean.TRUE;  
		}
		return Boolean.TRUE;
		
	}
	
	/*
	 * Date: 12-Jan-2018, SIT defect#956
	 * Comparing two float values received from FS and TPP.
	 * Both values must be treated equal mathematically if values are in below format.
	 * i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 == 0.20000, 001.00 == 1.00 etc.
	 * If values are equal then set the amount received from TPP to the amount object of FS response.
	 * By doing this, comparison would be passed in swagger's java file(compare method of this class) 
	 * and same amount would be available to TPP also in response of idempotent request. 
	 */
	private void compareAmount(PaymentSetupInitiationInstructedAmount responseInstructedAmount, PaymentSetupInitiationInstructedAmount requestInstructedAmount){
		if(Float.compare(Float.parseFloat(responseInstructedAmount.getAmount()), Float.parseFloat(requestInstructedAmount.getAmount())) == 0)
			responseInstructedAmount.setAmount(requestInstructedAmount.getAmount());
	}
	
}
