package com.capgemini.psd2.pisp.payment.setup.service;


import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;

public interface PaymentSetupService {
   public PaymentSetupPOSTResponse createPaymentSetupResource(CustomPaymentSetupPOSTRequest paymentRequest,boolean isPaymentValidated);
   public PaymentSetupPOSTResponse retrievePaymentSetupResource(PaymentRetrieveGetRequest paymentRetrieveRequest);    	
}
