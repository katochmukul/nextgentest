package com.capgemini.psd2.pisp.payment.setup.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.payment.setup.transformer.PaymentSetupResponseTransformer;

@Component
public class PaymentSetupResponseTransformerImpl implements PaymentSetupResponseTransformer{

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Override
	public PaymentSetupPOSTResponse paymentSetupResponseTransformer(CustomPaymentSetupPOSTResponse customPaymentSetupResponse, PaymentSetupPlatformResource paymentSetupPlatformResource, String methodType){
		
		PaymentSetupPOSTResponse paymentSetupResponse = new PaymentSetupPOSTResponse();
		paymentSetupResponse.setData(customPaymentSetupResponse.getData());
		paymentSetupResponse.setRisk(customPaymentSetupResponse.getRisk());
		paymentSetupResponse.getData().setStatus(PaymentSetupResponse.StatusEnum.fromValue(paymentSetupPlatformResource.getStatus()));			
		paymentSetupResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
			
			/*
			 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent 
			 * details do not have to send in response. 
			 * debtorFlag would be set as Null
			 */
			if(! Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())){
				paymentSetupResponse.getData().getInitiation().setDebtorAgent(null);
				paymentSetupResponse.getData().getInitiation().setDebtorAccount(null);
			}			
			
			/*
			 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to TPP.  
			 * 
			 */
			if(Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()) && ! Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails())){
				
				paymentSetupResponse.getData().getInitiation().getDebtorAccount().setName(null);
			}
			
			if(paymentSetupResponse.getLinks() == null)
				paymentSetupResponse.setLinks(populateLinks(paymentSetupPlatformResource.getPaymentId(), methodType));
			if(paymentSetupResponse.getMeta() == null)
				paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		
			return paymentSetupResponse;
	}	
		
	private PaymentSetupPOSTResponseLinks populateLinks(String paymentId, String methodType){
		PaymentSetupPOSTResponseLinks responseLink = new PaymentSetupPOSTResponseLinks();		
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(reqHeaderAtrributes.getSelfUrl());
		
		if(RequestMethod.POST.toString().equalsIgnoreCase(methodType)){
			if(urlBuilder.toString().lastIndexOf('/') == 0)
				urlBuilder.append("/");
			urlBuilder.append(paymentId);
		}
		responseLink.setSelf(urlBuilder.toString());
		return responseLink;
	}

}
