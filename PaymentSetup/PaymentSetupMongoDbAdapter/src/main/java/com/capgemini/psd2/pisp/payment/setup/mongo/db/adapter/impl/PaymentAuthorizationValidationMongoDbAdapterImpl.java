package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.Map;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;

public class PaymentAuthorizationValidationMongoDbAdapterImpl implements PaymentAuthorizationValidationAdapter{

	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(
			CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {
		
		if(paymentSetupResponse.getData().getInitiation().getDebtorAccount() != null && "FS_PMV_012".equalsIgnoreCase(paymentSetupResponse.getData().getInitiation().getEndToEndIdentification())){
			/*
			 * It is checking payment permission.
			 */			
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_ACCOUNT_PERMISSION);
		}
		String paymentValidationStatus = "PASS";
		if(paymentSetupResponse.getData().getInitiation().getDebtorAccount() != null && "FS_PMV_014".equalsIgnoreCase(paymentSetupResponse.getData().getInitiation().getEndToEndIdentification())){
				
			paymentValidationStatus = PaymentStatusEnum.REJECTED.getStatusCode();
		}
		
		
		PaymentSetupValidationResponse response = new PaymentSetupValidationResponse();
		response.setPaymentSetupValidationStatus(paymentValidationStatus);
		return response;
	}

}
