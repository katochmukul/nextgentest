package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.PaymentAuthorizationValidationMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.PaymentSetupStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.PaymentSetupValidationMongoDbAdapterImpl;


@Configuration
public class PaymentSetupMongoDbAdapterConfig {

	
	@Bean
	public PaymentSetupStagingAdapter paymentSetupStagingMongoDBAdapter(){
		return new PaymentSetupStagingMongoDbAdapterImpl();
	}
	
	@Bean
	public PaymentSetupValidationAdapter paymentSetupValidationMongoDBAdapter(){
		return new PaymentSetupValidationMongoDbAdapterImpl();
	}
	
	@Bean
	public PaymentAuthorizationValidationAdapter paymentAuthorizationValidationMongoDBAdapter(){
		return new PaymentAuthorizationValidationMongoDbAdapterImpl();
	}
	
	
}
