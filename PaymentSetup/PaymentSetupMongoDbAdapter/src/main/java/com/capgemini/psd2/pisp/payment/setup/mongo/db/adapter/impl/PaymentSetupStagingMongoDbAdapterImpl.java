package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.PaymentSetupFoundationRepository;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

public class PaymentSetupStagingMongoDbAdapterImpl implements PaymentSetupStagingAdapter {

	@Autowired
	private PaymentSetupFoundationRepository paymentSetupBankRepository;

/*	@Autowired
	private SequenceGenerator sequenceGenerator;

	private static final String PAYMENT_SETUP_SEQUENCE_NAME = "PAYMENTSETUP_SEQUENCE";
	
	private static final String PAYMENT_SUBMISSION_SEQUENCE_NAME = "PAYMENTSUBMISSION_SEQUENCE";
*/
	@Override
	public PaymentSetupStagingResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest,
			Map<String, String> paramsMap) {

		try {
			PaymentSetupStagingResponse paymentResponse = new PaymentSetupStagingResponse();

			if (paymentSetupRequest.getRisk().getDeliveryAddress() != null
					&& paymentSetupRequest.getRisk().getDeliveryAddress().getCountry() == null) {

				paymentSetupRequest.getRisk().getDeliveryAddress().setCountry("GB");
			}
			if (paymentSetupRequest.getRisk().getDeliveryAddress() == null) {
				RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
				deliveryAddress.setCountry("GB");
				paymentSetupRequest.getRisk().setDeliveryAddress(deliveryAddress);
			}
			PaymentSetupFoundationResource paymentSetupBankResource = transformPaymentSetupToBankResource(
					paymentSetupRequest);
			/*paymentSetupBankResource.setPaymentSubmissionId(String.valueOf(sequenceGenerator.getNextSequenceId(PAYMENT_SUBMISSION_SEQUENCE_NAME)));*/
			paymentSetupBankResource.setPaymentSubmissionId(GenerateUniqueIdUtilities.getUniqueId().toString());
			/*paymentSetupBankResource.getData()
					.setPaymentId(String.valueOf(sequenceGenerator.getNextSequenceId(PAYMENT_SETUP_SEQUENCE_NAME)));*/
			paymentSetupBankResource.getData().setPaymentId(GenerateUniqueIdUtilities.getUniqueId().toString());
			paymentSetupBankResource.getData().setStatus(StatusEnum.fromValue(paymentSetupRequest.getPaymentStatus()));
			paymentSetupBankResource.getData().setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());

			paymentSetupBankRepository.save(paymentSetupBankResource);

			if (!NullCheckUtils.isNullOrEmpty(paymentSetupBankResource.getData().getPaymentId())) {
				paymentResponse.setPaymentId(paymentSetupBankResource.getData().getPaymentId());

			}
			return paymentResponse;
		} catch (DataAccessResourceFailureException exception) {

			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_INSERTION);
		}

	}

	@Override
	public PaymentSetupStagingResponse updateStagedPaymentSetup(CustomPaymentSetupPOSTResponse paymentSetupBankResource,
			Map<String, String> paramsMap) {
		try {
			PaymentSetupFoundationResource paymentBankResource = paymentSetupBankRepository
					.findOneByDataPaymentId(paymentSetupBankResource.getData().getPaymentId());
			
			paymentBankResource.getData().getInitiation().setDebtorAgent(paymentSetupBankResource.getData().getInitiation().getDebtorAgent());
			paymentBankResource.getData().getInitiation().setDebtorAccount(paymentSetupBankResource.getData().getInitiation().getDebtorAccount());
			
			if(com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum.ACCEPTEDSETTLEMENTINPROCESS.toString().equalsIgnoreCase(paymentSetupBankResource.getPaymentStatus()))
				paymentBankResource.getData().setStatus(StatusEnum.ACCEPTEDCUSTOMERPROFILE);
			else if(com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum.REJECTED.toString().equalsIgnoreCase(paymentSetupBankResource.getPaymentStatus()))
				paymentBankResource.getData().setStatus(StatusEnum.REJECTED);
			else
				paymentBankResource.getData().setStatus(paymentSetupBankResource.getData().getStatus());
			
			
			paymentBankResource.setPaymentStatus(paymentSetupBankResource.getPaymentStatus());
			
			if (paymentSetupBankResource.getData().getCreationDateTime() != null)
				paymentBankResource.getData().setCreationDateTime(paymentSetupBankResource.getData().getCreationDateTime());
			else
				paymentBankResource.getData().setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());

			paymentSetupBankRepository.save(paymentBankResource);
			PaymentSetupStagingResponse stagedResponse = new PaymentSetupStagingResponse();
			if (paymentBankResource.getData().getPaymentId() != null) {

				stagedResponse.setPaymentId(paymentBankResource.getData().getPaymentId());

			}
			return stagedResponse;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_UPDATION);
		}
	}

	@Override
	public CustomPaymentSetupPOSTResponse retrieveStagedPaymentSetup(String paymentId, Map<String, String> paramsMap) {
		try {
			PaymentSetupFoundationResource paymentBankResource = paymentSetupBankRepository
					.findOneByDataPaymentId(paymentId);

			if (paymentBankResource == null || paymentBankResource.getData() == null
					|| paymentBankResource.getData().getInitiation() == null)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND);
			paymentBankResource.setId(null);
			paymentBankResource.setPaymentSubmissionId(null);
			
			if(paymentBankResource.getRisk().getDeliveryAddress() != null && 
					NullCheckUtils.isNullOrEmpty(paymentBankResource.getRisk().getDeliveryAddress().getTownName())){
				paymentBankResource.getRisk().setDeliveryAddress(null);
			}

			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE);
		}

	}

	private PaymentSetupFoundationResource transformPaymentSetupToBankResource(
			CustomPaymentSetupPOSTRequest paymentSetupRequest) {

		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
		return JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentFoundationRequest, PaymentSetupFoundationResource.class);
	}

}
