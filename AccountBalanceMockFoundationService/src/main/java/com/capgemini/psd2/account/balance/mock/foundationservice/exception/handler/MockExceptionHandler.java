/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mock.foundationservice.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.account.balance.mock.foundationservice.domain.ValidationViolation;
import com.capgemini.psd2.account.balance.mock.foundationservice.domain.ValidationViolations;

/**
 * The Class MockExceptionHandler.
 */
@ControllerAdvice
public class MockExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Invalid parameter request exception.
	 *
	 * @param ex the ex
	 * @return the response entity
	 */
	@ExceptionHandler(InvalidParameterRequestException.class)
	public final ResponseEntity<ValidationViolations> invalidParameterRequestException(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTB_001");
		exceptionResponce.setErrorText("Bad Request. Please check your request");
		exceptionResponce.setErrorField("Bad Request. Please check your request");
		exceptionResponce.setErrorValue("Bad request");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	/**
	 * Missing authentication header exception.
	 *
	 * @param ex the ex
	 * @return the response entity
	 */
	@ExceptionHandler(MissingAuthenticationHeaderException.class)
	public final ResponseEntity<ValidationViolations> missingAuthenticationHeaderException(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTB_002");
		exceptionResponce.setErrorText("You are not authorised to use this service");
		exceptionResponce.setErrorField("You are not authorised to use this service");
		exceptionResponce.setErrorValue("Header Missing");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

	/**
	 * Record not found.
	 *
	 * @param ex the ex
	 * @return the response entity
	 */
	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<ValidationViolations> recordNotFound(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTB_003");
		exceptionResponce.setErrorText("No account detail found for the requested account");
		exceptionResponce.setErrorField("No account detail found for the requested account");
		exceptionResponce.setErrorValue("Not Found");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}

	/**
	 * Internal server error.
	 *
	 * @param ex the ex
	 * @return the response entity
	 */
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ValidationViolations> internalServerError(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTB_004");
		exceptionResponce.setErrorText("Technical Error. Please try again later");
		exceptionResponce.setErrorField("Technical Error. Please try again later");
		exceptionResponce.setErrorValue("Server Error");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
