
package com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.ValidationViolations;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.client.ValidatePreStagePaymentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.utility.ValidatePreStagePaymentFoundationServiceUtility;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class ValidatePreStagePaymentFoundationServiceAdapter implements PaymentSetupValidationAdapter {

	@Value("${foundationService.validatePreStagePaymentBaseURL:#{null}}")
	private String validatePreStagePaymentBaseURL;

	@Autowired
	private ValidatePreStagePaymentFoundationServiceUtility validatePreStagePaymentFoundationServiceUtility;

	@Autowired
	private ValidatePreStagePaymentFoundationServiceClient validatePreStagePaymentFoundationServiceClient;

	@Autowired
	private AdapterUtility adapterUtility;

	@Override
	public PaymentSetupValidationResponse validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest, Map<String, String> params) {

		ValidationPassed validationPassed = null;
		ValidationViolations validationViolations = null;
		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = validatePreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSetupRequest, params);
		requestInfo.setUrl(validatePreStagePaymentBaseURL);
		PaymentInstruction paymentInstruction = validatePreStagePaymentFoundationServiceUtility. transformRequestFromAPIToFS(paymentSetupRequest);

		try {
			validationPassed = validatePreStagePaymentFoundationServiceClient.validatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
		} catch (AdapterException e) {
			validationViolations = (ValidationViolations) e.getFoundationError();
			if (NullCheckUtils.isNullOrEmpty(validationViolations)) {
				throw e;
			}
			String errorCode = validationViolations.getValidationViolation().get(0).getErrorCode();	
			String throwableError = adapterUtility.getThrowableError().get(errorCode);
			if (!com.capgemini.psd2.utilities.NullCheckUtils.isNullOrEmpty(throwableError)) {
				throw e;
			} else {
				return validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
			}
		} catch (Exception e) {			
			throw AdapterException.populatePSD2Exception(e.getMessage(),AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
	}

}
