package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.ValidationViolation;
import com.capgemini.psd2.adapter.exceptions.ValidationViolations;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.ValidatePreStagePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.client.ValidatePreStagePaymentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.utility.ValidatePreStagePaymentFoundationServiceUtility;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;


@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePreStagePaymentFoundationServiceAdapterTest {
	
	@InjectMocks
	ValidatePreStagePaymentFoundationServiceAdapter adapter;
	
	@Mock
	private ValidatePreStagePaymentFoundationServiceUtility validatePreStagePaymentFoundationServiceUtility;

	@Mock
	private ValidatePreStagePaymentFoundationServiceClient validatePreStagePaymentFoundationServiceClient;

	@Mock
	private AdapterUtility adapterUtility;
	
		
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void validatePreStagePaymentSetupTest(){
		
		ReflectionTestUtils.setField(adapter, "validatePreStagePaymentBaseURL", "http://localhost:9081/fs-payment-web-service/services/ValidatePreStagePayment");
		HttpHeaders headers = new HttpHeaders();
		HashMap<String, String> params = new HashMap<>();
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePreStagePaymentFoundationServiceClient.validatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		
		paymentSetupValidationResponse = adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest, params);
		assertNotNull(paymentSetupValidationResponse);
	}
	
	@Test()
	public void validatePreStagePaymentSetupTest2(){
		
		ReflectionTestUtils.setField(adapter, "validatePreStagePaymentBaseURL", "http://localhost:9081/fs-payment-web-service/services/ValidatePreStagePayment");
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode("123");
		validationViolations.getValidationViolation().add(validationViolation);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePreStagePaymentFoundationServiceClient.validatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Adapter Exception", errorInfo, validationViolations));
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		
		paymentSetupValidationResponse = adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest, params);
		assertNotNull(paymentSetupValidationResponse);
	
	}
	
	@Test(expected = AdapterException.class)
	public void validatePreStagePaymentSetupInternalServerErrorTest(){
		
		ReflectionTestUtils.setField(adapter, "validatePreStagePaymentBaseURL", "http://localhost:9081/fs-payment-web-service/services/ValidatePreStagePayment");
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		map.put("123", "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode("123");
		validationViolations.getValidationViolation().add(validationViolation);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePreStagePaymentFoundationServiceClient.validatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Adapter Exception", errorInfo, validationViolations));
		Mockito.when(adapterUtility.getThrowableError()).thenReturn(map);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		
		adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest, params);
		
	
	}
	
	@Test(expected = PSD2Exception.class)
	public void validatePreStagePaymentSetupPSD2ExceptionTest(){
		
		ReflectionTestUtils.setField(adapter, "validatePreStagePaymentBaseURL", "http://localhost:9081/fs-payment-web-service/services/ValidatePreStagePayment");
		HttpHeaders headers = new HttpHeaders();
		HashMap<String, String> params = new HashMap<>();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePreStagePaymentFoundationServiceClient.validatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR));;
		Mockito.when(validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		
		adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest, params);
	
	}
	
	
	
	
	
}
