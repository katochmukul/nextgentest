package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "deviceFieldName", "deviceFieldType", "deviceFieldValue" })
public class DeviceData {

	@JsonProperty("deviceFieldName")
	private String deviceFieldName;
	@JsonProperty("deviceFieldType")
	private String deviceFieldType;
	@JsonProperty("deviceFieldValue")
	private String deviceFieldValue;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("deviceFieldName")
	public String getDeviceFieldName() {
		return deviceFieldName;
	}

	@JsonProperty("deviceFieldName")
	public void setDeviceFieldName(String deviceFieldName) {
		this.deviceFieldName = deviceFieldName;
	}

	@JsonProperty("deviceFieldType")
	public String getDeviceFieldType() {
		return deviceFieldType;
	}

	@JsonProperty("deviceFieldType")
	public void setDeviceFieldType(String deviceFieldType) {
		this.deviceFieldType = deviceFieldType;
	}

	@JsonProperty("deviceFieldValue")
	public String getDeviceFieldValue() {
		return deviceFieldValue;
	}

	@JsonProperty("deviceFieldValue")
	public void setDeviceFieldValue(String deviceFieldValue) {
		this.deviceFieldValue = deviceFieldValue;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
