
package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.constants.FraudnetViaMulesoftProxyConstants;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Account;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Address;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.ContactInformation;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.DeviceData;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.ElectronicDeviceSession;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.OnlineUser;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.OnlineUserSession;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Person;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Source;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Transaction;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.TransactionEventAmount;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.UserEvent;
import com.capgemini.psd2.fraudsystem.domain.Emails;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.domain.PSD2FinancialAccount;
import com.capgemini.psd2.fraudsystem.domain.PhoneNumbers;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class FraudnetViaMulesoftProxyTransformer  {
		
	public FraudServiceRequest transformFraudnetRequest(Map<String, Map<String, Object>> fraudSystemRequest){
		
		Map<String, Object> eventMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP);
		Map<String, Object> deviceMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP);
		Map<String, Object> customerMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP);
		PSD2CustomerInfo psd2CustomerInfo =null;
		List<PSD2FinancialAccount> psd2FinancialAccountList =null;
		if (!NullCheckUtils.isNullOrEmpty(customerMap) && !NullCheckUtils.isNullOrEmpty(eventMap)) {
			psd2CustomerInfo = (PSD2CustomerInfo) customerMap.get(FraudnetViaMulesoftProxyConstants.CONTACT_DETAILS);
			psd2FinancialAccountList = (List<PSD2FinancialAccount>) eventMap.get(FraudnetViaMulesoftProxyConstants.FIN_ACCNTS);
		}
		
		//userEvent
		UserEvent userEvent = new UserEvent();
		
		setUserEventBasicData(eventMap, userEvent);
		
		ElectronicDeviceSession electronicDeviceSession = processDeviceData(deviceMap);
		
		processRequestHeaderData(deviceMap, electronicDeviceSession);
		
		if(electronicDeviceSession.getDeviceData() != null && !electronicDeviceSession.getDeviceData().isEmpty())
			userEvent.setElectronicDeviceSession(electronicDeviceSession);
		
		processOnlineSessionData(deviceMap, userEvent);
		
		//RemainingAttempts will come from Api removed
		
		
		
		//userEvent/person for valid login
		if(deviceMap!=null && deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN)!=null && (Boolean)deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN)){
			processLoginPersonnelInfo(eventMap, customerMap, psd2CustomerInfo, psd2FinancialAccountList, userEvent);
		}

		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		fraudServiceRequest.setUserEvent(userEvent);
		return fraudServiceRequest;
				
	}

	private void setUserEventBasicData(Map<String, Object> eventMap, UserEvent userEvent) {
		if(!NullCheckUtils.isNullOrEmpty(eventMap)) {
			
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE) ))
			userEvent.setType(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE).toString());
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TIME) ) )
			userEvent.setTime(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TIME).toString()); 
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_CHANNEL) ))
			userEvent.setChannelClassificationCode(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_CHANNEL).toString());
		
		//userEvent/source     
		Source source = new Source();
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM) ))
			source.setSourceSystem(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM).toString());
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM) ))
			source.setSourceSubSystem(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM).toString());
		
		userEvent.setSource(source);//all must
		}
		}
		
	private void processOnlineSessionData(Map<String, Object> deviceMap, UserEvent userEvent) {
		//userEvent/onlineUserSession  optional 
		if (deviceMap!=null && !NullCheckUtils.isNullOrEmpty(deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_ID))) {
			OnlineUserSession session = new OnlineUserSession();

		
			//Test porpose as clarification pending , To Remove
			if( (Boolean)deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN)){
				session.setUserSessionActive(true);//login success
				session.setTwoFactorAuthentication(true);
			}else{
				session.setUserSessionActive(false);
				session.setTwoFactorAuthentication(false);
		}
			
			
			session.setSessionId(deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_ID).toString());
			userEvent.setOnlineUserSession(session);
		}
	}

	private void processRequestHeaderData(Map<String, Object> deviceMap,
			ElectronicDeviceSession electronicDeviceSession) {
		//Header optional 
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS) != null ){
			Map<String, String> headerData = null;
			try {
				headerData = new ObjectMapper().readValue(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS).toString(), new TypeReference<HashMap<String, String>>() {});
			}catch (Exception e) {
				throw AdapterException.populatePSD2Exception("FraudNet service error processing Header information", AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			if (!NullCheckUtils.isNullOrEmpty(headerData)){
				for (Map.Entry<String, String> header : headerData.entrySet()) {
					DeviceData headerDetail = new DeviceData();
					headerDetail.setDeviceFieldName(header.getKey());
					headerDetail.setDeviceFieldType("Header");
					headerDetail.setDeviceFieldValue(header.getValue());
					electronicDeviceSession.getDeviceData().add(headerDetail);
				}
			}
		}
		
		//Cookie optional
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES) != null ){
				Map<String, String> cookiesData = ( Map<String, String>)deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES) ;
			for (Map.Entry<String, String> cookies : cookiesData.entrySet()) {
				DeviceData cookieDetail = new DeviceData();
				cookieDetail.setDeviceFieldName(cookies.getKey());
				cookieDetail.setDeviceFieldType("Cookie");
				cookieDetail.setDeviceFieldValue(cookies.getValue());
				electronicDeviceSession.getDeviceData().add(cookieDetail);
			}	
		}
	}
		
	private ElectronicDeviceSession processDeviceData(Map<String, Object> deviceMap) {
		//userEvent/electronicDeviceSession
		ElectronicDeviceSession electronicDeviceSession = new ElectronicDeviceSession();
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_IP) != null )
			electronicDeviceSession.setDeviceAddress(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_IP).toString());
		
		 
		//userEvent/electronicDeviceSession/deviceData
		//JSC   
		List<DeviceData> deviceData=new ArrayList<>();
		DeviceData jscData = new DeviceData();
		
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_JSC) != null ){
			jscData.setDeviceFieldName("jsc");
			jscData.setDeviceFieldType("JSC");
			jscData.setDeviceFieldValue(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_JSC).toString());
			deviceData.add(jscData);
			
			}
			
		electronicDeviceSession.setDeviceData(deviceData);
			
		
		//HDIM  must
		DeviceData hdimData = new DeviceData();
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD) != null ){
			hdimData.setDeviceFieldName("hdim");
			hdimData.setDeviceFieldType("HDIM");
			hdimData.setDeviceFieldValue(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD).toString());//payload 
		
			
			electronicDeviceSession.getDeviceData().add(hdimData);
		}
		return electronicDeviceSession;
		}
		
	private void processLoginPersonnelInfo(Map<String, Object> eventMap, Map<String, Object> customerMap,
			PSD2CustomerInfo psd2CustomerInfo, List<PSD2FinancialAccount> psd2FinancialAccountList,
			UserEvent userEvent) {
		String amount = amountProcessing(eventMap, userEvent);
			//login success 
			Person person = new Person();
		
		if (psd2CustomerInfo != null){
				person.setPartyName(psd2CustomerInfo.getName());// must
				person.setEmployersName(psd2CustomerInfo.getCompany());//optional
				
				if(psd2CustomerInfo.getDob()!= null && psd2CustomerInfo.getDob().length() >= 10)
					person.setBirthDate(psd2CustomerInfo.getDob().substring(0, 10));//optional
				//mothersMaidenName : optional
				//taxIdNumber optional
			}
			
		if(psd2CustomerInfo != null)
			processContactInfo(psd2CustomerInfo, person);
		
		//userEvent/person/onlineUser   optional
		if (!NullCheckUtils.isNullOrEmpty(customerMap.get(FraudnetViaMulesoftProxyConstants.ACCOUNT_ID))) {
			OnlineUser onlineUser = new OnlineUser();
			onlineUser.setUserIdentifier(customerMap.get(FraudnetViaMulesoftProxyConstants.ACCOUNT_ID).toString());
			person.setOnlineUser(onlineUser);
		}
		
		//userEvent/person/account  
		if (!NullCheckUtils.isNullOrEmpty(psd2FinancialAccountList)){
			List<Account> accounts = new ArrayList<>();
			for (PSD2FinancialAccount psd2FinancialAccountData : psd2FinancialAccountList) {
				Account account = new Account();
				if(psd2CustomerInfo!= null ){
					account.setSourceSystemAccountType(psd2CustomerInfo.getClientType());
					account.setAccountName(psd2CustomerInfo.getName());
				}
				account.setAccountNumber(psd2FinancialAccountData.getHashedAccountNumber());
				account.setBranchSubNationalSortCode(psd2FinancialAccountData.getRoutingNumber());
				account.setAccountCurrencyReference(psd2FinancialAccountData.getCurrency()); 
				accounts.add(account);
			}
			person.setAccount(accounts);
		}
		
		
		txnPaymentInstruction(eventMap, amount, person);
		userEvent.setPerson(person);//person set
	}

	private void processContactInfo(PSD2CustomerInfo psd2CustomerInfo, Person person) {
			//userEvent/person/contactInformation    must 
			ContactInformation contactInformation = new ContactInformation();
			List<Emails> emailData = psd2CustomerInfo.getEmails();
			if (!NullCheckUtils.isNullOrEmpty(emailData))
				for (Emails email : emailData)
					contactInformation.setEmailAddress(email.getEmail());
			List<PhoneNumbers> phoneNumberData = psd2CustomerInfo.getPhoneNumbers();
			if (!NullCheckUtils.isNullOrEmpty(phoneNumberData))
				for (PhoneNumbers number : phoneNumberData)
					contactInformation.setOtherPhoneNumber(number.getNumber());
			person.setContactInformation(contactInformation);
			
			//userEvent/person/address    must
			Address address = new Address();
			if(!NullCheckUtils.isNullOrEmpty(psd2CustomerInfo.getAddress()))		{
				address.setFirstAddressLine(psd2CustomerInfo.getAddress().getStreetLine1());
				address.setSecondAddressLine(psd2CustomerInfo.getAddress().getStreetLine2());
				address.setThirdAddressLine(psd2CustomerInfo.getAddress().getCity());
				address.setPostCodeNumber(psd2CustomerInfo.getAddress().getPostal());
				person.setAddress(address);
			}
			}
			
	private void txnPaymentInstruction(Map<String, Object> eventMap, String amount, Person person) {
		if(eventMap != null && !eventMap.isEmpty())
			if(!(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE).toString().equalsIgnoreCase("Login")) && eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM).toString().equalsIgnoreCase("PISP") ){
				Transaction transaction = new Transaction();//optional
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_ID))) {
					PaymentInstruction paymentInstruction = new PaymentInstruction();
				paymentInstruction.setPaymentInstructionNumber(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_ID).toString()); 
					transaction.setPaymentInstruction(paymentInstruction);
				}
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT))) {
					TransactionEventAmount transactionEventAmount = new TransactionEventAmount();
					transactionEventAmount.setTransactionCurrency(amount);
					transaction.setTransactionEventAmount(transactionEventAmount);
				}
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_TIME)))
					transaction.setOccurenceDate(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_TIME).toString());
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO)))
					transaction.setTransactionDecription(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO).toString());
				
				person.setTransaction(transaction); //tx for 
			}
		}

	private String amountProcessing(Map<String, Object> eventMap, UserEvent userEvent) {
		String amount = "00.00";
		if(eventMap!=null && !eventMap.isEmpty() && eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT) != null ){
			FinancialEventAmount financialEventAmount = new FinancialEventAmount();
			try {
				amount = eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT).toString();
			} catch (Exception e) {
				financialEventAmount.setTransactionCurrency(amount);
			}
				
			financialEventAmount.setTransactionCurrency(amount);// must
			userEvent.setFinancialEventAmount(financialEventAmount);
		}
		return amount;
	}

	public <T> T transformFraudnetResponse(FraudServiceResponse fraudServiceResponse) {

		if (NullCheckUtils.isNullOrEmpty(fraudServiceResponse))
			throw AdapterException.populatePSD2Exception("FraudNet service response is null", AdapterErrorCodeEnum.TECHNICAL_ERROR);

		return (T) fraudServiceResponse;

	}
	
}
