package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sessionId",
    "userSessionActive",
    "twoFactorAuthentication"
})
public class OnlineUserSession {

    @JsonProperty("sessionId")
    private String sessionId;
    @JsonProperty("userSessionActive")
    private Boolean userSessionActive;
    @JsonProperty("twoFactorAuthentication")
    private Boolean twoFactorAuthentication;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sessionId")
    public String getSessionId() {
        return sessionId;
	}

    @JsonProperty("sessionId")
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
	}

    @JsonProperty("userSessionActive")
    public Boolean getUserSessionActive() {
        return userSessionActive;
	}

    @JsonProperty("userSessionActive")
    public void setUserSessionActive(Boolean userSessionActive) {
        this.userSessionActive = userSessionActive;
	}

    @JsonProperty("twoFactorAuthentication")
    public Boolean getTwoFactorAuthentication() {
        return twoFactorAuthentication;
	}

    @JsonProperty("twoFactorAuthentication")
    public void setTwoFactorAuthentication(Boolean twoFactorAuthentication) {
        this.twoFactorAuthentication = twoFactorAuthentication;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
