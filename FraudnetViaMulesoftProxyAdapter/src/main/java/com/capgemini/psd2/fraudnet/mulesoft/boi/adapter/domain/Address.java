package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "firstAddressLine", "secondAddressLine", "thirdAddressLine", "fourthAddressLine", "postCodeNumber", "countryCode" })
public class Address {

	@JsonProperty("firstAddressLine")
	private String firstAddressLine;
	@JsonProperty("secondAddressLine")
	private String secondAddressLine;
	@JsonProperty("thirdAddressLine")
	private String thirdAddressLine;
	@JsonProperty("fourthAddressLine")
	private String fourthAddressLine;
	@JsonProperty("postCodeNumber")
	private String postCodeNumber;
	@JsonProperty("countryCode")
	private String countryCode;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("firstAddressLine")
	public String getFirstAddressLine() {
		return firstAddressLine;
	}

	@JsonProperty("firstAddressLine")
	public void setFirstAddressLine(String firstAddressLine) {
		this.firstAddressLine = firstAddressLine;
	}

	@JsonProperty("secondAddressLine")
	public String getSecondAddressLine() {
		return secondAddressLine;
	}

	@JsonProperty("secondAddressLine")
	public void setSecondAddressLine(String secondAddressLine) {
		this.secondAddressLine = secondAddressLine;
	}

	@JsonProperty("thirdAddressLine")
	public String getThirdAddressLine() {
		return thirdAddressLine;
	}

	@JsonProperty("thirdAddressLine")
	public void setThirdAddressLine(String thirdAddressLine) {
		this.thirdAddressLine = thirdAddressLine;
	}

	@JsonProperty("fourthAddressLine")
	public String getFourthAddressLine() {
		return fourthAddressLine;
	}

	@JsonProperty("fourthAddressLine")
	public void setFourthAddressLine(String fourthAddressLine) {
		this.fourthAddressLine = fourthAddressLine;
	}

	@JsonProperty("postCodeNumber")
	public String getPostCodeNumber() {
		return postCodeNumber;
	}

	@JsonProperty("postCodeNumber")
	public void setPostCodeNumber(String postCodeNumber) {
		this.postCodeNumber = postCodeNumber;
	}

	@JsonProperty("countryCode")
	public String getCountryCode() {
		return countryCode;
	}

	@JsonProperty("countryCode")
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
