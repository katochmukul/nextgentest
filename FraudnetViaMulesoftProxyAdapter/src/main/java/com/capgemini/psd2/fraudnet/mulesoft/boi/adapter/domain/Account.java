package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "sourceSystemAccountType", "accountName", "accountNumber", "branchSubNationalSortCode", "accountCurrencyReference" })
public class Account {

	@JsonProperty("sourceSystemAccountType")
	private String sourceSystemAccountType;
	@JsonProperty("accountName")
	private String accountName;
	@JsonProperty("accountNumber")
	private String accountNumber;
	@JsonProperty("branchSubNationalSortCode")
	private String branchSubNationalSortCode;
	@JsonProperty("accountCurrencyReference")
	private String accountCurrencyReference;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("sourceSystemAccountType")
	public String getSourceSystemAccountType() {
		return sourceSystemAccountType;
	}

	@JsonProperty("sourceSystemAccountType")
	public void setSourceSystemAccountType(String sourceSystemAccountType) {
		this.sourceSystemAccountType = sourceSystemAccountType;
	}

	@JsonProperty("accountName")
	public String getAccountName() {
		return accountName;
	}

	@JsonProperty("accountName")
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@JsonProperty("accountNumber")
	public String getAccountNumber() {
		return accountNumber;
	}

	@JsonProperty("accountNumber")
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonProperty("branchSubNationalSortCode")
	public String getBranchSubNationalSortCode() {
		return branchSubNationalSortCode;
	}

	@JsonProperty("branchSubNationalSortCode")
	public void setBranchSubNationalSortCode(String branchSubNationalSortCode) {
		this.branchSubNationalSortCode = branchSubNationalSortCode;
	}

	@JsonProperty("accountCurrencyReference")
	public String getAccountCurrencyReference() {
		return accountCurrencyReference;
	}

	@JsonProperty("accountCurrencyReference")
	public void setAccountCurrencyReference(String accountCurrencyReference) {
		this.accountCurrencyReference = accountCurrencyReference;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
