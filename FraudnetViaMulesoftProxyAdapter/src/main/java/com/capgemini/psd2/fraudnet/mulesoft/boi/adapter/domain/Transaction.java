package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "paymentInstruction", "transactionEventAmount", "occurenceDate", "transactionDecription" })
public class Transaction {

	@JsonProperty("paymentInstruction")
	private PaymentInstruction paymentInstruction;
	@JsonProperty("transactionEventAmount")
	private TransactionEventAmount transactionEventAmount;
	@JsonProperty("occurenceDate")
	private String occurenceDate;
	@JsonProperty("transactionDecription")
	private String transactionDecription;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("paymentInstruction")
	public PaymentInstruction getPaymentInstruction() {
		return paymentInstruction;
	}

	@JsonProperty("paymentInstruction")
	public void setPaymentInstruction(PaymentInstruction paymentInstruction) {
		this.paymentInstruction = paymentInstruction;
	}

	@JsonProperty("transactionEventAmount")
	public TransactionEventAmount getTransactionEventAmount() {
		return transactionEventAmount;
	}

	@JsonProperty("transactionEventAmount")
	public void setTransactionEventAmount(TransactionEventAmount transactionEventAmount) {
		this.transactionEventAmount = transactionEventAmount;
	}

	@JsonProperty("occurenceDate")
	public String getOccurenceDate() {
		return occurenceDate;
	}

	@JsonProperty("occurenceDate")
	public void setOccurenceDate(String occurenceDate) {
		this.occurenceDate = occurenceDate;
	}

	@JsonProperty("transactionDecription")
	public String getTransactionDecription() {
		return transactionDecription;
	}

	@JsonProperty("transactionDecription")
	public void setTransactionDecription(String transactionDecription) {
		this.transactionDecription = transactionDecription;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
