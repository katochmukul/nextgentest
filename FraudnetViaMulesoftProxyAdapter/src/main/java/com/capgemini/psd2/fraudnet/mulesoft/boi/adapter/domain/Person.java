package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "partyName", "employersName", "gcisCustomertype", "birthDate", "mothersMaidenName", "taxIdNumber",
		"contactInformation", "address", "onlineUser", "account", "transaction" })
public class Person {

	@JsonProperty("partyName")
	private String partyName;
	@JsonProperty("employersName")
	private String employersName;
	@JsonProperty("gcisCustomertype")
	private String gcisCustomertype;
	@JsonProperty("birthDate")
	private String birthDate;
	@JsonProperty("mothersMaidenName")
	private String mothersMaidenName;
	@JsonProperty("taxIdNumber")
	private String taxIdNumber;
	@JsonProperty("contactInformation")
	private ContactInformation contactInformation;
	@JsonProperty("address")
	private Address address;
	@JsonProperty("onlineUser")
	private OnlineUser onlineUser;
	@JsonProperty("account")
    private List<Account> account = null;
	@JsonProperty("transaction")
	private Transaction transaction;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("partyName")
	public String getPartyName() {
		return partyName;
	}

	@JsonProperty("partyName")
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	@JsonProperty("employersName")
	public String getEmployersName() {
		return employersName;
	}

	@JsonProperty("employersName")
	public void setEmployersName(String employersName) {
		this.employersName = employersName;
	}

	@JsonProperty("gcisCustomertype")
	public String getGcisCustomertype() {
		return gcisCustomertype;
	}

	@JsonProperty("gcisCustomertype")
	public void setGcisCustomertype(String gcisCustomertype) {
		this.gcisCustomertype = gcisCustomertype;
	}

	@JsonProperty("birthDate")
	public String getBirthDate() {
		return birthDate;
	}

	@JsonProperty("birthDate")
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@JsonProperty("mothersMaidenName")
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	@JsonProperty("mothersMaidenName")
	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	@JsonProperty("taxIdNumber")
	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	@JsonProperty("taxIdNumber")
	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	@JsonProperty("contactInformation")
	public ContactInformation getContactInformation() {
		return contactInformation;
	}

	@JsonProperty("contactInformation")
	public void setContactInformation(ContactInformation contactInformation) {
		this.contactInformation = contactInformation;
	}

	@JsonProperty("address")
	public Address getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(Address address) {
		this.address = address;
	}

	@JsonProperty("onlineUser")
	public OnlineUser getOnlineUser() {
		return onlineUser;
	}

	@JsonProperty("onlineUser")
	public void setOnlineUser(OnlineUser onlineUser) {
		this.onlineUser = onlineUser;
	}

	@JsonProperty("account")
    public List<Account> getAccount() {
		return account;
	}

	@JsonProperty("account")
    public void setAccount(List<Account> account) {
		this.account = account;
	}

	@JsonProperty("transaction")
	public Transaction getTransaction() {
		return transaction;
	}

	@JsonProperty("transaction")
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
