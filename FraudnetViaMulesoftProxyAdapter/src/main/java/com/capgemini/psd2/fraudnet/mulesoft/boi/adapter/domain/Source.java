package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "sourceSystem", "sourceSubSystem" })
public class Source {

	@JsonProperty("sourceSystem")
	private String sourceSystem;
	@JsonProperty("sourceSubSystem")
	private String sourceSubSystem;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("sourceSystem")
	public String getSourceSystem() {
		return sourceSystem;
	}

	@JsonProperty("sourceSystem")
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	@JsonProperty("sourceSubSystem")
	public String getSourceSubSystem() {
		return sourceSubSystem;
	}

	@JsonProperty("sourceSubSystem")
	public void setSourceSubSystem(String sourceSubSystem) {
		this.sourceSubSystem = sourceSubSystem;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
