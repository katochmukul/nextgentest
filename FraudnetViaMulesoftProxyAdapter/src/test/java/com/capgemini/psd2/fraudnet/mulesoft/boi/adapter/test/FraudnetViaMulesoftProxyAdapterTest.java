package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.FraudnetViaMulesoftProxyAdapter;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.client.FraudnetViaMulesoftProxyClient;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.utility.FraudnetViaMulesoftProxyUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class FraudnetViaMulesoftProxyAdapterTest {

	@InjectMocks
	private FraudnetViaMulesoftProxyAdapter fraudnetViaMulesoftProxyAdapter;

	@Mock
	private FraudnetViaMulesoftProxyUtility fraudnetViaMulesoftProxyUtility;

	@Mock
	private FraudnetViaMulesoftProxyClient fraudnetViaMulesoftProxyClient;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void retrieveFraudScoreTest() {

		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();
		HttpHeaders headers = new HttpHeaders();
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyAdapter, "fraudnetMulesoftEndpointURL", "test");

		Mockito.when(fraudnetViaMulesoftProxyUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(fraudnetViaMulesoftProxyUtility.transformRequestFromAPIToFN(anyObject())).thenReturn(fraudServiceRequest);
		Mockito.when(fraudnetViaMulesoftProxyClient.fraudnetViaMulesoftCall(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(fraudServiceResponse);
		Mockito.when(fraudnetViaMulesoftProxyUtility.transformResponseFromFNToAPI(anyObject())).thenReturn(fraudServiceResponse);

		FraudServiceResponse response = fraudnetViaMulesoftProxyAdapter.retrieveFraudScore(fraudSystemRequest);

		assertNotNull(response);
	}

	@Test(expected = AdapterException.class)
	public void retrieveFraudScoreAdapterExceptionTest() {

		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();
		HttpHeaders headers = new HttpHeaders();
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyAdapter, "fraudnetMulesoftEndpointURL", "test");
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");

		Mockito.when(fraudnetViaMulesoftProxyUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(fraudnetViaMulesoftProxyUtility.transformRequestFromAPIToFN(anyObject())).thenReturn(fraudServiceRequest);
		Mockito.when(fraudnetViaMulesoftProxyClient.fraudnetViaMulesoftCall(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Adapter Exception", errorInfo));
		Mockito.when(fraudnetViaMulesoftProxyUtility.transformResponseFromFNToAPI(anyObject())).thenReturn(fraudServiceResponse);

		 fraudnetViaMulesoftProxyAdapter.retrieveFraudScore(fraudSystemRequest);
	}

	@Test(expected = AdapterException.class)
	public void retrieveFraudScoreExceptionTest() {

		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();
		HttpHeaders headers = new HttpHeaders();
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyAdapter, "fraudnetMulesoftEndpointURL", "test");

		Mockito.when(fraudnetViaMulesoftProxyUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(fraudnetViaMulesoftProxyUtility.transformRequestFromAPIToFN(anyObject())).thenReturn(fraudServiceRequest);
		Mockito.when(fraudnetViaMulesoftProxyClient.fraudnetViaMulesoftCall(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new NullPointerException());
		Mockito.when(fraudnetViaMulesoftProxyUtility.transformResponseFromFNToAPI(anyObject())).thenReturn(fraudServiceResponse);

		fraudnetViaMulesoftProxyAdapter.retrieveFraudScore(fraudSystemRequest);

	}

}
