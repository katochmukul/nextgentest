// public/js/app.js
"use strict";
var consentApp = angular.module("consentApp", ["ui.router", "ui.bootstrap", "blockUI", "isoCurrency", "pascalprecht.translate", "fraudAnalyzer", "consentPartials"]);

consentApp.value("envVariables", {
    blockChain: false
});

consentApp.run(["$rootScope", "config", "$window", "$state", "$location", "$fraudAnalyze", function ($rootScope, config, $window, $state, $location, $fraudAnalyze) {
    $rootScope.defaultCssTheme = config.defaultCssTheme;
    $rootScope.$on("$stateChangeSuccess", function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        if (toState.url !== config.applicationType) {
            if ((toState.name === "aispAccount") ||
                (toState.name === "pispAccount") ||
                (toState.name === "aispAuthorisationRenewal")) {
                event.preventDefault();
                return false;
            } else {
                if ((toState.name === "aispReview" && toParams.accountRequestDetails === null) ||
                    (toState.name === "pispReview" && toParams.paymentDetails === null) ||
                    (toState.name === "aispAuthorisationRenewal")
                ) {
                    event.preventDefault();
                }
            }
        }
    });
    $fraudAnalyze.loaded.then(function () {
        $fraudAnalyze.init = function () {
            if (!$("#boiukppm").length) {
                $("body").append("<input type='hidden' id='boiukppm' name='boiukppm'>");
                $("body").append("<input type='hidden' id='boiukprefs2' name='boiukprefs2'>");
                var fraudHdmInfoData = angular.fromJson($("#fraudHdmInfo").val());
                var args = [];
                args[0] = decodeURIComponent(fraudHdmInfoData.resUrl); // using decodeURIComponent() for decode the url
                args[1] = decodeURIComponent(fraudHdmInfoData.hdmUrl);
                args[2] = fraudHdmInfoData.hdmInputName;
                $window.boiukns.boiukfn("boiukppm", args);
            }
        };
        $fraudAnalyze.capture = function (dateTime) {
            $window.boiukns.boiukcfn("boiukppm");
            var headers = {};
            headers["X-fn-device-jsc"] = $("#boiukppm").val();
            headers["X-fn-device-hdim-payload"] = $("#boiukprefs2").val();
            headers["X-fn-event-time"] = dateTime;
            return headers;
        };
    });
}]);

consentApp.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "blockUIConfig", "$translateProvider", "$qProvider",
    "$translatePartialLoaderProvider", "config", "$fraudAnalyzeProvider",
    function ($stateProvider, $urlRouterProvider, $httpProvider, blockUIConfig, $translateProvider, $qProvider, $translatePartialLoaderProvider, config, $fraudAnalyzeProvider) {
        var $ = window.jQuery;
        var staticContent = angular.fromJson($("#staticContent").val());
        $translatePartialLoaderProvider.addPart("index");
        $translateProvider.translations("en", staticContent);
        $translateProvider.preferredLanguage("en");
        $translateProvider.fallbackLanguage("en");
        $translateProvider.useSanitizeValueStrategy("sceParameters");

        var $jscFilePath = jQuery("#jsc-file-path").val();
        if ($jscFilePath && $jscFilePath.length) {
            $fraudAnalyzeProvider.setJsCollectors([$jscFilePath]);
        }

        blockUIConfig.templateUrl = "views/loading-spinner.html";
        $qProvider.errorOnUnhandledRejections(false);
        $httpProvider.interceptors.push(["$q", "blockUI", "$timeout", function ($q, blockUI, $timeout) {
            return {
                request: function (config) {
                    blockUI.start();
                    return config;
                },
                requestError: function (rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                },
                response: function (response) {
                    blockUI.stop();
                    return response;
                },
                responseError: function (rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                }
            };
        }]);


        $stateProvider.
            state("aispAccount", {
                params: { selectedAccountDetails: null },
                "url": "/aisp-account",
                templateUrl: "views/aisp-account.html",
                controller: "AispAccountCtrl",
                resolve: {
                    fontResolved: ["fontloaderService", function (fontloaderService) {
                        return fontloaderService.isFontLoad();
                    }]
                }
            }).
            state("aispReview", {
                params: { accountRequestDetails: null },
                "url": "/aisp-review",
                templateUrl: "views/aisp-review.html",
                controller: "AispReviewCtrl"
            }).
            state("aispAuthorisationRenewal", {
                params: { accountRequestDetails: null },
                "url": "/aisp-authorisation-renewal",
                templateUrl: "views/aisp-authorisation-renewal.html",
                controller: "AispAuthorisationRenewalCtrl"
            }).
            state("pispAccount", {
                params: {
                    pispContractDetails: null
                },
                "url": "/pisp-account",
                templateUrl: "views/pisp-account.html",
                controller: "PispAccountCtrl",
                resolve: {
                    fontResolved: ["fontloaderService", function (fontloaderService) {
                        return fontloaderService.isFontLoad();
                    }]
                }
            }).
            state("pispReview", {
                params: {
                    paymentDetails: null
                },
                "url": "/pisp-review",
                templateUrl: "views/pisp-review.html",
                controller: "PispReviewCtrl"
            });

        var authorisationRenewal = $("#renewalAuthorisation").val();
        var consentType = $("#consentType").val();
        if (authorisationRenewal === "true" && consentType === config.aispConsent) {
            config.applicationType = "/aisp-authorisation-renewal";
        }
        else {
            if (consentType === config.pispConsent) {
                config.applicationType = "/pisp-account";
            } else {
                config.applicationType = "/aisp-account";
            }
        }
        $urlRouterProvider.otherwise(config.applicationType);
    }]);

// Constants for application
"use strict";
consentApp.constant("config", {
    accTypeChecking: "Checking",
    accTypeCredit: "CreditCard",
    minConsentPeriod: 1,
    maxConsentPeriod: 180,
    minTransHistoryPeriod: 1,
    maxTransHistoryPeriod: 36,
    lang: "en",
    defaultCssTheme: "boi-uk-theme.css",
    logoPath: "img/boi_logo.svg",
    aispConsent:"AISP",
    pispConsent:"PISP",
    cispConsent:"CISP",
    searchOnAccounts: false,
    chromeBorderStartVersion:45,
    chromeBorderEndVersion:47,
    pageSize:10,
    maxPaginationSize:4,
    maskAccountNumberLength:4,
    totalNumberVisibleRows:3,
    modelpopupConfig: {
        open: function() {/**/},
        "modelpopupTitle": "Title",
        "modelpopupBodyContent": "Content",
        "escBtn":false,
        "btn": {
            "okbtn": { "visible": true, "label": "", "action": null },
            "cancelbtn": { "visible": true, "label": "", "action": null }
        }
    }
});

"use strict";
angular.module("consentApp").controller("AispAccountCtrl", ["$scope", "$uibModal", "AcctService", "$filter", "$translate", "$state",
    "ConsentService", "config", "blockUI", "$window", "$timeout", "$sce",
    function ($scope, $uibModal, AcctService, $filter, $translate, $state, ConsentService, config, blockUI, $window, $timeout, $sce, fontResolved) {
        var $ = window.jQuery;
        $scope.init = function () {
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.accReq = null;
            $scope.allAccounts = null;
            $scope.errorData = null;
            $scope.retry = null;
            $scope.pageSize = config.pageSize;
            $scope.maxPaginationSize = config.maxPaginationSize;
            $scope.searchOnAccounts = config.searchOnAccounts;
            $scope.maskAccountNumberLength = config.maskAccountNumberLength;
            $scope.AccountsByPage = {};
            $scope.accountsData = [];
            $scope.selAccounts = [];
            $scope.searchText = "";
            $scope.currentPage = 1;
            $scope.isAllAccountSelected = false;
            $scope.filteredAccts = [];
            $scope.modelPopUpConf = config.modelpopupConfig;

            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                // $scope.pageError = {errorCode:"731"};
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        }, 0);
                    } else {
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.accReq = angular.fromJson($("#account-request").val());
                        $scope.allAccounts = angular.fromJson($("#psuAccounts").val()).Data.Account;
                        if (!$scope.allAccounts.length) {
                            throw null;
                        }
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.tppInfo = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";
                        $scope.tillDate = $scope.accReq.TransactionToDateTime;
                        if (!$scope.isFromReview()) {
                            $scope.disabledMsgShow = false;
                            angular.forEach($scope.allAccounts, function (pacct) {
                                pacct.selected = false;
                                $scope.accountDisabledFilter(pacct);
                            });
                        }
                        $scope.pagination();
                        $scope.isSelectedAll();
                        $scope.$watch("currentPage", function (newVal, oldVal) {
                           // $scope.selectAllAccBtnEnabled = true;
                           $("#"+newVal).focus();
                            $scope.selectAllAcctBtnChecked();
                            if (newVal !== oldVal) {
                                $scope.isSelectedAll();
                            }
                        }, true);
                    } catch (e) {
                        $scope.createCustomErr({ "errorCode": "999" });
                    }
                }
            }
            $translate(["AISP.HEADER.LOGOURL", "AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "AISP.HEADER.ACCOUNT_ACCESS_LABEL", "AISP.HEADER.THIRD_PARTY_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ALL_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_MSG",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_ACCOUNT_MSG",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_ALL_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_NONE_BUTTON_LABEL",
                "AISP.FOOTER.ABOUT_US_LABEL", "AISP.FOOTER.PRIVACY_POLICY_LABEL",
                "AISP.FOOTER.TNC_LABEL", "AISP.FOOTER.HELP_LABEL",
                "AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW",
                "AISP.FOOTER.REGULATORY_LABEL",
                "AISP.FOOTER.ABOUT_US_URL", "AISP.FOOTER.PRIVACY_POLICY_URL",
                "AISP.FOOTER.TNC_URL", "AISP.FOOTER.HELP_URL", "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_SCREENREADER_MSG",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.accountAccessText = translations["AISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                    $scope.thirdPartyText = translations["AISP.HEADER.THIRD_PARTY_LABEL"];
                    $scope.accountSelText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER"];
                    $scope.preUserTitleText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL"];
                    $scope.postUserTitleText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL"];
                    $scope.logoUrl = translations["AISP.HEADER.LOGOURL"];
                    $scope.bankLogoImgAlt = translations["AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.selAccountText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER"];
                    $scope.selAccountTableCaptionText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_TABLE_CAPTION"];

                    $scope.selAllText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ALL_COLUMN_HEADER"];
                    $scope.nickNameText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER"];
                    $scope.acctNoText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER"];
                    $scope.currencyText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER"];
                    $scope.acctTypeText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER"];
                    $scope.selectedAccountText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER"];
                    $scope.selectedAccountTableCaptionText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                    $scope.removeText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_COLUMN_HEADER"];
                    $scope.continueBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL"];
                    $scope.retryBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL"];
                    $scope.cancelBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL"];
                    $scope.selectAllBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_ALL_BUTTON_LABEL"];
                    $scope.selectNoneBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_NONE_BUTTON_LABEL"];
                    $scope.aboutUsText = translations["AISP.FOOTER.ABOUT_US_LABEL"];
                    $scope.cookieText = translations["AISP.FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tncText = translations["AISP.FOOTER.TNC_LABEL"];
                    $scope.helpText = translations["AISP.FOOTER.HELP_LABEL"];
                    $scope.regulatoryText = translations["AISP.FOOTER.REGULATORY_LABEL"];
                    $scope.aboutUsUrl = translations["AISP.FOOTER.ABOUT_US_URL"];
                    $scope.privacyPolicyUrl = translations["AISP.FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncUrl = translations["AISP.FOOTER.TNC_URL"];
                    $scope.helpUrl = translations["AISP.FOOTER.HELP_URL"];
                    $scope.tooltipTt = translations["AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                    $scope.disabledPopoverMsg = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_MSG"];
                    $scope.disabledPopoverScrMsg = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_SCREENREADER_MSG"];
                    $scope.disabledAccountMsg = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_ACCOUNT_MSG"]);
                    $scope.cancelBtnPopText = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL"];
                });
        };
        $scope.createCustomErr = function (error) {
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "800";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };
        $scope.pagination = function () {
            $scope.pages = 1;
            $scope.currentPage = 1;
            $scope.AccountsByPage = AcctService.paged($scope.allAccounts, $scope.pageSize);
            $scope.accountsData = $scope.AccountsByPage[$scope.currentPage];
        };
        // Select all account while clicking on single check box
        $scope.accountToggleAll = function (toggleStatus) {
            angular.forEach($scope.AccountsByPage[$scope.currentPage], function (acct) {
                if (acct.accountDisabled) {
                    acct.selected = false;
                } else {
                    acct.selected = toggleStatus;
                }
            });
            $scope.selectedAccountList();
        };

        $scope.removeAccount = function (account, $event) {
            $event.preventDefault();
            angular.forEach($scope.allAccounts, function (acct) {
                if (acct.HashedValue === account.HashedValue) {
                    acct.selected = false;
                }
            });
            $scope.selectedAccountList();
        };
        $scope.isSelectedAll = function () {
            var selectedAcct = $filter("filter")($scope.AccountsByPage[$scope.currentPage], { "selected": true });
            var disabledAcct = $filter("filter")($scope.AccountsByPage[$scope.currentPage], { "accountDisabled": false });
            //code for select all button focus for jaws
            if(event && event.target.type === "button"){
                $timeout(function() {
                    $(".accountselectionBtn").focus();
                });
            }
            if (selectedAcct.length === disabledAcct.length && disabledAcct.length > 0) {
                $scope.isAllAccountSelected = true;
            } else {
                $scope.isAllAccountSelected = false;
            }
        };
        // Account selected and deselect on checkbox change
        $scope.selectedAccountList = function () {
            $scope.selAccounts = $filter("filter")($scope.allAccounts, { "selected": true });
            $scope.isSelectedAll();
        };
        $scope.goPreview = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            //var csrf = $("#csrf").val();
            var correlationId = $("#correlationId").val();
            var consentType = "aisp";
            ConsentService.checkSession(consentType, reqData, resumePath, correlationId).then(function (resp) {
                if (resp.status === 200) {
                    $scope.accReq["SelectedAccounts"] = $scope.selAccounts;
                    var accountDetails = {
                        accountReqData: $scope.accReq
                    };
                    $state.go("aispReview", {
                        "accountRequestDetails": accountDetails
                    });
                }
            },
                function (error) {
                    AcctService.errorFallback(error,$scope, blockUI);
                });
        };
        /* Cancel button code here........ */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var consentType = "aisp";
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.cancelRequest(consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    AcctService.errorFallback(error,$scope, blockUI);
                });
        };

        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };
        /* Cancel model dialog box */

        $scope.openModal = function () {

            $translate(["AISP.CANCEL_POPUP.CANCEL_POPUP_HEADER",
                "AISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE", "AISP.CANCEL_POPUP.YES_BUTTON_LABEL",
                "AISP.CANCEL_POPUP.NO_BUTTON_LABEL", "AISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL", "AISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","AISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.CANCEL_POPUP.CANCEL_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.CANCEL_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["AISP.CANCEL_POPUP.NO_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["AISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.cancelRequestScrLabel = translations["AISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();

        };

        /* sessionTimeoutSubmission button code here........ */
        $scope.openSessionOutModal = function () {
            $translate(["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL", "AISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","AISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["AISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        // If page is loaded using Back Button of Review page
        $scope.isFromReview = function () {
            $scope.accountReqDetails = $state.params.selectedAccountDetails;
            if ($scope.accountReqDetails === null) {
                return false;
            } else {
                $scope.selAccounts = $scope.accountReqDetails.accountReqData.SelectedAccounts;
                // making checkbox selected for selected accounts
                angular.forEach($scope.allAccounts, function (acct) {
                    acct.selected = false;
                    $scope.accountDisabledFilter(acct);
                    angular.forEach($scope.selAccounts, function (SelectedAcct) {
                        if (SelectedAcct.HashedValue === acct.HashedValue) {
                            acct.selected = true;
                        }
                    });
                });
                return true;
            }
        };

        $scope.accountDisabledFilter = function (acct) {
            if (acct.AdditionalInformation["ACCOUNT-PERMISSION"] === "JZ" || acct.AdditionalInformation["ACCOUNT-PERMISSION"] === "JX") {
                acct.accountDisabled = true;
                $scope.disabledMsgShow = true;
            } else {
                acct.accountDisabled = false;
            }
        };

        $scope.selectAllAcctBtnChecked = function(){
            $scope.selectAllAccBtnEnabled = true;
            $scope.enabledAccounts = $filter("filter")($scope.AccountsByPage[$scope.currentPage], { "accountDisabled": false });
            if($scope.enabledAccounts.length > 0 ){
                $scope.selectAllAccBtnEnabled = true;
            }else{
                $scope.selectAllAccBtnEnabled = false;
            }
        };

        $scope.init();
    }]);

"use strict";
angular.module("consentApp").controller("AispReviewCtrl", ["$scope", "$state", "$translate", "ConsentService", "AcctService", "blockUI",
    "$window", "$uibModal", "$filter", "config", "$timeout", "$sce", "$fraudAnalyze",
    function ($scope, $state, $translate, ConsentService, AcctService, blockUI, $window, $uibModal, $filter, config, $timeout, $sce, $fraudAnalyze) {
        /* start code of Agree/submit button clicked */
        $scope.init = function () {
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.modelPopUpConf = config.modelpopupConfig;
            if ($scope.pageError) {
                if ($scope.pageError.errorCode === "731") {
                    $scope.redirectUri = $("#resumePath").val();
                    $scope.sessiontimeoutflag = true;
                    $timeout(function () {
                        $scope.openSessionOutModal();
                    }, 0);
                }
            } else {
                $scope.accountReqDetails = $state.params.accountRequestDetails;
                $scope.permissionsList = $scope.accountReqDetails.accountReqData.Data.Permissions;
                $scope.selectedAcctTableData = $scope.accountReqDetails.accountReqData.SelectedAccounts;
                $scope.expiryDate = $scope.ignoreTimeZone($scope.accountReqDetails.accountReqData.Data.ExpirationDateTime);
                $scope.termsConditn = false;
                $scope.totalNumberVisibleRows = config.totalNumberVisibleRows;
                $scope.isCollapsed = $scope.selectedAcctTableData.length > $scope.totalNumberVisibleRows;
                $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                $scope.tppInfo = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";
                $scope.maskAccountNumberLength = config.maskAccountNumberLength;
                $scope.tillDate = $scope.ignoreTimeZone($scope.accountReqDetails.accountReqData.Data.TransactionToDateTime);
                $scope.fromDate = $scope.ignoreTimeZone($scope.accountReqDetails.accountReqData.Data.TransactionFromDateTime);

                $scope.permissionListData = [];
                angular.forEach($scope.permissionsList, function (perList) {
                    if (perList !== "ReadTransactionsCredits" && perList !== "ReadTransactionsDebits") {
                        if (perList === "ReadTransactionsBasic") {
                            if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                perList = "AllReadTransactionsBasic";
                            } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                perList = "ReadTransactionsCreditsBasic";
                            } else {
                                perList = "ReadTransactionsDebitsBasic";
                            }
                        } else if (perList === "ReadTransactionsDetail") {
                            if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                perList = "AllReadTransactionsDetail";
                            } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                perList = "ReadTransactionsCreditsDetail";
                            } else {
                                perList = "ReadTransactionsDebitsDetail";
                            }
                        }
                        $scope.permissionListData.push(perList);
                    }
                });
                // code for Info Section.............
                var ns = "AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.";
                var jDATA = ["IMPORTANT_INFORMATION_HEADING1", "IMPORTANT_INFORMATION_HEADING2",
                    "IMPORTANT_INFORMATION_HEADING3", "IMPORTANT_INFORMATION_HEADING4",
                    "IMPORTANT_INFORMATION_HEADING5", "IMPORTANT_INFORMATION_DESCRIPTION1",
                    "IMPORTANT_INFORMATION_DESCRIPTION2", "IMPORTANT_INFORMATION_DESCRIPTION3",
                    "IMPORTANT_INFORMATION_DESCRIPTION4", "IMPORTANT_INFORMATION_DESCRIPTION5"];
                $scope.infoData = {};
                $translate(jDATA.map(function (key) { return ns + key; })).then(function (translations) {
                    for (var i = 1; i <= 5; i++) {
                        if (translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i] !== "" ||
                            translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i] !== "") {
                            $scope.infoData[i] = {};
                            $scope.infoData[i].title = translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i];
                            $scope.infoData[i].desc = $sce.trustAsHtml(translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i]);
                        }
                    }
                });

                $translate(["AISP.HEADER.LOGOURL", "AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                    "AISP.HEADER.ACCOUNT_ACCESS_LABEL", "AISP.HEADER.THIRD_PARTY_LABEL",
                    "AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.REVIEW_CONFIRM_HEADER",
                    "AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL",
                    "AISP.FOOTER.ABOUT_US_LABEL",
                    "AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION",
                    "AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL",
                    "AISP.FOOTER.PRIVACY_POLICY_LABEL", "AISP.FOOTER.TNC_LABEL",
                    "AISP.FOOTER.HELP_LABEL", "AISP.FOOTER.REGULATORY_LABEL",
                    "AISP.FOOTER.ABOUT_US_URL", "AISP.FOOTER.PRIVACY_POLICY_URL",
                    "AISP.FOOTER.TNC_URL", "AISP.FOOTER.HELP_URL", "AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW",
                    "AISP.REVIEW_CONFIRM_PAGE.BUTTONS.CONFIRM_BUTTON_ENABLE_SCREENREADER_LABEL"]).then(function (translations) {
                        $scope.accountAccessText = translations["AISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                        $scope.thirdPartyText = translations["AISP.HEADER.THIRD_PARTY_LABEL"];
                        $scope.accountSelText = translations["AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.REVIEW_CONFIRM_HEADER"];
                        $scope.selectedRiviewAccountTableCaptionText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                        $scope.preUserTitleText = translations["AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL"];
                        $scope.postUserTitleText = translations["AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL"];
                        $scope.logoUrl = translations["AISP.HEADER.LOGOURL"];
                        $scope.bankLogoImgAlt = translations["AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                        $scope.aboutUsText = translations["AISP.FOOTER.ABOUT_US_LABEL"];
                        $scope.cookieText = translations["AISP.FOOTER.PRIVACY_POLICY_LABEL"];
                        $scope.tncText = translations["AISP.FOOTER.TNC_LABEL"];
                        $scope.helpText = translations["AISP.FOOTER.HELP_LABEL"];
                        $scope.regulatoryText = translations["AISP.FOOTER.REGULATORY_LABEL"];
                        $scope.aboutUsUrl = translations["AISP.FOOTER.ABOUT_US_URL"];
                        $scope.privacyPolicyUrl = translations["AISP.FOOTER.PRIVACY_POLICY_URL"];
                        $scope.tncUrl = translations["AISP.FOOTER.TNC_URL"];
                        $scope.helpUrl = translations["AISP.FOOTER.HELP_URL"];
                        $scope.tooltipTt = translations["AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                        $scope.confirmBtnScrLabel = translations["AISP.REVIEW_CONFIRM_PAGE.BUTTONS.CONFIRM_BUTTON_ENABLE_SCREENREADER_LABEL"];                  

                    });

            }

        };

        /* Allow Button Submission Code  */
        $scope.allowSubmission = function () {
            var resumePath = $("#resumePath").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var payLoad = {};
            payLoad["accountDetails"] = $scope.selectedAcctTableData;
            payLoad["fsHeaders"] = $("#fsHeaders").val();
            var consentType = "aisp";
            var correlationId = $("#correlationId").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.accountRequest(consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                if (resp.status === 200) {
                    blockUI.start();
                    var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                    //$("#csrf").appendTo($form);
                    // $("#user_oauth_approval").appendTo($form);
                    $($form).appendTo("body");
                    $($form).submit();
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        $scope.backToAccountSelectPage = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var correlationId = $("#correlationId").val();
            var consentType = "aisp";
            ConsentService.checkSession(consentType, reqData, resumePath, correlationId).then(function (resp) {
                if (resp.status === 200) {
                    $state.go("aispAccount", {
                        "selectedAccountDetails": $scope.accountReqDetails
                    });
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };


        /* cancle Button Submission Code  */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var consentType = "aisp";
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.cancelRequest(consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };
        /* Cancel model dialog box */

        $scope.openModal = function () {
            $translate(["AISP.CANCEL_POPUP.CANCEL_POPUP_HEADER",
                "AISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE", "AISP.CANCEL_POPUP.YES_BUTTON_LABEL",
                "AISP.CANCEL_POPUP.NO_BUTTON_LABEL", "AISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL", "AISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","AISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.CANCEL_POPUP.CANCEL_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.CANCEL_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["AISP.CANCEL_POPUP.NO_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["AISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.cancelRequestScrLabel = translations["AISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };

        $scope.openSessionOutModal = function () {
            $translate(["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL",
                "AISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","AISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["AISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };
        $scope.ignoreTimeZone = function (val) {
            if (val !== undefined && val !== "" && val !== null) {
                var newDate = new Date(val.substring(0, val.indexOf("T" )));
                return newDate;
            } else {
                return null;
            }
        };

        $scope.init();
    }]);

"use strict";
angular.module("consentApp").controller("AispAuthorisationRenewalCtrl", ["$scope", "$state", "$translate", "ConsentService", "AcctService", "blockUI",
    "$window", "$uibModal", "$filter", "config", "$timeout", "$sce", "$fraudAnalyze",
    function ($scope, $state, $translate, ConsentService, AcctService, blockUI, $window, $uibModal, $filter, config, $timeout, $sce, $fraudAnalyze) {
        /* start code of Agree/submit button clicked */
        $scope.init = function () {
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.modelPopUpConf = config.modelpopupConfig;
            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                // $scope.pageError = { errorCode: "731" };
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        }, 0);
                    } else {
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.accountReqDetails = angular.fromJson($("#account-request").val());
                        $scope.permissionsList = $scope.accountReqDetails.Data.Permissions;
                        $scope.selectedAcctTableData = angular.fromJson($("#psuAccounts").val()).Data.Account;
                        $scope.expiryDate = $scope.ignoreTimeZone($scope.accountReqDetails.Data.ExpirationDateTime);
                        $scope.termsConditn = false;
                        $scope.totalNumberVisibleRows = config.totalNumberVisibleRows;
                        $scope.isCollapsed = $scope.selectedAcctTableData.length > $scope.totalNumberVisibleRows;
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.tppInfo = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";
                        $scope.maskAccountNumberLength = config.maskAccountNumberLength;
                        $scope.tillDate = $scope.ignoreTimeZone($scope.accountReqDetails.Data.TransactionToDateTime);
                        $scope.fromDate = $scope.ignoreTimeZone($scope.accountReqDetails.Data.TransactionFromDateTime);

                        $scope.permissionListData = [];
                        angular.forEach($scope.permissionsList, function (perList) {
                            if (perList !== "ReadTransactionsCredits" && perList !== "ReadTransactionsDebits") {
                                if (perList === "ReadTransactionsBasic") {
                                    if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                        perList = "AllReadTransactionsBasic";
                                    } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                        perList = "ReadTransactionsCreditsBasic";
                                    } else {
                                        perList = "ReadTransactionsDebitsBasic";
                                    }
                                } else if (perList === "ReadTransactionsDetail") {
                                    if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                        perList = "AllReadTransactionsDetail";
                                    } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                        perList = "ReadTransactionsCreditsDetail";
                                    } else {
                                        perList = "ReadTransactionsDebitsDetail";
                                    }
                                }
                                $scope.permissionListData.push(perList);
                            }
                        });
                    } catch (e) {
                        $scope.createCustomErr({ "errorCode": "999" });
                    }
                }
            }
            // code for Info Section.............
            var ns = "AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.";
            var jDATA = ["IMPORTANT_INFORMATION_HEADING1", "IMPORTANT_INFORMATION_HEADING2",
                "IMPORTANT_INFORMATION_HEADING3", "IMPORTANT_INFORMATION_HEADING4",
                "IMPORTANT_INFORMATION_HEADING5", "IMPORTANT_INFORMATION_DESCRIPTION1",
                "IMPORTANT_INFORMATION_DESCRIPTION2", "IMPORTANT_INFORMATION_DESCRIPTION3",
                "IMPORTANT_INFORMATION_DESCRIPTION4", "IMPORTANT_INFORMATION_DESCRIPTION5"];
            $scope.infoData = {};
            $translate(jDATA.map(function (key) { return ns + key; })).then(function (translations) {
                for (var i = 1; i <= 5; i++) {
                    if (translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i] !== "" ||
                        translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i] !== "") {
                        $scope.infoData[i] = {};
                        $scope.infoData[i].title = translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i];
                        $scope.infoData[i].desc = $sce.trustAsHtml(translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i]);
                    }
                }
            });

            $translate(["AISP.HEADER.LOGOURL", "AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "AISP.HEADER.ACCOUNT_ACCESS_LABEL", "AISP.HEADER.THIRD_PARTY_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.AUTHORISATION_RENEWAL_HEADER",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART1",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART2",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART3",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER",
                "AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_ALL_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_LESS_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_HEADER",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.RENEW_AUTHORISATION_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL",
                "AISP.FOOTER.ABOUT_US_LABEL",
                "AISP.FOOTER.PRIVACY_POLICY_LABEL", "AISP.FOOTER.TNC_LABEL",
                "AISP.FOOTER.HELP_LABEL", "AISP.FOOTER.REGULATORY_LABEL",
                "AISP.FOOTER.ABOUT_US_URL", "AISP.FOOTER.PRIVACY_POLICY_URL",
                "AISP.FOOTER.TNC_URL", "AISP.FOOTER.HELP_URL",
            "AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW","AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"]).then(function (translations) {
                    $scope.accountAccessText = translations["AISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                    $scope.thirdPartyText = translations["AISP.HEADER.THIRD_PARTY_LABEL"];
                    $scope.accountRenewalText = translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.AUTHORISATION_RENEWAL_HEADER"];
                    $scope.instructionTextPart1 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART1"]);
                    $scope.instructionTextPart2 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART2"]);
                    $scope.instructionTextPart3 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART3"]);
                    $scope.nickNameText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER"];
                    $scope.acctNoText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER"];
                    $scope.currencyText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER"];
                    $scope.acctTypeText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER"];
                    $scope.selectedAccountText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER"];
                    $scope.selectedAccountTableCaptionText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                    $scope.showAllText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_ALL_LABEL"];
                    $scope.showLessText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_LESS_LABEL"];
                    $scope.permissionsListHeaderText = translations["AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_HEADER"];
                    $scope.transactionAccessDateHeader = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER"];
                    $scope.fromDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL"];
                    $scope.noConsentLabel = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL"];
                    $scope.toDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL"];
                    $scope.preTillDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL"];
                    $scope.expiryInvalidDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL"];
                    $scope.postTillDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL"];
                    $scope.denyAuthorisationButtonLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL"];
                    $scope.pressDenyScreenReaderLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL"];
                    $scope.allowAuthorisationButtonLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.RENEW_AUTHORISATION_BUTTON_LABEL"];
                    $scope.selectedRenewAuthAccountTableCaptionText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                    $scope.logoUrl = translations["AISP.HEADER.LOGOURL"];
                    $scope.bankLogoImgAlt = translations["AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.aboutUsText = translations["AISP.FOOTER.ABOUT_US_LABEL"];
                    $scope.cookieText = translations["AISP.FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tncText = translations["AISP.FOOTER.TNC_LABEL"];
                    $scope.helpText = translations["AISP.FOOTER.HELP_LABEL"];
                    $scope.regulatoryText = translations["AISP.FOOTER.REGULATORY_LABEL"];
                    $scope.aboutUsUrl = translations["AISP.FOOTER.ABOUT_US_URL"];
                    $scope.privacyPolicyUrl = translations["AISP.FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncUrl = translations["AISP.FOOTER.TNC_URL"];
                    $scope.helpUrl = translations["AISP.FOOTER.HELP_URL"];
                    $scope.tooltipTt = translations["AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];

                });
        };

        $scope.createCustomErr = function (error) {
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "800";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };

        /* Allow Button Submission Code  */
        $scope.allowSubmission = function () {
            var resumePath = $("#resumePath").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var payLoad = {};
            payLoad["accountDetails"] = $scope.selectedAcctTableData;
            payLoad["fsHeaders"] = $("#fsHeaders").val();
            var consentType = "aisp";
            var correlationId = $("#correlationId").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.accountRequest(consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                if (resp.status === 200) {
                    blockUI.start();
                    var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                    // $("#renewalAuthorisation").appendTo($form);
                    // $("#user_oauth_approval").appendTo($form);
                    $($form).appendTo("body");
                    $($form).submit();
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        /* cancle Button Submission Code  */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var consentType = "aisp";
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.cancelRequest(consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };
        /* Cancel model dialog box */

        $scope.openModal = function () {
            $translate(["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_HEADER",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.YES_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.CLOSE_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.GO_BACK_BUTTON_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_HEADER"];
                    var popupContent = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE"] + $scope.tppInfo + translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.modelpopupBodyContent = popupContent;
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.GO_BACK_BUTTON_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };

        $scope.openSessionOutModal = function () {
            $translate(["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        $scope.ignoreTimeZone = function (val) {
            if (val !== undefined && val !== "" && val !== null) {
                var newDate = new Date(val.substring(0, val.indexOf("T" )));
                return newDate;
            } else {
                return null;
            }   
        };
        $scope.toggleDisable = function ($event) {
            if ($event.keyCode === "32") {
                $event.preventDefault();
                return false;
            }
            else {
                $scope.$parent.toggleOpen();
            }
        };

        $scope.init();
    }]);

"use strict";
angular.module("consentApp").controller("PispAccountCtrl", ["$scope", "$rootScope", "$state", "$filter", "$uibModal",
    "ConsentService", "AcctService", "config", "blockUI", "$translate", "$window", "$timeout",
    function ($scope, $rootScope, $state, $filter, $uibModal, ConsentService, AcctService, config, blockUI, $translate, $window, $timeout, fontResolved) {
        $scope.init = function () {
            var $ = window.jQuery;
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.modelPopUpConf = config.modelpopupConfig;
            $scope.psuAcct = null;
            $scope.errorData = null;
            $scope.retry = null;
            $scope.isDataFound = false;

            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.isDataFound = true;
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        });
                    } else {
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.paymentinfo = angular.fromJson($("#paymentSetup").val());
                        $scope.psuAcct = angular.fromJson($("#psuAccounts").val());
                        $scope.accountSelected = angular.fromJson($("#accountSelected").val());
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.payInstBy = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")" ;

                        if (!$scope.psuAcct.Data.Account.length) {
                            throw null;
                        }
                        $scope.maskAccountNumberLength = config.maskAccountNumberLength;
                        $scope.isAccSelected = false;
                        $scope.termsConditn = false;
                        $scope.stopCnfirm = true;

                        $scope.accData = $scope.psuAcct.Data.Account;
                        if ($scope.accountSelected) {
                            $scope.selectedAcctObject = $scope.accData[0];
                            $scope.stopCnfirm = false;
                        }

                        $scope.payeeDetails = $scope.paymentinfo.Data.Initiation;
                        $scope.payeeName = $scope.payeeDetails.CreditorAccount.Name;
                        if ($scope.payeeDetails.CreditorAccount &&
                            $scope.payeeDetails.CreditorAccount.SecondaryIdentification &&
                            $scope.payeeDetails.RemittanceInformation &&
                            $scope.payeeDetails.RemittanceInformation.Unstructured &&
                            $scope.payeeDetails.RemittanceInformation.Reference) {
                            $scope.payeeReference = $scope.payeeDetails.CreditorAccount.SecondaryIdentification;
                        } else if ($scope.payeeDetails.CreditorAccount && $scope.payeeDetails.CreditorAccount.SecondaryIdentification &&
                            $scope.payeeDetails.RemittanceInformation &&
                            $scope.payeeDetails.RemittanceInformation.Reference) {
                            $scope.payeeReference = $scope.payeeDetails.CreditorAccount.SecondaryIdentification;
                        } else if ($scope.payeeDetails.CreditorAccount &&
                            $scope.payeeDetails.CreditorAccount.SecondaryIdentification) {
                            $scope.payeeReference = $scope.payeeDetails.CreditorAccount.SecondaryIdentification;
                        } else if ($scope.payeeDetails.RemittanceInformation &&
                            $scope.payeeDetails.RemittanceInformation.Reference &&
                            $scope.payeeDetails.RemittanceInformation.Unstructured) {
                            $scope.payeeReference = $scope.payeeDetails.RemittanceInformation.Reference;
                        } else if ($scope.payeeDetails.RemittanceInformation &&
                            $scope.payeeDetails.RemittanceInformation.Reference) {
                            $scope.payeeReference = $scope.payeeDetails.RemittanceInformation.Reference;
                        } else if ($scope.payeeDetails.RemittanceInformation &&
                            $scope.payeeDetails.RemittanceInformation.Unstructured) {
                            $scope.payeeReference = null;
                        } else {
                            $scope.payeeReference = null;
                        }
                        $scope.amount = $scope.payeeDetails.InstructedAmount.Amount;
                        $scope.currency = $scope.payeeDetails.InstructedAmount.Currency;

                        //code for back button
                        if ($state.params.pispContractDetails !== null) {
                            $scope.selectedAcctObject = $state.params.pispContractDetails.accountDetails;
                            $scope.selectAccount($scope.selectedAcctObject, event);
                        }

                    } catch (e) {
                        $scope.createCustomErr({ "errorCode": "999" });
                    }
                }
            }

            $translate(["PISP.HEADER.LOGOURL", "PISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
            "PISP.HEADER.ACCOUNT_ACCESS_LABEL", "PISP.HEADER.THIRD_PARTY_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER",
            "PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_DETAILS_HEADER",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_INITIATED_BY_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_NAME_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.AMOUNT_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_REFERENCE_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAY_FROM_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.SELECT_ACCOUNT_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL",
            "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL",
            "PISP.FOOTER.ABOUT_US_LABEL", "PISP.FOOTER.PRIVACY_POLICY_LABEL",
            "PISP.FOOTER.TNC_LABEL", "PISP.FOOTER.HELP_LABEL",
            "PISP.FOOTER.REGULATORY_LABEL",
            "PISP.FOOTER.ABOUT_US_URL",
            "PISP.FOOTER.PRIVACY_POLICY_URL", "PISP.FOOTER.TNC_URL",
            "PISP.FOOTER.HELP_URL","PISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"]).then(function (translations) {

                $scope.accountAccessText = translations["PISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                $scope.thirdPartyText = translations["PISP.HEADER.THIRD_PARTY_LABEL"];
                $scope.accountSelText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER"];
                $scope.paymentCheckText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL"];
                $scope.logoUrl = translations["PISP.HEADER.LOGOURL"];
                $scope.bankLogoImgAlt = translations["PISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                $scope.paymentInfoText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_DETAILS_HEADER"];
                $scope.paymtIntByText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_INITIATED_BY_LABEL"];
                $scope.payNameText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_NAME_LABEL"];
                $scope.PayAmountText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.AMOUNT_LABEL"];
                $scope.payRefText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_REFERENCE_LABEL"];
                $scope.payFromText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAY_FROM_LABEL"];
                $scope.selectAccountText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.SELECT_ACCOUNT_LABEL"];
                $scope.contBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL"];
                $scope.rtrBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL"];
                $scope.cancelBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL"];
                $scope.aboutUsText = translations["PISP.FOOTER.ABOUT_US_LABEL"];
                $scope.cookieText = translations["PISP.FOOTER.PRIVACY_POLICY_LABEL"];
                $scope.tncText = translations["PISP.FOOTER.TNC_LABEL"];
                $scope.helpText = translations["PISP.FOOTER.HELP_LABEL"];
                $scope.regulatoryText = translations["PISP.FOOTER.REGULATORY_LABEL"];
                $scope.aboutUsUrl = translations["PISP.FOOTER.ABOUT_US_URL"];
                $scope.privacyPolicyUrl = translations["PISP.FOOTER.PRIVACY_POLICY_URL"];
                $scope.tncUrl = translations["PISP.FOOTER.TNC_URL"];
                $scope.helpUrl = translations["PISP.FOOTER.HELP_URL"];
                $scope.tooltipTt = translations["PISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
            });
        };

        $scope.confirm = function () {
            var selectedAccounts = $filter("filter")($scope.accData, function (d) {
                return d.Account.Identification === $scope.selectedAcctObject.Account.Identification;
            });

            var paymentDetails = {
                payInstBy: $scope.payInstBy,
                payeeName: $scope.payeeName,
                payeeRef: $scope.payeeReference,
                amount: $scope.amount,
                currency: $scope.currency,
                accountDetails: selectedAccounts[0]
            };
            var resumePath = $("#resumePath").val();
            var reqData = selectedAccounts[0];
            var user = $("#x-user-id").val();
            var correlationId = $("#correlationId").val();
            var consentType = "pisp";
            ConsentService.fundsCheckRequest(consentType, reqData, resumePath, user, correlationId).then(function (resp) {
                if (resp.status === 200) {
                    $state.go("pispReview", {
                        "paymentDetails": paymentDetails
                    });
                }
            },
                function (error) {
                    $scope.stopCnfirm = true;
                    AcctService.errorFallback(error,$scope, blockUI);
                });
        };

        $scope.selectAccount = function (data, event) {
            if (event) {
                event.preventDefault();
            }
            if (data !== null && data !== "") {
                $scope.selectedAcctObject = data;
                $scope.isAccSelected = true;
                $scope.stopCnfirm = false;
            } else {
                $scope.selectedAcctObject = null;
                $scope.stopCnfirm = true;
                $scope.isAccSelected = false;
            }
        };

        /* Cancel button code here........ */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var consentType = "pisp";
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.cancelRequest(consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    AcctService.errorFallback(error,$scope, blockUI);
                });
        };

        /* Cancel model dialog box */
        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };

        $scope.openCancelModal = function () {
            $translate(["PISP.CANCEL_POPUP.CANCEL_POPUP_HEADER",
            "PISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE", "PISP.CANCEL_POPUP.YES_BUTTON_LABEL",
            "PISP.CANCEL_POPUP.NO_BUTTON_LABEL","PISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL","PISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","PISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                $scope.modelPopUpConf.modelpopupTitle = translations["PISP.CANCEL_POPUP.CANCEL_POPUP_HEADER"];
                $scope.modelPopUpConf.modelpopupBodyContent = translations["PISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE"];
                $scope.modelPopUpConf.btn.okbtn.label = translations["PISP.CANCEL_POPUP.YES_BUTTON_LABEL"];
                $scope.modelPopUpConf.btn.cancelbtn.label = translations["PISP.CANCEL_POPUP.NO_BUTTON_LABEL"];
                $scope.modelPopUpConf.closebtn = translations["PISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL"];
                $scope.modelPopUpConf.cancelRequestScrLabel = translations["PISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                $scope.modelPopUpConf.noBtnScrLabel = translations["PISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"];
            });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };
        $scope.openSessionOutModal = function () {
            $translate(["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
            "PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
            "PISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL","PISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","PISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                $scope.modelPopUpConf.modelpopupTitle = translations["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                $scope.modelPopUpConf.modelpopupBodyContent = translations["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                $scope.modelPopUpConf.btn.okbtn.label = translations["PISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["PISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                $scope.modelPopUpConf.noBtnScrLabel = translations["PISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
            });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };


        $scope.createCustomErr = function (error) {
            $scope.isDataFound = true;
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "999";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };

        $scope.init();
    }]);

"use strict";
angular.module("consentApp").controller("PispReviewCtrl", ["$scope", "$rootScope", "$state", "$uibModal", "blockUI", "$filter",
    "config", "$translate", "ConsentService", "AcctService", "$sce", "$window", "$timeout", "$fraudAnalyze",
    function ($scope, $rootScope, $state, $uibModal, blockUI, $filter, config, $translate, ConsentService, AcctService, $sce, $window, $timeout, $fraudAnalyze) {
        $scope.init = function () {
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.modelPopUpConf = config.modelpopupConfig;
            $scope.pageError = angular.fromJson($("#error").val());
            if ($scope.pageError) {
                if ($scope.pageError.errorCode === "731") {
                    $scope.redirectUri = $("#resumePath").val();
                    $scope.sessiontimeoutflag = true;
                    $timeout(function () {
                        $scope.openSessionOutModal();
                    }, 0);
                }
            } else {

                $scope.stopCnfirm = true;
                $scope.paymentDetails = $state.params.paymentDetails;
                $scope.payInstBy = $scope.paymentDetails.payInstBy;
                $scope.payeeName = $scope.paymentDetails.payeeName;
                $scope.payeeRef = $scope.paymentDetails.payeeRef;
                $scope.amount = $scope.paymentDetails.amount;
                $scope.currency = $scope.paymentDetails.currency;
                $scope.acountNumber = $scope.paymentDetails.accountDetails.Account.Identification;
                $scope.nickName = $scope.paymentDetails.accountDetails.Nickname;
                $scope.termsConditn = false;
                $scope.maskAccountNumberLength = config.maskAccountNumberLength;

                // code for Info Section.............
                var ns = "PISP.REVIEW_CONFIRM_PAGE.JURISDICTION_INFO.";
                var jDATA = ["JURISDICTION_INFO_HEADING1", "JURISDICTION_INFO_HEADING2",
                    "JURISDICTION_INFO_HEADING3", "JURISDICTION_INFO_HEADING4",
                    "JURISDICTION_INFO_HEADING5", "JURISDICTION_INFO_DESCRIPTION1",
                    "JURISDICTION_INFO_DESCRIPTION2", "JURISDICTION_INFO_DESCRIPTION3",
                    "JURISDICTION_INFO_DESCRIPTION4", "JURISDICTION_INFO_DESCRIPTION5"];
                $scope.infoData = {};
                $translate(jDATA.map(function (key) { return ns + key; })).then(function (translations) {
                    for (var i = 1; i <= 5; i++) {
                        if (translations["PISP.REVIEW_CONFIRM_PAGE.JURISDICTION_INFO.JURISDICTION_INFO_HEADING" + i] !== "" ||
                            translations["PISP.REVIEW_CONFIRM_PAGE.JURISDICTION_INFO.JURISDICTION_INFO_DESCRIPTION" + i] !== "") {
                            $scope.infoData[i] = {};
                            $scope.infoData[i].title = translations["PISP.REVIEW_CONFIRM_PAGE.JURISDICTION_INFO.JURISDICTION_INFO_HEADING" + i];
                            $scope.infoData[i].desc = $sce.trustAsHtml(translations["PISP.REVIEW_CONFIRM_PAGE.JURISDICTION_INFO.JURISDICTION_INFO_DESCRIPTION" + i]);
                        }
                    }
                });


                $translate(["PISP.HEADER.LOGOURL", "PISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                    "PISP.HEADER.ACCOUNT_ACCESS_LABEL", "PISP.HEADER.THIRD_PARTY_LABEL",
                    "PISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.REVIEW_CONFIRM_HEADER",
                    "PISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL",
                    "PISP.FOOTER.ABOUT_US_LABEL", "PISP.FOOTER.PRIVACY_POLICY_LABEL",
                    "PISP.FOOTER.TNC_LABEL", "PISP.FOOTER.HELP_LABEL", "PISP.FOOTER.ABOUT_US_LABEL",
                    "PISP.FOOTER.REGULATORY_LABEL", "PISP.FOOTER.ABOUT_US_URL",
                    "PISP.FOOTER.PRIVACY_POLICY_URL", "PISP.FOOTER.TNC_URL", "PISP.FOOTER.HELP_URL", "PISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW","PISP.REVIEW_CONFIRM_PAGE.BUTTONS.PAY_NOW_BTN_ENABLE_SCREENREADER_LABEL"]).then(function (translations) {
                        $scope.accountAccessText = translations["PISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                        $scope.thirdPartyText = translations["PISP.HEADER.THIRD_PARTY_LABEL"];
                        $scope.accountSelText = translations["PISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.REVIEW_CONFIRM_HEADER"];
                        $scope.preUserTitleText = translations["PISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL"];
                        $scope.logoUrl = translations["PISP.HEADER.LOGOURL"];
                        $scope.bankLogoImgAlt = translations["PISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                        $scope.aboutUsText = translations["PISP.FOOTER.ABOUT_US_LABEL"];
                        $scope.cookieText = translations["PISP.FOOTER.PRIVACY_POLICY_LABEL"];
                        $scope.tncText = translations["PISP.FOOTER.TNC_LABEL"];
                        $scope.helpText = translations["PISP.FOOTER.HELP_LABEL"];
                        $scope.regulatoryText = translations["PISP.FOOTER.REGULATORY_LABEL"];
                        $scope.aboutUsUrl = translations["PISP.FOOTER.ABOUT_US_URL"];
                        $scope.privacyPolicyUrl = translations["PISP.FOOTER.PRIVACY_POLICY_URL"];
                        $scope.tncUrl = translations["PISP.FOOTER.TNC_URL"];
                        $scope.helpUrl = translations["PISP.FOOTER.HELP_URL"];
                        $scope.tooltipTt = translations["PISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                        $scope.confirmBtnScrLabel = translations["PISP.REVIEW_CONFIRM_PAGE.BUTTONS.PAY_NOW_BTN_ENABLE_SCREENREADER_LABEL"];

                    });
            }

        };


        $scope.createPayment = function () {
            var payLoad = {};
            var accList = [$scope.paymentDetails.accountDetails];
            payLoad["accountDetails"] = accList;
            payLoad["fsHeaders"] = $("#fsHeaders").val();
            var resumePath = $("#resumePath").val();
            var consentType = "pisp";
            var correlationId = $("#correlationId").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.accountRequest(consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                if (resp.status === 200) {
                    blockUI.start();
                    var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                    $("#user_oauth_approval").appendTo($form);
                    $($form).appendTo("body");
                    $($form).submit();
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        $scope.backToAccountSelectPage = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var correlationId = $("#correlationId").val();
            var consentType = "pisp";
            ConsentService.checkSession(consentType, reqData, resumePath, correlationId).then(function (resp) {
                if (resp.status === 200) {
                    $state.go("pispAccount", {
                        "pispContractDetails": $scope.paymentDetails
                    });
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        /* cancle Button Submission Code  */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var consentType = "pisp";
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();

            ConsentService.cancelRequest(consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    AcctService.errorFallback(error, $scope, blockUI);
                });
        };

        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };
        /* Cancel model dialog box */
        $scope.openCancelModal = function () {
            $translate(["PISP.CANCEL_POPUP.CANCEL_POPUP_HEADER",
                "PISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE", "PISP.CANCEL_POPUP.YES_BUTTON_LABEL",
                "PISP.CANCEL_POPUP.NO_BUTTON_LABEL", "PISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL", "PISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","PISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["PISP.CANCEL_POPUP.CANCEL_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["PISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["PISP.CANCEL_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["PISP.CANCEL_POPUP.NO_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["PISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.cancelRequestScrLabel = translations["PISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["PISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };
        /* sessionTimeoutSubmission button code here........ */
        $scope.openSessionOutModal = function () {
            $translate(["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "PISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL", "PISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL","PISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["PISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["PISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["PISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        $scope.init();
    }
]);

"use strict";
angular.module("consentApp").directive("pageHeader",
    function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/acct-header.html"
        };
    });

"use strict";
angular.module("consentApp").directive("pageFooter",
    ["$sce", "$translate", function($sce, $translate) {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/acct-footer.html",
            link: function($scope) {
                $translate(["AISP.ACCOUNT_SELECTION_PAGE.FOOTER.COPY_RIGHT_LABEL"]).then(function(translations) {
                    $scope.copyRight = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.FOOTER.COPY_RIGHT_LABEL"]);
                });
            }
        };
    }]);

// public/js/directives/acct-pagination.js
"use strict";
angular.module("consentApp").directive("acctPagination", ["config", function (config) {
    return {
        restrict: "E",
        scope: {
            acctPage: "=",
            maxSize: "=",
            forceEllipse: "@",
            rotate: "@",
            filteredAccounts: "=",
            currentPage: "=",
            accountsByPage: "="
        },
        templateUrl: "views/acct-pagination.html",

        link: function (scope) {
            scope.pages = [];
            scope.totalAccounts = [];
            scope.$watch("accountsByPage", function (newVal) {
                if (newVal) {
                    scope.pages = scope.range(newVal.length);
                    if (scope.filteredAccounts) {
                        scope.totalAccounts = scope.filteredAccounts.length;
                    }
                }
            }, true);

            scope.chromeBrowserVersion = function () {
                var userAgent = navigator.userAgent;
                var browserfullVersion;// = "" + parseFloat(navigator.appVersion);
                var browserOffsetVersion, browserMajorVersion;
                if ((browserOffsetVersion = userAgent.indexOf("Chrome")) !== -1) {
                    browserfullVersion = userAgent.substring(browserOffsetVersion + 7);
                    browserMajorVersion = parseInt("" + browserfullVersion, 10);
                }
                return browserMajorVersion;
            };
            scope.applyChromeBorder = false;
            var chromeVersion = scope.chromeBrowserVersion();
            if (config.chromeBorderStartVersion <= chromeVersion && config.chromeBorderEndVersion >= chromeVersion) {
                scope.applyChromeBorder = true;
            }
            scope.range = function (input, total) {
                var ret = [];
                if (!total) {
                    total = input;
                    input = 0;
                }
                for (var i = input; i < total; i++) {
                    ret.push(i);
                }
                return ret;
            };
        }
    };
}]);

"use strict";
angular.module("consentApp").directive("errorNotice",
    ["$translate", "$sce", function ($translate, $sce) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                errorData: "="

            },
            templateUrl: "views/error-notice.html",
            link: {
                pre: function ($scope, el, attrs) {
                    $scope.$watch("errorData", function () {
                        var errorCodeMsg = "ERROR_MESSAGES." + $scope.errorData.errorCode;
                        var errorCodeMsgDesc = "ERROR_MESSAGES." + $scope.errorData.errorCode + "_DETAILS";
                        var correlationCodeMsg = "";
                        $scope.errorCssClass = ($scope.errorData.errorCSSClass === "warning") ? "page-alert-warning" : "page-alert-danger";
                        if ($scope.errorData.correlationId) {
                            correlationCodeMsg = $scope.errorData.correlationId;
                        }

                        $translate(["ERROR_MESSAGES.UNABLE_REQUEST_PROCESS",
                            "ERROR_MESSAGES.ALERT_LABEL", errorCodeMsg, errorCodeMsgDesc,
                            "ERROR_MESSAGES.CORRELATION_LABEL", correlationCodeMsg]).then(function (translations) {
                                $scope.alertText = translations["ERROR_MESSAGES.ALERT_LABEL"] + ":";
                                $scope.errorMsgText = (translations[errorCodeMsg] === errorCodeMsg) ?
                                    $sce.trustAsHtml(translations["ERROR_MESSAGES.UNABLE_REQUEST_PROCESS"]) :
                                    $sce.trustAsHtml(translations[errorCodeMsg]);
                                $scope.corRelationText = translations["ERROR_MESSAGES.CORRELATION_LABEL"] + ":";
                                $scope.corRelationMsgText = translations[correlationCodeMsg];
                                if (translations[errorCodeMsgDesc] !== errorCodeMsgDesc && translations[errorCodeMsgDesc] !== "") {
                                    $scope.isErrorMsgTextDescription = true;
                                    $scope.errorMsgTextDescription = $sce.trustAsHtml(translations[errorCodeMsgDesc]);
                                }
                            });
                    });

                }
            }
        };
    }]);

"use strict";
angular.module("consentApp").directive("modelPopUp", ["$uibModal", function($uibModal) {
    return {
        restrict: "E",
        scope: {
            modal: "="
        },
        link: function($scope, el, attrs) {
            $scope.modal.open = function() {
                $scope.modalInstance = $uibModal.open({
                    templateUrl: "views/modalPopUp.html",
                    animation: true,
                    backdrop: $scope.modal.backdrop,
                    keyboard: $scope.modal.escBtn,
                    windowClass: "pop-confirm",
                    scope: $scope,
                    controller: "modalCtrl"
                });
                $scope.modalInstance.result.then(function() {
                    //success
                });
            };
        }
    };
}]);
angular.module("consentApp").controller("modalCtrl", ["$scope", "$uibModalInstance", function($scope, $uibModalInstance) {

    $scope.cancelBtnClicked = function() {
        $uibModalInstance.dismiss("cancel");
    };

    $scope.okBtnClicked = function() {
        $uibModalInstance.dismiss("cancel");
        $scope.modal.btn.okbtn.action();
    };

}]);

"use strict";
angular.module("consentApp").directive("customUibAccordian", ["$timeout", function ($timeout) {
    return {
        restrict: "E",
        scope: {
            pl: "="
        },
        templateUrl: "views/customUibAccordian.html",
        link: function (scope, ele) {
            scope.oneAtATime = true;
            $timeout(function () {
                $(".panel-heading", ele).removeAttr("role");
                $("a", ele).addClass("panel-title").unwrap();
            });
            scope.toggleDisable = function ($event) {
                if ($event.keyCode === 32) {
                    $event.preventDefault();
                    return false;
                }
                          };
        }
    };
}]);

"use strict";
angular.module("consentApp").factory("AcctService", ["blockUI", function (blockUI) {
    function searchUtil(item, toSearch) {
        return (item.accountNumber.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
    }
    return {
        searched: function (valLists, toSearch) {
            return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
        },
        paged: function (valLists, pageSize) {
            var retVal = [];
            for (var i = 0; i < valLists.length; i++) {
                if (i % pageSize === 0) {
                    retVal[Math.floor(i / pageSize) + 1] = [valLists[i]];
                } else {
                    retVal[Math.floor(i / pageSize) + 1].push(valLists[i]);
                }
            }
            return retVal;
        },
        errorFallback: function (error, $scope, blockUI) {
            //  var error = { data:{exception:{errorCode:"727"}} };
            if (error.data.exception && error.data.exception.errorCode === "731") {
                $scope.redirectUri = error.data.redirectUri;
                $scope.openSessionOutModal();
            } else {
                $scope.errorData = {};
                $scope.errorData.errorCode = error.data.exception ? error.data.exception.errorCode : "800";
                $scope.errorData.correlationId = error.headers("correlationId") || null;
                blockUI.stop();
            }
        }
    };
}]);

"use strict";
angular.module("consentApp").service("ConsentService", ["$http", function ($http) {
    return {
        accountRequest: function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, headers) {
            if (reqData) {
                return $http.post("./" + consentType + "/consent?resumePath=" + resumePath + "&correlationId=" + correlationId + "&refreshTokenRenewalFlow=" + refreshTokenRenewalFlow, reqData, { "headers": headers });
            }
        },
        cancelRequest: function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag) {
            return $http.put("./" + consentType + "/cancelConsent?resumePath=" + resumePath + "&correlationId=" + correlationId + "&refreshTokenRenewalFlow=" + refreshTokenRenewalFlow + "&serverErrorFlag=" + serverErrorFlag, reqData);
        },
        fundsCheckRequest: function (consentType, reqData, resumePath, user, correlationId) {
            return $http.post("./" + consentType + "/preAuthorisation?resumePath=" + resumePath + "&x-user-id=" + user + "&correlationId=" + correlationId, reqData);
        },
        sessionLogout: function () {
            return $http({
                url: "./logoutActionOnClose",
                method: "POST",
                async: false
            });
        },
        checkSession: function (consentType, reqData, resumePath, correlationId) {
            return $http.get("./" + consentType + "/checkSession?correlationId=" + correlationId + "&resumePath=" + resumePath, reqData);
        }
    };
}]);

"use strict";
angular.module("consentApp").factory("fontloaderService", ["$q", "$window", "blockUI", function ($q, $window, blockUI) {
    return {
        isFontLoad: function () {
            blockUI.start();
            var deferred = $q.defer();
            $window.WebFont.load({
                custom: {
                    families: ["FontAwesome", "OpenSans-Bold", "OpenSans-Italic",
                        "OpenSans-Light", "OpenSans-Regular", "OpenSans-Semibold", "Glyphicons Halflings"]
                },
                active: function (familyName, fvd) {
                    deferred.resolve("font loaded");
                    blockUI.stop();
                }
            });
            return deferred.promise;
        }
    };
}]);

"use strict";
angular.module("consentApp").filter("maskNumber", function () {
  return function (str, charLength) {
    //var newStr = new Array(str.length-(str.length-1)).join('~') + str.substr(str.length-charLength, charLength);
    var newStr = "~" + str.substr(str.length - charLength, charLength);
    return newStr;
  };
});
