(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/acct-footer.html',
    '<!--footer start-->\n' +
    '<footer role="contentinfo">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12">\n' +
    '                <ul class="footer-links">\n' +
    '                    <li>\n' +
    '                        <a href="{{aboutUsUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="aboutUsText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{privacyPolicyUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="cookieText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{tncUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="tncText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '\n' +
    '                    <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{helpUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="helpText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <p class="regulatory-statement" ng-bind="regulatoryText"></p>\n' +
    '    </div>\n' +
    '</footer>\n' +
    '<!--footer end -->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/acct-header.html',
    '<!--header start-->\n' +
    '<header role="banner">\n' +
    ' <div class="header-container">\n' +
    '    <div class="container">        \n' +
    '        <img class="logo" ng-src={{logoUrl}} title="{{bankLogoImgAlt}}" alt="{{bankLogoImgAlt}}" ng-if="bankLogoImgAlt.length"/>        \n' +
    '        <div class="portal-details">\n' +
    '            <h1 ng-bind="accountAccessText"> </h1>\n' +
    '            <h2 ng-bind="thirdPartyText"></h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    ' </div>\n' +
    '</header>\n' +
    '<!--header end-->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/acct-pagination.html',
    '<div class="pagination-section">\n' +
    '    <ul uib-pagination ng-class="{\'border-chrome45\': applyChromeBorder}" total-items="totalAccounts" ng-model="currentPage" template-url="views/pagination.html" items-per-page="acctPage" max-size="maxSize" force-ellipses="forceEllipse" rotate="rotate" aria-label="{{\'AISP.ACCOUNT_SELECTION_PAGE.PAGINATION.SELECT_ACCOUNT_PAGINATION_SCREENREADER_LABEL\'|translate}}"></ul>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/aisp-account.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h3 ng-bind="accountSelText"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '    <!-- page errors STARTS-->\n' +
    '    <div class="row" ng-if="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page errors ENDS-->\n' +
    '    <!-- page description STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h4 class="page-description">\n' +
    '                <span ng-bind="preUserTitleText"></span>\n' +
    '                <span ng-bind="tppInfo"></span>\n' +
    '                <span ng-bind="postUserTitleText"></span>\n' +
    '            </h4>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page description ENDS-->\n' +
    '    <!--list of accounts table ENDS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-bind="selAccountText"></h4>\n' +
    '                <button type="button" ng-show="!isAllAccountSelected" role="button" class="btn btn-default btn-primary-bg accountselectionBtn" ng-disabled="!selectAllAccBtnEnabled" ng-click=\'accountToggleAll(true,$event)\'\n' +
    '                    ng-model="isAllAccountSelected" ng-bind="selectAllBtn" ></button>\n' +
    '                <button type="button" ng-show="isAllAccountSelected" role="button" class="btn btn-default btn-primary-bg accountselectionBtn" ng-click=\'accountToggleAll(false,$event)\'\n' +
    '                 ng-attr-aria-hidden="{{isAllAccountSelected?\'false\':\'true\'}}"  ng-bind="selectNoneBtn"></button>\n' +
    '            </div>\n' +
    '            <div class="details-container">\n' +
    '                    <div class="disabled-account-msg-section" ng-if="disabledMsgShow">\n' +
    '                    <p class="" ng-bind-html="disabledAccountMsg"></p>\n' +
    '                </div>\n' +
    '\n' +
    '\n' +
    '                <div class="account-Details-section">\n' +
    '                    <!--desktop view STARTS-->\n' +
    '                    <div class="table-view hidden-xs">\n' +
    '                        <table class="table table-responsive"  id="table-data">\n' +
    '                            <caption class="sr-only" ng-bind="selAccountTableCaptionText"></caption>\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col">\n' +
    '                                        <div class="checkbox">\n' +
    '                                            <input type="checkbox" id="checkall" ng-disabled="!selectAllAccBtnEnabled" ng-click="accountToggleAll(isAllAccountSelected,$event)" ng-model="isAllAccountSelected"\n' +
    '                                            />\n' +
    '                                            <label for="checkall" class="text-o" ng-bind="selAllText"> </label>\n' +
    '                                        </div>\n' +
    '                                    </th>\n' +
    '                                    <th scope="col" ng-bind="nickNameText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctNoText"> </th>\n' +
    '                                    <th scope="col" ng-bind="currencyText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctTypeText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse" ng-class-even="{even:true}" ng-class-odd="{odd:true}"\n' +
    '                                    ng-class="{\'acct-disable\' : acct.accountDisabled }">\n' +
    '                                    <td>\n' +
    '                                        <div class="checkbox" ng-show="!acct.accountDisabled">\n' +
    '                                            <input type="checkbox" ng-click="selectedAccountList()" ng-model="acct.selected" aria-label="{{acct.Account.Identification | maskNumber:maskAccountNumberLength}}"\n' +
    '                                                id="{{acct.HashedValue}}" />\n' +
    '                                            <label class="text-o" for="{{acct.HashedValue}}" ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></label>\n' +
    '                                        </div>\n' +
    '                                        <div class="disable-account" ng-show="acct.accountDisabled">\n' +
    '                                            <a href="javascript:void(0)" ng-attr-aria-label="{{acctNoText }} {{acct.Account.Identification  | maskNumber:maskAccountNumberLength}} {{ disabledPopoverScrMsg}}" popover-trigger="\'focus mouseenter\'" popover-placement="right" popover-class="tooltip-content"\n' +
    '                                                uib-popover="{{disabledPopoverMsg}}">\n' +
    '                                                <i class="fa fa-question-circle" ></i>\n' +
    '                                                                                          </a>\n' +
    '                                        </div>\n' +
    '                                    </td>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountType"></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <!--desktop view ENDS-->\n' +
    '                    <!--mobile view STARTS-->\n' +
    '                    <div class="div-view hidden-md hidden-lg hidden-sm">\n' +
    '                        <table class="table-responsive" id="table-data">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col">\n' +
    '                                        <div class="checkbox">\n' +
    '                                            <input role="checkbox" type="checkbox" ng-checked="checkall" id="checkall"  ng-click="accountToggleAll(isAllAccountSelected)"\n' +
    '                                                ng-model="isAllAccountSelected" />\n' +
    '                                            <label for="mob-checkall">\n' +
    '                                                <span class="sr-only" ng-bind="selAllText"></span>\n' +
    '                                            </label>\n' +
    '                                        </div>\n' +
    '                                    </th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="nickNameText"></th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="acctNoText"> </th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="currencyText"></th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="acctTypeText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse" ng-class-even="{even:true}"\n' +
    '                                ng-class-odd="{odd:true}"\n' +
    '                                ng-class="{\'acct-disable\' : acct.accountDisabled }">\n' +
    '                                    <td>\n' +
    '                                        <div class="checkbox" ng-show="!acct.accountDisabled">\n' +
    '                                            <input role="checkbox" type="checkbox"  ng-click="selectedAccountList()" ng-model="acct.selected"\n' +
    '                                                aria-label="{{acct.Account.Identification | maskNumber:maskAccountNumberLength}}"\n' +
    '                                                id="{{\'mob_\'+ acct.HashedValue}}" />\n' +
    '                                            <label class="text-o" for="{{\'mob_\'+ acct.HashedValue }}" ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></label>\n' +
    '                                        </div>\n' +
    '                                        <div class="disable-account" ng-show="acct.accountDisabled">\n' +
    '                                            <a href="javascript:void(0)" aria-label="" popover-trigger="\'focus mouseenter\'" popover-placement="right" popover-class="tooltip-content"\n' +
    '                                                uib-popover="{{disabledPopoverMsg}}">\n' +
    '                                                <i class="fa fa-question-circle" ></i>\n' +
    '                                            </a>\n' +
    '                                        </div>\n' +
    '                                    </td>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification  | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountType"></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <!--mobile view ENDS-->\n' +
    '                </div>\n' +
    '                <acct-pagination acct-page="pageSize" max-size="maxPaginationSize" force-ellipse="true" rotate="false" current-page="currentPage"\n' +
    '                    filtered-accounts="allAccounts" accounts-by-page="AccountsByPage" ng-show="allAccounts.length > pageSize"></acct-pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--list of accounts table ENDS-->\n' +
    '    <!--selected accounts table STARTS-->\n' +
    '    <div class="row selected-account-section"  id="selected-table" ng-hide="errorData" ng-if="selAccounts.length || selAccounts.account">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-bind="selectedAccountText"> </h4>\n' +
    '            </div>\n' +
    '            <div class="details-container">\n' +
    '                <div class="selected-account-Details-section">\n' +
    '                    <div class="table-view-selected-account hidden-xs">\n' +
    '                        <table class="table table-responsive">\n' +
    '                            <caption class="sr-only" ng-bind="selectedAccountTableCaptionText"></caption>\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col" width="25%" ng-bind="nickNameText"> </th>\n' +
    '                                    <th scope="col" width="30%" ng-bind="acctNoText"></th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="currencyText"> </th>\n' +
    '                                    <th scope="col" width="15%" ng-bind="acctTypeText"> </th>\n' +
    '                                    <th scope="col" width="5%" ng-bind="removeText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in selAccounts track by $index" ng-class="$odd ? \'even\' : \'odd\'">\n' +
    '                                    <span ng-bind="$index"></span>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountType"></td>\n' +
    '                                    <td class="text-center">\n' +
    '                                        <a role="button" href class="fa fa-times remove-icon" title="{{\'AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_COLUMN_HEADER\' | translate}}"\n' +
    '                                            ng-click="removeAccount(acct,$event)" aria-label="{{ \'AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_SELECTED_ACCOUNT_SCREENREADER_LABEL\' | translate}} {{acct.Account.Identification | maskNumber:maskAccountNumberLength }} "></a>\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="div-view-selected-account hidden-md hidden-lg hidden-sm">\n' +
    '                        <table class="table-responsive">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col" width="25%" ng-bind="nickNameText">\n' +
    '                                        </th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="acctNoText">\n' +
    '                                        </th>\n' +
    '                                    <th scope="col" width="20%" ng-bind="currencyText">\n' +
    '                                        </th>\n' +
    '                                    <th scope="col" width="25%" ng-bind="acctTypeText">\n' +
    '                                        </th>\n' +
    '                                    <th scope="col" width="5%" ng-bind="removeText" >\n' +
    '                                        </th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <!--    <tr ng-repeat="acct in psuAcct2">-->\n' +
    '                                <tr ng-repeat="acct in selAccounts track by $index" ng-class="$odd ? \'even\' : \'odd\'">\n' +
    '                                    <span ng-bind="$index"></span>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountType"></td>\n' +
    '                                    <td>\n' +
    '                                        <a href class="fa fa-times remove-icon" title="{{\'AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_COLUMN_HEADER\' | translate}}"\n' +
    '                                            ng-click="removeAccount(acct,$event)" aria-label="{{ \'AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_SELECTED_ACCOUNT_SCREENREADER_LABEL\' | translate}} {{acct.Account.Identification | maskNumber:maskAccountNumberLength }} "></a>\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--selected accounts table ENDS-->\n' +
    '    <!-- button section STARTS-->\n' +
    '    <div class="row mobile-section-btn">\n' +
    '        <div class="col-md-10 col-md-offset-1 btn-section">\n' +
    '            <button role="button" ng-hide="errorData" class="btn btn-primary pull-right" ng-disabled="(!selAccounts.length) && (!selAccounts.account)"\n' +
    '                ng-click="goPreview()" ng-bind="continueBtn"> </button>\n' +
    '            <a ng-if="retry" ng-href="{{retry}}" class="btn btn-primary pull-right" ng-bind="retryBtn"> </a>\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="openModal()" aria-label="{{cancelBtnPopText}}"  ng-bind="cancelBtn"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '       <!-- button section ENDS-->\n' +
    '</section>\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<page-footer></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/aisp-authorisation-renewal.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h3 ng-bind="accountRenewalText"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '    <!-- page description STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h4 class="page-description renewal-info">\n' +
    '                <span ng-bind-html="instructionTextPart1"></span>\n' +
    '                <span ng-bind="tppInfo"></span>\n' +
    '                <span ng-bind-html="instructionTextPart2"></span>\n' +
    '                <span ng-bind="tppInfo"></span>\n' +
    '                <span ng-bind-html="instructionTextPart3"></span>\n' +
    '            </h4>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page description ENDS-->\n' +
    '    <!--confirm selected accounts table STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-bind="selectedAccountText"> </h4>\n' +
    '            </div>\n' +
    '            <div class="details-container" ng-class="selectedAcctTableData.length > totalNumberVisibleRows? \'review-card-details\': \'\' ">\n' +
    '                <div class="account-table pisp-account-review-table">\n' +
    '                    <table class="table table-responsive">\n' +
    '                        <caption class="sr-only" ng-bind="selectedRenewAuthAccountTableCaptionText"></caption>\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th scope="col" ng-bind="nickNameText"></th>\n' +
    '                                <th scope="col" ng-bind="acctNoText"> </th>\n' +
    '                                <th scope="col" ng-bind="currencyText"></th>\n' +
    '                                <th scope="col" ng-bind="acctTypeText"></th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="acct in selectedAcctTableData | limitTo:totalNumberVisibleRows" ng-class="$odd ? \'even\' : \'odd\'">\n' +
    '                                <td ng-bind="acct.Nickname"></td>\n' +
    '                                <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                <td ng-bind="acct.Currency"></td>\n' +
    '                                <td ng-bind="acct.AccountType"></td>\n' +
    '                            </tr>\n' +
    '                            <tr uib-collapse="isCollapsed" ng-repeat="acct in selectedAcctTableData | limitTo: (totalNumberVisibleRows - selectedAcctTableData.length)"\n' +
    '                                ng-show="selectedAcctTableData.length > totalNumberVisibleRows" ng-class="$odd ? \'odd\' : \'even\'">\n' +
    '                                <td ng-bind="acct.Nickname"></td>\n' +
    '                                <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                <td ng-bind="acct.Currency"></td>\n' +
    '                                <td ng-bind="acct.AccountType"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <div ng-show="selectedAcctTableData.length > totalNumberVisibleRows" role="presentation">\n' +
    '                <div class="collapse-container" ng-class="{\'show-more\' : isCollapsed, \'show-less\' : !isCollapsed}">\n' +
    '                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed" ng-attr-aria-expanded="{{isCollapsed?\'false\':\'true\'}}">\n' +
    '                                               <span ng-show="isCollapsed" class="btn-span">\n' +
    '                            <i class="fa fa-plus"></i>\n' +
    '                            <span ng-bind="showAllText"></span>\n' +
    '                        </span>\n' +
    '                        <span ng-hide="isCollapsed" class="btn-span">\n' +
    '                            <i class="fa fa-minus"></i>\n' +
    '                            <span ng-bind="showLessText"></span>\n' +
    '                        </span>\n' +
    '                    </button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--confirm selected accounts table ENDS-->\n' +
    '    <!--list of permissions STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-bind="permissionsListHeaderText"> </h4>\n' +
    '            </div>\n' +
    '            <ul class="permission-list">\n' +
    '                <li ng-repeat="pl in permissionListData" ng-switch="(\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+pl+ \'_ELEMENTS\' | uppercase | translate).length">\n' +
    '                    <div ng-switch-when="0">\n' +
    '                        <span class="list-group-item consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.\'+pl| uppercase)|translate)"\n' +
    '                            ng-bind="perm_list"> </span>\n' +
    '                    </div>\n' +
    '                    <div ng-switch-default>\n' +
    '                        <custom-uib-accordian status="status" pl="pl"></custom-uib-accordian>\n' +
    '                    </div>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--list of permissions ENDS-->\n' +
    '    <!--requested transaction access STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-bind="transactionAccessDateHeader"> </h4>\n' +
    '            </div>\n' +
    '            <div class="row transaction-details-container">\n' +
    '                <div class="col-md-6">\n' +
    '                    <span class="boi-input" ng-bind="fromDateLabel"> </span>\n' +
    '                    <span class="boi-input" ng-bind="fromDate? (fromDate | date: \'dd MMMM yyyy\') : noConsentLabel">\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '                <div class="col-md-6">\n' +
    '                    <span class="boi-input" ng-bind="toDateLabel"></span>\n' +
    '                    <span class="boi-input" ng-bind="tillDate? (tillDate | date: \'dd MMMM yyyy\') : noConsentLabel">\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--requested transaction access ENDS-->\n' +
    '    <!--consent validity STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="box-container consent-validity-section">\n' +
    '                <span class="boi-input" ng-if="expiryDate" ng-bind="preTillDateLabel">&nbsp;</span>\n' +
    '                <span class="boi-input date-data"\n' +
    '                    ng-bind="expiryDate ? (expiryDate | date:\'dd MMMM yyyy\') : expiryInvalidDateLabel"></span>\n' +
    '                <span class="boi-input" ng-if="expiryDate" ng-bind="postTillDateLabel"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--consent validity ENDS-->\n' +
    '    <!-- page error STARTS-->\n' +
    '    <div class="row" ng-if="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page error ENDS-->\n' +
    '    <!-- button section STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <button role="button" class="btn btn-primary pull-right" ng-click="allowSubmission()" ng-bind="allowAuthorisationButtonLabel"\n' +
    '                ng-hide="errorData"> </button>\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="openModal()" aria-label="{{\'AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL\'| translate}}"\n' +
    '                ng-bind="denyAuthorisationButtonLabel">\n' +
    '                  </button>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- button section ENDS-->\n' +
    '    <div class="row hori-line-sec" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1 horizontal-line"> </div>\n' +
    '    </div>\n' +
    '    <!-- important info section STARTS-->\n' +
    '    <div class="row important-info" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="box-container">\n' +
    '                <div class="help-list" ng-repeat="idata in infoData">\n' +
    '                    <h5 class="boi-label-semibold" ng-bind="idata.title" ng-if="idata.title">\n' +
    '                    </h5>\n' +
    '                    <p class="boi-input-sm-light" ng-bind-html="idata.desc"></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- important info section ENDS-->\n' +
    '</section>\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<page-footer></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/aisp-review.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h3 ng-init="REVIEW_CONFIRM=(\'AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.REVIEW_CONFIRM_HEADER\'|translate)" ng-bind="REVIEW_CONFIRM"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '    <!-- page description STARTS-->\n' +
    '    <div class="row" ng-hide="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h4 class="page-description">\n' +
    '                <span ng-init="pre_user_text=(\'AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL\'|translate)" ng-bind="pre_user_text"></span>\n' +
    '                <span ng-bind="tppInfo"></span>\n' +
    '                <span ng-init="post_user_text=(\'AISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL\'|translate)" ng-bind="post_user_text"></span>\n' +
    '            </h4>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page description ENDS-->\n' +
    '    <!--confirm selected accounts table STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-init="SEL_ACCOUNT_TITLE=(\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER\'|translate)"\n' +
    '                    ng-bind="SEL_ACCOUNT_TITLE"> </h4>\n' +
    '            </div>\n' +
    '            <div class="details-container" ng-class="selectedAcctTableData.length > totalNumberVisibleRows? \'review-card-details\': \'\' ">\n' +
    '                <div class="account-table pisp-account-review-table">\n' +
    '                    <table class="table table-responsive">\n' +
    '                        <caption class="sr-only" ng-bind="selectedRiviewAccountTableCaptionText"></caption>\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th scope="col" ng-init="NICK_NAME=(\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER\'|translate)"\n' +
    '                                    ng-bind="NICK_NAME"></th>\n' +
    '                                <th scope="col" ng-init="ACCOUNT_NUMBER=(\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER\'|translate)"\n' +
    '                                    ng-bind="ACCOUNT_NUMBER"> </th>\n' +
    '                                <th scope="col" ng-init="CURRENCY=(\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER\'|translate)" ng-bind="CURRENCY"></th>\n' +
    '                                <th scope="col" ng-init="ACCOUNT_TYPE=(\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER\'|translate)"\n' +
    '                                    ng-bind="ACCOUNT_TYPE"></th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '\n' +
    '                            <tr ng-repeat="acct in selectedAcctTableData | limitTo:totalNumberVisibleRows" ng-class="$odd ? \'even\' : \'odd\'">\n' +
    '                                <td ng-bind="acct.Nickname"></td>\n' +
    '                                <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                <td ng-bind="acct.Currency"></td>\n' +
    '                                <td ng-bind="acct.AccountType"></td>\n' +
    '                            </tr>\n' +
    '                            <tr uib-collapse="isCollapsed" ng-repeat="acct in selectedAcctTableData | limitTo: (totalNumberVisibleRows - selectedAcctTableData.length)"\n' +
    '                                ng-if="selectedAcctTableData.length > totalNumberVisibleRows" ng-class="$odd ? \'odd\' : \'even\'"\n' +
    '                                ng-attr-hidden="{{isCollapsed?\'true\':\'false\'}}">\n' +
    '                                <td ng-bind="acct.Nickname"></td>\n' +
    '                                <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                <td ng-bind="acct.Currency"></td>\n' +
    '                                <td ng-bind="acct.AccountType"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div ng-show="selectedAcctTableData.length > totalNumberVisibleRows" role="presentation">\n' +
    '                <div class="collapse-container" ng-class="{\'show-more\' : isCollapsed, \'show-less\' : !isCollapsed}">\n' +
    '                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed" ng-attr-aria-expanded="{{isCollapsed?\'false\':\'true\'}}">\n' +
    '                        <span ng-show="isCollapsed" class="btn-span">\n' +
    '                            <i class="fa fa-plus"></i>\n' +
    '                            <span translate="{{\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_ALL_LABEL\'}}"></span>\n' +
    '                        </span>\n' +
    '                        <span ng-hide="isCollapsed" class="btn-span">\n' +
    '                            <i class="fa fa-minus"></i>\n' +
    '                            <span translate="{{\'AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_LESS_LABEL\'}}"></span>\n' +
    '                        </span>\n' +
    '                    </button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <!--confirm selected accounts table ENDS-->\n' +
    '    <!--list of permissions STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-init="PERMISSION_TITLE=(\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_HEADER\'|translate)"\n' +
    '                    ng-bind="PERMISSION_TITLE"> </h4>\n' +
    '            </div>\n' +
    '            <ul class="permission-list">\n' +
    '                <li ng-repeat="pl in permissionListData" ng-switch="(\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+pl+ \'_ELEMENTS\' | uppercase | translate).length">\n' +
    '                    <div ng-switch-when="0">\n' +
    '                        <span class="list-group-item consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.\'+pl| uppercase)|translate)"\n' +
    '                            ng-bind="perm_list"> </span>\n' +
    '                    </div>\n' +
    '                    <div ng-switch-default>\n' +
    '                        <custom-uib-accordian status="status" pl="pl"></custom-uib-accordian>\n' +
    '                    </div>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--list of permissions ENDS-->\n' +
    '    <!--requested transaction access STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="table-header" ng-init="ACCESS_DATE=(\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER\'|translate)"\n' +
    '                    ng-bind="ACCESS_DATE"> </h4>\n' +
    '            </div>\n' +
    '            <div class="row transaction-details-container">\n' +
    '                <div class="col-md-6 col-sm-6">\n' +
    '                    <span class="boi-input" ng-init="FROM_DATE=(\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL\'|translate)"\n' +
    '                        ng-bind="FROM_DATE"> </span>\n' +
    '                    <span class="boi-input" ng-bind="fromDate? (fromDate | date: \'dd MMMM yyyy\') : (\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL\'| translate)">\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 col-sm-6">\n' +
    '                    <span class="boi-input" ng-init="TO_DATE=(\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL\'|translate)" ng-bind="TO_DATE"></span>\n' +
    '                    <span class="boi-input" ng-bind="tillDate? (tillDate | date: \'dd MMMM yyyy\') : (\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL\'| translate)">\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--requested transaction access ENDS-->\n' +
    '    <!--consent validity STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="box-container consent-validity-section">\n' +
    '                <span class="boi-input" ng-if="expiryDate" ng-init="PRE_TILL_DATE_TEXT=(\'AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL\'|translate)"\n' +
    '                    ng-bind="PRE_TILL_DATE_TEXT">&nbsp;</span>\n' +
    '                <span class="boi-input date-data" ng-init="transExpiryDate= expiryDate? (expiryDate | date:\'dd MMMM yyyy\') : (\'AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL\' | translate)"\n' +
    '                    ng-bind="transExpiryDate"></span>\n' +
    '                <span class="boi-input" ng-if="expiryDate" ng-init="POST_TILL_DATE_TEXT=(\'AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL\'|translate)"\n' +
    '                    ng-bind="POST_TILL_DATE_TEXT"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--consent validity ENDS-->\n' +
    '    <!-- page error STARTS-->\n' +
    '    <div class="row" ng-if="errorData">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page error ENDS-->\n' +
    '    <!-- confirm authorisation STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="confirm-section box-container" ng-class="{selectedConfirmSection : termsConditn}">\n' +
    '                <div class="checkbox">\n' +
    '                    <label for="accpt_cond" class="boi-input" ng-init="ACCEPT_CONSENT_REQUEST=(\'AISP.REVIEW_CONFIRM_PAGE.ACCEPT_CONSENT_REQUEST_LABEL\'|translate)"\n' +
    '                        ng-bind="ACCEPT_CONSENT_REQUEST"></label>\n' +
    '                    <input role="checkbox" type="checkbox" id="accpt_cond" name="termsncondition" ng-model="termsConditn" aria-label="{{\'AISP.REVIEW_CONFIRM_PAGE.ACCEPT_CONSENT_REQUEST_LABEL\'|translate}}">\n' +
    '                    <span></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- confirm authorisation ENDS-->\n' +
    '    <!-- button section STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <button role="button" class="btn btn-primary pull-right" ng-disabled="!termsConditn" ng-click="allowSubmission()" ng-init="CONFIRM_BUTTON=(\'AISP.REVIEW_CONFIRM_PAGE.BUTTONS.CONFIRM_BUTTON_LABEL\'|translate)"\n' +
    '                ng-bind="CONFIRM_BUTTON" ng-attr-aria-label="{{termsConditn?confirmBtnScrLabel:\'\'}}"> </button>\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="backToAccountSelectPage()" ng-init="BACK_BUTTON=(\'AISP.REVIEW_CONFIRM_PAGE.BUTTONS.BACK_BUTTON_LABEL\'|translate)"\n' +
    '                ng-bind="BACK_BUTTON"> </button>\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="openModal()" aria-label="{{\'AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL\'| translate}}"\n' +
    '                ng-init="CANCEL_BUTTON=(\'AISP.REVIEW_CONFIRM_PAGE.BUTTONS.CANCEL_BUTTON_LABEL\'|translate)" ng-bind="CANCEL_BUTTON">\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- button section ENDS-->\n' +
    '    <div class="row hori-line-sec">\n' +
    '        <div class="col-md-10 col-md-offset-1 horizontal-line"> </div>\n' +
    '    </div>\n' +
    '    <!-- important info section STARTS-->\n' +
    '    <div class="row important-info">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="box-container">\n' +
    '                <div class="help-list" ng-repeat="idata in infoData" ng-class="{\'help-list-with-title\' : idata.title}">\n' +
    '                    <h5 class="boi-label-semibold" ng-bind="idata.title" ng-if="idata.title">\n' +
    '                    </h5>\n' +
    '                    <p class="boi-input-sm-light" ng-bind-html="idata.desc" ng-if="idata.desc"></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- important info section ENDS-->\n' +
    '</section>\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<page-footer></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/cancel-confirm-popup.html',
    '<div class="modal-header pop-confirm-header title-header">\n' +
    '    <button type="button" aria-expanded="false" aria-label="close" role="button" class="close" ng-click="cancelNo()" data-dismiss="modal">\n' +
    '         <i class="fa fa-close"></i> \n' +
    '    </button>\n' +
    '    <h2 class="boi-widget"> {{\'CANCEL_POPUP_HEADER\' | translate}} </h2>         \n' +
    '</div>\n' +
    '<div class="modal-body pop-confirm-body">\n' +
    '    <div class="confirm-msg">\n' +
    '        <p translate="CANCEL_POPUP_BODY.0" class="boi-input text-center"><p>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<div class="modal-footer pop-confirm-footer button-section">\n' +
    '        <button class="btn btn-primary pull-right col-xs-12 col-sm-1 col-md-1 secondary" ng-click="canecelYes()"> {{\'YES_BUTTON\' | translate}}  </button>\n' +
    '        <button type="button" class="btn btn-secondary col-xs-12 col-sm-1 col-md-1 primary" ng-click="cancelNo()" data-dismiss="modal"> {{\'NO_BUTTON\' | translate}} </button>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/cisp-account.html',
    '<section class="container main-container">\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-12 page-header">\n' +
    '            <h2 class="text-capitalize header-title-txt-clr">{{\'BANK_TITLE\' | translate}}</h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container" ng-if="errorMsg">\n' +
    '        <div class="col-xs-10 col-sm-10 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row shadow-container">\n' +
    '                <div class="col-xs-12 col-sm-12 col-md-12 content tpp-info">\n' +
    '                    <h3 class="text-capitalize" ng-class "infoMsg?\'text-info\':\'text-danger\'">{{errorMsg | translate}}</h3>\n' +
    '                    <h5 ng-if="correlationId" class="text-capitalize text-danger">{{ \'ERROR_REFERENCE_LABEL\' | translate}} <strong> {{correlationId}}</strong></h5>\n' +
    '                    <a ng-if="aispUrl" ng-href="{{aispUrl}}" class="go-back pull-right"><strong class="text-capitalize">{{ \'GO_BACK_TO\' | translate}} <u> {{aispName}} </u></strong></a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row shadow-container ">\n' +
    '                <h5 class="main-header"> {{ \'PRE_USER_TITLE_TEXT\' | translate }} {{ tppInfo }} {{ \'POST_USER_TITLE_TEXT\' | translate }}</h5>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row title-header acct-header-txt-clr acct-header-bg hidden-md hidden-lg hidden-sm">\n' +
    '                <h2 class=""> {{\'SEL_ACCOUNT_TITLE\' | translate}} </h2>\n' +
    '            </div>\n' +
    '            <div class="row shadow-container account-select">\n' +
    '                <div class="account-search-box">\n' +
    '                    <label class="sr-only" for="srch-term">{{\'SEARCH\' | translate}}</label>\n' +
    '                    <input type="text" class="form-control" value="search" placeholder="Search" name="srch-term" id="srch-term" ng-model="searchText" ng-change="search()">\n' +
    '                    <button class="btn btn-default btn-primary-bg btn-primary-txt-clr search-icon glyphicon glyphicon-search"><span class="sr-only">search</span></button>\n' +
    '                </div>\n' +
    '\n' +
    '                <div class="account-Details-section">\n' +
    '                    <div class="table-view hidden-xs">\n' +
    '                        <table class="table table-responsive">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th><label class="text-o">radio</label></th>\n' +
    '                                    <th scope="col"> {{\'NICK_NAME\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_NUMBER\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'CURRENCY\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_TYPE\' | translate}} </th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse">\n' +
    '                                    <td>\n' +
    '                                        <div class="radio">\n' +
    '                                            <input type="radio" ng-model="selAccounts.account" ng-value="acct" name="singleSelectAccount" id="{{acct.accountNumber}}" value="acct" />\n' +
    '                                            <label class="text-o" for="{{acct.accountNumber}}">table data</label>\n' +
    '                                        </div>\n' +
    '                                    </td>\n' +
    '                                    <td>{{acct.nickname}}</td>\n' +
    '                                    <td>{{acct.accountNumber}}</td>\n' +
    '                                    <td>{{acct.currency}}</td>\n' +
    '                                    <td>{{acct.accountType}}</td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="div-view hidden-md hidden-lg hidden-sm">\n' +
    '                        <div class="col-xs-12 acc-cont" ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse">\n' +
    '                            <div class="radio">\n' +
    '                                <input type="radio" ng-model="selAccounts.account" ng-value="acct" name="singleSelectAccountMobile" id="{{acct.accountNumber}}_id" value="acct" />\n' +
    '                                <label for="{{acct.accountNumber}}_id"> {{acct.accountNumber}}  </label>\n' +
    '                            </div>\n' +
    '                            <p ng-show="acct.selected"> {{\'NICK_NAME\' | translate}} : {{acct.nickname}} </p>\n' +
    '                            <p ng-show="acct.selected"> {{\'CURRENCY\' | translate}} : {{acct.currency}} </p>\n' +
    '                            <p ng-show="acct.selected"> {{\'ACCOUNT_TYPE\' | translate}} : {{acct.accountType}} </p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <acct-pagination acct-page="pageSize" max-size="maxPaginationSize" force-ellipse="true" rotate="false" current-page="currentPage" filtered-accounts="filteredAccounts" accounts-by-page="AccountsByPage"></acct-pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="row content-container" ng-if="selAccounts.length || selAccounts.account">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row title-header acct-header-txt-clr acct-header-bg">\n' +
    '                <h2 class=""> {{\'SELECT_ACCOUNT_TITLE\' | translate}} </h2>\n' +
    '            </div>\n' +
    '            <div class="row shadow-container ">\n' +
    '                <div class="selected-account-Details-section">\n' +
    '                    <div class="table-view-selected-account hidden-xs">\n' +
    '                        <table class="table table-responsive">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col"> {{\'NICK_NAME\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_NUMBER\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'CURRENCY\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_TYPE\' | translate}} </th>\n' +
    '                                    <th scope="col" class="sr-only">remove</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <!--	<tr ng-repeat="acct in psuAcct2">-->\n' +
    '                                <tr ng-repeat="acct in selAccounts track by $index">\n' +
    '                                    {{ $index }}\n' +
    '                                    <td>{{acct.nickname}}</td>\n' +
    '                                    <td>{{acct.accountNumber}}</td>\n' +
    '                                    <td>{{acct.currency}}</td>\n' +
    '                                    <td>{{acct.accountType}}</td>\n' +
    '                                    <td>\n' +
    '                                        <a href="#" class="glyphicon glyphicon-remove remove-icon" ng-click="removeAccount(acct,$event)"></a>\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="div-view-selected-account hidden-md hidden-lg hidden-sm">\n' +
    '                        <div class="col-xs-12 acc-cont" ng-repeat="acct in selAccounts track by $index">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <p> {{\'NICK_NAME\' | translate}} : {{acct.nickname}} </p>\n' +
    '                                <p> {{\'ACCOUNT_NUMBER\' | translate}} : {{acct.accountNumber}} </p>\n' +
    '                                <p> {{\'CURRENCY\' | translate}} : {{acct.currency}} </p>\n' +
    '                                <p> {{\'ACCOUNT_TYPE\' | translate}} : {{acct.accountType}} </p>\n' +
    '                                <button class="btn-secondary-bg btn-secondary-txt-clr btn-secondary-border-clr btn btn-secondary btn-inline text-upper-case col-xs-12" ng-click="removeAccount(acct,$event)"> {{\'DELETE_BUTTON\' | translate}}  </button>\n' +
    '                            </div>\n' +
    '\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 text-right nopadding btn-area">\n' +
    '            <button class="btn-primary-bg btn-primary-txt-clr btn-primary-border-clr btn btn-primary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 secondary" ng-disabled="(!selAccounts.length) && (!selAccounts.account)" ng-click="goPreview()"> {{\'NEXT\' | translate}}  </button>\n' +
    '            <button class="btn-secondary-bg btn-secondary-txt-clr btn-secondary-border-clr btn btn-secondary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 primary"> {{\'CANCEL\' | translate}} </button>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/cisp-review.html',
    '<section class="container main-container">\n' +
    '    <div class="row content-container">\n' +
    '		<div class="col-xs-12 col-sm-12 col-md-12 page-header">\n' +
    '			<h2 class="text-capitalize header-title-txt-clr">{{\'BANK_TITLE\' | translate}}</h2>\n' +
    '		</div>\n' +
    '	</div>\n' +
    '\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row shadow-container ">\n' +
    '                <h5 > {{ \'PRE_USER_TITLE_TEXT\' | translate }} {{ tppInfo }} {{ \'POST_USER_TITLE_TEXT_REVIEW\' | translate }}</h5>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    \n' +
    '    \n' +
    '    <div class="row content-container">\n' +
    '		<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '			<div class="row title-header acct-header-txt-clr acct-header-bg">\n' +
    '				<h2 class=""> {{\'SELECT_ACCOUNT_TITLE\' | translate}} </h2>\n' +
    '				</div>\n' +
    '			<div class="row shadow-container ">\n' +
    '				<div class="account-table hidden-xs">\n' +
    '					<table class="table table-responsive">\n' +
    '						<thead>\n' +
    '							<tr>\n' +
    '								<th scope="col"> {{\'NICK_NAME\' | translate}} </th>\n' +
    '                                <th scope="col"> {{\'ACCOUNT_NUMBER\' | translate}} </th>\n' +
    '                                <th scope="col"> {{\'CURRENCY\' | translate}} </th>\n' +
    '                                <th scope="col"> {{\'ACCOUNT_TYPE\' | translate}} </th>\n' +
    '						</tr>\n' +
    '						</thead>\n' +
    '						<tbody> \n' +
    '							<tr ng-repeat="acct in selectedAcctTableData ">\n' +
    '								<td>{{acct.nickname}}</td>\n' +
    '                                <td>{{acct.accountNumber}}</td>\n' +
    '                                <td>{{acct.currency}}</td>\n' +
    '                                <td>{{acct.accountType}}</td>\n' +
    '                        	</tr>\n' +
    '							</tbody>\n' +
    '					</table>\n' +
    '				</div>\n' +
    '                <div class="div-view-selected-account hidden-md hidden-lg hidden-sm">\n' +
    '						<div class="col-xs-12 acc-cont" ng-repeat="acct in selectedAcctTableData">\n' +
    '							<div class="col-xs-12">\n' +
    '								<p> {{\'NICK_NAME\' | translate}} : {{acct.nickname}} </p>\n' +
    '                                <p> {{\'ACCOUNT_NUMBER\' | translate}} : {{acct.accountNumber}} </p>\n' +
    '                                <p> {{\'CURRENCY\' | translate}} : {{acct.currency}} </p>\n' +
    '                                <p> {{\'ACCOUNT_TYPE\' | translate}} : {{acct.accountType}} </p>\n' +
    '							</div>\n' +
    '\n' +
    '						</div>\n' +
    '					</div>\n' +
    '			</div>\n' +
    '		</div>\n' +
    '	</div>\n' +
    '\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '        	<div class="row title-header acct-header-txt-clr acct-header-bg">\n' +
    '				<h2 class=""> {{\'PERMISSION_TITLE\' | translate}} </h2>\n' +
    '			</div>\n' +
    '            <div class="row shadow-container hidden-xs">\n' +
    '                <ul type="square" class="permission-list">\n' +
    '                    <li ng-repeat="pl in permissionsList"> \n' +
    '                        <h3 class="consent-header"> {{pl | uppercase | translate}} </h3>\n' +
    '                        <p> {{pl+\'_ELEMENTS\' | uppercase | translate}} </p>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '                <div class="shadow-container hidden-md hidden-lg hidden-sm">\n' +
    '                <div class="permission-small-device-view">\n' +
    '                    <div class="panel-group" id="accordion">\n' +
    '                        <div class="panel panel-default" ng-repeat="pl in permissionsList">\n' +
    '                        <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapse{{$index+1}}">\n' +
    '                            <h4 class="panel-title accordion-toggle">\n' +
    '                            <a href=""> {{pl | uppercase | translate}} </a>\n' +
    '                            </h4>\n' +
    '                        </div>\n' +
    '                        <div id="collapse{{$index+1}}" class="panel-collapse collapse">\n' +
    '                            <div class="panel-body">{{pl+\'_ELEMENTS\' | uppercase | translate}} </div>\n' +
    '                        </div>\n' +
    '                        </div>  \n' +
    '                    </div>                \n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    ' \n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row shadow-container center-align main-header validity">\n' +
    '                <h5>{{\'PRE_TILL_DATE_TEXT\' | translate}} {{ tillDate | date }} {{\'POST_TILL_DATE_TEXT\' | translate}}</h5>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="row content-container" ng-if="errorMsg">\n' +
    '            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '                <div class="row shadow-container center-align main-header validity error-container">\n' +
    '                   <h3 class="text-capitalize text-danger ng-binding" style="font-weight: bold;">176: Rest Transport Adaptor Technical Error</h3>\n' +
    '                    <h5 class="text-capitalize text-danger ng-binding ng-scope"> Your error refernce number is  <strong class="ng-binding"> 026734a0-11fO-4573-8a3b-64a19bb76d51 </strong></h5> \n' +
    '	                <!-- <a ng-href="http://www.google.com" class="go-back pull-right" href="http://www.google.com"><strong class="text-capitalize ng-binding"> Go back to <u class="ng-binding">Moneywise</u></strong></a> -->\n' +
    '                </div>\n' +
    '            </div>\n' +
    '     </div> \n' +
    '\n' +
    '    <div class="row content-container">\n' +
    '     <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 term-cond-block">\n' +
    '            <div class="checkbox">\n' +
    '            <input type="checkbox" id="accpt_cond" name="termsncondition" ng-model="termsConditn"> <label for="accpt_cond" >{{\'I_ACCEPT\' | translate}}</label> <u class="cursr-pointer">{{\'TERM_CONDITION\' | translate}}</u>\n' +
    '        </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 text-right nopadding btn-area">\n' +
    '             <button class="btn-primary-bg btn-primary-txt-clr btn-primary-border-clr btn btn-primary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 primary" ng-disabled="!termsConditn" ng-click="allowSubmission()"> {{\'ALLOW_BUTTON\' | translate}} </button>\n' +
    '             <button class="btn-secondary-bg btn-secondary-txt-clr btn-secondary-border-clr btn btn-primary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 secondary"> {{\'DONOT_ALLOW_BUTTON\' | translate}}  </button>\n' +
    '             <button class="btn-secondary-bg btn-secondary-txt-clr btn-secondary-border-clr btn btn-primary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 secondary" ng-click="back()"> {{\'BACK_BUTTON\' | translate}}  </button>\n' +
    '        </div>\n' +
    '     </div>\n' +
    '</section>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/customUibAccordian.html',
    '<uib-accordion>\n' +
    '    <div uib-accordion-group class="permission-data" is-open="status.open"  ng-keypress="toggleDisable($event)">\n' +
    '       <uib-accordion-heading >\n' +
    '              <i class="fa" ng-class="{\'fa-minus\': status.open, \'fa-plus\': !status.open}"></i>\n' +
    '          <span  ng-attr-aria-expanded="{{status.open?\'true\':\'false\'}}" class="consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.\'+pl  | uppercase)|translate)" ng-bind="perm_list">  </span>\n' +
    '          <!-- <i class="pull-right fa" ng-class="{\'fa-chevron-up\': status.open, \'fa-chevron-down\': !status.open}"></i> -->\n' +
    '       </uib-accordion-heading>\n' +
    '       <span   class="hidden-md hidden-lg hidden-sm boi-input-sm"  ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+pl+\'_ELEMENTS\' | uppercase)|translate)" ng-bind="pl_elm"></span>\n' +
    '       <span id="collapsAccrd" class="hidden-xs boi-input-placeholder" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+pl+\'_ELEMENTS\' | uppercase)|translate)" ng-bind="pl_elm"></span>\n' +
    '    </div>\n' +
    ' </uib-accordion>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/error-notice.html',
    '<div class="alert page-alert" role="alert" aria-atomic="true" ng-class="errorCssClass">\n' +
    '    <p>\n' +
    '        <span role="alert" class="alert-prefix" ng-bind="alertText"></span>\n' +
    '        <span role="alert" class="alert-suffix" ng-bind-html="errorMsgText"></span>\n' +
    '        <ng-template ng-if="errorData.correlationId">\n' +
    '            <span class="alert-prefix" ng-bind="corRelationText"> </span>\n' +
    '            <span class="alert-suffix" ng-bind="corRelationMsgText"></span>\n' +
    '        </ng-template>\n' +
    '    </p>\n' +
    '    <p ng-if="isErrorMsgTextDescription" ng-bind-html="errorMsgTextDescription"> </p>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/loading-spinner.html',
    '<div class="block-ui-overlay"></div>\n' +
    '<div class="block-ui-message-container" aria-live="assertive" aria-atomic="true">\n' +
    '	<div class="block-ui-message" ng-class="$_blockUiMessageClass">\n' +
    '		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
    '	</div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/modalPopUp.html',
    '<div ng-if="modal.modelpopupType == \'cancelpopup\'">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="modal.cancelRequestScrLabel"></span>\n' +
    '            <!-- <button type="button " class="close " aria-hidden="true "  ng-click="sessionOutClicked() ">&times;</button>-->\n' +
    '            <span class="boi-widget" id="modal_title" ng-bind="modal.modelpopupTitle"></span>\n' +
    '            <a tabindex="0" type="button" role="button" class="btn-close pull-right" ng-click="cancelBtnClicked()">\n' +
    '                    <span class="sr-only" ng-bind="modal.closebtn"></span>\n' +
    '                    <i class="fa fa-close" aria-hidden="true" title="{{modal.closebtn}}"></i></a>\n' +
    '        </div>\n' +
    '\n' +
    '\n' +
    '        <div class="modal-body pop-confirm-body">\n' +
    '            <div class="confirm-msg">\n' +
    '                <p class="text-center" ng-bind="modal.modelpopupBodyContent"> </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '            <div class="text-center">\n' +
    '                <button role="button" ng-show="modal.btn.okbtn.visible" class="btn btn-primary pull-right" ng-click="okBtnClicked()" ng-bind=" modal.btn.okbtn.label" autofocus></button>\n' +
    '                <button role="button" ng-show="modal.btn.cancelbtn.visible" class="btn btn-secondary pull-left"  ng-click="cancelBtnClicked()"\n' +
    '                    data-dismiss="modal">\n' +
    '                    <span ng-bind="modal.btn.cancelbtn.label"></span>\n' +
    '                    <span class="sr-only" aria-label="{{modal.noBtnScrLabel}}"></span>\n' +
    '                        </button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '\n' +
    '<div class="session-timeout" ng-if="modal.modelpopupType == \'sessionTimeOutpopup\'">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="modal.sessionTimeoutScrLabel"></span>\n' +
    '            <!-- <button type="button " class="close " aria-hidden="true "  ng-click="sessionOutClicked() ">&times;</button>-->\n' +
    '            <h2 class="boi-widget" id="modal_title" ng-bind=" modal.modelpopupTitle"> </h2>\n' +
    '        </div>\n' +
    '        <div class="modal-body pop-confirm-body">\n' +
    '            <div class="confirm-msg">\n' +
    '                <p class="text-center" ng-bind="modal.modelpopupBodyContent"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '            <div class="text-center">\n' +
    '                <button role="button" ng-show="modal.btn.okbtn.visible" class="btn btn-primary pull-right" ng-click="okBtnClicked()" ng-bind="modal.btn.okbtn.label" autofocus> </button>\n' +
    '                <button role="button" ng-show="modal.btn.cancelbtn.visible" class="btn btn-secondary pull-left"  ng-click="cancelBtnClicked()"\n' +
    '                    data-dismiss="modal"> \n' +
    '                    <span ng-bind="modal.btn.cancelbtn.label"></span>\n' +
    '                    <span class="sr-only" aria-label="{{modal.noBtnScrLabel}}"></span>\n' +
    '                </button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/pagination.html',
    '<li role="menuitem" ng-if="::boundaryLinks" ng-class="{boi_widget_sm: noPrevious()||ngDisabled}" class="pagination-first">\n' +
    '    <a role="button" href="/first" ng-click="selectPage(1, $event)" ng-disabled="noPrevious()||ngDisabled" uib-tabindex-toggle>{{::getText(\'first\')}}</a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-if="::directionLinks" ng-class="{boi_widget_sm_disable: noPrevious()||ngDisabled,boi_widget_sm_active: !noPrevious()&&!ngDisabled}"\n' +
    '    class="pagination-prev">\n' +
    '    <a role="button" href="/previous" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()||ngDisabled" aria-label="Previous"\n' +
    '        uib-tabindex-toggle>\n' +
    '        <i class="fa fa-chevron-left" aria-hidden="true"></i>\n' +
    '    </a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-repeat="page in pages track by $index" ng-class="{boi_widget_sm_blue: page.active,boi_widget_sm: !ngDisabled&&!page.active}"\n' +
    '    class="pagination-page">\n' +
    '    <a role="button" href="/selectpage" ng-click="selectPage(page.number, $event)" ng-disabled="ngDisabled&&!page.active" id="{{page.text}}"\n' +
    '        uib-tabindex-toggle ng-bind="page.text"></a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-if="::directionLinks" ng-class="{boi_widget_sm_disable: noNext()||ngDisabled,boi_widget_sm_active: !noNext()&&!ngDisabled}"\n' +
    '    class="pagination-next">\n' +
    '    <a role="button" href="/next" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()||ngDisabled" aria-label="Next"\n' +
    '        uib-tabindex-toggle>\n' +
    '        <i class="fa fa-chevron-right" aria-hidden="true"></i>\n' +
    '    </a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-if="::boundaryLinks" ng-class="{boi_widget_sm: noNext()||ngDisabled}" class="pagination-last">\n' +
    '    <a role="button" href="/last" ng-click="selectPage(totalPages, $event)" ng-disabled="noNext()||ngDisabled" uib-tabindex-toggle>{{::getText(\'last\')}}</a>\n' +
    '</li>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/pisp-account.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container pisp-section" ng-hide="sessiontimeoutflag">\n' +
    '     <!-- page title STARTS--> \n' +
    '    <div class="row">\n' +
    '      <div class="col-md-10 col-md-offset-1">\n' +
    '         <h3 ng-bind="accountSelText"></h3>\n' +
    '      </div>\n' +
    '   </div>\n' +
    '   <!-- page title ENDS-->\n' +
    '   <!-- page description STARTS-->\n' +
    '    <div class="row" ng-hide="isDataFound">\n' +
    '        <div class="col-md-10 col-md-offset-1 payment-dis">\n' +
    '                <h4 class="main-header page-description" ng-bind="paymentCheckText"></h4>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page description ENDS-->\n' +
    '      <!-- Payee details table starts-->\n' +
    '    <div class="row content-container" ng-hide="isDataFound">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 ng-bind="paymentInfoText"> </h4>\n' +
    '            </div>\n' +
    '            <div class="row shadow-container card-container card-detail-viewer">\n' +
    '                <table>\n' +
    '                    <caption class="sr-only" ng-bind="paymentInfoText"></caption>\n' +
    '                    <tr ng-hide="isDataFound">\n' +
    '                        <th ng-bind="paymtIntByText"></th>\n' +
    '                        <td ng-bind="payInstBy"></td>\n' +
    '                    </tr>\n' +
    '                    <tr ng-hide="isDataFound">\n' +
    '                        <th ng-bind="payNameText"></th>\n' +
    '                        <td ng-bind="payeeName"></td>\n' +
    '                    </tr>\n' +
    '                    <tr ng-hide="isDataFound">\n' +
    '                        <th ng-bind="PayAmountText"></th>\n' +
    '                        <td ng-bind="amount | isoCurrency:currency "></td>\n' +
    '                    </tr>\n' +
    '                    <tr ng-hide="isDataFound">\n' +
    '                        <th ng-bind="payRefText"></th>\n' +
    '                        <td ng-bind="payeeReference"></td>\n' +
    '                    </tr>\n' +
    '                    <tr ng-hide="isDataFound">\n' +
    '                        <th ng-bind="payFromText"></th>\n' +
    '                        <td>\n' +
    '                            <div class="dropdown-button">\n' +
    '                                <div class="btn-group" role="application" uib-dropdown keyboard-nav>\n' +
    '                                    <button type="button" class="btn btn-default text-center" aria-live="assertive" ng-disabled="{{accountSelected}}" uib-dropdown-toggle aria-haspopup="true" aria-expanded="false" ng-model="selectedAcctObject" ng-bind="selectedAcctObject?(selectedAcctObject.Nickname)+(selectedAcctObject.Account.Identification | maskNumber:maskAccountNumberLength):selectAccountText">\n' +
    '                                    </button>\n' +
    '                                    <i class="fa fa-chevron-down"></i>\n' +
    '                                    <ul role="menu" class="dropdown-menu dropdown-menu-data dropdown scrollable-menu" uib-dropdown-menu ng-hide="accountSelected">\n' +
    '                                        <li  ng-repeat="acct in accData" value="{{acct.HashedValue}}">\n' +
    '                                            <a role="menuitem" href="#" class="dropdown-menu-anchor" ng-click="selectAccount(acct,$event);" ng-bind="(acct.Nickname) + (acct.Account.Identification | maskNumber:maskAccountNumberLength)"> </a>\n' +
    '                                        </li>\n' +
    '                                    </ul>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </table>\n' +
    '\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- Payee details table ENDS-->\n' +
    '    <!-- page errors STARTS-->\n' +
    '    <div class="row content-container" ng-if="errorData">\n' +
    '        <div class="col-xs-10 col-sm-10 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page errors ENDS-->\n' +
    '    <!-- button section STARTS-->\n' +
    '    <div class="row content-container mobile-section-btn">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <button ng-hide="isDataFound" class="btn btn-primary pull-right" ng-disabled="stopCnfirm" ng-click="confirm()" ng-bind="contBtn" aria-controls="LiveRegion"> </button>\n' +
    '            <a ng-if="retry" ng-href="{{retry}}" class="btn btn-primary pull-right" ng-bind="rtrBtn"> </a>\n' +
    '            <button class="btn btn-secondary pull-left" ng-click="openCancelModal()" aria-label="{{\'PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL\'| translate}}" ng-bind="cancelBtn"> </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- button section ENDS-->\n' +
    '\n' +
    '</section>\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<page-footer></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/pisp-review.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container pisp-section" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <h3 ng-init="review_consent=(\'PISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.REVIEW_CONFIRM_HEADER\' |translate)" ng-bind="review_consent"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '    <!-- page description STARTS-->\n' +
    '    <div class="row" ng-hide="isDataFound">\n' +
    '        <div class="col-md-10 col-md-offset-1 payment-dis">\n' +
    '            <h4 class="main-header page-description" ng-init="consent_review_provided_label = (\'PISP.REVIEW_CONFIRM_PAGE.PAGE_INSTRUCTION.CONSENT_REVIEW_PROVIDED_LABEL\' | translate)"\n' +
    '                ng-bind="consent_review_provided_label"></h4>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page description ENDS-->\n' +
    '    <!-- Payee details table STARTS-->\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-md-10 col-md-offset-1">\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 ng-init="pay_info=(\'PISP.REVIEW_CONFIRM_PAGE.PAYEE_TABLE.PAYEE_DETAILS_HEADER\' |translate)" ng-bind="pay_info"> </h4>\n' +
    '            </div>\n' +
    '            <div class="row shadow-container card-container card-detail-viewer">\n' +
    '                <table>\n' +
    '                    <caption class="sr-only" ng-bind="pay_info"></caption>\n' +
    '                    <tr>\n' +
    '                        <th ng-init="paymt_int_by = (\'PISP.REVIEW_CONFIRM_PAGE.PAYEE_TABLE.PAYMENT_INITIATED_BY_LABEL\' | translate)" ng-bind="paymt_int_by"></th>\n' +
    '                        <td ng-bind="payInstBy"></td>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <th ng-init="paye_name = (\'PISP.REVIEW_CONFIRM_PAGE.PAYEE_TABLE.PAYEE_NAME_LABEL\' | translate)" ng-bind="paye_name"></th>\n' +
    '                        <td ng-bind="payeeName"></td>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <th ng-init="AMOUNT = (\'PISP.REVIEW_CONFIRM_PAGE.PAYEE_TABLE.AMOUNT_LABEL\' | translate)" ng-bind="AMOUNT"></th>\n' +
    '                        <td ng-bind="amount | isoCurrency:currency"></td>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <th ng-init="payref = (\'PISP.REVIEW_CONFIRM_PAGE.PAYEE_TABLE.PAYEE_REFERENCE_LABEL\' | translate)" ng-bind="payref"></th>\n' +
    '                        <td ng-bind="payeeRef"></td>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <th ng-init="payfrm = (\'PISP.REVIEW_CONFIRM_PAGE.PAYEE_TABLE.PAY_FROM_LABEL\' | translate)" ng-bind="payfrm"></th>\n' +
    '                        <td ng-bind="(nickName)+(acountNumber | maskNumber:maskAccountNumberLength)"></td>\n' +
    '                    </tr>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- Payee details table STARTS-->\n' +
    '    <!-- page errors STARTS-->\n' +
    '    <div class="row content-container" ng-if="errorData">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page errors ENDS-->\n' +
    '\n' +
    '    <!-- confirm authorisation STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1 payment-dis">\n' +
    '            <div class="confirm-section box-container" ng-class="{selectedConfirmSection : termsConditn}">\n' +
    '                <div class="checkbox">\n' +
    '                    <label for="accpt_cond" class="boi-input" ng-init="accept_req=(\'PISP.REVIEW_CONFIRM_PAGE.ACCEPT_PAYMENT_REQUEST_LABEL\'| translate)"\n' +
    '                        ng-bind="accept_req"></label>\n' +
    '                    <input role="checkbox" type="checkbox" id="accpt_cond" name="termsncondition" ng-model="termsConditn">\n' +
    '                    <span></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- confirm authorisation ENDS-->\n' +
    '    <!-- button section STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1 payment-dis">\n' +
    '            <button role="button" class="btn btn-primary pull-right" ng-disabled="!termsConditn" ng-click="createPayment()" ng-init="CONFIRM_BUTTON = (\'PISP.REVIEW_CONFIRM_PAGE.BUTTONS.PAY_BUTTON_LABEL\' | translate)"\n' +
    '                ng-bind="CONFIRM_BUTTON" ng-attr-aria-label="{{termsConditn?confirmBtnScrLabel:\'\'}}"> </button>\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="backToAccountSelectPage()" aria-control="LiveRegion"\n' +
    '                ng-init="BACK_BUTTON = (\'PISP.REVIEW_CONFIRM_PAGE.BUTTONS.BACK_BUTTON_LABEL\' | translate)" ng-bind="BACK_BUTTON">\n' +
    '            </button>\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="openCancelModal()" aria-label="{{\'PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL\'| translate}}"\n' +
    '                ng-init="CANCEL_BUTTON = (\'PISP.REVIEW_CONFIRM_PAGE.BUTTONS.CANCEL_BUTTON_LABEL\' | translate)" ng-bind="CANCEL_BUTTON">\n' +
    '            </button>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- button section ENDS-->\n' +
    '\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1 horizontal-line"> </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <!-- important info section STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-10 col-md-offset-1 info-section">\n' +
    '            <div class="box-container">\n' +
    '                <div class="help-list" ng-repeat="idata in infoData" ng-class="{\'help-list-with-title\' : idata.title}">\n' +
    '                    <h5 class="boi-label-semibold" ng-bind="idata.title" ng-if="idata.title"></h5>\n' +
    '                    <p class="boi-input-sm-light marg-top8" ng-bind-html="idata.desc" ng-if="idata.desc"></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '<!-- important info section ENDS-->\n' +
    '</section>\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<page-footer>\n' +
    '</page-footer>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/terms-condition-popup.html',
    '<div class="modal-header pop-confirm-header">\n' +
    '    {{\'TERM_CONDITION\' | translate}}\n' +
    '</div>\n' +
    '<div class="modal-body pop-confirm-body">\n' +
    '    <div class="confirm-msg">\n' +
    '        <P translate="TERM_CONDITION_DETAILS.0"></P>\n' +
    '        <br/>\n' +
    '        <P translate="TERM_CONDITION_DETAILS.1"></P>\n' +
    '        <br/>\n' +
    '        <P translate="TERM_CONDITION_DETAILS.2"></P>\n' +
    '    </div>\n' +
    '\n' +
    '</div>\n' +
    '<div class="modal-footer pop-confirm-footer">\n' +
    '    <div class=" text-center">\n' +
    '        <button ng-click="cancel()" data-dismiss="modal" class="btn-primary-bg btn-primary-txt-clr btn-primary-border-clr btn btn-primary text-upper-case primary" type="button">\n' +
    '			{{\'OK_BUTTON\' | translate}}\n' +
    '		</button>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();
