/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.AccountBalanceFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.client.AccountBalanceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate.AccountBalanceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Balance;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;


/**
 * The Class AccountBalanceFoundationServiceAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceAdapterTest {
	
	/** The account balance foundation service adapter. */
	@InjectMocks
	private AccountBalanceFoundationServiceAdapter accountBalanceFoundationServiceAdapter = new AccountBalanceFoundationServiceAdapter();
	
	/** The account balance foundation service client. */
	@Mock
	private AccountBalanceFoundationServiceClient accountBalanceFoundationServiceClient;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/** The account balance foundation service delegate. */
	@Mock
	private AccountBalanceFoundationServiceDelegate accountBalanceFoundationServiceDelegate;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account balance FS.
	 */
	@Test
	public void testAccountBalanceFS() {
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(1000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(1000.00d));
		Accnt accnt = new Accnt();
		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt acc= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		filteredAccounts.getAccount().add(acc);	
		Accounts accounts = new Accounts();
		
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(anyObject(), anyObject())).thenReturn(accountMapping);
		
		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(any(), any(), any())).thenReturn(accounts);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new BalancesGETResponse());
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		
		ReflectionTestUtils.setField(accountBalanceFoundationServiceAdapter, "consentFlowType", "AISP");
		BalancesGETResponse res = accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String,String>());
		assertNotNull(res);
	}
	
	
	
//	/**
//	 * Test create request headers.

	@Test
	public void testCreateRequestHeaders() {
	
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
	httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
	httpHeaders.add("X-CORRELATION-ID", "header correlation Id");

		RequestInfo requestInfo = new RequestInfo();
	requestInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		Mockito.when(accountBalanceFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyMap())).thenReturn(httpHeaders);
		Map<String,String> params = new HashMap<>();
		params.put("channelId","channel123");
		httpHeaders = accountBalanceFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping,params);

		assertNotNull(httpHeaders);
	}
	



//	 * Test exception.
//	 */
@Test(expected=AdapterException.class)
	public void testExceptionFilteredAccounts(){

		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}
	
	@Test(expected=AdapterException.class)
	public void testExceptionNullCheckUtils(){
		
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
		
	}
	
	@Test(expected=AdapterException.class)
	public void testExceptionNullParams(){
		
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
		
	}
	
	@Test(expected=AdapterException.class)
	public void testExceptionNullCheckUtil(){
		
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId(null);
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
		
	}
	
	@Test(expected=AdapterException.class)
	public void testExceptionNullAccountMappingPsu(){
		
		AccountMapping accountMapping = null;
		Map<String, String> params = new HashMap<>();
		
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
		
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(),anyObject(),anyObject())).thenReturn(null);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String,String>());
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(),anyObject(),anyObject())).thenReturn(null);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String,String>());
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingElse() {
	AccountMapping accountMapping = new AccountMapping();
	AccountDetails accDet = new AccountDetails();
	List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	accDet.setAccountId("12345");
	accDet.setAccountNSC("nsc1234");
	accDet.setAccountNumber("acct1234");
	accDetList.add(accDet);
	accountMapping.setAccountDetails(null);
	accountMapping.setTppCID("test");
	accountMapping.setPsuId("test");
	com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
	com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
	accntFilter.setAccountNumber("US2345");
	accntFilter.setAccountNSC("1234");
	filteredAccounts.getAccount().add(accntFilter);
	Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(),anyObject(),anyObject())).thenReturn(null);
	Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
	Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(anyObject(),anyObject(),anyObject())).thenReturn(null);
	accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String,String>());
	}
/**
	 * Test retrieve account balance null check.
	 */
	@Test(expected=AdapterException.class)
	public void testRetrieveAccountBalanceNullCheck(){
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(1000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(1000.00d));
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(any(), any(), any())).thenReturn(null);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		Mockito.when(accountBalanceFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyMap())).thenReturn(httpHeaders);
		Map<String,String> params = new HashMap<>();
		params.put("channelId","channel123");
		httpHeaders = accountBalanceFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping,params);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String,String>());
	}
}
