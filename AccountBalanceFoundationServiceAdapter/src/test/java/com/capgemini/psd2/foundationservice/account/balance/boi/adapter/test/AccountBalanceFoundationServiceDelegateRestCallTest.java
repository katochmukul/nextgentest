/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.Balance.TypeEnum;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Data5;
//import com.capgemini.psd2.aisp.domain.BalancesGETResponseAmount;
import com.capgemini.psd2.aisp.domain.Data5Amount;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.client.AccountBalanceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate.AccountBalanceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Balance;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountBalanceFoundationServiceDelegateRestCallTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceDelegateRestCallTest {

	/** The delegate. */
	@InjectMocks
	private AccountBalanceFoundationServiceDelegate delegate;
	
	/** The account balance foundation service client. */
	@InjectMocks
	private AccountBalanceFoundationServiceClient accountBalanceFoundationServiceClient;
	
	
	/** The account balance FS transformer. */
	@Mock
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test rest transport for single account balance.
	 */
	@Test
	public void testRestTransportForSingleAccountBalance() {
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(5000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		
		Accounts res = accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(requestInfo,Accounts.class,httpHeaders);
		assertNotNull("Null object returned for getting single account balance",res);
		assertEquals("Available balance is not 5000", new BigDecimal(5000.00d), res.getAccount().get(0).getBalance().getAvailableBalance());
	}

	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {
		
		String accountNSC = "nsc";
		String accountNumber = "number"; 
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt/accounts";
		String finalURL = "http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number";
		
		String testURL = delegate.getFoundationServiceURL(accountNSC,accountNumber,baseURL);
		
		assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}
	
	/**
	 * Test get foundation service with invalid URL.
	 */
	@Test
	public void testGetFoundationServiceWithInvalidURL() {
		
		String accountNSC = "nsc";
		String accountNumber = "number"; 
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		String finalURL = "http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number";
		
		String testURL = delegate.getFoundationServiceURL(accountNSC,accountNumber,baseURL);
		
		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}
	
	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {
		
		String accountNSC = null;
		String accountNumber = "number"; 
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		
		delegate.getFoundationServiceURL(accountNSC,accountNumber,baseURL);
		
		fail("Invalid account NSC");
	}
	
	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {
		
		String accountNSC = "nsc";
		String accountNumber = null; 
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		
		delegate.getFoundationServiceURL(accountNSC,accountNumber,baseURL);
		
		fail("Invalid Account Number");
	}
	
	/**
	 * Test get foundation service with base URL as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {
		
		String accountNSC = "nsc";
		String accountNumber = "number"; 
		String baseURL = null;
		
		delegate.getFoundationServiceURL(accountNSC,accountNumber,baseURL);
		
		fail("Invalid base URL");
	}
	
	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		Data5 data5 = new Data5();
		List<com.capgemini.psd2.aisp.domain.Balance> balanceList = new ArrayList<>();
		data5.setBalance(balanceList);
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(5000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);
		
		BalancesGETResponse finalGBResponseObj = new BalancesGETResponse();
		finalGBResponseObj.setData(data5);
		Data5Amount amount = new Data5Amount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		com.capgemini.psd2.aisp.domain.Balance responseDataObj = new com.capgemini.psd2.aisp.domain.Balance();
		responseDataObj.setAccountId(params.get("accountId"));
		responseDataObj.type(TypeEnum.CLOSINGBOOKED);
		responseDataObj.setAmount(amount);
		finalGBResponseObj.getData().getBalance().add(responseDataObj);
		Mockito.when(accountBalanceFSTransformer.transformAccountBalance(anyObject(), anyObject())).thenReturn(finalGBResponseObj);
		
		BalancesGETResponse balancesGETResponse = delegate.transformResponseFromFDToAPI(accounts, new HashMap<String, String>());
		assertNotNull(balancesGETResponse);
	}
	
	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {
		
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "correlationReqHeader", "X-CORRELATION-ID");
		Map<String,String> params = new HashMap<>();
		params.put("channelId","channel123");
		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), accountMapping,params);

		assertNotNull(httpHeaders);
	}
	
}
