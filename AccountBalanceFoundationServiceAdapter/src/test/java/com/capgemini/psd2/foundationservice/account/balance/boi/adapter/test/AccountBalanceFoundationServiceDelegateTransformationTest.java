/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doNothing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Balance.TypeEnum;
import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Data5;
//import com.capgemini.psd2.aisp.domain.BalancesGETResponseAmount;
import com.capgemini.psd2.aisp.domain.Data5Amount;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Balance;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountBalanceFoundationServiceDelegateTransformationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceDelegateTransformationTest {
	
	/** The account balance FS transformer. */
	@InjectMocks
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer=new AccountBalanceFoundationServiceTransformer();
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/** The psd 2 validator. */
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		Data5 data5 = new Data5();
		List<com.capgemini.psd2.aisp.domain.Balance> balanceList = new ArrayList<>();
		data5.setBalance(balanceList);
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(5000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);
		 
		BalancesGETResponse finalGBResponseObj = new BalancesGETResponse();
		finalGBResponseObj.setData(data5);
		Data5Amount amount = new Data5Amount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		com.capgemini.psd2.aisp.domain.Balance responseDataObj = new com.capgemini.psd2.aisp.domain.Balance();
		responseDataObj.setAccountId(params.get("accountId"));
		responseDataObj.type(TypeEnum.CLOSINGBOOKED);
		responseDataObj.setAmount(amount);
		finalGBResponseObj.getData().getBalance().add(responseDataObj);
		doNothing().when(psd2Validator).validate(anyObject());
		BalancesGETResponse res = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		assertNotNull(res);
//		assertEquals("Test for transformation failed.","5000.0", res.getData().get(0).getAmount().getAmount());
	}
	
}
