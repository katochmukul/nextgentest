/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.client.AccountBalanceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate.AccountBalanceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountBalanceFoundationServiceAdapter.
 */
@Component
public class AccountBalanceFoundationServiceAdapter implements AccountBalanceAdapter {

	/** The single account balance base URL. */
	@Value("${foundationService.singleAccountBalanceBaseURL}")
	private String singleAccountBalanceBaseURL;
	
	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	/** The account balance foundation service delegate. */
	@Autowired
	private AccountBalanceFoundationServiceDelegate accountBalanceFoundationServiceDelegate;

	/** The account balance foundation service client. */
	@Autowired
	private AccountBalanceFoundationServiceClient accountBalanceFoundationServiceClient;
	
	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	
	@Override
	public BalancesGETResponse retrieveAccountBalance(AccountMapping accountMapping, Map<String, String> params) {
		
		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		} 

		if(params == null)
		{
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
	    params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null || filteredAccounts.getAccount().isEmpty()) {
            throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		} 
		adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);
		
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountBalanceFoundationServiceDelegate.createRequestHeaders(requestInfo,
				accountMapping, params);

		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null
				&& !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);

		String finalURL = accountBalanceFoundationServiceDelegate.getFoundationServiceURL(
				accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), singleAccountBalanceBaseURL);
		requestInfo.setUrl(finalURL);

		Accounts accounts = accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(requestInfo,
				Accounts.class, httpHeaders);
		if (NullCheckUtils.isNullOrEmpty(accounts)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
		}
		return accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(accounts, params);

	}

}
