/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountBalanceFoundationServiceDelegate.
 */
@Component
public class AccountBalanceFoundationServiceDelegate {
	
	/** The account balance FS transformer. */
	@Autowired
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer;
	
	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-WORKSTATION}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-BOI-WORKSTATION}}")
	private String correlationReqHeader;
	
	/** The platform. */
	@Value("${app.platform}")
	private String platform;

	
	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC the account NSC
	 * @param accountNumber the account number
	 * @param baseURL the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String accountNSC, String accountNumber, String baseURL){
		if(NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNSC + "/" +accountNumber;
	}
	
	/**
	 * Transform response from FD to API.
	 *
	 * @param accounts the accounts
	 * @param params the params
	 * @return the balances GET response
	 */
	public BalancesGETResponse transformResponseFromFDToAPI(Accounts accounts, Map<String, String> params){
		return accountBalanceFSTransformer.transformAccountBalance(accounts, params);
	}
	
	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo the request info
	 * @param accountMapping the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader, params.get(AccountBalanceFoundationServiceConstants.CHANNEL_ID));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationReqHeader, accountMapping.getCorrelationId());
		return httpHeaders;
	}


}
