package com.capgemini.psd2.pisp.payment.submission.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl.PaymentAuthorizationValidationRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing.PaymentSubmissionAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentAuthorizationValidationRoutingAdapterImplTest {

	@InjectMocks
	PaymentAuthorizationValidationRoutingAdapterImpl paymentAuthorizationValidationRoutingAdapterImpl;
	
	@Mock
	private PaymentSubmissionAdapterFactory factory;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testPreAuthorizationPaymentValidation() {
		ReflectionTestUtils.setField(paymentAuthorizationValidationRoutingAdapterImpl, "paymentAuthorizationValidationAdapter", "test");
		PaymentAuthorizationValidationAdapterMock paymentAuthorizationValidationAdapterMock = new PaymentAuthorizationValidationAdapterMock();
		when(factory.getPaymentAuthorizationValidationAdapterInstance(anyObject())).thenReturn(paymentAuthorizationValidationAdapterMock);
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		Map<String, String> params = new HashMap<>();
		PaymentSetupValidationResponse setupValidationResponse = paymentAuthorizationValidationRoutingAdapterImpl.preAuthorizationPaymentValidation(paymentSetupPOSTResponse, params);
		assertNotNull(setupValidationResponse);
	}
}
