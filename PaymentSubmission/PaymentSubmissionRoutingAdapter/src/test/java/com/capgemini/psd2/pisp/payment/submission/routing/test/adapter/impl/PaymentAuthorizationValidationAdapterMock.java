package com.capgemini.psd2.pisp.payment.submission.routing.test.adapter.impl;

import java.util.Map;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public class PaymentAuthorizationValidationAdapterMock implements PaymentAuthorizationValidationAdapter{
	
	
	
	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(
			CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {
		return new PaymentSetupValidationResponse();
	}
	
	

}
