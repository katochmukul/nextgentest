package com.capgemini.psd2.pisp.payment.submission.routing.test.adapter.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl.PaymentAuthorizationValidationRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl.PaymentSubmissionExecutionRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing.PaymentSubmissionCoreSystemAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionCoreSystemAdapterFactoryTest {
	
	@InjectMocks
	PaymentSubmissionCoreSystemAdapterFactory paymentSubmissionCoreSystemAdapterFactory;
	
	@Mock
	ApplicationContext applicationContext;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testGetPaymentSubmissionExecutionAdapterInstance() {
		PaymentSubmissionExecutionRoutingAdapterImpl adapterImpl = new PaymentSubmissionExecutionRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapterImpl);
		
		PaymentSubmissionExecutionAdapter adapter = paymentSubmissionCoreSystemAdapterFactory.getPaymentSubmissionExecutionAdapterInstance("test");
		assertNotNull(adapter);
	}
	
	@Test
	public void testGetPaymentAuthorizationValidationAdapterInstance() {
		PaymentAuthorizationValidationRoutingAdapterImpl adapterImpl = new PaymentAuthorizationValidationRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapterImpl);
		PaymentAuthorizationValidationAdapter adapter = paymentSubmissionCoreSystemAdapterFactory.getPaymentAuthorizationValidationAdapterInstance("test");
		assertNotNull(adapter);
	}
	
	@Test
	public void testSetApplicationContext() {
		paymentSubmissionCoreSystemAdapterFactory.setApplicationContext(new ApplicationContextMock());
	}
	
}
