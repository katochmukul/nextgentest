package com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing.PaymentSubmissionAdapterFactory;

public class PaymentAuthorizationValidationRoutingAdapterImpl implements PaymentAuthorizationValidationAdapter{

	@Value("${app.paymentAuthorizationValidationAdapter}")
	private String paymentAuthorizationValidationAdapter;
	
	@Autowired
	private PaymentSubmissionAdapterFactory factory;
	
	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(
			CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {
		return getRoutingAdapter().preAuthorizationPaymentValidation(paymentSetupResponse, params);
	}

	private PaymentAuthorizationValidationAdapter getRoutingAdapter(){
		return factory.getPaymentAuthorizationValidationAdapterInstance(paymentAuthorizationValidationAdapter);
	}

}
