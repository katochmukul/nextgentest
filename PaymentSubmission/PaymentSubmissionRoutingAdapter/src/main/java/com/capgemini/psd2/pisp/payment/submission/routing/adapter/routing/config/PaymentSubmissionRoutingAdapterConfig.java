package com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl.PaymentAuthorizationValidationRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl.PaymentSubmissionExecutionRoutingAdapterImpl;

@Configuration
public class PaymentSubmissionRoutingAdapterConfig {

		
	@Bean(name = "paymentSubmissionExecutionAdapter")
	public PaymentSubmissionExecutionAdapter paymentSubmissionExecutionAdapter() {
		return new PaymentSubmissionExecutionRoutingAdapterImpl();
	}
	
	@Bean(name = "paymentAuthorizationValidationAdapter")
	public PaymentAuthorizationValidationAdapter paymentAuthorizationValidationAdapter(){
		return new PaymentAuthorizationValidationRoutingAdapterImpl();
	}
}
