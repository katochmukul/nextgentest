package com.capgemini.psd2.pisp.payment.submission.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.service.PaymentSubmissionService;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;

@RestController
public class PaymentSubmissionController {

	@Autowired
	private PaymentSubmissionService submissionService;
	@Autowired
	private PaymentValidator submissionValidator;
	
	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@RequestMapping(value="/payment-submissions",method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })	
	@ResponseBody
	public ResponseEntity<PaymentSubmitPOST201Response> createPaymentSubmissionResource(@RequestBody PaymentSubmissionPOSTRequest paymentSubmissionRequest){
		
			try{
				CustomPaymentSubmissionPOSTRequest customPaymentSubmissionRequest= new CustomPaymentSubmissionPOSTRequest();				
				customPaymentSubmissionRequest.setData(paymentSubmissionRequest.getData());
				customPaymentSubmissionRequest.setRisk(paymentSubmissionRequest.getRisk());
				submissionValidator.validatePaymentSubmissionRequest(customPaymentSubmissionRequest);
				PaymentSubmitPOST201Response submissionResponse = submissionService.createPaymentSubmissionResource(customPaymentSubmissionRequest);
				
				return new ResponseEntity<>(submissionResponse, HttpStatus.CREATED);
			}catch(PSD2Exception psd2Exception){	
				if(Boolean.valueOf(responseErrorMessageEnabled))
					throw psd2Exception;			
				return new ResponseEntity<>(HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));			
			}		
	}
}
