package com.capgemini.psd2.pisp.payment.submission.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.transformer.PaymentSubmissionResponseTransformer;

@Component
public class PaymentSubmissionResponseTransformerImpl implements PaymentSubmissionResponseTransformer{

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
		
	@Override
	public PaymentSubmitPOST201Response paymentSubmissionResponseTransformer(PaymentSubmissionPlatformResource paymentSubmissionPlatformResource) {
		
		PaymentSubmitPOST201Response paymentSubmissionResponse = new PaymentSubmitPOST201Response();
		PaymentSetupResponse1 paymentSetupResponse= new PaymentSetupResponse1();
		paymentSetupResponse.setPaymentSubmissionId(paymentSubmissionPlatformResource.getPaymentSubmissionId());
		paymentSetupResponse.setPaymentId(paymentSubmissionPlatformResource.getPaymentId());	
		paymentSetupResponse.setStatus(StatusEnum.fromValue(paymentSubmissionPlatformResource.getStatus()));
		paymentSetupResponse.setCreationDateTime(paymentSubmissionPlatformResource.getCreatedAt());
		paymentSubmissionResponse.setData(paymentSetupResponse);
		paymentSubmissionResponse.setLinks(populateLinks(paymentSubmissionPlatformResource.getPaymentSubmissionId()));
		paymentSubmissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		return paymentSubmissionResponse;
	}
	
	private PaymentSetupPOSTResponseLinks populateLinks(String paymentSubmissionId){
		PaymentSetupPOSTResponseLinks responseLink = new PaymentSetupPOSTResponseLinks();		
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(reqHeaderAtrributes.getSelfUrl());
		
		if(urlBuilder.toString().lastIndexOf('/') == 0)
			urlBuilder.append("/");
		
		urlBuilder.append(paymentSubmissionId);
		
		responseLink.setSelf(urlBuilder.toString());
		return responseLink;
	}

}
