package com.capgemini.psd2.pisp.payment.submission.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.controller.PaymentSubmissionController;
import com.capgemini.psd2.pisp.payment.submission.service.PaymentSubmissionService;
import com.capgemini.psd2.pisp.payment.submission.test.mock.data.PaymentSubmissionPOSTRequestResponseMockData;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionControllerTest {

	private MockMvc mockMvc;
	
	@InjectMocks
	PaymentSubmissionController paymentSubmissionController;
	
	@Mock
	private PaymentSubmissionService submissionService;
	
	@Mock
	private PaymentValidator submissionValidator;
	
	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(paymentSubmissionController).dispatchOptions(true).build();
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testCreatePaymentSubmissionResource() throws Exception{
		PaymentSubmitPOST201Response paymentSubmitPOST201Response = new PaymentSubmitPOST201Response();
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new PaymentSubmissionPOSTRequest();
		when(submissionValidator.validatePaymentSubmissionRequest(anyObject())).thenReturn(true);
		when(submissionService.createPaymentSubmissionResource(anyObject())).thenReturn(paymentSubmitPOST201Response);
		this.mockMvc.perform(post("/payment-submissions").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSubmissionPOSTRequestResponseMockData.asJsonString(paymentSubmissionPOSTRequest))).andExpect(status().isCreated());
	}
	
	@Test
	public void testExceptionCreatePaymentSubmissionResource() throws Exception{
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new PaymentSubmissionPOSTRequest();
		when(submissionValidator.validatePaymentSubmissionRequest(anyObject())).thenReturn(true);
		when(submissionService.createPaymentSubmissionResource(anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		this.mockMvc.perform(post("/payment-submissions").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSubmissionPOSTRequestResponseMockData.asJsonString(paymentSubmissionPOSTRequest))).andExpect(status().isBadRequest());
	}
	
	@Test
	public void testExceptionCreatePaymentSubmissionResource2() {
		ReflectionTestUtils.setField(paymentSubmissionController, "responseErrorMessageEnabled", "true");
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new PaymentSubmissionPOSTRequest();
		when(submissionValidator.validatePaymentSubmissionRequest(anyObject())).thenReturn(true);
		when(submissionService.createPaymentSubmissionResource(anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		try {
			this.mockMvc.perform(post("/payment-submissions").contentType(MediaType.APPLICATION_JSON_VALUE).content(PaymentSubmissionPOSTRequestResponseMockData.asJsonString(paymentSubmissionPOSTRequest))).andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(true, e.getMessage().contains("IBAN Identification value of Creditor/Debtor Account is not valid."));
		}
	}
	
	@After
	public void tearDown() throws Exception {
		paymentSubmissionController = null;
		mockMvc = null;
		submissionService = null; 
		submissionValidator = null;
		
	}
	
}
