package com.capgemini.psd2.pisp.payment.submission.test.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.comparator.PaymentSubmissionPayloadComparator;
import com.capgemini.psd2.pisp.payment.submission.service.impl.PaymentSubmissionServiceImpl;
import com.capgemini.psd2.pisp.payment.submission.transformer.PaymentSubmissionResponseTransformer;
import com.capgemini.psd2.pisp.validation.PispUtilities;

import com.capgemini.psd2.token.Token;


@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionServiceImplTest {

	@InjectMocks
	PaymentSubmissionServiceImpl paymentSubmissionServiceImpl = new PaymentSubmissionServiceImpl("0", "0");
	
	@Mock
	private PaymentSubmissionPayloadComparator paymentComparator;
	@Mock
	private PaymentSubmissionResponseTransformer responseTransformer;
	@Mock
	private PaymentSetupPlatformAdapter paymentPlatformAdapter;
	@Mock
	private PaymentSubmissionPlatformAdapter submissionPlatformAdapter;		
	@Mock
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	@Mock
	private PaymentSubmissionAdapterHelper submissionAdapterHelper;
	@Mock
	private PispConsentAdapter pispConsentAdapter;
	@Mock
	private PispConsentMongoRepository pispConsentMongoRepository;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test(expected = PSD2Exception.class)
	public void testCreatePaymentSubmissionResource() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPaymentId("123");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		paymentResponseInfo.setPaymentSubmissionId("123");
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setStatus("Pending");
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId("123");// here
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse setupResponse = new PaymentSetupResponse();
		PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
		responseInitiation.setDebtorAgent(new DebtorAgent());
		responseInitiation.setDebtorAccount(new DebtorAccount());
		setupResponse.setInitiation(responseInitiation);
		paymentSetupPOSTResponse.setData(setupResponse);
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setDebtorAgent(null);
		data.setPaymentId("123");
		data.setInitiation(initiation);
		paymentSubmissionPOSTRequest.setData(data);
		Token token = new Token();
		token.setRequestId("123");
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		when(submissionAdapterHelper.prePaymentSubmissionValidation(anyObject())).thenReturn(paymentResponseInfo);
		when(submissionPlatformAdapter.createPaymentSubmissionResource(anyObject(), anyObject())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentSetupAdapterHelper.updateStagedPaymentSetup(anyObject())).thenReturn(new PaymentSetupStagingResponse());
		when(submissionAdapterHelper.executePaymentSubmission(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
		Mockito.doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		when(submissionPlatformAdapter.retrievePaymentSubmissionResource(anyObject())).thenReturn(null);
		doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());

		 PaymentSubmitPOST201Response psubmission = new PaymentSubmitPOST201Response();
				PaymentSetupResponse1 data1 = new PaymentSetupResponse1();
				psubmission.setData(data1);
		when(responseTransformer.paymentSubmissionResponseTransformer(anyObject())).thenReturn(psubmission);
		when( pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(pispConsent);
		
		
		PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
		assertNotNull(response);
		/*
		 * PISP_TOKEN_PAYMENT_ID_NOT_MATCHED_WITH_SUBMISSION_PAYMENT_ID
		 */
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		token.setRequestId("456");
		response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testException_PISP_PAYMENT_SETUP_RESOURCE_EXPIRED() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPaymentId("123");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
		DateTime dateTime = new DateTime();
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setStatus("Pending");
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId(null);// here
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse setupResponse = new PaymentSetupResponse();
		PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
		responseInitiation.setDebtorAgent(new DebtorAgent());
		responseInitiation.setDebtorAccount(new DebtorAccount());
		setupResponse.setInitiation(responseInitiation);
		paymentSetupPOSTResponse.setData(setupResponse);
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setDebtorAgent(null);
		data.setPaymentId("123");
		data.setInitiation(initiation);
		paymentSubmissionPOSTRequest.setData(data);
		Token token = new Token();
		token.setRequestId("123");
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)-1);
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testException_PISP_SUBMISSION_PAYLOAD_COMPARISON_FAILED() {
		
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPaymentId("123");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		DateTime dateTime = new DateTime();
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setStatus("Pending");
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId(null);// here
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse setupResponse = new PaymentSetupResponse();
		PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
		responseInitiation.setDebtorAgent(new DebtorAgent());
		responseInitiation.setDebtorAccount(new DebtorAccount());
		setupResponse.setInitiation(responseInitiation);
		paymentSetupPOSTResponse.setData(setupResponse);
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setDebtorAgent(null);
		data.setPaymentId("456");
		data.setInitiation(initiation);
		paymentSubmissionPOSTRequest.setData(data);
		Token token = new Token();
		token.setRequestId("123");
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0,1);
		when(submissionAdapterHelper.prePaymentSubmissionValidation(anyObject())).thenReturn(paymentResponseInfo);
		when(submissionPlatformAdapter.createPaymentSubmissionResource(anyObject(), anyObject())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentSetupAdapterHelper.updateStagedPaymentSetup(anyObject())).thenReturn(new PaymentSetupStagingResponse());
		when(submissionAdapterHelper.executePaymentSubmission(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
		Mockito.doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		when(submissionPlatformAdapter.retrievePaymentSubmissionResource(anyObject())).thenReturn(null);
		doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());

		 PaymentSubmitPOST201Response psubmission = new PaymentSubmitPOST201Response();
				PaymentSetupResponse1 data1 = new PaymentSetupResponse1();
				psubmission.setData(data1);
		when(responseTransformer.paymentSubmissionResponseTransformer(anyObject())).thenReturn(psubmission);
		when( pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(pispConsent);
		
		PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
		assertNotNull(response);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testException_PISP_DUPLICATE_SUBMISSION_ID() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPaymentId("123");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		paymentResponseInfo.setPaymentSubmissionId("123");
		DateTime dateTime = new DateTime();
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setStatus("Pending");
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId("123");// here
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse setupResponse = new PaymentSetupResponse();
		PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
		responseInitiation.setDebtorAgent(new DebtorAgent());
		responseInitiation.setDebtorAccount(new DebtorAccount());
		setupResponse.setInitiation(responseInitiation);
		paymentSetupPOSTResponse.setData(setupResponse);
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setDebtorAgent(null);
		data.setPaymentId("123");
		data.setInitiation(initiation);
		paymentSubmissionPOSTRequest.setData(data);
		Token token = new Token();
		token.setRequestId("123");
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		when(submissionAdapterHelper.prePaymentSubmissionValidation(anyObject())).thenReturn(paymentResponseInfo);
		when(submissionPlatformAdapter.createPaymentSubmissionResource(anyObject(), anyObject())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentSetupAdapterHelper.updateStagedPaymentSetup(anyObject())).thenReturn(new PaymentSetupStagingResponse());
		when(submissionAdapterHelper.executePaymentSubmission(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
		Mockito.doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		when(submissionPlatformAdapter.retrievePaymentSubmissionResource(anyObject())).thenReturn(new PaymentSubmissionPlatformResource());
		paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testException_PISP_BAD_REQUEST() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPaymentId("123");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		DateTime dateTime = new DateTime();
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setStatus("Rejected");
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId(null);// here
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse setupResponse = new PaymentSetupResponse();
		PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
		responseInitiation.setDebtorAgent(new DebtorAgent());
		responseInitiation.setDebtorAccount(new DebtorAccount());
		setupResponse.setInitiation(responseInitiation);
		paymentSetupPOSTResponse.setData(setupResponse);
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setDebtorAgent(null);
		data.setPaymentId("123");
		data.setInitiation(initiation);
		paymentSubmissionPOSTRequest.setData(data);
		Token token = new Token();
		token.setRequestId("123");
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		when(submissionAdapterHelper.prePaymentSubmissionValidation(anyObject())).thenReturn(paymentResponseInfo);
		when(submissionPlatformAdapter.createPaymentSubmissionResource(anyObject(), anyObject())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentSetupAdapterHelper.updateStagedPaymentSetup(anyObject())).thenReturn(new PaymentSetupStagingResponse());
		when(submissionAdapterHelper.executePaymentSubmission(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
		Mockito.doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		when(submissionPlatformAdapter.retrievePaymentSubmissionResource(anyObject())).thenReturn(null);
		doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		PaymentSubmitPOST201Response psubmission = new PaymentSubmitPOST201Response();
		PaymentSetupResponse1 data1 = new PaymentSetupResponse1();
		psubmission.setData(data1);
		when(responseTransformer.paymentSubmissionResponseTransformer(anyObject())).thenReturn(psubmission);
		when( pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(pispConsent);
		
		
		PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
		assertNotNull(response);
	}
	
	@Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource1(){
           PispConsent pispConsent = new PispConsent();
           pispConsent.setPaymentId("123");
           PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
           paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
           CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
           PaymentSetupResponse setupResponse = new PaymentSetupResponse();
           paymentSetupPOSTResponse.setData(setupResponse);
           CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmission data = new PaymentSubmission();
           data.setPaymentId("123");
           paymentSubmissionPOSTRequest.setData(data);
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
           }
    @Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource2(){
           PispConsent pispConsent = new PispConsent();
           pispConsent.setPaymentId("123");
           PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
           paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
           DateTime dateTime = new DateTime();
           PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
           paymentSubmissionPlatformResource.setStatus("Pending");
           paymentSubmissionPlatformResource.setPaymentId("123");
           paymentSubmissionPlatformResource.setPaymentSubmissionId("123");// here
           PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
           CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
           PaymentSetupResponse setupResponse = new PaymentSetupResponse();
           paymentSetupPOSTResponse.setData(setupResponse);
           paymentSetupPlatformResource.setPaymentId("123");
           paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
           paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
           
           CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmission data = new PaymentSubmission();
           data.setPaymentId("123");
           paymentSubmissionPOSTRequest.setData(data);
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(submissionPlatformAdapter.retrievePaymentSubmissionResourceByPaymentId(anyObject())).thenReturn(paymentSubmissionPlatformResource);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
           
    }
    
    @Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource3(){
           PispConsent pispConsent = new PispConsent();
           pispConsent.setPaymentId("123");
           PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
           paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
           PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
           DateTime dateTime = new DateTime();
           PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
           paymentSubmissionPlatformResource.setStatus("Pending");
           paymentSubmissionPlatformResource.setPaymentId("123");
           paymentSubmissionPlatformResource.setPaymentSubmissionId(null);
           paymentSubmissionPlatformResource.setPaymentSubmissionId("123");// here
           PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
           CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
           PaymentSetupResponse setupResponse = new PaymentSetupResponse();
           PaymentSetupResponseInitiation ini1 = new PaymentSetupResponseInitiation();
           setupResponse.setInitiation(ini1);
           paymentSetupPOSTResponse.setData(setupResponse);
           paymentSetupPlatformResource.setPaymentId("123");
           paymentSetupPlatformResource.setStatus("AcceptedCustomer");
           paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
           CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmission data = new PaymentSubmission();
           PaymentSetupResponseInitiation ini = new PaymentSetupResponseInitiation();
           data.setInitiation(ini);
           data.setPaymentId("123");
           paymentSubmissionPOSTRequest.setData(data);
           
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
           when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
           when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
           when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
           when(submissionAdapterHelper.prePaymentSubmissionValidation(anyObject())).thenReturn(paymentResponseInfo);
           when(submissionPlatformAdapter.createPaymentSubmissionResource(anyObject(), anyObject())).thenReturn(paymentSubmissionPlatformResource);
           when(paymentSetupAdapterHelper.updateStagedPaymentSetup(anyObject())).thenReturn(new PaymentSetupStagingResponse());
           when(submissionAdapterHelper.executePaymentSubmission(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
           Mockito.doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
           when(submissionPlatformAdapter.retrievePaymentSubmissionResource(anyObject())).thenReturn(null);
           doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
           PaymentSubmitPOST201Response psubmission = new PaymentSubmitPOST201Response();
   		PaymentSetupResponse1 data1 = new PaymentSetupResponse1();
   		psubmission.setData(data1);
   		
           when(responseTransformer.paymentSubmissionResponseTransformer(anyObject())).thenReturn(psubmission);
           when( pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
           when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(pispConsent);
          
           
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
    
           
           
    }
    
    @Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource4(){
           PispConsent pispConsent = new PispConsent();
           pispConsent.setPaymentId("123");
           PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
           paymentSubmissionExecutionResponse.setPaymentSubmissionId("123");
           DateTime dateTime = new DateTime();
           PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
           paymentSubmissionPlatformResource.setStatus("Pending");
           paymentSubmissionPlatformResource.setPaymentId("123");
           paymentSubmissionPlatformResource.setPaymentSubmissionId(null);// here
           PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
           CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
           PaymentSetupResponse setupResponse = new PaymentSetupResponse();
           paymentSetupPOSTResponse.setData(setupResponse);
           paymentSetupPlatformResource.setPaymentId("123");
           paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
           paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
           CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmission data = new PaymentSubmission();
           data.setPaymentId("123");
           paymentSubmissionPOSTRequest.setData(data);
           
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
           when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
           when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
           when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(1);
           
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
    }
    
    @Test(expected = PSD2Exception.class)
    public void testException_PISP_PAYMENT_SUBMISSION_CREATION_FAILED() {
    	
    	PispConsent pispConsent = new PispConsent();
		pispConsent.setPaymentId("123");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		paymentSubmissionExecutionResponse.setPaymentSubmissionId(null);
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		DateTime dateTime = new DateTime();
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setStatus("Pending");
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId(null);// here
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse setupResponse = new PaymentSetupResponse();
		PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
		responseInitiation.setDebtorAgent(new DebtorAgent());
		responseInitiation.setDebtorAccount(new DebtorAccount());
		setupResponse.setInitiation(responseInitiation);
		paymentSetupPOSTResponse.setData(setupResponse);
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setStatus("AcceptedCustomerProfile");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setDebtorAgent(null);
		data.setPaymentId("123");
		data.setInitiation(initiation);
		paymentSubmissionPOSTRequest.setData(data);
		Token token = new Token();
		token.setRequestId("123");
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
		ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		when(paymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0,1);
		when(submissionAdapterHelper.prePaymentSubmissionValidation(anyObject())).thenReturn(paymentResponseInfo);
		when(submissionPlatformAdapter.createPaymentSubmissionResource(anyObject(), anyObject())).thenReturn(paymentSubmissionPlatformResource);
		when(paymentSetupAdapterHelper.updateStagedPaymentSetup(anyObject())).thenReturn(new PaymentSetupStagingResponse());
		when(submissionAdapterHelper.executePaymentSubmission(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
		Mockito.doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		when(submissionPlatformAdapter.retrievePaymentSubmissionResource(anyObject())).thenReturn(null);
		doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		when(responseTransformer.paymentSubmissionResponseTransformer(anyObject())).thenReturn(new PaymentSubmitPOST201Response());
		when( pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(pispConsent);
		
		doNothing().when(submissionPlatformAdapter).updatePaymentSubmissionResource(anyObject());
		
		PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
		assertNotNull(response);

    	
    }
    
    /*
     * 
     */
    
    @Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource6(){
    	CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmissionPlatformResource submissionPlatformResource =new PaymentSubmissionPlatformResource();
           PaymentSubmission data = new PaymentSubmission();
           
           paymentSubmissionPOSTRequest.setData(data);
           data.setPaymentId("123");
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(submissionPlatformResource);
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
    }
    
    @Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource7(){
    	CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmissionPlatformResource submissionPlatformResource =new PaymentSubmissionPlatformResource();
           PaymentSetupPlatformResource paymentPlatformResource =new PaymentSetupPlatformResource();
           
           paymentPlatformResource.setPaymentId("123");
           PaymentSubmission data = new PaymentSubmission();
           submissionPlatformResource.setPaymentId("328");
           paymentSubmissionPOSTRequest.setData(data);
           data.setPaymentId("123");
           //paymentPlatformAdapter.retrievePaymentSetupResource("123");
           //paymentPlatformResource=paymentPlatformAdapter.retrievePaymentSetupResource("123");
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(submissionPlatformResource);
           when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentPlatformResource);
           
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
    }
    
    @Test(expected = PSD2Exception.class)
    public void testCreatePaymentSubmissionResource8(){
    	CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest= new CustomPaymentSubmissionPOSTRequest();
           PaymentSubmissionPlatformResource submissionPlatformResource =new PaymentSubmissionPlatformResource();
           PaymentSetupPlatformResource paymentPlatformResource =new PaymentSetupPlatformResource();
           
           paymentPlatformResource.setPaymentId("123");
           PaymentSubmission data = new PaymentSubmission();
           submissionPlatformResource.setPaymentId("328");
           paymentSubmissionPOSTRequest.setData(data);
           data.setPaymentId("123");
           Token token = new Token();
           token.setRequestId("123");
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "paymentSetupResourceExpiry", (long)999999999);
           ReflectionTestUtils.setField(paymentSubmissionServiceImpl, "idempotencyDuration", (long)0);
           when(reqHeaderAtrributes.getToken()).thenReturn(token);
           when(submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(anyLong())).thenReturn(submissionPlatformResource);
           when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentPlatformResource);
           
           PaymentSubmitPOST201Response response = paymentSubmissionServiceImpl.createPaymentSubmissionResource(paymentSubmissionPOSTRequest);
           assertNotNull(response);
    }


}
