package com.capgemini.psd2.pisp.payment.submission.mongo.db.test.adapter.impl;


import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.impl.PaymentAuthorizationValidationMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository.PaymentSetupRetrieveFoundationRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentAuthorizationValidationMongoDbAdapterImplTest {
	
	@Mock
	private PaymentSetupRetrieveFoundationRepository paymentSetupFoundationRepository;
	
	@InjectMocks
	PaymentAuthorizationValidationMongoDbAdapterImpl paymentAuthorizationValidationMongoDbAdapterImpl;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void preAuthorizationPaymentValidationTest() {
		CustomPaymentSetupPOSTResponse paymentSetupPostResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse paymentSetupResponse = new PaymentSetupResponse();
		
		PaymentSetupValidationResponse response = new PaymentSetupValidationResponse();
		
		DebtorAccount debtorAccount = new DebtorAccount();
		PaymentSetupResponseInitiation paymentSetupResponseInitiation = new PaymentSetupResponseInitiation();
		paymentSetupPostResponse.setData(paymentSetupResponse);
		paymentSetupResponse.setInitiation(paymentSetupResponseInitiation);
		paymentSetupResponseInitiation.setDebtorAccount(debtorAccount);
		Map<String, String> params = new HashMap<String, String>();
		paymentSetupResponseInitiation.setEndToEndIdentification("FS_PMV_013");
		assertNotNull(paymentSetupPostResponse.getData().getInitiation().getDebtorAccount());
		when(paymentSetupFoundationRepository.findOneByDataPaymentId(anyString())).thenReturn(new PaymentSetupFoundationResource());
		response = paymentAuthorizationValidationMongoDbAdapterImpl.preAuthorizationPaymentValidation(paymentSetupPostResponse, params);
		assertNotNull(response);
		
	}
	
	@Test
	public void preAuthorizationPaymentValidationTestIfDebtorAccountIsNull() {
		CustomPaymentSetupPOSTResponse paymentSetupPostResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse paymentSetupResponse = new PaymentSetupResponse();
		
		PaymentSetupValidationResponse response = new PaymentSetupValidationResponse();
		
		DebtorAccount debtorAccount = new DebtorAccount();
		PaymentSetupResponseInitiation paymentSetupResponseInitiation = new PaymentSetupResponseInitiation();
		paymentSetupPostResponse.setData(paymentSetupResponse);
		paymentSetupResponse.setInitiation(paymentSetupResponseInitiation);
		
		Map<String, String> params = new HashMap<String, String>();
		paymentSetupResponseInitiation.setEndToEndIdentification("FS_PMV_013");
		when(paymentSetupFoundationRepository.findOneByDataPaymentId(anyString())).thenReturn(new PaymentSetupFoundationResource());
		response = paymentAuthorizationValidationMongoDbAdapterImpl.preAuthorizationPaymentValidation(paymentSetupPostResponse, params);
		assertNotNull(response);
		
	}
	

	@Test(expected=PSD2Exception.class)
	public void preAuthorizationPaymentNotValidateTest(){
		CustomPaymentSetupPOSTResponse paymentSetupPostResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse paymentSetupResponse = new PaymentSetupResponse();
		
		
		
		DebtorAccount debtorAccount = new DebtorAccount();
		PaymentSetupResponseInitiation paymentSetupResponseInitiation = new PaymentSetupResponseInitiation();
		paymentSetupPostResponse.setData(paymentSetupResponse);
		paymentSetupResponse.setInitiation(paymentSetupResponseInitiation);
		paymentSetupResponseInitiation.setEndToEndIdentification("FS_PMV_014");
		paymentSetupResponseInitiation.setDebtorAccount(debtorAccount);
		Map<String, String> params = new HashMap<String, String>();
		PaymentSetupValidationResponse response = new PaymentSetupValidationResponse();
		when(paymentSetupFoundationRepository.findOneByDataPaymentId(anyString())).thenThrow(DataAccessResourceFailureException.class);
		response = paymentAuthorizationValidationMongoDbAdapterImpl.preAuthorizationPaymentValidation(paymentSetupPostResponse, params);
		assertNotNull(response);
		}
	
	}
	

	
	


