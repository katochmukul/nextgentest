
package com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.impl.PaymentAuthorizationValidationMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.impl.PaymentSubmissionExecutionMongoDbAdapterImpl;

@Configuration
public class PaymentSubmissionMongoDbAdapterConfig {
	
	@Bean
	public PaymentAuthorizationValidationAdapter paymentSubmissionAuthorizationValidationMongoDBAdapter(){
		return new PaymentAuthorizationValidationMongoDbAdapterImpl();
	}
	
	@Bean
	public PaymentSubmissionExecutionAdapter paymentSubmissionExecutionMongoDBAdapter(){
		return new PaymentSubmissionExecutionMongoDbAdapterImpl();
	}
	
}
