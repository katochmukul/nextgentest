package com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionFoundationResource;

public interface PaymentSubmissionFoundationRepository extends MongoRepository<PaymentSubmissionFoundationResource, String>{
	public PaymentSubmissionFoundationResource findOneByDataPaymentId(String paymentId );
}
