package com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;

public interface PaymentSetupRetrieveFoundationRepository extends MongoRepository<PaymentSetupFoundationResource, String>{

	  public PaymentSetupFoundationResource findOneByDataPaymentId(String paymentId );	
}
