package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.client.ValidatePaymentFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePaymentFoundationServiceClientImplTest {
	
	@InjectMocks
	private ValidatePaymentFoundationServiceClientImpl service;
	
	@Mock
	private RestClientSync restClient;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test validation passed.
	 */
	@Test
	public void validatePaymentTest(){
	RequestInfo requestInfo= new RequestInfo();
	PaymentInstruction paymentInstruction= new PaymentInstruction();
	ValidationPassed validationPassed = new ValidationPassed();
	HttpHeaders httpHeaders = new HttpHeaders();
	
	Mockito.when(service.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
	
	validationPassed= service.validatePayment(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
	assertNotNull(validationPassed);
	}
	

}
