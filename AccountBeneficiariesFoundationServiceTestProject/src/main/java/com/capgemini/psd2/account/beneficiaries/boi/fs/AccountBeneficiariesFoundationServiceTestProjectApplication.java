package com.capgemini.psd2.account.beneficiaries.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.AccountBeneficiariesFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountBeneficiariesFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountBeneficiariesFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAccountBalanceFSController{
	
	@Autowired
	private AccountBeneficiariesFoundationServiceAdapter adapter;
	
//	@Autowired
//	private BeneficiariesRepository repository;
	
	@RequestMapping("/testAccountBeneficiary")
	public BeneficiariesGETResponse getResponse(){
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345678");
		accDet.setAccountNSC("903779");
		accDet.setAccountNumber("25369621");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("12345678");
		
		Map<String, String> params = new HashMap<String, String>();
//		Beneficiaries beneficiaries = new Beneficiaries();
//		beneficiaries.setAccountNumber("12345678");
//		beneficiaries.setNsc("123456");
//		Beneficiary beneficiary = new Beneficiary();
//		beneficiary.setId("12345678");
//		beneficiary.setAccountNumber("12345678");
//		beneficiary.setNSC("123456");
//		beneficiary.setIBAN("GB29NWBK60161331926819");
//		beneficiary.setABACode("abaCode");
//		beneficiary.setName("Name");
//		beneficiary.setReference("Reference");
//		beneficiary.setCountryCode("GB");
//		beneficiary.setStatus(BeneficiaryStatus.C);
//		beneficiary.setBeneRegisteredAs(BeneRegisteredAsType.B);
//		beneficiary.setLimitBand(LimitBandType.P);
//		beneficiary.setBIC("BOFIIE2D");
//		beneficiary.setType("Type");
//		beneficiaries.getBeneficiary().add(beneficiary);
//		
//		repository.save(beneficiaries);
		params.put("x-channel-id", "BOL");
		params.put("channelId", "BOL");
		params.put(PSD2Constants.CHANNEL_ID, "BOL");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		return adapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
}