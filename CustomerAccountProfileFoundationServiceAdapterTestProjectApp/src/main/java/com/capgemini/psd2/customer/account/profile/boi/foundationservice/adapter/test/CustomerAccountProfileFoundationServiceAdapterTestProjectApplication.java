package com.capgemini.psd2.customer.account.profile.boi.foundationservice.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.CustomerAccountProfileFoundationServiceAdapter;


@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class CustomerAccountProfileFoundationServiceAdapterTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerAccountProfileFoundationServiceAdapterTestProjectApplication.class, args);
	}
}


@RestController
@ResponseBody
class TestCustomerAccountProfileController
{
	
	@Autowired
	private CustomerAccountProfileFoundationServiceAdapter custAccProFouSerAdapter;
	
	@RequestMapping("/testCustomerAccountProfile")
	public AccountGETResponse getAccountlist()
	{
		 Map<String,String> params=new HashMap<String ,String>();
		    params.put("consentFlowType", "AISP");
		    params.put("x-channel-id", "BOL");
		    params.put("x-user-id", "BOI999");
		    params.put("X-BOI-PLATFORM", "platform");
		    params.put("x-fapi-interaction-id", "87654321");
			return custAccProFouSerAdapter.retrieveCustomerAccountList("87654321",params);
			//return custAccProFouSerAdapter.retrieveCustomerAccountList("87654321",params);
	}
	

}