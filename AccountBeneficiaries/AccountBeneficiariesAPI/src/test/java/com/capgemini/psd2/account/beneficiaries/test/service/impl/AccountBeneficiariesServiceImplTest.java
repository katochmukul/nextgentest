package com.capgemini.psd2.account.beneficiaries.test.service.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.beneficiaries.service.impl.AccountBeneficiariesServiceImpl;
import com.capgemini.psd2.account.beneficiaries.test.mock.data.AccountBeneficiariesMockData;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesServiceImplTest 
{
	/** The adapter. */
	@Mock
	private AccountBeneficiariesAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	/** The service. */
	@InjectMocks
	private AccountBeneficiariesServiceImpl service = new AccountBeneficiariesServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountBeneficiariesMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
		.thenReturn(AccountBeneficiariesMockData.getMockAccountMapping());
		
	}

	/**
	 * Retrieve account beneficiaries test (empty list).
	 */
	@Test
	public void retrieveAccountBeneficiariesTest() {
		when(adapter.retrieveAccountBeneficiaries(anyObject(), anyObject()))
				.thenReturn(AccountBeneficiariesMockData.getBlankResponse());
		BeneficiariesGETResponse beneficiariesGETResponse = service.retrieveAccountBeneficiaries(
				"123");
		assertTrue(beneficiariesGETResponse.getData().getBeneficiary().isEmpty());
	}


	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
	

}
