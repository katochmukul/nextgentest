package com.capgemini.psd2.account.beneficiaries.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.beneficiaries.service.AccountBeneficiariesService;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class AccountBeneficiariesServiceImpl implements AccountBeneficiariesService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account beneficiaries adapter. */
	@Autowired
	@Qualifier("accountBeneficiariesAdapterImpl")
	private AccountBeneficiariesAdapter accountBeneficiariesAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Override
	public BeneficiariesGETResponse retrieveAccountBeneficiaries(String accountId) {

		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);
		
		BeneficiariesGETResponse beneficiariesGETResponse = accountBeneficiariesAdapter
				.retrieveAccountBeneficiaries(accountMapping, reqHeaderAtrributes.getToken().getSeviceParams());

		if (beneficiariesGETResponse.getLinks() == null)
			beneficiariesGETResponse.setLinks(new Links());

		if (beneficiariesGETResponse.getMeta() == null)
			beneficiariesGETResponse.setMeta(new MetaData());

		beneficiariesGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		beneficiariesGETResponse.getMeta().setTotalPages(1);

		return beneficiariesGETResponse;
	}
}
