/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.aisp.domain.Beneficiary;
import com.capgemini.psd2.aisp.domain.Data4;
import com.capgemini.psd2.aisp.domain.Data4CreditorAccount;
import com.capgemini.psd2.aisp.domain.Data4Servicer;
import com.capgemini.psd2.aisp.domain.Data4Servicer.SchemeNameEnum;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountBeneficiariesMongoDbAdaptorImpl.
 */
@Component
public class AccountBeneficiariesMongoDbAdapterImpl implements AccountBeneficiariesAdapter {
	@Override
	public BeneficiariesGETResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		BeneficiariesGETResponse beneficiariesResponse = new BeneficiariesGETResponse();
		Data4 data = new Data4();
		List<Beneficiary> beneficiaryList = new ArrayList<>();
		Beneficiary beneficiary = new Beneficiary();
		beneficiary.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
		beneficiary.beneficiaryId("Ben1");
		beneficiary.setReference("Beneficiary Reference");
		Data4Servicer servicer= new Data4Servicer();
		servicer.setIdentification("ServicerIdentification");
		servicer.setSchemeName(SchemeNameEnum.BICFI);
		beneficiary.setServicer(servicer);
		Data4CreditorAccount creditorAccount= new Data4CreditorAccount();
		creditorAccount.setIdentification("IE29AIBK93115212345678");
		creditorAccount.setSchemeName(com.capgemini.psd2.aisp.domain.Data4CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setName("CreditorDemo");
		creditorAccount.setSecondaryIdentification("SecondaryIdentification");
		beneficiary.setCreditorAccount(creditorAccount);
		beneficiaryList.add(beneficiary);
		data.setBeneficiary(beneficiaryList);
		beneficiariesResponse.setData(data);
		return beneficiariesResponse;
	}
}
