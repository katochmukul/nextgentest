/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.beneficiaries.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.beneficiaries.routing.adapter.routing.AccountBeneficiariesAdapterFactory;

import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountBeneficiariesRoutingAdapter.
 */
public class AccountBeneficiariesRoutingAdapter implements AccountBeneficiariesAdapter {

	/** The account beneficiaries adapter factory. */
	@Autowired
	private AccountBeneficiariesAdapterFactory accountBeneficiariesAdapterFactory;

	/** The default adapter. */
	@Value("${app.defaultAccountBeneficiariesAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public BeneficiariesGETResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		
		AccountBeneficiariesAdapter accountBeneficiariesAdapter = accountBeneficiariesAdapterFactory
				.getAdapterInstance(defaultAdapter);
		
		return accountBeneficiariesAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
}