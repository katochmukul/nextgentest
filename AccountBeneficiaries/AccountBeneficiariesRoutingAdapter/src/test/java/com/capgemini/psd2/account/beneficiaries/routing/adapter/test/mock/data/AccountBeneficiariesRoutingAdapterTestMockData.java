package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.aisp.domain.Beneficiary;
import com.capgemini.psd2.aisp.domain.Data4;

public class AccountBeneficiariesRoutingAdapterTestMockData {

	public static BeneficiariesGETResponse getMockBeneficiariesGETResponse() {
		BeneficiariesGETResponse beneficiariesGETResponse = new BeneficiariesGETResponse();
		Data4 data = new Data4();
		List<Beneficiary> beneficiary = new ArrayList<>();
		data.setBeneficiary(beneficiary);
		beneficiariesGETResponse.setData(data);
		return beneficiariesGETResponse;
	}
}
