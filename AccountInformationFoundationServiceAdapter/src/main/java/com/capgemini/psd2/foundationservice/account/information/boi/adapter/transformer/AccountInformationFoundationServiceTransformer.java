/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.enumerator.CurrencyEnum;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.aisp.transformer.AccountInformationTransformer;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationFoundationServiceTransformer.
 */
@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class AccountInformationFoundationServiceTransformer implements AccountInformationTransformer {

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;

	/** The Account filters */
	private Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();

	private Map<String, Map<String, String>> jurisdiction = new HashMap<>();

	@Override
	public <T> AccountGETResponse transformAccountInformation(T inputAccountObj, Map<String, String> params) {
		Data2 data2 = new Data2();
		List<Account> accountList = new ArrayList<>();
		AccountGETResponse finalAIResponseObj = new AccountGETResponse();
		Accnts accounts = (Accnts) inputAccountObj;

		if (accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION) != null) {
			Set<String> regions = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION)
					.get(params.get(PSD2Constants.CONSENT_FLOW_TYPE)).stream().map(u -> u.substring(0, u.indexOf(".")))
					.collect(Collectors.toSet());
			regions.stream().forEach(x -> {
				List<String> filterLst = accountFiltering
						.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION)
						.get(params.get(PSD2Constants.CONSENT_FLOW_TYPE)).stream().filter(u -> u.startsWith(x))
						.collect(Collectors.toList());
				Map<String, String> data = filterLst.stream().collect(Collectors.toMap(
						s1 -> s1.substring(x.length() + 1, s1.indexOf("=")), s1 -> s1.substring(s1.indexOf("=") + 1)));
				jurisdiction.put(x, data);
			});
		}

		for (Accnt accnt : accounts.getAccount()) {
			String accountId = params.get(accnt.getAccountNumber() + "_" + accnt.getAccountNSC());
			if (accountId != null) {
				Account responseObj = new Account();
				responseObj.setAccountId(accountId);
				Data2Account acc = new Data2Account();
				Data2Servicer servicer = new Data2Servicer();
				if (!NullCheckUtils.isNullOrEmpty(accnt.getJurisdiction())) {
					HashMap<String, String> valuesMap = (HashMap) jurisdiction.get(accnt.getJurisdiction().value());

					acc.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum
							.valueOf(valuesMap.get(AccountInformationFoundationServiceConstants.SCHEMENAME)));

					String identification = valuesMap.get(AccountInformationFoundationServiceConstants.IDENTIFICATION);

					if (!NullCheckUtils.isNullOrEmpty(identification)) {
						if (identification.equals(AccountInformationFoundationServiceConstants.SORTCODEACCOUNTNUMBER)) {
							acc.setIdentification(accnt.getAccountNSC() + accnt.getAccountNumber());
						} else if (identification.equals(AccountInformationFoundationServiceConstants.IBAN)) {
							acc.setIdentification(accnt.getIban());
						}
					}
					if (!NullCheckUtils.isNullOrEmpty(valuesMap.get(AccountInformationFoundationServiceConstants.SERVICERSCHEMENAME))){
					if ((valuesMap.get(AccountInformationFoundationServiceConstants.SERVICERSCHEMENAME)
							.equals(AccountInformationFoundationServiceConstants.BICFI))) {
						servicer.setSchemeName(Data2Servicer.SchemeNameEnum
								.valueOf(valuesMap.get(AccountInformationFoundationServiceConstants.SERVICERSCHEMENAME)));
					} 
					}
					String serIden = valuesMap.get(AccountInformationFoundationServiceConstants.SERVICERIDENTIFICATION);
					if (!NullCheckUtils.isNullOrEmpty(serIden)) {
					if (serIden.equals(AccountInformationFoundationServiceConstants.BIC)){
						servicer.setIdentification(accnt.getBic());
					}
					}
				}
				validator.validate(acc);
				responseObj.setAccount(acc);
				validator.validateEnum(CurrencyEnum.class, accnt.getCurrency());
				validator.validate(accnt);
				responseObj.setCurrency(accnt.getCurrency());
				responseObj.setNickname(accnt.getAccountName());
				if(servicer.getIdentification()!=null && servicer.getSchemeName()!=null)
				{
					validator.validate(servicer);
					responseObj.setServicer(servicer);
				}

				validator.validate(responseObj);
				data2.setAccount(accountList);
				finalAIResponseObj.setData(data2);
				finalAIResponseObj.getData().getAccount().add(responseObj);
			}
		}
		 validator.validate(finalAIResponseObj);
		return finalAIResponseObj;
	}

	public Map<String, Map<String, List<String>>> getAccountFiltering() {
		return accountFiltering;
	}

	public void setAccountFiltering(Map<String, Map<String, List<String>>> accountFiltering) {
		this.accountFiltering = accountFiltering;
	}

	public Map<String, Map<String, String>> getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Map<String, Map<String, String>> jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

}
