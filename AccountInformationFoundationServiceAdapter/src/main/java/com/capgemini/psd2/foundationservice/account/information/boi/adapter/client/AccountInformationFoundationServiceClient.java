/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

/**
 * The Class AccountInformationFoundationServiceClient.
 */
@Component
public class AccountInformationFoundationServiceClient {
	
	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	/**
	 * Rest transport for single account balance.
	 *
	 * @param reqInfo the req info
	 * @param responseType the response type
	 * @param headers the headers
	 * @return the accounts
	 */
	public Accnts restTransportForSingleAccountInformation(RequestInfo reqInfo, Class <Accounts> responseType, HttpHeaders headers) {
		
		Accounts accounts=restClient.callForGet(reqInfo, responseType, headers);
		Accnts accnts=new Accnts();
		accnts.getAccount().add(accounts.getAccount());
		return accnts;
	}
	public Accnts restTransportForMultipleAccountInformation(RequestInfo reqInfo, Class <ChannelProfile> responseType, HttpHeaders headers) {
		ChannelProfile channelProfile=restClient.callForGet(reqInfo, responseType, headers);
		return channelProfile.getAccounts();
	}


}
