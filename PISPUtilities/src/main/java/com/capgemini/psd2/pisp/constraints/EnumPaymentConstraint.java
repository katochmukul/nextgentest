package com.capgemini.psd2.pisp.constraints;

public enum EnumPaymentConstraint {	
	IBAN("2","IBAN"),
	SORTCODEACCOUNTNUMBER("3","SORTCODEACCOUNTNUMBER"),
	ADDRESSLINE_LENGTH(70),
	COUNTRYSUBDIVISION_LENGTH(35),
	IDEMPOTENCY_KEY_LENGTH(40),
	ADDITIONAL_PARAMETERS("ADDITIONAL_PARAMETERS"),
	PAYMENT_SETUP_VALIDATION_STATUS("PAYMENT_SETUP_VALIDATION_STATUS");
	
	private String value;
	private String code;
	private int size;
	private String status; 
	private EnumPaymentConstraint(String code,String value){
		this.code = code;
		this.value = value;
	}
	private EnumPaymentConstraint(int value){
		this.size = value;
	}
	
	private EnumPaymentConstraint(String value){
		this.value = value;
	}
	
	public int getSize() {
		return size;
	}
		
	public String getStatus() {
		return status;
	}
	public String getValue() {
		return value;
	}
	
	public String getCode() {
		return code;
	}
		
}
