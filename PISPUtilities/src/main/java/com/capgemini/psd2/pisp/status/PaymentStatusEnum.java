package com.capgemini.psd2.pisp.status;

public enum PaymentStatusEnum {

	ACTIVE("0","Active"),
	CREATED("1","Created"),
	REVOKED("2","Revoked"),
	PENDING("3","Pending"),
	ACCEPTEDSETTLEMENTINPROCESS("4","AcceptedSettlementInProcess"),
	ACCEPTEDCUSTOMERPROFILE("5","AcceptedCustomerProfile"),
	ACCEPTEDTECHNICALVALIDATION("6","AcceptedTechnicalValidation"),
	REJECTED("7","Rejected"),
	PASS("8","Pass"),
	ERROR("9","Error"),
	ACCEPTED("10","Accepted"),
	PASSED("11","Passed");
		
	private String statusId;
	private String statusCode;
	
	PaymentStatusEnum(String statusId, String statusCode) {
		this.statusId = statusId;
		this.statusCode = statusCode;
	}
	
    public String getStatusId() {
			return statusId;
	}
	public String getStatusCode() {
			return statusCode;
	}
	
	public static final PaymentStatusEnum findStatusFromEnum(String statusId){
		PaymentStatusEnum currentStatus = null;
		PaymentStatusEnum[] enums = PaymentStatusEnum.values();
		for(PaymentStatusEnum statusEnum : enums) {
			if(statusEnum.getStatusId().equalsIgnoreCase(statusId)) {
				currentStatus = statusEnum;
				break;
			}
		}
		return currentStatus;
	}
	
	public static final boolean isActive(String statusId){
		PaymentStatusEnum currentStatus=null;
		PaymentStatusEnum[] enums = PaymentStatusEnum.values();
		for(PaymentStatusEnum status : enums) {
            if(status.getStatusId().equalsIgnoreCase(statusId)) {
            	currentStatus = status;
                  break;
            }
     }
     return currentStatus != null && currentStatus.equals(ACTIVE);	
	}
}
