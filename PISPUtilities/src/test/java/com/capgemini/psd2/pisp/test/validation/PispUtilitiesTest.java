package com.capgemini.psd2.pisp.test.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.validation.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PispUtilitiesTest {
	
	@Test
    public void testConstructorIsPrivate() throws Exception {
      Constructor<PispUtilities> constructor = PispUtilities.class.getDeclaredConstructor();
      assertTrue(Modifier.isPrivate(constructor.getModifiers()));
      constructor.setAccessible(true);
      constructor.newInstance();
    }
	/*
	 * Test with not null value
	 */
	@Test
	public void testIsEmptyNotNull(){
		String str = "Test";
		assertNotEquals(true, PispUtilities.isEmpty(str));
		
	}
	
	/*
	 * Test with null value
	 */
	@Test
	public void testIsEmptyNull(){
		String str = null;
		assertEquals(true, PispUtilities.isEmpty(str));
		
	}
	
	/*
	 * Test with empty string
	 */
	@Test
	public void testIsEmptyString(){
		String str = "";
		assertEquals(true, PispUtilities.isEmpty(str));
		
	}
	
	@Test
	public void testValidateIBANPositive(){
		String iban = "IE29AIBK93115212345678";
		PispUtilities.validateIBAN(iban);
	}
	@Test(expected = PSD2Exception.class)
	public void testValidateIBANException(){
		String iban = "ABC";
		PispUtilities.validateIBAN(iban);
	}
			
	@Test
	public void testValidateISOCountrySecondPositive(){
		String countryCode = "GB";
		PispUtilities.validateISOCountry(countryCode);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateISOCountryException(){
		String countryCode = "ABC";
		PispUtilities.validateISOCountry(countryCode);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateBBANNullException(){
		String bbanValue = null;
		PispUtilities.validateBBAN(bbanValue);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateBBANInvalidException(){
		String bbanValue = "123";
		PispUtilities.validateBBAN(bbanValue);
	}
	@Test
	public void testValidateBBANPositive(){
		String validBbanValue = "NWBK60161331926819";
		PispUtilities.validateBBAN(validBbanValue);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testIsValidCurrencyNullException(){
		String currencyCode = null;
		PispUtilities.isValidCurrency(currencyCode);		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testIsValidCurrencyInvalidException(){
		String currencyCode = "ABC";
		PispUtilities.isValidCurrency(currencyCode);		
	}
	
	@Test
	public void testIsValidCurrencyPositive(){
		String currencyCode = "EUR";
		assertEquals(true, PispUtilities.isValidCurrency(currencyCode));
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetMilliSecondsNullException(){
		String duration = null;
		PispUtilities.getMilliSeconds(duration);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetMilliSecondsInvalidException(){
		String duration = "ABC";
		PispUtilities.getMilliSeconds(duration);
	}
	
	@Test
	public void testGetMilliSecondsPositive(){
		String[] inputData = {"24H","24M","24S"};
		for(String duration: inputData)
			PispUtilities.getMilliSeconds(duration);
	}
	

}
