package com.capgemini.psd2.tpp.block.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;

@Component
public class TppBlockCoreSystemAdapterFactory implements ApplicationContextAware, TppBlockAdapterFactory {

	private ApplicationContext applicationContext;
	
	@Override
	public TppBlockAdapter getAdapterInstance(String adapterName){
		return (TppBlockAdapter) applicationContext.getBean(adapterName);		
	}
		
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}

}
