package com.capgemini.psd2.tpp.block.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;

@FunctionalInterface
public interface TppBlockAdapterFactory {
	public TppBlockAdapter getAdapterInstance(String coreSystemName);
}
