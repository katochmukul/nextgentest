package com.capgemini.psd2.tpp.block.routing.adapter.test.routing;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.tpp.block.routing.adapter.routing.TppBlockCoreSystemAdapterFactory;


public class TppBlockCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext applicationContext;
	
	@InjectMocks
	private TppBlockCoreSystemAdapterFactory tppBlockCoreSystemAdapterFactory=new TppBlockCoreSystemAdapterFactory();
	//tppBlockCoreSystemAdapterFactory.setApplicationContext
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testSetterMethod() {
		tppBlockCoreSystemAdapterFactory.setApplicationContext(applicationContext);
	}
	
	@Test
	public void testGetAdapterInstance() {
		when(applicationContext.getBean(anyString())).thenReturn(null);
		tppBlockCoreSystemAdapterFactory.getAdapterInstance("tppBlockLdapAdapter");
	}
}
