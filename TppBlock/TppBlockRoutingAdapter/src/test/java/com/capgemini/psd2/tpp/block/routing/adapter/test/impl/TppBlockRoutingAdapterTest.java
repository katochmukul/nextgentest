package com.capgemini.psd2.tpp.block.routing.adapter.test.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;
import com.capgemini.psd2.tpp.block.routing.adapter.impl.TppBlockRoutingAdapter;
import com.capgemini.psd2.tpp.block.routing.adapter.routing.TppBlockAdapterFactory;

public class TppBlockRoutingAdapterTest {

	@Mock
	private TppBlockAdapterFactory adapters;
	
	@InjectMocks
	private TppBlockRoutingAdapter obj=new TppBlockRoutingAdapter();
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFetchStatusDetails() {
		ReflectionTestUtils.setField(obj, "defaultAdapter", "tppBlockLdapAdapter");
		when(adapters.getAdapterInstance(anyString())).thenReturn(new TppBlockAdapter() {
			
			@Override
			public void updateTppStatus(String tppId, ActionEnum action) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public TppStatusDetails fetchTppStatusDetails(String tppId) {
				// TODO Auto-generated method stub
				return new TppStatusDetails();
			}
		});
		obj.updateTppStatus("123456", ActionEnum.UNBLOCK);
		obj.fetchTppStatusDetails("123456");
		
	}
}
