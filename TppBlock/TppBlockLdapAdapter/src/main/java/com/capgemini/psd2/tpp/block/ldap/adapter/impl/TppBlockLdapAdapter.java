package com.capgemini.psd2.tpp.block.ldap.adapter.impl;

import java.util.Arrays;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;
import com.capgemini.psd2.tpp.block.constants.TppBlockConstants;

@Component
public class TppBlockLdapAdapter implements TppBlockAdapter {

	@Value("${ldap.tppgroup.basedn}")
	private String tppDn;

	@Autowired
	private LdapTemplate ldapTemplate;

	@Override
	public TppStatusDetails fetchTppStatusDetails(String tppId) {
		List<Object> tppList = null;
		TppStatusDetails tppStatusDetails = null;

		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes(TppBlockConstants.ASTRICK, TppBlockConstants.PLUS)
				.base(tppDn).filter("(" + TppBlockConstants.CN + tppId + ")");

		try {
			tppList = getLdapSearchResult(ldapQuery);
		} catch (Exception ex) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}

		if (tppList == null || tppList.isEmpty())
			return tppStatusDetails;

		Attributes object = (Attributes) tppList.get(0);
		String[] str;
		str = object.get(TppBlockConstants.BLOCK_ATTRIBUTE).toString().split(":");
		String status = (Arrays.asList(str[1].split(", "))).get(0).trim();

		tppStatusDetails = new TppStatusDetails();
		tppStatusDetails.setTppId(tppId);
		tppStatusDetails.setxBlock(Boolean.valueOf(status));
		return tppStatusDetails;
	}

	public List<Object> getLdapSearchResult(LdapQuery ldapQuery) {
		return ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attributes) throws NamingException {
				return attributes;
			}
		});
	}

	@Override
	public void updateTppStatus(String tppId, ActionEnum action) {
		String tppCN = TppBlockConstants.CN + tppId + TppBlockConstants.COMMA + tppDn;
		try {
			ModificationItem[] mods = new ModificationItem[1];
			mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
					new BasicAttribute(TppBlockConstants.BLOCK_ATTRIBUTE, action.getValue().toString()));
			ldapTemplate.modifyAttributes(tppCN, mods);
		} catch (Exception e) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}

}
