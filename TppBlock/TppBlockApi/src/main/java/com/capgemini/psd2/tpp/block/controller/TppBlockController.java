/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.tpp.block.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.internal.apis.domain.TppBlockInput;
import com.capgemini.psd2.tpp.block.domain.TppBlockPathVariables;
import com.capgemini.psd2.tpp.block.service.TppBlockService;
import com.capgemini.psd2.tpp.block.utilities.TppBlockDataValidator;

@RestController
public class TppBlockController {

	@Autowired
	private TppBlockService tppBlockService;

	@RequestMapping(value = { "/tpp/{tppId}", "/tpp/" }, method = RequestMethod.PUT)
	public void updateTppStatus(TppBlockPathVariables pathvariables, @RequestBody TppBlockInput inputBody) {
		TppBlockDataValidator.validateData(inputBody, pathvariables);
		tppBlockService.updateTppStatus(pathvariables.getTppId(), inputBody.getAction(), inputBody.getDescription());
	}
}