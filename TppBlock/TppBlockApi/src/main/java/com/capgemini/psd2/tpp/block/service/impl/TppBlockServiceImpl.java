package com.capgemini.psd2.tpp.block.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;
import com.capgemini.psd2.tpp.block.service.TppBlockService;

@Service
public class TppBlockServiceImpl implements TppBlockService {

	@Autowired
	@Qualifier("tppBlockRoutingAdapter")
	private TppBlockAdapter directoryService;

	@Override
	public void updateTppStatus(String tppId, ActionEnum action, String description) {

		TppStatusDetails tppDetails;
		tppDetails = directoryService.fetchTppStatusDetails(tppId);

		if (tppDetails == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TPP_DOES_NOT_EXIST);
		}

		if (tppDetails.getxBlock() == action.getValue())
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.ACTION_ALREADY_APPLIED);

		directoryService.updateTppStatus(tppId, action);
	}
}
