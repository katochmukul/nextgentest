package com.capgemini.psd2.tpp.block.service.test.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;
import com.capgemini.psd2.tpp.block.service.impl.TppBlockServiceImpl;

public class TppBlockServiceImplTest {

	@Mock
	private TppBlockAdapter tppBlockAdapter;
	
	@InjectMocks
	private TppBlockServiceImpl obj=new TppBlockServiceImpl();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testUpdateTppStatus() {
		/*TppBlockInput obj=new TppBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("i dont like");*/
		
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(false);
		
		when(tppBlockAdapter.fetchTppStatusDetails(anyString())).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject());
		obj.updateTppStatus("123456", ActionEnum.BLOCK, "malicious");
		
	}
	
	@Test(expected=Exception.class)
	public void testUpdateTppStatus1() {
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(true);
		when(tppBlockAdapter.fetchTppStatusDetails(anyString())).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject());
		obj.updateTppStatus("123456", ActionEnum.BLOCK, "malicious");	
	}
	
	@Test(expected=Exception.class)
	public void testUpdateTppStatus2() {
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(false);
		//when(tppBlockAdapter.fetchTppStatusDetails(anyString())).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject());
		obj.updateTppStatus("123456", ActionEnum.BLOCK, "malicious");
	}
}
