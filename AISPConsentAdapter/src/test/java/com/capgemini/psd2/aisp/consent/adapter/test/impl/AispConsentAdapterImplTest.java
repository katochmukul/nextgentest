/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.consent.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.aisp.consent.adapter.test.mock.data.AispConsentAdapterMockData;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;


/**
 * The Class ConsentMappingAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentAdapterImplTest {

	/** The consent mapping repository. */
	@Mock
	private AispConsentMongoRepository aispConsentMongoRepository;

	/** The req header attributes. */
	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;
	
	

	/** The consent mapping adapter impl. */
	@InjectMocks
	private AispConsentAdapterImpl aispConsentAdapterImpl = new AispConsentAdapterImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test retrieve consent success flow.
	 */
	@Test
	public void testRetrieveConsentSuccessFlow() {
		when(aispConsentMongoRepository.findByConsentId(anyString()))
		.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		AispConsent consent = aispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
		assertEquals(AispConsentAdapterMockData.getConsentMockData().getPsuId(),
				consent.getPsuId());
		assertEquals(AispConsentAdapterMockData.getConsentMockData().getConsentId(),
				consent.getConsentId());
	}

	/**
	 * Test retrieve consent data access resource failure exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentDataAccessResourceFailureException() {
		when(aispConsentMongoRepository.findByConsentId(anyString())).thenThrow(new DataAccessResourceFailureException("Test"));
		aispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
	}

	/**
	 * Test retrieve consent null exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentNullException() {
		when(aispConsentMongoRepository.findByConsentId(anyString())).thenReturn(null);
		aispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
	}

	/**
	 * Test retrieve account mapping by account id success flow.
	 */
	@Test
	public void testRetrieveAccountMappingByAccountIdSuccessFlow() {
		when(aispConsentMongoRepository.findByAccountDetailsAccountId(anyString()))
		.thenReturn(AispConsentAdapterMockData.getConsentListMockData());
		when(reqHeaderAttributes.getCorrelationId()).thenReturn("95212678-4d0c-450f-8268-25dcfc95bfa1");
		assertEquals(AispConsentAdapterMockData.getAccountMapping().getPsuId(), aispConsentAdapterImpl
				.retrieveAccountMappingByAccountId("a1e590ad-73dc-453a-841e-2a2ef055e878", "22289").getPsuId());

	}

	/**
	 * Test retrieve account mapping by account id no account details exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountMappingByAccountIdNoAccountDetailsException() {
		when(aispConsentMongoRepository.findByConsentId(anyString()))
		.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		when(reqHeaderAttributes.getCorrelationId()).thenReturn("95212678-4d0c-450f-8268-25dcfc95bfa1");
		aispConsentAdapterImpl.retrieveAccountMappingByAccountId("a1e590ad-73dc-453a-841e-2a2ef055e878", "22280");
	}

	/**
	 * Test retrieve account mapping success flow.
	 */
	@Test
	public void testRetrieveAccountMappingSuccessFlow() {
		when(aispConsentMongoRepository.findByConsentId(anyString()))
		.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		assertEquals(AispConsentAdapterMockData.getAccountMapping().getPsuId(),
				aispConsentAdapterImpl.retrieveAccountMapping("a1e590ad-73dc-453a-841e-2a2ef055e878").getPsuId());
	}

	@Test 
	public void testCreateConsent(){
		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenReturn(AispConsentAdapterMockData.getConsentMockData());
		AispConsent aispConsent=AispConsentAdapterMockData.getConsentMockData();
		aispConsentAdapterImpl.createConsent(aispConsent);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateConsentWithException(){
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatus(anyString(),any())).thenReturn(AispConsentAdapterMockData.getConsentMockData());
		
		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.createConsent(AispConsentAdapterMockData.getConsentMockData());
	}
	

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testConsentAlreadyExist(){
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatus(anyString(),any())).thenReturn(AispConsentAdapterMockData.getConsentMockData());
		
		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenThrow(PSD2Exception.class);
		AispConsent aispConsent=AispConsentAdapterMockData.getConsentMockData();
		aispConsentAdapterImpl.createConsent(aispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails(){
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatus(anyString(),any())).thenReturn(null);
		AispConsent aispConsent=AispConsentAdapterMockData.getConsentMockDataWithOutAccountDetails();
		aispConsentAdapterImpl.createConsent(aispConsent);
	}
	
	@Test
	public void testRetrieveConsentByAccountRequestIdSuccessFlow(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatus(anyString(),any())).thenReturn(consent);
		assertEquals(consent.getPsuId(),aispConsentAdapterImpl.retrieveConsentByAccountRequestId("1234",ConsentStatusEnum.AWAITINGAUTHORISATION).getPsuId());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRetrieveConsentByAccountRequestIdDataAccessResourceFailureException(){
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatus(anyString(),any())).thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.retrieveConsentByAccountRequestId("1234",ConsentStatusEnum.AWAITINGAUTHORISATION);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testUpdateConsentStatusPSD2Exception(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenThrow(PSD2Exception.class);
		aispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testUpdateConsentStatusException(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}
	
	@Test
	public void testRetrieveConsentByPsuIdAndConsentStatusNull(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		List<AispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(aispConsentMongoRepository.findByPsuId(anyString())).thenReturn(consentList);
	    aispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234",null);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentByPsuIdAndConsentStatusException(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		List<AispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(aispConsentMongoRepository.findByPsuId(anyString())).thenThrow(DataAccessResourceFailureException.class);
	    aispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", null);
	}
		
	@Test
	public void testRetrieveConsentByPsuIdAndConsentStatus(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		List<AispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(aispConsentMongoRepository.findByPsuIdAndStatus(anyString(),anyObject())).thenReturn(consentList);
	    aispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}
	
	@Test
	public void testUpdateConsentStatusWithResponse(){
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		aispConsentAdapterImpl.updateConsentStatusWithResponse("12345", ConsentStatusEnum.AUTHORISED);
	}
	
}
