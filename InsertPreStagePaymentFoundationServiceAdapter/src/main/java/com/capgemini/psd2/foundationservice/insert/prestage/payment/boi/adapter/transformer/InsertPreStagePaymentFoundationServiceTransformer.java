/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.constants.InsertPreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.DeliveryAddress;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.FraudInputs;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PayeeInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.Payment;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationFoundationServiceTransformer.
 */
@Component
public class InsertPreStagePaymentFoundationServiceTransformer {
	
	/** The psd 2 validator. */
	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator psd2Validator;
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	public PaymentInstruction transformPaymentInformationAPIToFDForInsert(CustomPaymentSetupPOSTRequest paymentSetupPostReq, Map<String, String> params) {
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		Payment payment = new Payment();
		PaymentInformation paymentInfo = null;
		PayeeInformation payeeInfo = null;
		PayerInformation payerInfo = null;
		
		//PaymentSetup/Data
		paymentInfo = new PaymentInformation();
		paymentInfo.setPaymentDate(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));
		payment.setRequestType("I");
		paymentInfo.setStatus(paymentSetupPostReq.getPaymentStatus());
		payment.setCreatedOn(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));
		paymentInfo.setCreatedDateTime(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));
		//PaymentSetup/Initiation
		PaymentSetupInitiation initiation = paymentSetupPostReq.getData().getInitiation();
		paymentInfo.setInstructionIdentification(initiation.getInstructionIdentification());
		paymentInstruction.setEndToEndIdentification(initiation.getEndToEndIdentification());
		BigDecimal amount = new BigDecimal(initiation.getInstructedAmount().getAmount());
		paymentInfo.setAmount(amount);
		payeeInfo = new PayeeInformation();
		payeeInfo.setBeneficiaryCurrency(initiation.getInstructedAmount().getCurrency());
		//PaymentSetup/Data/Initiation/DebtorAgent
		payerInfo = new PayerInformation();
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAgent())){
			if(SchemeNameEnum.BICFI.toString().equals(initiation.getDebtorAgent().getSchemeName().toString()))
	              payerInfo.setBic(initiation.getDebtorAgent().getIdentification());
		}
       //PaymentSetup/Data/Initiation/DebtorAccount
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAccount())){
			if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){      
				payerInfo.setNsc(initiation.getDebtorAccount().getIdentification().substring(0, 6));
            	payerInfo.setAccountNumber(initiation.getDebtorAccount().getIdentification().substring(6, 14));
			}
	       else if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getDebtorAccount().getSchemeName().toString()))       
	              payerInfo.setIban(initiation.getDebtorAccount().getIdentification());
	        payerInfo.setAccountName(initiation.getDebtorAccount().getName());
	        payerInfo.setAccountSecondaryID(initiation.getDebtorAccount().getSecondaryIdentification());
		}
        
       //PaymentSetup/Data/Initiation/CreditorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getCreditorAgent())){
	        if(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI.toString().equals(initiation.getCreditorAgent().getSchemeName().toString()))
	        	payeeInfo.setBic(initiation.getCreditorAgent().getIdentification());
        }
       //PaymentSetup/Data/Initiation/CreditorAccount
        if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){
        	payeeInfo.setBeneficiaryNsc(initiation.getCreditorAccount().getIdentification().substring(0, 6));
        	payeeInfo.setBeneficiaryAccountNumber(initiation.getCreditorAccount().getIdentification().substring(6, 14));
        	payeeInfo.setBeneficiaryCountry(InsertPreStagePaymentFoundationServiceConstants.BENEFICIARY_COUNTRY);
        }
		else if (com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())) {
			payeeInfo.setIban(initiation.getCreditorAccount().getIdentification());
			payeeInfo.setBeneficiaryCountry(initiation.getCreditorAccount().getIdentification().substring(0, 2));
		}
        payeeInfo.setBeneficiaryName(initiation.getCreditorAccount().getName());
        payeeInfo.setAccountSecondaryID(initiation.getCreditorAccount().getSecondaryIdentification());
        if(!NullCheckUtils.isNullOrEmpty(initiation.getRemittanceInformation())){
        	paymentInfo.setUnstructured(initiation.getRemittanceInformation().getUnstructured());
            paymentInfo.setBeneficiaryReference(initiation.getRemittanceInformation().getReference());
        }
		//PaymentSetup/Risk
        if(!NullCheckUtils.isNullOrEmpty(paymentSetupPostReq.getRisk().getPaymentContextCode())){
        	paymentInfo.setPaymentContextCode(paymentSetupPostReq.getRisk().getPaymentContextCode().toString());
        }
		payeeInfo.setMerchentCategoryCode(paymentSetupPostReq.getRisk().getMerchantCategoryCode());
		payerInfo.setMerchantCustomerIdentification(paymentSetupPostReq.getRisk().getMerchantCustomerIdentification());
		
		//PaymentSetup/DeliveryAddress		
		if (!NullCheckUtils.isNullOrEmpty(paymentSetupPostReq.getRisk().getDeliveryAddress())) {
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			RiskDeliveryAddress riskDeliveryAddress = paymentSetupPostReq.getRisk().getDeliveryAddress();
			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getAddressLine())) {
				int addressLineLength = riskDeliveryAddress.getAddressLine().size();
				if (addressLineLength > 0)
					deliveryAddress.setAddressLine1(riskDeliveryAddress.getAddressLine().get(0));
				if (addressLineLength > 1)
					deliveryAddress.setAddressLine2(riskDeliveryAddress.getAddressLine().get(1));
			}
			deliveryAddress.setStreetName(riskDeliveryAddress.getStreetName());
			deliveryAddress.setBuildingNumber(riskDeliveryAddress.getBuildingNumber());
			deliveryAddress.setPostCode(riskDeliveryAddress.getPostCode());
			deliveryAddress.setTownName(riskDeliveryAddress.getTownName());

			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getCountrySubDivision())) {
				int countrySubDivisionCount = riskDeliveryAddress.getCountrySubDivision().size();
				if (countrySubDivisionCount > 0)
					deliveryAddress.setCountrySubDivision1(riskDeliveryAddress.getCountrySubDivision().get(0));
				if (countrySubDivisionCount > 1)
					deliveryAddress.setCountrySubDivision2(riskDeliveryAddress.getCountrySubDivision().get(1));
			}
			deliveryAddress.setCountry(riskDeliveryAddress.getCountry());
			payeeInfo.setDeliveryAddress(deliveryAddress);
		}
		
		//preparingResponseObject
		payment.setPaymentInformation(paymentInfo);
		payment.setPayeeInformation(payeeInfo);
		payment.setPayerInformation(payerInfo);
		paymentInstruction.setPayment(payment);
		return paymentInstruction;
	}

	public PaymentSetupStagingResponse transformPaymentInformationFDToAPIForInsert(ValidationPassed validationPassed) {
		PaymentSetupStagingResponse paymentSetupStageResponse = new PaymentSetupStagingResponse();
		if(NullCheckUtils.isNullOrEmpty(validationPassed) || NullCheckUtils.isNullOrEmpty(validationPassed.getSuccessMessage()))
			throw AdapterException.populatePSD2Exception("FS response is null for Stage Payment Insert.", AdapterErrorCodeEnum.TECHNICAL_ERROR);
		paymentSetupStageResponse.setPaymentId(validationPassed.getSuccessMessage());
		return paymentSetupStageResponse;
	}
	
	public PaymentInstruction transformPaymentInformationAPIToFDForUpdate(CustomPaymentSetupPOSTResponse paymentSetupPostResp, Map<String, String> params) {
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		Payment payment = new Payment();
		PaymentInformation paymentInfo = null;
		PayeeInformation payeeInfo = null;
		PayerInformation payerInfo = null;
				
		//PaymentSetup/Data
		paymentInfo = new PaymentInformation();
		paymentInfo.setPaymentDate(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));
		payment.setRequestType("U");
		payment.setPaymentId(paymentSetupPostResp.getData().getPaymentId());
		paymentInfo.setStatus(paymentSetupPostResp.getPaymentStatus());
		paymentInfo.setCreatedDateTime(timeZoneDateTimeAdapter.parseDateTimeFS(paymentSetupPostResp.getData().getCreationDateTime()));
		payment.setChannelUserID(params.get(PSD2Constants.USER_IN_REQ_HEADER));
		paymentInfo.setInitiatingPartyName(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		payerInfo = new PayerInformation();
		payerInfo.setPayerCurrency(paymentSetupPostResp.getPayerCurrency());
		paymentInfo.setJurisdiction(paymentSetupPostResp.getPayerJurisdiction());
		payerInfo.setNsc(paymentSetupPostResp.getAccountNSC());
		payerInfo.setAccountNumber(paymentSetupPostResp.getAccountNumber());
		payerInfo.setBic(paymentSetupPostResp.getAccountBic());
		
		//PaymentSetup/Initiation
		PaymentSetupResponseInitiation initiation = paymentSetupPostResp.getData().getInitiation();
		paymentInfo.setInstructionIdentification(initiation.getInstructionIdentification());
		paymentInstruction.setEndToEndIdentification(initiation.getEndToEndIdentification());
		BigDecimal amount = new BigDecimal(initiation.getInstructedAmount().getAmount());
		paymentInfo.setAmount(amount);
		payeeInfo = new PayeeInformation();
		payeeInfo.setBeneficiaryCurrency(initiation.getInstructedAmount().getCurrency());
		
		//PaymentSetup/Data/Initiation/DebtorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAgent())){
			if(SchemeNameEnum.BICFI.toString().equals(initiation.getDebtorAgent().getSchemeName().toString()))
	              payerInfo.setBic(initiation.getDebtorAgent().getIdentification());
		}
       
       //PaymentSetup/Data/Initiation/DebtorAccount
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAccount())){
			if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){       
				payerInfo.setNsc(initiation.getDebtorAccount().getIdentification().substring(0, 6));
	            payerInfo.setAccountNumber(initiation.getDebtorAccount().getIdentification().substring(6, 14));
	       }
	       else if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){       
	              payerInfo.setIban(initiation.getDebtorAccount().getIdentification());
	       }
	        payerInfo.setAccountName(initiation.getDebtorAccount().getName());
	        payerInfo.setAccountSecondaryID(initiation.getDebtorAccount().getSecondaryIdentification());
		}
       
       //PaymentSetup/Data/Initiation/CreditorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getCreditorAgent())){
	        if(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI.toString().equals(initiation.getCreditorAgent().getSchemeName().toString())){
	        	payeeInfo.setBic(initiation.getCreditorAgent().getIdentification());
	       }
       }
       
       //PaymentSetup/Data/Initiation/CreditorAccount
        if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){
        	payeeInfo.setBeneficiaryNsc(initiation.getCreditorAccount().getIdentification().substring(0, 6));
            payeeInfo.setBeneficiaryAccountNumber(initiation.getCreditorAccount().getIdentification().substring(6, 14));
            payeeInfo.setBeneficiaryCountry(InsertPreStagePaymentFoundationServiceConstants.BENEFICIARY_COUNTRY);
		} else if (com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())) {
			payeeInfo.setIban(initiation.getCreditorAccount().getIdentification());
			payeeInfo.setBeneficiaryCountry(initiation.getCreditorAccount().getIdentification().substring(0, 2));
		}
       
        payeeInfo.setBeneficiaryName(initiation.getCreditorAccount().getName());
        payeeInfo.setAccountSecondaryID(initiation.getCreditorAccount().getSecondaryIdentification());
        if(!NullCheckUtils.isNullOrEmpty(initiation.getRemittanceInformation())){
        	paymentInfo.setUnstructured(initiation.getRemittanceInformation().getUnstructured());
            paymentInfo.setBeneficiaryReference(initiation.getRemittanceInformation().getReference());
        }
        //PaymentSetup/Risk
        if(!NullCheckUtils.isNullOrEmpty(paymentSetupPostResp.getRisk().getPaymentContextCode())){
        	paymentInfo.setPaymentContextCode(paymentSetupPostResp.getRisk().getPaymentContextCode().toString());
        }
		payeeInfo.setMerchentCategoryCode(paymentSetupPostResp.getRisk().getMerchantCategoryCode());
		payerInfo.setMerchantCustomerIdentification(paymentSetupPostResp.getRisk().getMerchantCustomerIdentification());
		
		//PaymentSetup/DeliveryAddress
		
		if (!NullCheckUtils.isNullOrEmpty(paymentSetupPostResp.getRisk().getDeliveryAddress())) {
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			RiskDeliveryAddress riskDeliveryAddress = paymentSetupPostResp.getRisk().getDeliveryAddress();
			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getAddressLine())) {
				int addressLineLength = riskDeliveryAddress.getAddressLine().size();
				if (addressLineLength > 0)
					deliveryAddress.setAddressLine1(riskDeliveryAddress.getAddressLine().get(0));
				if (addressLineLength > 1)
					deliveryAddress.setAddressLine2(riskDeliveryAddress.getAddressLine().get(1));
			}
			deliveryAddress.setStreetName(riskDeliveryAddress.getStreetName());
			deliveryAddress.setBuildingNumber(riskDeliveryAddress.getBuildingNumber());
			deliveryAddress.setPostCode(riskDeliveryAddress.getPostCode());
			deliveryAddress.setTownName(riskDeliveryAddress.getTownName());

			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getCountrySubDivision())) {
				int countrySubDivisionCount = riskDeliveryAddress.getCountrySubDivision().size();
				if (countrySubDivisionCount > 0)
					deliveryAddress.setCountrySubDivision1(riskDeliveryAddress.getCountrySubDivision().get(0));
				if (countrySubDivisionCount > 1)
					deliveryAddress.setCountrySubDivision2(riskDeliveryAddress.getCountrySubDivision().get(1));
			}
			deliveryAddress.setCountry(riskDeliveryAddress.getCountry());
			payeeInfo.setDeliveryAddress(deliveryAddress);
		}	
		
			
		
		//fraud input data
		Object fnResponse =  paymentSetupPostResp.getFraudnetResponse();
		if(fnResponse != null ){
			if(fnResponse instanceof  FraudServiceResponse){
				FraudServiceResponse fraudnetResp = (FraudServiceResponse) fnResponse;
				FraudInputs fnInputs = new FraudInputs();
				fnInputs.setSystemResponse( fraudnetResp.getDecisionType());
				paymentInfo.setFraudInputs(fnInputs);
			}
			
		}
		//preparingResponseObject
		payment.setPaymentInformation(paymentInfo);
		payment.setPayeeInformation(payeeInfo);
		payment.setPayerInformation(payerInfo);
		paymentInstruction.setPayment(payment);
		
		return paymentInstruction;
	}

	public PaymentSetupStagingResponse transformPaymentInformationFDToAPIForUpdate(ValidationPassed validationPassed) {
		PaymentSetupStagingResponse paymentSetupStageResponse = new PaymentSetupStagingResponse();
		if(NullCheckUtils.isNullOrEmpty(validationPassed) || NullCheckUtils.isNullOrEmpty(validationPassed.getSuccessMessage()))
			throw AdapterException.populatePSD2Exception("FS response is null for Stage Payment Insert.", AdapterErrorCodeEnum.TECHNICAL_ERROR);
		paymentSetupStageResponse.setPaymentId(validationPassed.getSuccessMessage());
		return paymentSetupStageResponse;
	}

	public CustomPaymentSetupPOSTResponse transformPaymentRetrieval(PaymentInstruction paymentInstruction) {

		Payment payment = paymentInstruction.getPayment();
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse paymentSetupResponse = null;
		PaymentSetupResponseInitiation initiation = null;
		DebtorAgent debtorAgent = null;
		DebtorAccount debtorAccount = null;
		CreditorAgent creditorAgent = null;
		CreditorAccount creditorAccount = null;
		RemittanceInformation remittanceInformation = null;
		Risk risk = new Risk();
		RiskDeliveryAddress deliveryAddress = null;
		
		paymentSetupResponse = new PaymentSetupResponse();
		paymentSetupResponse.setPaymentId(payment.getPaymentId());
		if(!NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getStatus())){
			paymentSetupResponse.setStatus(StatusEnum.fromValue(payment.getPaymentInformation().getStatus()));
			paymentSetupPOSTResponse.setPaymentStatus(payment.getPaymentInformation().getStatus());
		}
		
		paymentSetupResponse.setCreationDateTime(timeZoneDateTimeAdapter.parseDateTimeCMA(payment.getCreatedOn()));
		
		if(!NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getJurisdiction())){
			paymentSetupPOSTResponse.setPayerJurisdiction(payment.getPaymentInformation().getJurisdiction());
		}
		
		//Initiation
		initiation = new PaymentSetupResponseInitiation();
		initiation.setInstructionIdentification(payment.getPaymentInformation().getInstructionIdentification());
		initiation.setEndToEndIdentification(paymentInstruction.getEndToEndIdentification());
		
		// could not find initiation.setCurrency  so setting currency in PaymentSetupInitiationInstructedAmount--> currency
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount(payment.getPaymentInformation().getAmount().toString());
		instructedAmount.setCurrency(payment.getPayeeInformation().getBeneficiaryCurrency());
		initiation.setInstructedAmount(instructedAmount);
		psd2Validator.validate(instructedAmount);
		
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation())){
			
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getPayerCurrency())){
				paymentSetupPOSTResponse.setPayerCurrency(payment.getPayerInformation().getPayerCurrency());
			}
			
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getNsc())){
				paymentSetupPOSTResponse.setAccountNSC(payment.getPayerInformation().getNsc());
			}
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getAccountNumber())){
				paymentSetupPOSTResponse.setAccountNumber(payment.getPayerInformation().getAccountNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getIban())){
				paymentSetupPOSTResponse.setAccountIban(payment.getPayerInformation().getIban());
			}
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getBic())){
				paymentSetupPOSTResponse.setAccountBic(payment.getPayerInformation().getBic());
			}
			
			//PaymentSetupResponseInitiation -- > DebtorAgent
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getBic()) 
							&& !NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getIban())){
				debtorAgent = new DebtorAgent();
				debtorAgent.setIdentification(payment.getPayerInformation().getBic());
				debtorAgent.setSchemeName(SchemeNameEnum.BICFI);
				initiation.setDebtorAgent(debtorAgent);
				psd2Validator.validate(debtorAgent);
	        } 
			
			//PaymentSetupResponseInitiation -- > DebtorAccount
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getIban())){ 
				debtorAccount = new DebtorAccount();
				debtorAccount.setIdentification(payment.getPayerInformation().getIban());
				debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
				debtorAccount.setName(payment.getPayerInformation().getAccountName());
				debtorAccount.setSecondaryIdentification(payment.getPayerInformation().getAccountSecondaryID());
				initiation.debtorAccount(debtorAccount);
				psd2Validator.validate(debtorAccount);
		     }
		    else if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getAccountNumber()) && !NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getNsc())){ 
				debtorAccount = new DebtorAccount();
				debtorAccount.setIdentification(payment.getPayerInformation().getNsc()+payment.getPayerInformation().getAccountNumber());
				debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
				debtorAccount.setName(payment.getPayerInformation().getAccountName());
				debtorAccount.setSecondaryIdentification(payment.getPayerInformation().getAccountSecondaryID());
				initiation.debtorAccount(debtorAccount);
				psd2Validator.validate(debtorAccount);
		     }
			
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getMerchantCustomerIdentification()))
				risk.setMerchantCustomerIdentification(payment.getPayerInformation().getMerchantCustomerIdentification());
			
		}

		//PaymentSetupResponseInitiation -- > CreditorAgent
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBic())){
			creditorAgent = new CreditorAgent();
			creditorAgent.setIdentification(payment.getPayeeInformation().getBic());
			creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
			initiation.setCreditorAgent(creditorAgent);
			psd2Validator.validate(creditorAgent);
        } 
		
		//PaymentSetupResponseInitiation -- > CreditorAccount
		creditorAccount = new CreditorAccount();
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBeneficiaryAccountNumber()) && !NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBeneficiaryNsc())){
			creditorAccount.setIdentification(payment.getPayeeInformation().getBeneficiaryNsc()+payment.getPayeeInformation().getBeneficiaryAccountNumber());
			creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		}
		else if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getIban())){       
	    	creditorAccount.setIdentification(payment.getPayeeInformation().getIban());
	    	creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		}
			
		creditorAccount.setName(payment.getPayeeInformation().getBeneficiaryName());
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getAccountSecondaryID()))
			creditorAccount.setSecondaryIdentification(payment.getPayeeInformation().getAccountSecondaryID());
		initiation.creditorAccount(creditorAccount);
		psd2Validator.validate(creditorAccount);
	
		if(!NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getUnstructured()) || !NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getBeneficiaryReference())){
			remittanceInformation = new RemittanceInformation();
			if(!NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getUnstructured()))
				remittanceInformation.setUnstructured(payment.getPaymentInformation().getUnstructured());
			if(!NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getBeneficiaryReference()))
				remittanceInformation.setReference(payment.getPaymentInformation().getBeneficiaryReference());
			initiation.setRemittanceInformation(remittanceInformation);
			psd2Validator.validate(remittanceInformation);
		}
		
		if(!NullCheckUtils.isNullOrEmpty(payment.getPaymentInformation().getPaymentContextCode()))
			risk.setPaymentContextCode(PaymentContextCodeEnum.fromValue(payment.getPaymentInformation().getPaymentContextCode()));
		
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getMerchentCategoryCode()))
			risk.setMerchantCategoryCode(payment.getPayeeInformation().getMerchentCategoryCode());
		
		if(! NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress())){
			deliveryAddress = new RiskDeliveryAddress();
			deliveryAddress.setTownName(payment.getPayeeInformation().getDeliveryAddress().getTownName());
			
			List<String> addressLine = new ArrayList<>();
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getAddressLine1()))
				addressLine.add(payment.getPayeeInformation().getDeliveryAddress().getAddressLine1());
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getAddressLine2()))
				addressLine.add(payment.getPayeeInformation().getDeliveryAddress().getAddressLine2());
			if(!addressLine.isEmpty()){
				deliveryAddress.setAddressLine(addressLine);
			}
	
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getStreetName())){
				deliveryAddress.setStreetName(payment.getPayeeInformation().getDeliveryAddress().getStreetName());
			}
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getBuildingNumber())){
				deliveryAddress.setBuildingNumber(payment.getPayeeInformation().getDeliveryAddress().getBuildingNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getPostCode())){
				deliveryAddress.setPostCode(payment.getPayeeInformation().getDeliveryAddress().getPostCode());
			}
			
			List<String> countrySubDiv = new ArrayList<>();
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getCountrySubDivision1()))
				countrySubDiv.add(payment.getPayeeInformation().getDeliveryAddress().getCountrySubDivision1());
			if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getDeliveryAddress().getCountrySubDivision2()))
				countrySubDiv.add(payment.getPayeeInformation().getDeliveryAddress().getCountrySubDivision2());
			
			if(!countrySubDiv.isEmpty()){
				deliveryAddress.setCountrySubDivision(countrySubDiv);
			}
			
			deliveryAddress.setCountry(payment.getPayeeInformation().getDeliveryAddress().getCountry());
			risk.setDeliveryAddress(deliveryAddress);
			psd2Validator.validate(deliveryAddress);
		}
		
		//fraudnet data setup
		FraudInputs fnInputs = paymentInstruction.getPayment().getPaymentInformation().getFraudInputs();
		if(fnInputs != null){
			FraudServiceResponse fnResponse = new FraudServiceResponse();
			fnResponse.setDecisionType(fnInputs.getSystemResponse());
			paymentSetupPOSTResponse.setFraudnetResponse(fnResponse);	 
		}
		
		paymentSetupPOSTResponse.setRisk(risk);
		psd2Validator.validate(risk);
		paymentSetupPOSTResponse.setData(paymentSetupResponse);
		psd2Validator.validate(paymentSetupResponse);
		paymentSetupPOSTResponse.getData().setInitiation(initiation);
		psd2Validator.validate(initiation);	
			
		return paymentSetupPOSTResponse;
	}
	
}
