package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiaries;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceClientImplTest {
	@InjectMocks
	private AccountBeneficiariesFoundationServiceClientImpl accountBeneficiariesFoundationServiceClientImpl;
	@Mock
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	@Mock
	private DataMask dataMask;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRestTransportForAccountBeneficiaryFS(){
		RequestInfo requestInfo = new RequestInfo();
		Beneficiaries beneficiaries = new Beneficiaries();
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceClientImpl, "maskBeneficiariesResponse", true);
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		queryParams.add("nsc", "123456");
		queryParams.add("accountNumber", "12345678");
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		requestInfo.setUrl("http://localhost:9087/fs-abt-service/services/user/12345678/beneficiaries");
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(beneficiaries);
		Mockito.when(dataMask.maskResponse(anyObject(), anyObject())).thenReturn(beneficiaries);
//		maskResponse is true
		Beneficiaries response = accountBeneficiariesFoundationServiceClientImpl.restTransportForAccountBeneficiaryFS(requestInfo, Beneficiaries.class, queryParams, httpHeaders);
		assertThat(response).isEqualTo(beneficiaries);
//		maskResponse is false
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceClientImpl, "maskBeneficiariesResponse", false);
		response = accountBeneficiariesFoundationServiceClientImpl.restTransportForAccountBeneficiaryFS(requestInfo, Beneficiaries.class, queryParams, httpHeaders);
		assertThat(response).isEqualTo(beneficiaries);
	}
}
