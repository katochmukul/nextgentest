
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants;

public class AccountBeneficiariesFoundationServiceConstants {
	public static final String ACCOUNT_ID = "accountId";
	public static final String NSC = "nsc";
	public static final String ACCOUNT_NUMBER = "accountNumber";
    
}
