
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client;

import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiaries;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public interface AccountBeneficiariesFoundationServiceClient {

	public Beneficiaries restTransportForAccountBeneficiaryFS(RequestInfo requestInfo, Class <Beneficiaries> beneficiaries, MultiValueMap<String, String> params, HttpHeaders httpHeaders);

}
