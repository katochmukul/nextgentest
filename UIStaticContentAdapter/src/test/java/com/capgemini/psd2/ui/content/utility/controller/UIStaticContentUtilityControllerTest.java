package com.capgemini.psd2.ui.content.utility.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

public class UIStaticContentUtilityControllerTest {

	@Mock
	private RestClientSyncImpl restClientSync;

	private MockMvc mockMvc;

	@InjectMocks
	private UIStaticContentUtilityController uIStaticContentUtilityController;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(uIStaticContentUtilityController).dispatchOptions(true).build();
	}

	@Test
	public void testUpdateStaticContentoForUI() throws Exception {
		ReflectionTestUtils.setField(uIStaticContentUtilityController, "uiAppName", "accountinformationv1");
		ReflectionTestUtils.setField(uIStaticContentUtilityController, "profile", "local");
		when(restClientSync.callForGet(any(), any(), any()))
				.thenReturn("spring:" + "/n" + "application:" + "/n" + "name: accountinformationv1");
		this.mockMvc.perform(get("/ui/staticContentUpdate").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetStaticContentForUI() throws Exception {
		this.mockMvc.perform(get("/ui/staticContentGet").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}
}
