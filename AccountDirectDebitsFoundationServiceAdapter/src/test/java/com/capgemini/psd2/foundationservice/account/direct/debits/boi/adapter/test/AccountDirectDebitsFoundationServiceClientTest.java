package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.test;
import static org.junit.Assert.assertNotNull;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client.AccountDirectDebitsFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.delegate.AccountDirectDebitsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.transformer.AccountDirectDebitsFoundationServiceTransformer;

import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

public class AccountDirectDebitsFoundationServiceClientTest {

	/** The delegate. */
	@InjectMocks
	private AccountDirectDebitsFoundationServiceDelegate delegate; 
	
	/** The account information foundation service client. */
	@InjectMocks
	private AccountDirectDebitsFoundationServiceClientImpl accountDirectDebitsFoundationServiceClientImpl;

	/** The account information FS transformer. */
	@Mock
	private AccountDirectDebitsFoundationServiceTransformer accountDirectDebitsFoundationServiceTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The adapterUtility. */
	@Mock
	private AdapterUtility adapterUtility;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void test() {
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		
		DirectDebit directDebit = new DirectDebit();
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(directDebit);
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(" http://localhost:9089/fs-entity-service/services/account");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		DirectDebit res = accountDirectDebitsFoundationServiceClientImpl.restTransportForDirectDebitsFS(requestInfo, DirectDebit.class, httpHeaders);
		assertNotNull(res);
		
	}

}
