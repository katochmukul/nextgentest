package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.AccountDirectDebitsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client.AccountDirectDebitsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.delegate.AccountDirectDebitsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsFoundationServiceAdapterTest {
	@InjectMocks
	private AccountDirectDebitsFoundationServiceAdapter accountDirectDebitsFoundationServiceAdapter;

	@Mock
	private AccountDirectDebitsFoundationServiceDelegate accountDirectDebitsFoundationServiceDelegate;

	@Mock
	private AccountDirectDebitsFoundationServiceClient accountDirectDebitsFoundationServiceClient;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	private Map<String, String> params = new HashMap<String, String>();
	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private HttpHeaders httpHeaders = new HttpHeaders();
	private com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new Accnts();
	private Accnt accnt = new Accnt();
	private AccountGETResponse1 response = new AccountGETResponse1();
	private DirectDebit directDebit = new DirectDebit();
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		params.put("x-channel-id", "BOL");
		params.put("channelId", "BOL");
		params.put(PSD2Constants.CHANNEL_ID, "BOL");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		accnt.setAccountNSC("123456");
		accnt.setAccountNumber("12345678");
		filteredAccounts.getAccount().add(accnt);
		accDet.setAccountId("1234");
		accDet.setAccountNSC("123456");
		accDet.setAccountNumber("12345678");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("12345678");
		accountMapping.setCorrelationId("12345678");
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceAdapter, "consentFlowType", "AISP");
		ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceAdapter, "accountDirectDebitsBaseURL", "http://localhost:9089/fs-entity-service/services/account");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
		Mockito.when(accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL(anyString(), anyString(), anyString())).thenReturn("https://localhost:9089/fs-entity-service/services/account/123456/12345678/directdebits");
		Mockito.when(accountDirectDebitsFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(accountDirectDebitsFoundationServiceClient.restTransportForDirectDebitsFS(anyObject(), anyObject(), anyObject())).thenReturn(directDebit);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(anyObject(), anyObject())).thenReturn(accountMapping);
		Mockito.when(accountDirectDebitsFoundationServiceDelegate.transform(anyObject(), anyObject())).thenReturn(response);
	}
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testRetrieveAccountDirectDebits(){
		AccountGETResponse1 response = accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
		assertThat(response).isEqualTo(this.response);
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingNull(){
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(null, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingPsuIdNull(){
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testParamsNull(){
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, null);
	}
	
	@Test(expected = AdapterException.class)
	public void testfilteredAccountsNull(){
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(null);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testfilteredAccountsAccountNull(){
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new Accnts();
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountDetailsNull(){
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		accountMapping.setAccountDetails(null);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountDetailsEmpty(){
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		List<AccountDetails> list = new ArrayList<>();
		accountMapping.setAccountDetails(list);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
}
