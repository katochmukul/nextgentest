
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public interface AccountDirectDebitsFoundationServiceClient {

	public DirectDebit restTransportForDirectDebitsFS(RequestInfo requestInfo, Class<DirectDebit> response, HttpHeaders httpHeaders);

}
