
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.aisp.transformer.AccountDirectDebitsTransformer;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountDirectDebitsFoundationServiceDelegate {

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationReqHeader;

	/** The platform. */
	@Value("${foundationService.platform}")
	private String platform;

	@Autowired
	private AccountDirectDebitsTransformer accountDirectDebitsTransformer;

	public AccountGETResponse1 transform(DirectDebit directDebit, Map<String, String> params) {
		return accountDirectDebitsTransformer.transformAccountDirectDebits(directDebit, params);
	}

	public String getFoundationServiceURL(String nsc, String accountNumber, String baseURL) {
		if (NullCheckUtils.isNullOrEmpty(nsc) || NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		return baseURL + "/" + nsc + "/" + accountNumber + "/" + "directdebits";
	}

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_NAME));
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeader, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(correlationReqHeader, accountMapping.getCorrelationId());
		return httpHeaders;
	}

}
