
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.Data6;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.aisp.transformer.AccountDirectDebitsTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class AccountDirectDebitsFoundationServiceTransformer implements AccountDirectDebitsTransformer {

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;

	@Override
	public <T> AccountGETResponse1 transformAccountDirectDebits(T source, Map<String, String> params) {
		AccountGETResponse1 accountGETResponse1 = new AccountGETResponse1();
		Data6 data = new Data6();
		data.setDirectDebit(new ArrayList<>());
		accountGETResponse1.setData(data);
		return accountGETResponse1;
	}

}
