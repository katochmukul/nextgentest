/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.filteration;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.bohnman.squiggly.Squiggly;
import com.github.bohnman.squiggly.util.SquigglyUtils;

/**
 * The Class ResponseFilterImpl.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class ResponseFilterImpl implements ResponseFilter {

	/** The filter. */
	private Map<String, Map<String,String>> filter = new HashMap<>();	

	/** The req header attribute. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.filteration.ResponseFilter#filterResponse(java.lang.Object, java.lang.String)
	 */
	@Override
	public <T> T filterResponse(T filterObject, String methodName){
		if(filter==null || filter.isEmpty())
			return filterObject;
		return this.filterResponse(filterObject, methodName,
				reqHeaderAttribute.getClaims().stream()
				.map( Object::toString )
				.collect(Collectors.toList()));
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.filteration.ResponseFilter#filterResponse(java.lang.Object, java.lang.String, java.util.List)
	 */
	@Override
	public <T> T filterResponse(T filterObject, String methodName, List<String> claims) {
		if(filter==null || filter.isEmpty())
			return filterObject;

		if(claims==null || claims.isEmpty())
			return filterObject;

		Map<String, String> codes = filter.get(methodName);

		if(codes==null || codes.isEmpty())
			return filterObject;

		StringBuilder filterString = new StringBuilder();
		for(String claim : codes.keySet()) {
			if(claims.contains(claim)){
				if (!StringUtils.isEmpty(filterString.toString())) {
					filterString.append(PSD2Constants.COMMA);
				}
				filterString.append(codes.get(claim));
			}
		}
		return this.filterMessage(filterObject, filterString.toString());

	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.filteration.ResponseFilter#filterMessage(java.lang.Object, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T filterMessage(T filterObject, String filter) {
		if(filterObject==null){
			return null;
		}
		ObjectMapper mapper = Squiggly.init(new ObjectMapper(), filter);
		try {
			return (T) mapper.readValue(SquigglyUtils.stringify(mapper, filterObject), filterObject.getClass());
		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}


	/**
	 * Gets the filter.
	 *
	 * @return the filter
	 */
	public Map<String, Map<String, String>> getFilter() {
		return filter;
	}

	/**
	 * Sets the filter.
	 *
	 * @param filter the filter
	 */
	public void setFilter(Map<String, Map<String, String>> filter) {
		this.filter = filter;
	}

}
