/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;


/**
 * The Class DateUtilites.
 */
public final class DateUtilites {

	private static final String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	/**
	 * Instantiates a new date utilites.
	 */
	private DateUtilites() {

	}

	/**
	 * Generate current time stamp.
	 *
	 * @return the string
	 */
	public static String generateCurrentTimeStamp() {
		return Instant.now().toString();
	}

	public static Instant generateCurrentTimeStampInInstant() {
		return Instant.now();
	}

	/**
	 * Generate current time stamp.
	 *
	 * @return the string
	 */
	public static String validateAndUpdateDateTime(String dateTime) {

		String updatedDateTime = null;
		if (dateTime == null || dateTime.isEmpty()) {
			return null;
		} else {
			try {
				if (!(':' == dateTime.charAt(22))) {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
				}

				dateTime = dateTime.substring(0, 22).concat(dateTime.substring(23));
				if ('-' == dateTime.charAt(19) && "0000".equals(dateTime.substring(20))) {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
				}

				DateTimeFormatter dtf = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT);
				updatedDateTime = dtf.format(dtf.parse(dateTime));
				updatedDateTime = updatedDateTime.substring(0, 22).concat(":")
						.concat(updatedDateTime.substring(22, 24));
				ZonedDateTime fromDateTime = ZonedDateTime.parse(updatedDateTime,
						DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				ZoneOffset fromOffset = fromDateTime.getOffset();
				if (fromOffset.compareTo(ZoneOffset.of("+14:00")) < 0
						|| fromOffset.compareTo(ZoneOffset.of("-12:00")) > 0) {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
				}
			} catch (StringIndexOutOfBoundsException | DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
		return updatedDateTime;
	}

	public static boolean isDateComparisonPassed(String inputfromDateTime, String inputtoDateTime) {

		boolean dateTimeComparison = false;
		ZonedDateTime fromDateTime = null;
		ZonedDateTime toDateTime = null;

		try {
			fromDateTime = ZonedDateTime.parse(inputfromDateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			toDateTime = ZonedDateTime.parse(inputtoDateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		} catch (DateTimeParseException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.VALIDATION_ERROR);
		}

		if (fromDateTime.isBefore(toDateTime) || fromDateTime.isEqual(toDateTime)) {
			dateTimeComparison = true;
		}

		return dateTimeComparison;
	}

	public static void validateStringInRFCDateFormat(String requestHeader) {
		if (requestHeader != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
				sdf.setLenient(false);
				sdf.format(sdf.parse(requestHeader));
			} catch (ParseException e) {
				throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
	}

	public static void validateStringInISODateTimeWithoutTimeZone(String requestParam) {
		if (requestParam != null) {
			try {
				DateTimeFormatter sdf =DateTimeFormatter.ISO_LOCAL_DATE_TIME;
				sdf.parse(requestParam);
			} catch (DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
	}

	public static String formatMilisecondsToISODateFormat(Long l) {
		SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_TIME_FORMAT);
		sdf.setLenient(false);
		String str = sdf.format(l);
		str = str.substring(0, 22).concat(":").concat(str.substring(22, 24));
		return str;
	}

	public static Date getDateFromISOStringFormat(String isoFormatStringWithoutColonInOffset) {
		isoFormatStringWithoutColonInOffset = isoFormatStringWithoutColonInOffset.substring(0, 22)
				.concat(isoFormatStringWithoutColonInOffset.substring(23, 25));
		SimpleDateFormat format = new SimpleDateFormat(ISO_DATE_TIME_FORMAT);
		Date date = null;
		try {
			date = format.parse(isoFormatStringWithoutColonInOffset);
		} catch (ParseException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.VALIDATION_ERROR);
		}
		return date;
	}

	public static String getCurrentDateInISOFormat() {
		String isoDateTime = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT).format(ZonedDateTime.now());
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}
	public static String getCurrentDateInISOFormat(ZonedDateTime zonedDateTime) {
		String isoDateTime = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT).format(zonedDateTime);
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}

}
