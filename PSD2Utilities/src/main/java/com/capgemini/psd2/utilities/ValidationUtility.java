package com.capgemini.psd2.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
@Component
public  class ValidationUtility {


	private static String uuIdRegexPattern;
	
	@Value("${app.regex.uuId:#{null}}")
	public  void setUuIdRegexPattern(String uuIdRegexPtrn) {
		uuIdRegexPattern = uuIdRegexPtrn;
	}
	 
	public static void isValidUUID(String uuidString) {
		if(uuidString != null && uuIdRegexPattern != null) {
			if(!uuidString.matches(uuIdRegexPattern)) {
				throw PSD2Exception.populatePSD2Exception("The provided input string is not valid UUID",ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
	}
}
