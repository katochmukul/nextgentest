/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

/**
 * The Class PSD2Constants.
 */
public class PSD2Constants {
	
	//** The Constant CORRELATION_ID. *//*
	
	
	public static final String CORRELATION_ID = "x-fapi-interaction-id";
	
	/** The Constant ACCESS_TOKEN_NAME. */
	public static final String ACCESS_TOKEN_NAME = "Authorization";
    
    /** The Constant COLON. */
    public static final String COLON = " : ";
    
    /** The Constant SEMI_COLON. */
    public static final String SEMI_COLON = ";";
    
    /** The Constant COMMA. */
    public static final String COMMA = ",";
    
    /** The Constant CUSTOMER_IP_ADDRESS. */
    public static final String CUSTOMER_IP_ADDRESS="x-fapi-customer-ip-address";
    
    /** The Constant CUSTOMER_LAST_LOGGED_TIME. */
    public static final String CUSTOMER_LAST_LOGGED_TIME="x-fapi-customer-last-logged-time";
    
    /** The Constant FINANCIAL_ID. */
    public static final String FINANCIAL_ID="x-fapi-financial-id";
    
    public static final String TOKEN_INTROSPECTION_DATA = "x-token-introspection-data";
    
    public static final String EQUAS = "=";
    
    public static final String QUESTIONMARK = "?";
    
    public static final String AMPERSAND ="&";
    
    public static final String OAUTH_ENC_URL = "oAuthEncUrl";
    
    public static final String CONSENT_FLOW_TYPE = "consentFlowType";
    
    public static final String CONSENT_FLOW_TYPE_LOWERCASE = "consentFlowTypeLowerCase";

    public static final String SLASH = "/";
    
    public static final String IDEMPOTENCY_KEY="x-idempotency-key";
    
    public static final String TPP_CID = "tppCID";
    
    public static final String USER_ID= "x-user-id";
    
    public static final String CHANNEL_ID= "x-channel-id";
    
    public static final String CORRELATION_REQ_HEADER = "X-CORRELATION-ID";
    
    public static final String PLATFORM_IN_REQ_HEADER  = "X-BOI-PLATFORM";
    
    public static final String CHANNEL_IN_REQ_HEADER = "X-BOI-CHANNEL";
    
    public static final String USER_IN_REQ_HEADER = "X-BOI-USER";
    
    public static final String PAYMENT_CREATED_ON = "PAYMENT-CREATED-ON";
    
    public static final String PAYMENT_VALIDATION_STATUS = "PAYMENT-VALIDATION-STATUS";
    
    public static final String PAYMENT_SUBMISSION_STATUS = "PAYMENT-SUBMISSION-STATUS";
    
    public static final String PISP_SUBMISSION_AUTHORIZATION_FLOW = "PISP-SUBMISSION-AUTHORIZATION-FLOW";
    
    public static final String PAYER_CURRENCY = "PAYER-CURRENCY";
    
    public static final String PAYER_JURISDICTION = "PAYER-JURISDICTION";
    
    public static final String  JS_MSG = "JAVASCRIPT_ENABLE_MSG";

	public static final String ERROR_MESSAGE = "ERROR_MESSAGES";
	
	public static final String ACCOUNT_NUMBER = "ACCOUNT-NUMBER";
	
	public static final String ACCOUNT_NSC = "ACCOUNT-NSC";
	
	public static final String IBAN = "IBAN";
	
	public static final String BIC = "BICFI";
	
	public static final String CO_RELATION_ID = "correlationId"; 
	
	public static final String SERVER_ERROR_FLAG_ATTR= "serverErrorFlag";
	
    public static final String CHANNEL_NAME = "channelId";
    
    public static final String APPLICATION_NAME = "applicationName";
    
    public static final String OPENID_ACCOUNTS = "openid accounts";
    
    public static final String ACCOUNTS = "accounts";

    public static final String OPENID_PAYMENTS = "openid payments";
    
    public static final String PAYMENTS = "payments";
    
    public static final String AISP = "AISP";
    
    public static final String PISP = "PISP";
    
    public static final String OPEN_ID = "openid";
    
    public static final String FLOWTYPE = "flowType";
	
	public static final String PAGETYPE = "pageType";
	
	public static final String AUTHENTICATIONSTATUS = "authenticationStatus";
													   	
	public static final String CONSENTSTATUS = "consentStatus";												

	public static final String PSUID = "psuId";	
	
	public static final String USERNAME = "userName";
	
	public static final String ACCOUNT_PERMISSION = "ACCOUNT-PERMISSION";
	
	public static final String UICONTENT = "UIContent";

	public static final String CONTENT_SECURITY_POLICY = "Content-Security-Policy";

	public static final String REFERRER_ORIGIN = "referrer origin";
    
    
}
