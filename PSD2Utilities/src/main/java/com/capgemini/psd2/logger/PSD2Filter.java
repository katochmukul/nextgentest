/*******************************************************************************

\ * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.multi.read.request.wrapper.MultiReadHttpServletRequest;
import com.capgemini.psd2.multi.read.request.wrapper.MultiReadHttpServletRequest;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class PSD2Filter.
 */
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class PSD2Filter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PSD2Filter.class);

	/** The req header attribute. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	@Value("${api.internalEndpoint:#{null}}")
	private String internalEndPoint;

	private List<String> claims = new ArrayList<>();

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	/**
	 * Sets the claims.
	 *
	 * @param claims
	 *            the new claims
	 */
	public void setClaims(List<String> claims) {
		this.claims = claims;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(
	 * javax. servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse,
			FilterChain filterChain) throws ServletException, IOException {

		try {
			// If block has been added in reference with the defect # 1150
			// The jackson Objectmapper was accepting multiroot jsons
			// To handle multi-root Request body we have used
			// com.google.gson.JsonParser for PUT and POST Requests
			// And In order to read the http request multiple time a wrapper has
			// been added that
			// creates cache of the current request and then can be used
			// multiple times
			MultiReadHttpServletRequest multiReadServletrequest = null;
			Boolean multiReadServletrequestFlag = Boolean.FALSE;

			if (RequestMethod.POST.toString().equalsIgnoreCase(httpServletRequest.getMethod())
					|| RequestMethod.PUT.toString().equalsIgnoreCase(httpServletRequest.getMethod())) {
				multiReadServletrequestFlag = Boolean.TRUE;
				multiReadServletrequest = new MultiReadHttpServletRequest((HttpServletRequest) httpServletRequest);
				String requestBody = multiReadServletrequest.getReader().lines()
						.collect(Collectors.joining(System.lineSeparator()));
				JSONUtilities.jsonParser(requestBody);
			}

			String currentCorrId = httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID);
			if (NullCheckUtils.isNullOrEmpty(currentCorrId)) {
				currentCorrId = GenerateUniqueIdUtilities.getUniqueId().toString();
				httpServletRequest.setAttribute(PSD2Constants.CORRELATION_ID, currentCorrId);
				reqHeaderAttribute.setCorrelationId(currentCorrId);
				LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", httpServletRequest.getRemoteAddr(),
						httpServletRequest.getRemoteHost(), loggerUtils.populateLoggerData("doFilterInternal"));
			} else {
				if (currentCorrId
						.matches("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")) {
					reqHeaderAttribute.setCorrelationId(currentCorrId);
				} else {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTERACTION_ID_WRONG_PATTERN);
				}
				LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", httpServletRequest.getRemoteAddr(),
						httpServletRequest.getRemoteHost(), loggerUtils.populateLoggerData("doFilterInternal"));
			}

			servletResponse.addHeader(PSD2Constants.CORRELATION_ID, currentCorrId);
			/* Adding missing response security header */
			if (servletResponse.getHeader(PSD2Constants.CONTENT_SECURITY_POLICY) == null)
				servletResponse.addHeader(PSD2Constants.CONTENT_SECURITY_POLICY, PSD2Constants.REFERRER_ORIGIN);
			String requestUri = httpServletRequest.getRequestURI();
			reqHeaderAttribute.setSelfUrl(
					requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
			reqHeaderAttribute.setMethodType(httpServletRequest.getMethod());
			if (internalEndPoint != null && httpServletRequest.getRequestURI().startsWith(internalEndPoint)) {
				filterChain.doFilter(httpServletRequest, servletResponse);
				LOG.info("{\"Exit\":\"{}\",\"{}\"}", "com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"));
				return;
			}

			validateAndSetHeaders(httpServletRequest);

			retrieveAndValidateTokenIntrospectionData(httpServletRequest);

			if(multiReadServletrequestFlag){
				filterChain.doFilter(multiReadServletrequest,servletResponse);
			}else{
				filterChain.doFilter(httpServletRequest,servletResponse);
			}
			LOG.info("{\"Exit\":\"{}\",\"{}\"}", "com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"));

		} catch (PSD2Exception e) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), e.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			servletResponse.setContentType("application/json");
			servletResponse.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
			servletResponse.getWriter().write(JSONUtilities.getJSONOutPutFromObject(e.getErrorInfo()));
		} catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			servletResponse.setContentType("application/json");
			servletResponse.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			servletResponse.getWriter().write(JSONUtilities.getJSONOutPutFromObject(psd2Exception.getErrorInfo()));
		}
	}

	private boolean validateClaims(Token token) {
		boolean valid = Boolean.FALSE;
		if (this.claims == null || this.claims.isEmpty()) {
			return Boolean.TRUE;
		}
		if (token.getClaims() == null || token.getClaims().isEmpty()) {
			return valid;
		}
		for (String allowedClaim : claims) {
			for (Object tokenClaim : reqHeaderAttribute.getClaims()) {
				if (allowedClaim.equalsIgnoreCase(tokenClaim.toString())) {
					// valid = Boolean.TRUE;
					return true;
				}
			}
		}
		return valid;
	}

	/**
	 * Retrieve and Validates Token Introspection Data
	 * 
	 * @param httpServletRequest
	 */
	private void retrieveAndValidateTokenIntrospectionData(HttpServletRequest httpServletRequest) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.logger.PSD2Filter.retrieveAndValidateTokenIntrospectionData()",
				loggerUtils.populateLoggerData("retrieveAndValidateTokenIntrospectionData"));
		String tokenIntrospectionData = httpServletRequest.getHeader(PSD2Constants.TOKEN_INTROSPECTION_DATA);
		if (StringUtils.isBlank(tokenIntrospectionData)) {
			throw PSD2Exception.populatePSD2Exception(
					PSD2Constants.TOKEN_INTROSPECTION_DATA + " is missing in headers.", ErrorCodeEnum.HEADER_MISSING);
		}
		Token token = null;
		if (tokenIntrospectionData != null && !tokenIntrospectionData.isEmpty()) {
			tokenIntrospectionData = tokenIntrospectionData.replace("\\", "");
			token = JSONUtilities.getObjectFromJSONString(tokenIntrospectionData, Token.class);
			reqHeaderAttribute.setAttributes(token);
			boolean valid = validateClaims(token);
			if (!valid) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_REQUIRED_PERMISSION_PRESENT);
			}
		}
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"tokenIntrospectionData\":{}}",
				"com.capgemini.psd2.logger.PSD2Filter.retrieveAndValidateTokenIntrospectionData()",
				loggerUtils.populateLoggerData("retrieveAndValidateTokenIntrospectionData"),
				JSONUtilities.getJSONOutPutFromObject(token));
	}

	/**
	 * Validate and set headers.
	 *
	 * @param httpServletRequest
	 *            the http servlet request
	 */
	private void validateAndSetHeaders(HttpServletRequest httpServletRequest) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.logger.PSD2Filter.validateAndSetHeaders()",
				loggerUtils.populateLoggerData("validateAndSetHeaders"));
		String financialId = httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID);
		if (StringUtils.isBlank(financialId)) {
			throw PSD2Exception.populatePSD2Exception(PSD2Constants.FINANCIAL_ID + " is missing in headers.",
					ErrorCodeEnum.HEADER_MISSING);
		}
		reqHeaderAttribute.setFinancialId(financialId);
		String customerIPAddress = httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS);
		if (customerIPAddress != null) {
			if (!customerIPAddress
					.matches("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
							+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		}

		reqHeaderAttribute.setCustomerIPAddress(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS));

		String customerLastLoggedInTime = httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME);
		DateUtilites.validateStringInRFCDateFormat(customerLastLoggedInTime);
		reqHeaderAttribute.setCustomerLastLoggedTime(customerLastLoggedInTime);

		reqHeaderAttribute.setIdempotencyKey(httpServletRequest.getHeader(PSD2Constants.IDEMPOTENCY_KEY));

		LOG.info(
				"{\"Exit\":\"{}\",\"financialId\":\"{}\",\"customerIPAddress\":\"{}\",\"customerLastLoggedTime\":\"{}\",\"idempotencyKey\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.logger.PSD2Filter.validateAndSetHeaders()", reqHeaderAttribute.getFinancialId(),
				reqHeaderAttribute.getCustomerIPAddress(), reqHeaderAttribute.getCustomerLastLoggedTime(),
				reqHeaderAttribute.getIdempotencyKey(), loggerUtils.populateLoggerData("validateAndSetHeaders"));
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/restart");
		excludeUrlPatterns.add("/health");
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
}
