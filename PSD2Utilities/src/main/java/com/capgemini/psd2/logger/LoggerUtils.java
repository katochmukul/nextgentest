/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.utilities.DateUtilites;

/**
 * The Class LoggerUtils.
 */
@Component
public class LoggerUtils {

	/** The req header attribute. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	/** The logger attribute. */
	@Autowired
	private LoggerAttribute loggerAttribute;
	
	/** The api id. */
	@Value("${spring.application.name}")
	private String apiId;
	
	/**
	 * Populate logger data.
	 *
	 * @return the logger attribute
	 */
	public LoggerAttribute populateLoggerData(String methodName) {
		loggerAttribute.setApiId(apiId);
		loggerAttribute.setCorrelationId(reqHeaderAttribute.getCorrelationId());
		loggerAttribute.setFinancialId(reqHeaderAttribute.getFinancialId());
		loggerAttribute.setCustomerIPAddress(reqHeaderAttribute.getCustomerIPAddress());
		loggerAttribute.setCustomerLastLoggedTime(reqHeaderAttribute.getCustomerLastLoggedTime());
		loggerAttribute.setRequestId(reqHeaderAttribute.getRequestId());
		loggerAttribute.setTppLegalEntityName(reqHeaderAttribute.getTppLegalEntityName());
		loggerAttribute.setTppCID(reqHeaderAttribute.getTppCID());
		loggerAttribute.setPsuId(reqHeaderAttribute.getPsuId());
		loggerAttribute.setPath(reqHeaderAttribute.getSelfUrl());
		loggerAttribute.setMethodType(reqHeaderAttribute.getMethodType());
		loggerAttribute.setMessage(null);
		loggerAttribute.setUpstream_api_id(null);
		loggerAttribute.setUpstream_start_time(null);
		loggerAttribute.setUpstream_end_time(null);
		loggerAttribute.setUpstream_request_payload(null);
		loggerAttribute.setUpstream_response_payload(null);
		loggerAttribute.setOperationName(methodName);
		loggerAttribute.setTimeStamp(DateUtilites.generateCurrentTimeStamp());
		return loggerAttribute;
	}

}
