/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.ValidationUtility;

/**
 * The Class PSD2Filter.
 */
public class InternalPSD2Filter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PSD2Filter.class);

	/** The req header attribute. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;


	/* (non-Javadoc)
	 * @see org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse,
			FilterChain filterChain) throws ServletException, IOException {

		try{
			String currentCorrId = httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID);
			if (NullCheckUtils.isNullOrEmpty(currentCorrId)) {
				currentCorrId = GenerateUniqueIdUtilities.getUniqueId().toString();
				httpServletRequest.setAttribute(PSD2Constants.CORRELATION_ID, currentCorrId);
				reqHeaderAttribute.setCorrelationId(currentCorrId);
				LOG.info("{\"Enter\":\"{}\",\"preCorrelationId\":\"Not Found\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",httpServletRequest.getRemoteAddr(), httpServletRequest.getRemoteHost(), loggerUtils.populateLoggerData("doFilterInternal"));
			} else {
				ValidationUtility.isValidUUID(currentCorrId);
				reqHeaderAttribute.setCorrelationId(currentCorrId);
				LOG.info("{\"Enter\":\"{}\",\"preCorrelationId\":\"Found\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()",httpServletRequest.getRemoteAddr(), httpServletRequest.getRemoteHost(), loggerUtils.populateLoggerData("doFilterInternal"));
			}
			
			servletResponse.addHeader(PSD2Constants.CORRELATION_ID, currentCorrId);
/*			reqHeaderAttribute.setSelfUrl(httpServletRequest.getRequestURI());
*/			filterChain.doFilter(httpServletRequest, servletResponse);
			LOG.info("{\"Exit\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", loggerUtils.populateLoggerData("doFilterInternal"));

		} 
		catch (PSD2Exception e) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}" ,
					"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", loggerUtils.populateLoggerData("doFilterInternal"), e.getErrorInfo());
			if(LOG.isDebugEnabled()){
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}" ,
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			servletResponse.setContentType("application/json");
			servletResponse.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
			servletResponse.getWriter().write(JSONUtilities.getJSONOutPutFromObject(e.getErrorInfo()));
		} catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}" ,
					"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if(LOG.isDebugEnabled()){
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}" ,
						"com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()", loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			servletResponse.setContentType("application/json");
			servletResponse.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			servletResponse.getWriter().write(JSONUtilities.getJSONOutPutFromObject(psd2Exception.getErrorInfo()));
		}
	}

	

/*	*//**
	 * Convert by refto token.
	 *
	 * @param token the token
	 * @return the map
	 *//*
	private Token convertByReftoToken(String token){
		try{
			Jwt jwt = JwtHelper.decode(token);
			return JSONUtilities.getObjectFromJSONString(jwt.getClaims(),Token.class);
		} catch(Exception ex){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_ACCESS_TOKEN);
		}
	}
*/
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/restart");
		excludeUrlPatterns.add("/health");
		AntPathMatcher matcher = new AntPathMatcher();
	    return excludeUrlPatterns.stream()
	    		.anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
}
