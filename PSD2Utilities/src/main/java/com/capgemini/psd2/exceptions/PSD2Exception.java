/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

/**
 * The Class PSD2Exception.
 */
public class PSD2Exception extends RuntimeException{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The error info. */
	private final ErrorInfo errorInfo;
	
	/**
	 * Instantiates a new PSD 2 exception.
	 *
	 * @param message the message
	 * @param errorInfo the error info
	 */
	public PSD2Exception(String message, ErrorInfo errorInfo){
		super(message);
		this.errorInfo = errorInfo;
	}
	
	
	/**
	 * Gets the error info.
	 *
	 * @return the error info
	 */
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}
	
	/**
	 * Populate PSD 2 exception.
	 *
	 * @param detailErrorMessage the detail error message
	 * @param errorCodeEnum the error code enum
	 * @return the PSD 2 exception
	 */
	public static PSD2Exception populatePSD2Exception(String detailErrorMessage,ErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), detailErrorMessage, errorCodeEnum.getStatusCode());
		return new PSD2Exception(detailErrorMessage, errorInfo);
	}
	
	public static PSD2Exception populateOAuth2Exception(OAuthErrorInfo errorInfo){
		return new PSD2Exception(errorInfo.getErrorMessage(),errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum the error code enum
	 * @return the PSD 2 exception
	 */
	public static PSD2Exception populatePSD2Exception(ErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		return new PSD2Exception(errorCodeEnum.getDetailErrorMessage(), errorInfo);
	}	
	
}
