package com.capgemini.psd2.exceptions;

import java.util.Map;

public class FraudSystemException extends PSD2Exception {

	private Map<String, String> params;

	public FraudSystemException(String message, ErrorInfo errorInfo, Map<String, String> params) {
		super(message, errorInfo);
		// TODO Auto-generated constructor stub
		this.params = params;
	}

	public Map<String, String> getParams() {
		return params;
	}

}
