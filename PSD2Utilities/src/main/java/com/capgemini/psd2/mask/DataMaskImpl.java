/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.mask;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;

/**
 * The Class DataMaskImpl.
 */
@Component
public class DataMaskImpl implements DataMask {

	/** The data mask rules. */
	private DataMaskRules dataMaskRules;

	/** The cresponse. */
	private Map<String, Map<String, String>> cresponse;

	/** The cresponse log. */
	private Map<String, Map<String, String>> cresponseLog;

	/** The crequest log. */
	private Map<String, Map<String, String>> crequestLog;

	/**
	 * Instantiates a new data mask impl.
	 *
	 * @param dataMaskRules
	 *            the data mask rules
	 */
	@Autowired
	public DataMaskImpl(DataMaskRules dataMaskRules) {
		super();
		this.cresponse = new HashMap<>();
		this.cresponseLog = new HashMap<>();
		this.crequestLog = new HashMap<>();
		this.cresponse.putAll(dataMaskRules.getResponse());
		this.cresponse = merge(this.cresponse, dataMaskRules.getMresponse());
		this.cresponseLog.putAll(dataMaskRules.getResponseLog());
		this.cresponseLog = merge(this.cresponseLog, dataMaskRules.getMresponseLog());
		this.crequestLog.putAll(dataMaskRules.getRequestLog());
		this.crequestLog = merge(this.crequestLog, dataMaskRules.getMrequestLog());
		this.dataMaskRules = dataMaskRules;
	}

	/**
	 * Merge.
	 *
	 * @param map1
	 *            the map 1
	 * @param map2
	 *            the map 2
	 * @return the map
	 */
	private Map<String, Map<String, String>> merge(Map<String, Map<String, String>> map1,
			Map<String, Map<String, String>> map2) {
		for (String key : map2.keySet()) {
			if (map1.containsKey(key))
				map1.get(key).putAll(map2.get(key));
			else
				map1.put(key, map2.get(key));
		}
		return map1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#maskRequestLog(java.lang.Object,
	 * java.lang.String)
	 */
	@Override
	public <T> T maskRequestLog(T input, String method) {
		return mask(input, this.crequestLog.get(method), MaskOutput.NEW);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#maskResponseLog(java.lang.Object,
	 * java.lang.String)
	 */
	@Override
	public <T> T maskResponseLog(T input, String method) {
		return mask(input, this.cresponseLog.get(method), MaskOutput.NEW);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#maskResponse(java.lang.Object,
	 * java.lang.String)
	 */
	@Override
	public <T> T maskResponse(T input, String method) {
		return mask(input, this.cresponse.get(method), MaskOutput.UPDATE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#maskMResponse(java.lang.Object,
	 * java.lang.String)
	 */
	@Override
	public <T> T maskMResponse(T input, String method) {
		return mask(input, dataMaskRules.getMresponse().get(method), MaskOutput.UPDATE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#maskMRequestLog(java.lang.Object,
	 * java.lang.String)
	 */
	@Override
	public <T> T maskMRequestLog(T input, String method) {
		return mask(input, dataMaskRules.getMrequestLog().get(method), MaskOutput.NEW);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#maskMResponseLog(java.lang.Object,
	 * java.lang.String)
	 */
	@Override
	public <T> T maskMResponseLog(T input, String method) {
		return mask(input, dataMaskRules.getMresponseLog().get(method), MaskOutput.NEW);
	}

	@Override
	public <T> String maskResponseGenerateString(T input, String method) {
		return mask(input, this.cresponse.get(method));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.mask.DataMask#mask(java.lang.Object,
	 * java.util.Map, com.capgemini.psd2.mask.MaskOutput)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T mask(T input, Map<String, String> rules, MaskOutput maskOutput) {
		if (rules == null || rules.isEmpty())
			return input;

		String jsonString = mask(input, rules);

		if (maskOutput == MaskOutput.UPDATE)
			return JSONUtilities.getObjectFromJSONString(jsonString, input);
		else
			return (T) JSONUtilities.getObjectFromJSONString(jsonString, input.getClass());
	}

	private <T> String mask(T input, Map<String, String> rules) {
		if (rules == null || rules.isEmpty())
			return JSONUtilities.getJSONOutPutFromObject(input);

		DocumentContext response = JsonPath.parse(JSONUtilities.getJSONOutPutFromObject(input));
		Object value;
		for (Map.Entry<String, String> entry : rules.entrySet()) {
			String key = entry.getKey().replaceAll("\\|(.*?)\\|", "[$1]");
			try {
				value = response.read(key);
				if (!(value instanceof List<?>)) {
					response.set(key, callReplaceWithMask(value, entry.getValue()));
				} else {
					maskListOfData(response, key, entry.getValue());
				}
			} catch (Exception ex) {
				// TO DO
			}

		}
		return response.jsonString();
	}

	/**
	 * Call replace with mask.
	 *
	 * @param value
	 *            the value
	 * @param regex
	 *            the regex
	 * @return the object
	 */
	private Object callReplaceWithMask(Object value, String regex) {
		Class<?> clazz = null;
		if (value instanceof String) {
			clazz = String.class;
		} else if (value instanceof Integer) {
			clazz = Integer.class;
		} else if (value instanceof Double) {
			clazz = Double.class;
		} else
			clazz = String.class;
		if (NullCheckUtils.isNullOrEmpty(value))
			return value;
		String stringToBeMasked = value.toString();
		if (stringToBeMasked.length() == 0) {
			return stringToBeMasked;
		}
		if (!StringUtils.isEmpty(regex)) {
			for (String r : regex.replace("\\|", "|").split("\\|"))
				stringToBeMasked = stringToBeMasked.replaceAll(r.split(",")[0], r.split(",")[1]);
		}
		return convert(clazz, stringToBeMasked);
	}

	/**
	 * Convert.
	 *
	 * @param targetType
	 *            the target type
	 * @param text
	 *            the text
	 * @return the object
	 */
	private Object convert(Class<?> targetType, String text) {
		PropertyEditor editor = PropertyEditorManager.findEditor(targetType);
		editor.setAsText(text);
		return editor.getValue();
	}

	/**
	 * Mask list of data.
	 *
	 * @param response
	 *            the response
	 * @param jsonPath
	 *            the json path
	 * @param regex
	 *            the regex
	 */
	private void maskListOfData(DocumentContext response, String jsonPath, String regex) {
		response.configuration().addOptions(Option.AS_PATH_LIST);
		Configuration conf = Configuration.builder().options(Option.AS_PATH_LIST).build();
		DocumentContext context = JsonPath.using(conf).parse(response.jsonString());
		List<String> pathList = context.read(jsonPath);
		for (String path : pathList) {
			Object value = response.read(path);
			response.set(path, callReplaceWithMask(value, regex));
		}
	}

}
