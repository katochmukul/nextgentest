/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class JSONUtilitiesTest.
 */
public class JSONUtilitiesTest {

	/** The json entity. */
	private JSONEntity jsonEntity;

	/** The mapper. */
	@Mock
	private ObjectMapper mapper;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		jsonEntity = new JSONEntity();
	}

	/**
	 * Test get JSON out put from object.
	 */
	@Test
	public void testGetJSONOutPutFromObject() {
		jsonEntity.setId("1");
		assertNotEquals(jsonEntity.getId(), JSONUtilities.getJSONOutPutFromObject(jsonEntity));
	}

	/**
	 * Test get JSON out put from object with exception.
	 */
	@Test
	public void testGetJSONOutPutFromObjectWithException() {
		jsonEntity.setId("1");
		assertNotEquals(jsonEntity.getId(), JSONUtilities.getJSONOutPutFromObject(jsonEntity));
	}

	/**
	 * Test get object from JSON string.
	 */
	@Test
	public void testGetObjectFromJSONString() {
		jsonEntity.setId("11");
		Object obj = new Object();
		String jsonObj = JSONUtilities.getJSONOutPutFromObject(jsonEntity);
		assertNotEquals(obj, JSONUtilities.getObjectFromJSONString(jsonObj, jsonEntity.getClass()));
	}

	/**
	 * Test get object from JSON str.
	 */
	@Test
	public void testGetObjectFromJSONStr(){
		jsonEntity.setId("11");
		String jsonObj=JSONUtilities.getJSONOutPutFromObject(jsonEntity);
		assertEquals(jsonEntity, JSONUtilities.getObjectFromJSONString(jsonObj, jsonEntity));
	}

	@Test
	public void testGetObjectWithMapper(){
		jsonEntity.setId("11");
		String jsonObj=JSONUtilities.getJSONOutPutFromObject(jsonEntity);
		mapper = new ObjectMapper();
		assertEquals(jsonEntity, JSONUtilities.getObjectFromJSONString(mapper,jsonObj, jsonEntity));
	}

	@Test
	public void testGetObjectWithMapperClass(){
		jsonEntity.setId("11");
		Object obj = new Object();
		String jsonObj=JSONUtilities.getJSONOutPutFromObject(jsonEntity);
		mapper = new ObjectMapper();
		assertNotEquals(obj, JSONUtilities.getObjectFromJSONString(mapper,jsonObj, jsonEntity.getClass()));
	}

	@Test
	public void testgetJSONOutPutFromArgsObject(){
		String[] str = new String[]{"A","B"};
		assertNotNull(JSONUtilities.getJSONOutPutFromArgsObject(str));
	}
	
	@Test
	public void testgetJSONOutPutFromArgs(){
		MockHttpServletRequest req = new MockHttpServletRequest(RequestMethod.GET.toString(), "/test");
		MockHttpServletRequest[] requests = new MockHttpServletRequest[]{req};
		assertNotNull(JSONUtilities.getJSONOutPutFromArgsObject(requests));
	}
	
	@Test
	public void testJsonParser(){
		JSONUtilities.jsonParser("{ \"brand\" : \"Mercedes\"}");
	}
	
	@Test(expected = PSD2Exception.class)
	public void testJsonParserException(){
		JSONUtilities.jsonParser("{ \"brand\" : \"Mercedes\"}{ \"brand\" : \"Mercedes\"}");
	}
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		jsonEntity = null;
		mapper = null;
	}
	
	
}
