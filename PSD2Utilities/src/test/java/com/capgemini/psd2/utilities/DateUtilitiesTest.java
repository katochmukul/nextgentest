/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;

import org.junit.Test;

import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class DateUtilitesTest.
 */
public class DateUtilitiesTest {

	
	/**
	 * Test.
	 */
	@Test
	public void test() {
		assertNotNull(DateUtilites.generateCurrentTimeStamp());
	}
	
	
	@Test
	public void testDateTimeComparison() {
		
		String dateTime = "2015-07-28T12:36:46+00:00";
		boolean isTrue = DateUtilites.isDateComparisonPassed(dateTime,DateUtilites.getCurrentDateInISOFormat());
		assertTrue(isTrue);
		
	}
	
	@Test
	public void testDateTimeComparisonOldDate() {
		String dateTime = "2015-07-28T12:36:46+00:00";
		boolean isFalse = DateUtilites.isDateComparisonPassed(DateUtilites.getCurrentDateInISOFormat(), dateTime);
		assertFalse(isFalse);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testDateTimeComparisonException() {
		String dateTime = "2015-07-28T12:36:46+05:00";
		DateUtilites.isDateComparisonPassed(ZonedDateTime.now().toString(), dateTime);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateAndUpdateIncorrectDateTimeFormatException() {
		String dateTime = "2015-07-28T12:36:46+000";
		DateUtilites.validateAndUpdateDateTime(dateTime);
	}
	
	@Test
	public void testValidateAndUpdate() {
		String dateTime = "2015-07-28T12:36:46+00:00";
		DateUtilites.validateAndUpdateDateTime(dateTime);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testDateTimeInvalidDateFormatException() {
		String dateTime = "2015-07-28T12:36:46-00:00";
		DateUtilites.validateAndUpdateDateTime(dateTime);
		
	}
	
	@Test
	public void testNullDateTimeException() {
		String dateTime=null;
		assertNull(DateUtilites.validateAndUpdateDateTime(dateTime));
	}
	
	@Test(expected = PSD2Exception.class)
	public void testIncorrectOffsetDateTimeException() {
		String dateTime = "2015-07-28T12:36:46-13:00";
		DateUtilites.validateAndUpdateDateTime(dateTime);
	}
	
	@Test
	public void testvalidateStringInISODateTimeWithoutTimeZone() {
		String dateTime = "2015-07-28T12:36:46.";
		DateUtilites.validateStringInISODateTimeWithoutTimeZone(dateTime);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testvalidateStringInISODateTimeWithoutTimeZoneException() {
		String dateTime = "2015-12-01T00:00:00.1234Z";
		DateUtilites.validateStringInISODateTimeWithoutTimeZone(dateTime);
	}

	@Test
	public void testgetCurrentDateInISOFormat() {
		DateUtilites.getCurrentDateInISOFormat();
	}
	
	@Test
	public void testgetCurrentDateInISOFormatWithArgs() {
		DateUtilites.getCurrentDateInISOFormat(ZonedDateTime.now());
	}
	@Test
	public void testformatMilisecondsToISODateFormat() {
		Long l = Long.parseLong("1511526507");
		DateUtilites.formatMilisecondsToISODateFormat(l);
	}
	
	@Test
	public void testgetDateFromISOStringFormat() {
		String str = "2015-07-28T12:36:46+00:00";
		DateUtilites.getDateFromISOStringFormat(str);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testgetDateFromISOStringFormatException() {
		String dateTime = "2015-07-28T12:36:46+0:000";
		DateUtilites.getDateFromISOStringFormat(dateTime);
	}
	
	
	

}
