/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;

/**
 * The Class RequestHeaderAttributesTest.
 */
public class RequestHeaderAttributesTest {

	/** The request correlation. */
	private RequestHeaderAttributes requestCorrelation = null;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		requestCorrelation = new RequestHeaderAttributes();
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
	}

	/**
	 * Test to string.
	 */
	@Test
	public void testToString() {
		assertNotNull(requestCorrelation.toString());
	}

	/**
	 * Test equals.
	 */
	@Test
	public void testEquals() {
		assertTrue(requestCorrelation.equals(requestCorrelation));
	}

	/**
	 * Test request header attribute.
	 */
	@Test
	public void testRequestHeaderAttribute() {
		// String token = "Bearer
		// eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cHBJbmZvLWxlZ2FsRW50aXR5TmFtZSI6Ik1vbmV5d2lzZSBQdnQuIEx0ZC4iLCJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiYmFua0lkIjoiMSIsImNvbnNlbnRJZCI6IjY3MTE4MjkxIiwidHBwSW5mby1yZWdpc3RlcmVkSWQiOiIwMDciLCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsInRwcEluZm8tZGlzcGxhdE5hbWUiOiJNb25leXdpc2UiLCJleHAiOjE0OTEwNDQ3MDcsImF1dGhvcml0aWVzIjpbIlJPTEVfTU9ORVlXSVNFIiwiUk9MRV9DVVNUT01FUlMiXSwianRpIjoiZWUwMzE4NTktYmIxOC00ZWNiLWJmNzAtMTJmYTMzOTYyMDJkIiwiY2xpZW50X2lkIjoibW9uZXl3aXNlIiwidHBwSW5mby10cHBUeXBlIjoiYWlzcCJ9.knD_aMYrmVJzQa8Jc5ypOWoPRU5TCkAEGSdhLOpXfqYLNyqB9QyUfIAb2PfnzZbwTRIsJ7T1rFV5l7779ktVKSv1OGw853J_8y3jUa2jfY7sFVnZxS4PLK17VnzaXUhFXT3L5R2PKSWlGYocLZkOb-BW9Go1L_nTzcx0xsCz5TaOPXh3xih_Bduzo15fu5JGMMVQKaBTd0x_YEtHB25cT57cJnbjrqYObMO0hh6WBkbA8fC4xePan3jyZsyjomVuWOGaS4xJBqhEL_TK5fVng7O1SlLHGkQYzRRrbg7pOL7W2AyJGkMV9BcUgIkKw7L5gPceB1IjL3ug4bPN9DRSXQ";

		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Token token = new Token();
		token.setClient_id("tppCID");
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("etity1");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		Set<String> tppRoles =new HashSet<>();
		tppRoles.add("AISP");
		tppRoles.add("PISP");
		tppRoles.add("CISP");
		TppInformationTokenData.setTppRoles(tppRoles);
		token.setTppInformation(TppInformationTokenData);
		token.setUser_name("1234");
		requestCorrelation.setAttributes(token);
		requestCorrelation.setToken(token);
		Set<Object> claims = new HashSet<>();
		claims.add(new Object());
		requestCorrelation.setClaims(claims);
		requestCorrelation.setCustomerIPAddress("10.1.1.23");
		requestCorrelation.setCustomerLastLoggedTime("10-10-2017T10:10");
		requestCorrelation.setFinancialId("finance1234");
		
		requestCorrelation.setSelfUrl("https://localhost:8080/accounts");

		assertEquals("https://localhost:8080/accounts", requestCorrelation.getSelfUrl());
		assertEquals("ba4f73f8-9a60-425b-aed8-2a7ef2509fea", requestCorrelation.getCorrelationId());
		assertEquals("tppCID", requestCorrelation.getTppCID());
		assertEquals("etity1", requestCorrelation.getTppLegalEntityName());
		assertEquals("1234", requestCorrelation.getPsuId());
		assertEquals(claims, requestCorrelation.getClaims());
		assertEquals("10.1.1.23", requestCorrelation.getCustomerIPAddress());
		assertEquals("finance1234", requestCorrelation.getFinancialId());
		assertEquals(token, requestCorrelation.getToken());
		assertEquals("10-10-2017T10:10", requestCorrelation.getCustomerLastLoggedTime());
		assertTrue(null != requestCorrelation.getRequestId());

	}

	@Test(expected=PSD2Exception.class)
	public void testRequestHeaderAttributeSetAttrbutePSD2Exception() {
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Token token = new Token();
		token.setUser_name("1234");
		requestCorrelation.setAttributes(token);
	}
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		requestCorrelation = null;
	}
}
