/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.capgemini.psd2.aspect.PSD2AspectUtils;

/**
 * The Class PSD2LoggerAspectTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class PSD2LoggerAspectTest {

	/** The join point. */
	@Mock
	private ProceedingJoinPoint joinPoint;

	/** The signature. */
	@Mock
	private  MethodSignature signature;

	/** The request. */
	@Mock
	private HttpServletRequest request;

	/** The attribute. */
	@Mock
	private LoggerAttribute attribute;

	/** The logger utils. */
	@Mock
	private LoggerUtils loggerUtils;
	
	/** The req attribute. */
	@Mock
	private RequestHeaderAttributes reqAttribute;

	/** The logger aspect. */
	@InjectMocks
	private PSD2AspectUtils loggerAspect = new PSD2AspectUtils();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test
	public void test() throws Throwable {
		String[] str = {"10"};
		when(joinPoint.getTarget()).thenReturn("accountService");
		when(joinPoint.getSignature()).thenReturn(signature);
		when(joinPoint.getArgs()).thenReturn(str);
		loggerAspect.methodAdvice(joinPoint);
	}
	

}
