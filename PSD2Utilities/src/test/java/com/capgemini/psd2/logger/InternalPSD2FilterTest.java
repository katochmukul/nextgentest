package com.capgemini.psd2.logger;

import static org.assertj.core.api.Assertions.doesNotHave;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternalPSD2FilterTest {
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private FilterChain filterChain;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAttribute;
	
	@InjectMocks
	private InternalPSD2Filter filter = new InternalPSD2Filter();
	
	@Test
	public void doFilterInternalWithCorrelatioIdNotNullTest() throws ServletException, IOException{
		when(request.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("12345");
		filter.doFilterInternal(request, response, filterChain);
	}
	
	@Test
	public void doFilterInternalWithCorrelatioIdNullTest() throws ServletException, IOException{
		filter.doFilterInternal(request, response, filterChain);
	}
	
	@Test
	public void doInternalFilterExceptionTest() throws ServletException, IOException{
		when(response.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		filter.doFilterInternal(null, response, filterChain);
	}
	
	@Test
	public void shouldNotFilter() throws ServletException{
		when(request.getServletPath()).thenReturn("12345");
		filter.shouldNotFilter(request);
	}
	
	
}
