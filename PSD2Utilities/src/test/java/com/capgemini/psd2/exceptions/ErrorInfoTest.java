/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * The Class ErrorInfoTest.
 */
public class ErrorInfoTest {

	/**
	 * Test.
	 */
	@Test
	public void test() {
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.setJti("jti");
		errorInfo.setOauthServerTokenRefreshURL("htps://localhost:9001/oAuthServer");
		ReflectionTestUtils.setField(errorInfo, "sendErrorPayload", false);
		ReflectionTestUtils.setField(errorInfo, "sendDetailErrorMessage", true);
		assertEquals("detail error message", errorInfo.getDetailErrorMessage());
		assertEquals(null, errorInfo.getErrorCode());
		assertEquals(null, errorInfo.getErrorMessage());
		assertEquals("jti", errorInfo.getJti());
		assertEquals("htps://localhost:9001/oAuthServer", errorInfo.getOauthServerTokenRefreshURL());
	}
	
	@Test
	public void testErrorWithArg() {
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "500");
		assertEquals("500", errorInfo.getStatusCode());
		assertEquals("error code", errorInfo.getErrorCode());
		assertEquals("error message", errorInfo.getErrorMessage());
	}

}
