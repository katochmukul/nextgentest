
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class AuthenticationApplicationFoundationServiceDelegate {

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationIdInReqHeader:#{X-CORRELATION-ID}}")
	private String correlationIdInReqHeader;

	@Value("${app.platform}")
	private String platform;

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(AdapterSecurityConstants.USER_HEADER));
		httpHeaders.add(channelInReqHeader, params.get(AdapterSecurityConstants.CHANNELID_HEADER));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationIdInReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		
		return httpHeaders;
	}

}
