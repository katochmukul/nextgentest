package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.AuthenticationRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationApplicationFoundationServiceClientTest {
	
	@InjectMocks
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;
	
	@Mock
	private RestClientSyncImpl restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void restTransportForAuthenticationApplication(){
	
	PrincipalImpl principal = new PrincipalImpl("boi123");
	CredentialsImpl credentials = new CredentialsImpl("1234");
	AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
	
	AuthenticationRequest authenticationRequest = new AuthenticationRequest();
	authenticationRequest.setUserName(authentication.getPrincipal().toString());
	authenticationRequest.setPassword(authentication.getCredentials().toString());
	
	HttpHeaders httpHeaders = new HttpHeaders();
	httpHeaders.add("X-BOI-USER", "header user");
	httpHeaders.add("X-BOI-CHANNEL", "header channel");
	httpHeaders.add("X-BOI-PLATFORM", "header platform");
	httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
	
	RequestInfo requestInfo = new RequestInfo();
	requestInfo.setUrl("http://localhost:8081/fs-login-business-service/services/loginServiceBusiness/channel/business/login");
	
	Mockito.when(restClient.callForPost(any(),any(),any(),any())).thenReturn("Successfully authenticated");
	
	String response = authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, authenticationRequest, String.class, httpHeaders);
	
	assertNotNull(response);
	
	}
}
