package com.capgemini.psd2.tppinformation.adaptor.ldap.impl;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.tppinformation.adaptor.ldap.model.ClientModel;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

@Component
public class TPPInformationAdaptorLDAPImpl implements TPPInformationAdaptor{

	@Autowired
	private LdapTemplate ldapTemplate;
	
	@Autowired
	private RestClientSync restClientSync;

	@Value("${pfclientreg.adminpwd:null}")
	private String adminpwd;
	
	@Value("${pf.prefix:null}")
	private String prefix;
	
	@Value("${pf.adminuser:null}")
	private String adminuser;
	
	@Value("${pf.clientByIdUrl:null}")
	private String clientByIdUrl;
	
	
	
	@Value("${ldap.clientAppgroup.basedn}")
	private String clientAppgroupBaseDn;
	
	@Value("${ldap.tppgroup.basedn}")
	private String tppgroupBaseDN;
	
	
	@Override
	public Object fetchTPPInformation(String clientId){
		
		String tppUniqueMember = null;
		
		List<String> uniqueMemberString = null;
		
		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes("*", "+").base(clientAppgroupBaseDn)
				.filter("(cn=" + clientId + ")");
		
		
		List<Object> tppAppDetails = ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});
		if(tppAppDetails == null || tppAppDetails.isEmpty()){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}
		
		uniqueMemberString = TPPInformationAdaptorLDAPImpl.getBasicAttributes((BasicAttributes) tppAppDetails.get(0),
				TPPInformationConstants.UNIQUE_MEMBER);
		
		for (String uniqueMemberDn : uniqueMemberString) {
			if (uniqueMemberDn.contains(tppgroupBaseDN)) {
				tppUniqueMember = uniqueMemberDn;
				break;
			}
		}
		

		Object tppDetails = ldapTemplate.lookup(tppUniqueMember, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});		
		
		if(tppDetails == null){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
		}
		return tppDetails;
	}
	
	public static List<String> getBasicAttributes(BasicAttributes object, String ldapAttr) {
		String[] str;
		List<String> retrunValue = null;
		try {
			if (object.get(ldapAttr) != null && object.get(ldapAttr).get() != null) {
				str = object.get(ldapAttr).toString().split(":");
				retrunValue = Arrays.asList(str[1].split(", "));
			}
		} catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
		}
		return retrunValue;
	}
	
	@Override
	public String fetchApplicationName(String clientId) {
		RequestInfo requestInfo = new RequestInfo();
		String pfOAuthClientByIdUrl = prefix + clientByIdUrl; ;
		pfOAuthClientByIdUrl = pfOAuthClientByIdUrl.replaceAll("\\{clientId\\}", clientId);
		requestInfo.setUrl(pfOAuthClientByIdUrl);
		HttpHeaders httpHeaders = populatePFHttpHeaders();
		ClientModel model= restClientSync.callForGet(requestInfo, ClientModel.class, httpHeaders);
		return model.getName();
	}
	
	
	
	
	private HttpHeaders populatePFHttpHeaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());
		requestHeaders.add("Authorization", "Basic " + encodeCredentials());
		return requestHeaders;
	}
	
	public  String encodeCredentials() {
		String credentials = adminuser.concat(":").concat(adminpwd);
		return Base64.getEncoder().encodeToString(credentials.getBytes());
	} 
}
