package com.capgemini.psd2.tppinformation.adaptor.ldap.constants;

public class TPPInformationConstants {
	public static final String UNIQUE_MEMBER = "uniqueMember";
	public static final String CN = "cn";
	public static final String LEGAL_ENTITY_NAME = "o";
	public static final String X_ROLES = "x-roles";
	public static final String X_BLOCK = "x-block";
}
