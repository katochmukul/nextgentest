package com.capgemini.psd2.integration.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.config.TokenTransformationConfigBean;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.integration.service.TokenTransformationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;

@RestController
public class TokenTransformationAPI {

	@Autowired
	private TokenTransformationConfigBean configBean;

	@Autowired
	private TokenTransformationService tokenTransformationService;
	
	@Autowired
	private TPPInformationService tppInformationService;
	
	@Autowired
	private HttpServletRequest request;
	

	@RequestMapping(value = "/transformToken/{intentId}", method = RequestMethod.GET)
	public Token transformToken(@PathVariable String intentId) {
		TPPInformationDTO tppInformationDTO = null;
		TppInformationTokenData tppInformationTokenData = null;
		Token token = new Token();

		String scopes = request.getHeader("scope");
		String clientId = request.getHeader("client_id");

		if (clientId == null || clientId.trim().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CLIENT_ID_MISSING_IN_REQUEST);
		}
		token.setClient_id(clientId);
		token.setRequestId(intentId);
		
		if (scopes != null && (scopes.equalsIgnoreCase(PSD2Constants.OPENID_ACCOUNTS) || scopes.equalsIgnoreCase(PSD2Constants.ACCOUNTS))) {			
			tokenTransformationService.transformAISPToken(token,intentId);
		}
		else if(scopes != null && (scopes.equalsIgnoreCase(PSD2Constants.OPENID_PAYMENTS) || scopes.equalsIgnoreCase(PSD2Constants.PAYMENTS))){
			tokenTransformationService.transformPISPToken(token,intentId);
		}
		else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SCOPE_MISSING_IN_REQUEST);
		}
		tppInformationDTO = tppInformationService.findTPPInformation(clientId);
		tppInformationTokenData = populateTppInformation(tppInformationDTO);

		token.setTppInformation(tppInformationTokenData);

		Map<String, String> serviceParams = new HashMap<String, String>();

		for (String serviceParam : configBean.getServiceparams()) {
			if (request.getHeader(serviceParam) != null && !request.getHeader(serviceParam).trim().isEmpty()) {
				serviceParams.put(serviceParam, request.getHeader(serviceParam));
			}
		}

		token.setSeviceParams(serviceParams);
		return token;
	}

	private TppInformationTokenData populateTppInformation(TPPInformationDTO tppInformationDTO) {
		TppInformationTokenData tppInformationTokenData = new TppInformationTokenData();
		tppInformationTokenData.setTppLegalEntityName(tppInformationDTO.getTppInformation().getTppLegalEntityName());
		tppInformationTokenData.setTppRegisteredId(tppInformationDTO.getTppInformation().getTppRegisteredId());
		tppInformationTokenData.setTppRoles(new HashSet<>(tppInformationDTO.getTppInformation().getTppRoles()));
		return tppInformationTokenData;
	}
}