package com.capgemini.psd2.integration.service.impl;

import java.util.Arrays;
import java.util.HashSet;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.token.TPPInformation;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;

@Component
public class TPPInformationServiceImpl implements TPPInformationService{

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Override
	public TPPInformationDTO findTPPInformation(String clientId) {

		TPPInformationDTO tppInformationDTO = null;
		BasicAttributes tppInfoAttributes = null;

		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(clientId);

		if(tppInformationObj instanceof BasicAttributes){
			tppInfoAttributes = (BasicAttributes)tppInformationObj;

			if(tppInfoAttributes != null){
				tppInformationDTO = populateTppInformationDTO(clientId, tppInfoAttributes);
			}
		}
		return tppInformationDTO;
	}

	private TPPInformationDTO populateTppInformationDTO(String clientId,BasicAttributes basicAttributes) {
		try{
			String registeredId = getAttributeValue(basicAttributes, TPPInformationConstants.CN);
			String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
			String roles = getAttributeValue(basicAttributes, TPPInformationConstants.X_ROLES);
			String blockFlag = getAttributeValue(basicAttributes, TPPInformationConstants.X_BLOCK);

			TPPInformation tppInformation = new TPPInformation();
			TPPInformationDTO tppInformationDTO = new TPPInformationDTO();

			tppInformationDTO.setClientId(clientId);

			tppInformation.setTppLegalEntityName(legalEntityName);
			tppInformation.setTppRegisteredId(registeredId);
			if(roles!=null){
				tppInformation.setTppRoles(new HashSet<String>(Arrays.asList(roles.split(","))));
			}	
			
			tppInformation.setTppBlock(blockFlag);
			tppInformationDTO.setTppInformation(tppInformation);
			return tppInformationDTO;
		}catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
		}
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}


}
