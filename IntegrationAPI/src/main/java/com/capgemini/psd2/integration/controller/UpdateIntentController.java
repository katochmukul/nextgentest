package com.capgemini.psd2.integration.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;

@RestController
public class UpdateIntentController {

	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	
	@Autowired
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	
	@Autowired
	private AccountRequestRepository accountRequestRepository;
	
	@Autowired
	@Qualifier("accountRequestRoutingAdapter")
	private AccountRequestAdapter accountRequestAdapter;
	
	@RequestMapping(value = "/updateIntent/{intentId}", method = RequestMethod.PUT)
	public Object updateIndent(@PathVariable String intentId,@RequestHeader String scope){
		
		AispConsent aispConsent = null;
		
		if (scope != null && scope.equalsIgnoreCase(PSD2Constants.OPENID_ACCOUNTS)) {
			
			Data1 accountRequestResponse =accountRequestRepository.findByAccountRequestId(intentId);
			
			if(accountRequestResponse != null && accountRequestResponse.getStatus().equals(Data1.StatusEnum.AUTHORISED)){
			
				aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId, ConsentStatusEnum.AUTHORISED);
			}
			else if(accountRequestResponse != null && accountRequestResponse.getStatus().equals(Data1.StatusEnum.AWAITINGAUTHORISATION)){
				aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId, ConsentStatusEnum.AWAITINGAUTHORISATION);
			}
						
			if(aispConsent == null){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
			}
			if(!aispConsent.getStatus().equals(ConsentStatusEnum.AUTHORISED)){
				AispConsent aispc = aispConsentAdapter.updateConsentStatusWithResponse(aispConsent.getConsentId(),ConsentStatusEnum.AUTHORISED);
				accountRequestAdapter.updateAccountRequestResponse(intentId,StatusEnum.AUTHORISED);
				return aispc;
			} else {
				return aispConsent;
			}
		}
		else if(scope != null && scope.equalsIgnoreCase(PSD2Constants.OPENID_PAYMENTS)){
			PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(intentId,  ConsentStatusEnum.AWAITINGAUTHORISATION);
			
			if(pispConsent == null){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
			}
			
			String channelId = pispConsent.getChannelId();
			String psuId  = pispConsent.getPsuId();
			
			if(channelId == null || channelId.trim().isEmpty() || psuId == null || psuId.trim().isEmpty()){
				throw PSD2Exception.populatePSD2Exception("Either Psu Id or channel id is not present",ErrorCodeEnum.VALIDATION_ERROR);
			}
			
			Map<String,String> paramsMap = new HashMap<String,String>();
			paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, psuId);
			paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, channelId);
			
			PispConsent pispc = pispConsentAdapter.updateConsentStatusWithResponse(pispConsent.getConsentId(), ConsentStatusEnum.AUTHORISED);
			paymentSetupAdapterHelper.updatePaymentSetupStatus(intentId, com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum.ACCEPTEDCUSTOMERPROFILE, paramsMap);
			return pispc;
		}
		else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SCOPE_MISSING_IN_REQUEST);
		}
	}	
}