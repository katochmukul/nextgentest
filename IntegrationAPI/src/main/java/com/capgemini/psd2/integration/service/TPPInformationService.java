package com.capgemini.psd2.integration.service;

import com.capgemini.psd2.integration.dtos.TPPInformationDTO;

public interface TPPInformationService {

	public TPPInformationDTO findTPPInformation(String clientId);
	
}
