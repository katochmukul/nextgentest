package com.capgemini.psd2.integration.service.impl;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.IntentIdValidationService;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.utilities.DateUtilites;

@Service
public class IntentIdValidationServiceImpl implements IntentIdValidationService {

	@Autowired
	private AccountRequestRepository accountRequestRepository;

	@Autowired
	private AispConsentMongoRepository accountConsentRepository;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	/** Account request expiry time. */
	@Value("${app.accountRequestExpiryTime:#{24}}")
	private Integer accountRequestExpiryTime;

	@Value("${app.paymentSetupExpiryTime:#{24}}")
	private Integer paymentSetupExpiryTime;

	@Override
	public void validateIntentId(String intentId, String clientId, String scope) {
		if ("openid accounts".equals(scope)) {
			Data1 accountRequestResponse = accountRequestRepository.findByAccountRequestId(intentId);

			if (accountRequestResponse != null) {
				
				AispConsent aispConsent = accountConsentRepository.findByAccountRequestIdAndStatus(
						accountRequestResponse.getAccountRequestId(), ConsentStatusEnum.AUTHORISED);

				Calendar creationDate = DatatypeConverter.parseDate(accountRequestResponse.getCreationDateTime());
				creationDate.add(Calendar.HOUR, accountRequestExpiryTime);

				if (!accountRequestResponse.getTppCID().equalsIgnoreCase(clientId)
						|| accountRequestResponse.getStatus().toString()
								.equalsIgnoreCase(Data1.StatusEnum.REJECTED.toString())
						|| accountRequestResponse.getStatus().toString()
								.equalsIgnoreCase(Data1.StatusEnum.REVOKED.toString())) {
					throw PSD2Exception.populatePSD2Exception(
							"Client Id does not match or intent is rejected / revoked",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}

				if (accountRequestResponse.getExpirationDateTime() != null) {
					Calendar cal = Calendar.getInstance();
					Date expirationDate = DateUtilites
							.getDateFromISOStringFormat(accountRequestResponse.getExpirationDateTime());
					cal.setTime(expirationDate);
					if (Calendar.getInstance().after(cal)) {
						throw PSD2Exception.populatePSD2Exception("Intent has been expired now.",
								ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
					}
				}

				if (aispConsent != null) {
					if (!accountRequestResponse.getStatus().toString()
							.equalsIgnoreCase(Data1.StatusEnum.AUTHORISED.toString())) {
						throw PSD2Exception.populatePSD2Exception("Account setup is not having authorized status.",
								ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
					}
				} else {
					if (!accountRequestResponse.getStatus().toString()
							.equalsIgnoreCase(Data1.StatusEnum.AWAITINGAUTHORISATION.toString())
							|| creationDate.before(Calendar.getInstance())) {
						throw PSD2Exception.populatePSD2Exception(
								"Account setup is not having correct status or Account Setup has not been requested within allowable timeframe.",
								ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
					}
				}

			} else {
				throw PSD2Exception.populatePSD2Exception("Account request intent does not exist",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		} else if ("openid payments".equals(scope)) {
			PaymentSetupPlatformResource paymentSetUpResponse = paymentSetupPlatformAdapter
					.retrievePaymentSetupResource(intentId);

			if (paymentSetUpResponse != null) {
				Date date = DateUtilites.getDateFromISOStringFormat(paymentSetUpResponse.getCreatedAt());
				Calendar creationDate = Calendar.getInstance();
				creationDate.setTime(date);
				creationDate.add(Calendar.HOUR, paymentSetupExpiryTime);
				if (!paymentSetUpResponse.getTppCID().equals(clientId)
						|| !PaymentSetupResponse.StatusEnum.ACCEPTEDTECHNICALVALIDATION.toString()
								.equalsIgnoreCase(paymentSetUpResponse.getStatus())
						|| creationDate.before(Calendar.getInstance()))
					throw PSD2Exception.populatePSD2Exception(
							"Payment setup is not having correct status or Payment setup has not been requested within allowable timeframe.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			} else {
				throw PSD2Exception.populatePSD2Exception("Payment request intent does not exist",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SCOPE_MISSING_IN_REQUEST);
		}

	}
}