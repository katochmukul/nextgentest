package com.capgemini.psd2.integration.controller.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.capgemini.psd2.integration.IntegrationApplication;

public class IntegrationApplicationTest {
	IntegrationApplication integrationApplication = new IntegrationApplication();

	@Test
	public void testApplication() {
		assertNotNull(integrationApplication.restTemplate());
		assertNotNull(integrationApplication.internalPSD2Filter());
	}


}
