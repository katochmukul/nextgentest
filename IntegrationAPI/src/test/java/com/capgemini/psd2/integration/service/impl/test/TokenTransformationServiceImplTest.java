package com.capgemini.psd2.integration.service.impl.test;

import static org.mockito.Matchers.anyObject;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.impl.TokenTransformationServiceImpl;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class TokenTransformationServiceImplTest {
	
	@Mock
	private AispConsentAdapter aispConsentAdapter;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;
	
	@Mock
	private AccountRequestAdapter accountRequestAdapter;
	
	@Mock
	private HttpServletRequest request;
	
	@InjectMocks
	private TokenTransformationServiceImpl service;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenAispConsentNullTest() {
		AispConsent aispConsent=null;
		Token token= new Token();
		AccountRequestPOSTResponse accountRequest=new AccountRequestPOSTResponse();
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyObject())).thenReturn(accountRequest);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyObject(),anyObject())).thenReturn(aispConsent);
		service.transformAISPToken(token, "fc345");
	}
	
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenAispConsentNotNullTest() {
		AispConsent aispConsent=new AispConsent();
		Token token= new Token();
		AccountRequestPOSTResponse accountRequest=null;
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyObject())).thenReturn(accountRequest);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyObject(),anyObject())).thenReturn(aispConsent);
		service.transformAISPToken(token, "fc345");
	}

	
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenPermissionNullTest() {
		AispConsent aispConsent=new AispConsent();
		Data1 data= new Data1();
		List<PermissionsEnum> permissions = null;
		AccountRequestPOSTResponse accountRequest=new AccountRequestPOSTResponse();
		Token token= new Token();
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyObject())).thenReturn(accountRequest);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyObject(),anyObject())).thenReturn(aispConsent);
		accountRequest.setData(data);
		data.setPermissions(permissions);
		service.transformAISPToken(token, "fc3");
	}
	
	@Test
	public void transformAISPTokenPermissionNotNullTest() {
		AispConsent aispConsent=new AispConsent();
		AccountRequestPOSTResponse accountRequest=IntegrationOperationsAPIMockdata.getAccountRequestPOSTResponse();
		Token token= new Token();
		aispConsent.setEndDate("2019-01-19");
		aispConsent.setConsentId("9876");
		aispConsent.setPsuId("psuId");
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyObject())).thenReturn(accountRequest);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyObject(),anyObject())).thenReturn(aispConsent);
		service.transformAISPToken(token, "fc3476857");
	}
	
	@Test(expected = PSD2Exception.class)
	public void transformPISPTokenNullPispConsentTest() {
		Token token= new Token();
		PispConsent pispConsent=null;
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(),anyObject())).thenReturn(pispConsent);
		service.transformPISPToken(token, "fc3476857");
	}
	
	@Test
	public void transformPISPTokenNotNullPispConsentTest() {
		Token token= new Token();
		PispConsent pispConsent=new PispConsent();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("2019-01-19");
		consentTokenData.setConsentId("124");
		pispConsent.setConsentId("76576456");
		pispConsent.setEndDate("2019-01-19");
		pispConsent.setPsuId("5668");
		
		
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(),anyObject())).thenReturn(pispConsent);
		service.transformPISPToken(token, "fc3476857");
	}
	

	}
	
	
	
