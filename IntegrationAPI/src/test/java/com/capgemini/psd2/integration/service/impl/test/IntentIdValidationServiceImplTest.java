package com.capgemini.psd2.integration.service.impl.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.impl.IntentIdValidationServiceImpl;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;

public class IntentIdValidationServiceImplTest {
	@Mock
	private AccountRequestRepository accountRequestRepository;
	
	@Mock
	private AispConsentMongoRepository accountConsentRepository;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@InjectMocks
	private IntentIdValidationServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = PSD2Exception.class)
	public void validateIntentIdAccountRequestResponseNotNullTest() {
		Data1 accountRequestResponse = new Data1();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(StatusEnum.AUTHORISED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyObject())).thenReturn(accountRequestResponse);
		Mockito.when(accountConsentRepository.findByAccountRequestIdAndStatus(anyString(),anyObject())).thenReturn(new AispConsent());
		service.validateIntentId("999", "123", "openid accounts");
	}

	@Test(expected = PSD2Exception.class)
	public void validateIntentIdAccountRequestResponseNullTest() {
		Data1 accountRequestResponse = null;
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyObject())).thenReturn(accountRequestResponse);
		service.validateIntentId("999", "123", "openid accounts");
	}

	@Test(expected = PSD2Exception.class)
	public void validateIntentIdPaymentsTest() {
		PaymentSetupPlatformResource paymentSetUpResponse = new PaymentSetupPlatformResource();
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupResource(anyObject()))
				.thenReturn(paymentSetUpResponse);
		paymentSetUpResponse.setTppCID("009");
		paymentSetUpResponse.setStatus(StatusEnum.AUTHORISED.toString());
		paymentSetUpResponse.setCreatedAt("2018-01-10T18:01:00+05:30");
		ReflectionTestUtils.setField(service, "paymentSetupExpiryTime", 24);
		service.validateIntentId("999", "123", "openid payments");
	}

	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseNullTest() {
		PaymentSetupPlatformResource paymentSetUpResponse = null;
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupResource(anyObject()))
				.thenReturn(paymentSetUpResponse);
		service.validateIntentId("999", "123", "openid payments");
	}

	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseTest() {
		service.validateIntentId("999", "123", "openid");
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}


}
