/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.product.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.product.mock.foundationservice.domain.Product;
import com.capgemini.psd2.account.product.mock.foundationservice.service.AccountProductService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

/**
 * The Class AccountProductController.
 */
@RestController
@RequestMapping("/fs-abt-service/services")
public class AccountProductController {

	/** The account Product service. */
	@Autowired
	private AccountProductService accountProductService;

	/**
	 * Channel A reterive account Product.
	 *
	 * @param accountNsc the account nsc
	 * @param accountNumber the account number
	 * @param boiUser the boi user
	 * @param boiChannel the boi channel
	 * @param boiPlatform the boi platform
	 * @param correlationID the correlation ID
	 * @return the product
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/account/{accountNSC}/{accountNumber}/product", method = RequestMethod.GET, produces = "application/xml")
	@ResponseBody
	public Product retrieveAccountStandingOrder(
			@PathVariable("accountNSC") String sortCode,
			@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) throws Exception{
		
		if(boiUser==null || boiChannel==null || boiPlatform==null || correlationID==null ){
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_ESPR);
		}
	
		return accountProductService.retrieveAccountProduct(sortCode,accountNumber) ;
		
	}	
	

}
