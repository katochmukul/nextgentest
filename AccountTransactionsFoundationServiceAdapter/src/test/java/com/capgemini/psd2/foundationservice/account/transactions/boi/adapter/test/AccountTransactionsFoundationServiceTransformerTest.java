package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.BalanceType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.GroupByDate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transaction;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.TransactionStatus;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.TransactionType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transactions;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer.AccountTransactionsFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ResponseValidatorImpl;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

public class AccountTransactionsFoundationServiceTransformerTest {
	
	/** The account information FS transformer. */
	@InjectMocks
	private AccountTransactionsFoundationServiceTransformer accountTransactionsFSTransformer = new AccountTransactionsFoundationServiceTransformer();
	
	@Mock
	private PSD2ResponseValidatorImpl psd2Validator;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		Accnt accnt1 =new Accnt();
		
		Transactions transactions = new Transactions();
		Transactions transactions1 =new Transactions();
		GroupByDate groupByDate1 = new GroupByDate();
		Transaction transaction11= new Transaction();
		Transaction transaction12= new Transaction();
		Transaction transaction13= new Transaction();
		Transaction transaction14= new Transaction();
		Transaction transaction15= new Transaction();
		Transaction transaction16= new Transaction();
		
		GroupByDate groupByDate2 = new GroupByDate();
		Transaction transaction21= new Transaction();
		Transaction transaction22= new Transaction();
		Transaction transaction23= new Transaction();
		Transaction transaction24= new Transaction();
		Transaction transaction25= new Transaction();
		Transaction transaction26= new Transaction();
		
		transaction11.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction11.setTransactionStatus(TransactionStatus.BOOKED);
		transaction11.setValue(new BigDecimal(2000.00));
		transaction11.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction11.setCurrency("GBP");
		transaction11.setNarrative1("AAAAAAAAAA");
		
		transaction12.setTransactionType(TransactionType.DR);
		transaction11.setTransactionCode("test");
		transaction12.setTransactionStatus(TransactionStatus.PENDING);
		transaction12.setValue(new BigDecimal(1000.00));
		transaction12.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction12.setCurrency("GBP");
		transaction12.setNarrative1("BBBBBBBBBBBBB");
		
		transaction13.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction13.setTransactionStatus(TransactionStatus.BOOKED);
		transaction13.setValue(new BigDecimal(500.00));
		transaction13.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction13.setCurrency("GBP");
		transaction13.setNarrative1("CCCCCCCCC");
		
		transaction14.setTransactionType(TransactionType.DR);
		transaction11.setTransactionCode("test");
		transaction14.setTransactionStatus(TransactionStatus.PENDING);
		transaction14.setValue(new BigDecimal(3000.00));
		transaction14.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction14.setCurrency("GBP");
		transaction14.setNarrative1("DDDDDDDDDDDDDD");
		
		transaction15.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction15.setTransactionStatus(TransactionStatus.BOOKED);
		transaction15.setValue(new BigDecimal(20000.00));
		transaction15.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction15.setCurrency("GBP");
		transaction15.setNarrative1("EEEEEEEEEEE");
		
		transaction16.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction16.setTransactionStatus(TransactionStatus.BOOKED);
		transaction16.setValue(new BigDecimal(10000.00));
		transaction16.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction16.setCurrency("GBP");
		transaction16.setNarrative1("FFFFFFFFFFFFFFF");
		
		groupByDate1.setBalanceType(BalanceType.AVAILABLE);
		groupByDate1.setCurrency("GBP");
		groupByDate1.setDatePosted(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		groupByDate1.setEodBalance(new BigDecimal(1200.00));
		groupByDate1.getTransaction().add(transaction11);
		groupByDate1.getTransaction().add(transaction12);
		groupByDate1.getTransaction().add(transaction13);
		groupByDate1.getTransaction().add(transaction14);
		groupByDate1.getTransaction().add(transaction15);
		groupByDate1.getTransaction().add(transaction16);
		
		transaction21.setTransactionType(TransactionType.CR);
		transaction21.setTransactionStatus(TransactionStatus.BOOKED);
		transaction21.setValue(new BigDecimal(5000.00));
		transaction21.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction21.setCurrency("GBP");
		transaction21.setNarrative1("ABABBABABBAAB");
		
		transaction22.setTransactionType(TransactionType.DR);
		transaction22.setTransactionStatus(TransactionStatus.PENDING);
		transaction22.setValue(new BigDecimal(400.00));
		transaction22.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction22.setCurrency("GBP");
		transaction22.setNarrative1("BCBCBBCCBBCBC");
		
		transaction23.setTransactionType(TransactionType.DR);
		transaction23.setTransactionStatus(TransactionStatus.PENDING);
		transaction23.setValue(new BigDecimal(500.00));
		transaction23.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction23.setCurrency("GBP");
		transaction23.setNarrative1("CDDCDCDDCCCD");
		
		transaction24.setTransactionType(TransactionType.CR);
		transaction24.setTransactionStatus(TransactionStatus.BOOKED);
		transaction24.setValue(new BigDecimal(600.00));
		transaction24.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction24.setCurrency("GBP");
		transaction24.setNarrative1("FEEFEFEFEFEFFFE");
		
		transaction25.setTransactionType(TransactionType.CR);
		transaction25.setTransactionStatus(TransactionStatus.BOOKED);
		transaction25.setValue(new BigDecimal(700.00));
		transaction25.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction25.setCurrency("GBP");
		transaction25.setNarrative1("SSSSSSSSSSSS");
		
		transaction26.setTransactionType(TransactionType.CR);
		transaction26.setTransactionStatus(TransactionStatus.BOOKED);
		transaction26.setValue(new BigDecimal(900.00));
		transaction26.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction26.setCurrency("GBP");
		transaction26.setNarrative1("RRRRRRRRRRRRRRRR");
		
		groupByDate2.setBalanceType(BalanceType.POSTED);
		groupByDate2.setCurrency("GBP");
		groupByDate2.setDatePosted(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		groupByDate2.setEodBalance(new BigDecimal(1500.00));
		groupByDate2.getTransaction().add(transaction21);
		groupByDate2.getTransaction().add(transaction22);
		groupByDate2.getTransaction().add(transaction23);
		groupByDate2.getTransaction().add(transaction24);
		groupByDate2.getTransaction().add(transaction25);
		groupByDate2.getTransaction().add(transaction26);
		
		transactions.getGroupByDate().add(groupByDate1);
		transactions.getGroupByDate().add(groupByDate2);
		transactions.setHasMoreTxns(true);
		transactions.setPageNumber(1);
		transactions.setPageSize(5);
		transactions.setTxnRetrievalKey("tranxKey");
		
		accnt.setAccountNumber("1234");
		accnt.setNsc("US2345");
		accnt.setTransactions(transactions);
	
		
		accounts.getAccount().add(accnt);	
		
		Map<String, String> params1 = new HashMap<String, String>();
		params1.put("FromBookingDateTime", "2017-01-01T00:00:00");
		params1.put("ToBookingDateTime",  "2018-12-31T23:59:59");
		params1.put("RequestedPageNumber", "1");
		params1.put("TransactionFilter", "CREDIT");
		params1.put("transactionRetrievalKey", "trnxKey");
		
		
		
		AccountTransactionsGETResponse res = accountTransactionsFSTransformer.transformAccountTransaction(accounts, params1);
		assertNotNull(res);
		
		transactions1.getGroupByDate().add(groupByDate1);
		transactions1.getGroupByDate().add(groupByDate2);
		transactions1.setHasMoreTxns(true);
		transactions1.setPageNumber(2);
		transactions1.setPageSize(5);
		transactions1.setTxnRetrievalKey("tranxKey");
		
		accnt1.setAccountNumber("1234");
		accnt1.setNsc("US2345");
		accnt1.setTransactions(transactions1);
		
		accounts.getAccount().add(accnt1);
		
		AccountTransactionsGETResponse res1 = accountTransactionsFSTransformer.transformAccountTransaction(accounts, params1);
		assertNotNull(res1);
		
		
	}
	
	@Test
	public void testTransformAccountTransactions() {
		accountTransactionsFSTransformer.transformAccountTransactions(new String("test"), new HashMap<>());
	}
	@Test
	public void testelseconditiontxnKey (){
		
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		Accnt accnt1 =new Accnt();
		
		Transactions transactions = new Transactions();
		Transactions transactions1 =new Transactions();
		GroupByDate groupByDate1 = new GroupByDate();
		Transaction transaction11= new Transaction();
		Transaction transaction12= new Transaction();
		Transaction transaction13= new Transaction();
		Transaction transaction14= new Transaction();
		Transaction transaction15= new Transaction();
		Transaction transaction16= new Transaction();
		
		GroupByDate groupByDate2 = new GroupByDate();
		Transaction transaction21= new Transaction();
		Transaction transaction22= new Transaction();
		Transaction transaction23= new Transaction();
		Transaction transaction24= new Transaction();
		Transaction transaction25= new Transaction();
		Transaction transaction26= new Transaction();
		
		transaction11.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction11.setTransactionStatus(TransactionStatus.BOOKED);
		transaction11.setValue(new BigDecimal(2000.00));
		transaction11.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction11.setCurrency("GBP");
		transaction11.setNarrative1("AAAAAAAAAA");
		
		transaction12.setTransactionType(TransactionType.DR);
		transaction11.setTransactionCode("test");
		transaction12.setTransactionStatus(TransactionStatus.PENDING);
		transaction12.setValue(new BigDecimal(1000.00));
		transaction12.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction12.setCurrency("GBP");
		transaction12.setNarrative1("BBBBBBBBBBBBB");
		
		transaction13.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction13.setTransactionStatus(TransactionStatus.BOOKED);
		transaction13.setValue(new BigDecimal(500.00));
		transaction13.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction13.setCurrency("GBP");
		transaction13.setNarrative1("CCCCCCCCC");
		
		transaction14.setTransactionType(TransactionType.DR);
		transaction11.setTransactionCode("test");
		transaction14.setTransactionStatus(TransactionStatus.PENDING);
		transaction14.setValue(new BigDecimal(3000.00));
		transaction14.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction14.setCurrency("GBP");
		transaction14.setNarrative1("DDDDDDDDDDDDDD");
		
		transaction15.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction15.setTransactionStatus(TransactionStatus.BOOKED);
		transaction15.setValue(new BigDecimal(20000.00));
		transaction15.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction15.setCurrency("GBP");
		transaction15.setNarrative1("EEEEEEEEEEE");
		
		transaction16.setTransactionType(TransactionType.CR);
		transaction11.setTransactionCode("test");
		transaction16.setTransactionStatus(TransactionStatus.BOOKED);
		transaction16.setValue(new BigDecimal(10000.00));
		transaction16.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction16.setCurrency("GBP");
		transaction16.setNarrative1("FFFFFFFFFFFFFFF");
		
		groupByDate1.setBalanceType(BalanceType.AVAILABLE);
		groupByDate1.setCurrency("GBP");
		groupByDate1.setDatePosted(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		groupByDate1.setEodBalance(new BigDecimal(1200.00));
		groupByDate1.getTransaction().add(transaction11);
		groupByDate1.getTransaction().add(transaction12);
		groupByDate1.getTransaction().add(transaction13);
		groupByDate1.getTransaction().add(transaction14);
		groupByDate1.getTransaction().add(transaction15);
		groupByDate1.getTransaction().add(transaction16);
		
		transaction21.setTransactionType(TransactionType.CR);
		transaction21.setTransactionStatus(TransactionStatus.BOOKED);
		transaction21.setValue(new BigDecimal(5000.00));
		transaction21.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction21.setCurrency("GBP");
		transaction21.setNarrative1("ABABBABABBAAB");
		
		transaction22.setTransactionType(TransactionType.DR);
		transaction22.setTransactionStatus(TransactionStatus.PENDING);
		transaction22.setValue(new BigDecimal(400.00));
		transaction22.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction22.setCurrency("GBP");
		transaction22.setNarrative1("BCBCBBCCBBCBC");
		
		transaction23.setTransactionType(TransactionType.DR);
		transaction23.setTransactionStatus(TransactionStatus.PENDING);
		transaction23.setValue(new BigDecimal(500.00));
		transaction23.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction23.setCurrency("GBP");
		transaction23.setNarrative1("CDDCDCDDCCCD");
		
		transaction24.setTransactionType(TransactionType.CR);
		transaction24.setTransactionStatus(TransactionStatus.BOOKED);
		transaction24.setValue(new BigDecimal(600.00));
		transaction24.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction24.setCurrency("GBP");
		transaction24.setNarrative1("FEEFEFEFEFEFFFE");
		
		transaction25.setTransactionType(TransactionType.CR);
		transaction25.setTransactionStatus(TransactionStatus.BOOKED);
		transaction25.setValue(new BigDecimal(700.00));
		transaction25.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction25.setCurrency("GBP");
		transaction25.setNarrative1("SSSSSSSSSSSS");
		
		transaction26.setTransactionType(TransactionType.CR);
		transaction26.setTransactionStatus(TransactionStatus.BOOKED);
		transaction26.setValue(new BigDecimal(900.00));
		transaction26.setTransactionTimestamp(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		transaction26.setCurrency("GBP");
		transaction26.setNarrative1("RRRRRRRRRRRRRRRR");
		
		groupByDate2.setBalanceType(BalanceType.POSTED);
		groupByDate2.setCurrency("GBP");
		groupByDate2.setDatePosted(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		groupByDate2.setEodBalance(new BigDecimal(-1.00));
		groupByDate2.getTransaction().add(transaction21);
		groupByDate2.getTransaction().add(transaction22);
		groupByDate2.getTransaction().add(transaction23);
		groupByDate2.getTransaction().add(transaction24);
		groupByDate2.getTransaction().add(transaction25);
		groupByDate2.getTransaction().add(transaction26);
		
		transactions.getGroupByDate().add(groupByDate1);
		transactions.getGroupByDate().add(groupByDate2);
		transactions.setHasMoreTxns(true);
		transactions.setPageNumber(1);
		transactions.setPageSize(5);
		
		
		accnt.setAccountNumber("1234");
		accnt.setNsc("US2345");
		accnt.setTransactions(transactions);
	
		
		accounts.getAccount().add(accnt);	
		
		Map<String, String> params1 = new HashMap<String, String>();
		params1.put("FromBookingDateTime", "2017-01-01T00:00:00");
		params1.put("ToBookingDateTime",  "2018-12-31T23:59:59");
		params1.put("RequestedPageNumber", "1");
		params1.put("TransactionFilter", "CREDIT");
		params1.put("transactionRetrievalKey", "trnxKey");
		
		
		
		AccountTransactionsGETResponse res = accountTransactionsFSTransformer.transformAccountTransaction(accounts, params1);
		assertNotNull(res);
		
	}

	
	

}
