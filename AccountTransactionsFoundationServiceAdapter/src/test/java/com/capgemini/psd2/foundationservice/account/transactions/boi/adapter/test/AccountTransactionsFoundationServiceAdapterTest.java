/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.AccountTransactionsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.client.AccountTransactionsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.delegate.AccountTransactionsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transactions;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountTransactionsFoundationServiceAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionsFoundationServiceAdapterTest {
	/** The account transactions foundation service adapter. */
	@InjectMocks
	private AccountTransactionsFoundationServiceAdapter accountTransactionsFoundationServiceAdapter = new AccountTransactionsFoundationServiceAdapter();
	
	/** The account transactions foundation service client. */
	@Mock
	private AccountTransactionsFoundationServiceClient accountTransactionsFoundationServiceClient;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/** The account transactions foundation service delegate. */
	@Mock
	AccountTransactionsFoundationServiceDelegate accountTransactionsFoundationServiceDelegate;
	
	@Mock
	private Map<String, String> foundationCustProfileUrl = new HashMap<>();
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	private Accounts accounts = new Accounts();
	private Accnt accnt = new Accnt();
	private Transactions transactions = new Transactions();
	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private TransactionDateRange transactionDateRange = new TransactionDateRange();
	private com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
	private com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
	private Map<String, String> params = new HashMap<String, String>();
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		
		transactions.setHasMoreTxns(true);
		transactions.setPageSize(25);
		accnt.setTransactions(transactions);
		accounts.getAccount().add(accnt);
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI123");
		accountMapping.setCorrelationId("test");
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("ConsentExpirationDateTime", "2018-12-01T23:59:59");
		params.put("RequestedFromConsentDateTime", "2017-01-01T23:59:59");
		params.put("RequestedToConsentDateTime", "2017-12-01T23:59:59");
		params.put("RequestedFromDateTime", "2017-02-01T23:59:59");
		params.put("RequestedToDateTime", "2017-03-31T00:00:00");
		params.put("newFilterFromDate", "2017-03-31T00:00:00");
		params.put("newFilterToDate", "2017-03-31T00:00:00");
		transactionDateRange.setNewFilterFromDate(new Date());
		transactionDateRange.setNewFilterToDate(new Date());
		Mockito.when(foundationCustProfileUrl.get(any())).thenReturn("http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile");
	    Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
	    Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenReturn(accounts);
		Mockito.when(accountTransactionsFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountTransactionsGETResponse());
		Mockito.when(accountTransactionsFoundationServiceDelegate.createTransactionDateRange(any())).thenReturn(transactionDateRange);
		Mockito.when(accountTransactionsFoundationServiceDelegate.fsCallFilter(any())).thenReturn(transactionDateRange);
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test account transactions FS.
	 */
	@Test(expected=AdapterException.class)
	public void testAccountTransactionsFS() {
		String fromBookingDateTimeInString ="test";
		String toBookingDateTimeInString ="tesrt";
		AccountTransactionsGETResponse res = accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
		assertNotNull(res);
		/*
		 * actualFSPageSize < minPageSize
		 */
		params.put("RequestedMinPageSize", "26");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
		
//		params.put("RequestedMinPageSize", "25");
	}
	
	@Test(expected=AdapterException.class)
	public void testAccountTransactionsFS2(){
		transactions.setHasMoreTxns(false);
		transactions.setPageSize(25);
		params.put("RequestedMaxPageSize", "24");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
		
//		params.put("RequestedMaxPageSize", "100");
	}
	
	@Test(expected=AdapterException.class)
	public void testAccountTransactionsFSAccountIdNull(){
		accDet.setAccountId(null);
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
		
//		accDet.setAccountId("1234");
	}
	
	@Test(expected=AdapterException.class)
	public void testFilteredAccountNull(){
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(null);
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
	}
	
	@Test
	public void testDecision(){
		transactionDateRange.setEmptyResponse(true);
		Mockito.when(accountTransactionsFoundationServiceDelegate.createTransactionDateRange(any())).thenReturn(transactionDateRange);
		AccountTransactionsGETResponse res = accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
		assertNotNull(res);
	}
	
//	@Test
//	public void testAccountsNull(){
//		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenReturn(null);
//		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
//	}
	
//	@Test(expected=AdapterException.class)
//	public void testAccountTransactionsFSfromDateNullcondition() {
//		Accounts accounts = new Accounts();
//		Accnt accnt = new Accnt();
//		Transactions transactions = new Transactions();
//		transactions.setHasMoreTxns(true);
//		transactions.setPageSize(25);
//		accnt.setTransactions(transactions);
//		accounts.getAccount().add(accnt);
//		AccountMapping accountMapping = new AccountMapping();
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("1234");
//		accDet.setAccountNSC("US2345");
//		accDet.setAccountNumber("1234");
//		accDetList.add(accDet);
//		accountMapping.setAccountDetails(accDetList);
//		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
//		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
//		accntFilter.setAccountNumber("US2345");
//		accntFilter.setAccountNSC("1234");
//		filteredAccounts.getAccount().add(accntFilter);
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("RequestedFromDateTime", null);
//		params.put("RequestedToDateTime", null);
//		params.put("RequestedPageNumber", "1");
//		params.put("RequestedTxnFilter", "CREDIT");
//		params.put("RequestedTxnKey", "trnxKey");	
//		params.put("channelId", "BOL");
//	    params.put("x-channel-id", "BOL");
//	    params.put("RequestedMinPageSize", "25");
//	    params.put("RequestedMaxPageSize", "100");
//	    params.put("RequestedPageSize", "25");
//	    TransactionDateRange transactionDateRange = new TransactionDateRange();
//	    transactionDateRange.setNewFilterFromDate(new Date());
//		transactionDateRange.setNewFilterToDate(new Date());
//	    Mockito.when(foundationCustProfileUrl.get(any())).thenReturn("http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile");
//	    Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
//	    Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
//		Mockito.when(accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
//		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenReturn(accounts);
//		Mockito.when(accountTransactionsFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountTransactionsGETResponse());
//		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
//		Mockito.when(accountTransactionsFoundationServiceDelegate.createTransactionDateRange(any())).thenReturn(transactionDateRange);
//		Mockito.when(accountTransactionsFoundationServiceDelegate.fsCallFilter(any())).thenReturn(transactionDateRange);
//		AccountTransactionsGETResponse res = accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
//		assertNotNull(res);
//	}
	/*@Test(expected=AdapterException.class)
	public void testAccountTransactionsFStoDateNullcondition() {
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		Transactions transactions = new Transactions();
		transactions.setHasMoreTxns(true);
		transactions.setPageSize(25);
		accnt.setTransactions(transactions);
		accounts.getAccount().add(accnt);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI123");
		accountMapping.setCorrelationId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", null);
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
	    TransactionDateRange transactionDateRange = new TransactionDateRange();
	    transactionDateRange.setNewFilterFromDate(new Date());
		transactionDateRange.setNewFilterToDate(new Date());
	    Mockito.when(foundationCustProfileUrl.get(any())).thenReturn("http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile");
	    Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
	    Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenReturn(accounts);
		Mockito.when(accountTransactionsFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountTransactionsGETResponse());
		Mockito.when(accountTransactionsFoundationServiceDelegate.createTransactionDateRange(any())).thenReturn(transactionDateRange);
		Mockito.when(accountTransactionsFoundationServiceDelegate.fsCallFilter(any())).thenReturn(transactionDateRange);
		
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		AccountTransactionsGETResponse res = accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
		
	}*/
	
	/**
	 * Test create request headers.
	 */
	@Test
	public void testCreateRequestHeaders() {
		
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId", "BOL");
		params.put("x-channel-id", "BOL");

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		Mockito.when(accountTransactionsFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyObject())).thenReturn(httpHeaders);
		
		httpHeaders = accountTransactionsFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping, params);

		assertNotNull(httpHeaders);
	}
	
	/**
	 * Test exception if Account mapping is null.
	 */
	@Test(expected=Exception.class)
	public void testExceptionIfAccountMappingNull(){
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("channelId", "BOL");
		params.put("x-channel-id", "BOL");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(null, params);
	}
	
	/**
	 * Test exception if start date null.
	 */
	@Test/*(expected=AdapterException.class)*/
	public void testExceptionIfStartDateNull(){
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setPsuId("BOI123");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("correlationId");
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("channelId", "BOL");
		params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
	}
	
	/**
	 * Test exception if end date null.
	 */
	@Test/*(expected=AdapterException.class)*/
	public void testExceptionIfEndDateNull(){
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setPsuId("BOI123");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("correlationId");
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
	}
	
	/**
	 * Test exception if date is invalid.
	 */
	@Test/*(expected=AdapterException.class)*/
	public void testExceptionIfInvalidDate(){
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setPsuId("userId");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("correlationId");
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
	}
	
	/**
	 * Test if params is null .
	 */
	@Test(expected=Exception.class)
	public void testIfAccountParamsNull(){
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setPsuId("userId");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("correlationId");
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, null);
	}
	
	/**
	 * Test retrieve account transactions null check.
	 */
	@Test/*(expected=AdapterException.class)*/
	public void testRetrieveAccountTransactionsNullCheck(){
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		Transactions transactions = new Transactions();
		transactions.setHasMoreTxns(true);
		transactions.setPageSize(25);
		accnt.setTransactions(transactions);
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
		Mockito.when(accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenReturn(accounts);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
		Mockito.when(accountTransactionsFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyObject())).thenReturn(httpHeaders);
		
		httpHeaders = accountTransactionsFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping, params);
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
	}
	
	@Test
	public void testRetrieveAccountTransactionsPSD2Exception400(){

		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI123");
		accountMapping.setCorrelationId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    TransactionDateRange transactionDateRange = new TransactionDateRange();
	    transactionDateRange.setNewFilterFromDate(new Date());
		transactionDateRange.setNewFilterToDate(new Date());

		Mockito.when(accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
	    Mockito.when(foundationCustProfileUrl.get(any())).thenReturn("http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile");
	    Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
	    Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT));
		Mockito.when(accountTransactionsFoundationServiceDelegate.createTransactionDateRange(any())).thenReturn(transactionDateRange);
		Mockito.when(accountTransactionsFoundationServiceDelegate.fsCallFilter(any())).thenReturn(transactionDateRange);
		assertEquals(new AccountTransactionsGETResponse(),accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params));
		
	}
	
	@Test(expected=AdapterException.class)
	public void testRetrieveAccountTransactionsPSD2Exception500(){

		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", "2015-01-01T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
		params.put("channelId", "BOL");
	    params.put("x-channel-id", "BOL");
		Mockito.when(accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8081/fs-abt-service/services/abt/accounts/US1234/1234/transactions");
		
		Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(any(), any(), any(), any())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, params);
	}
	
	@Test(expected=AdapterException.class)
	public void testException_BAD_REQUEST_RetrieveAccountTransaction() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("123");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts accounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accounts.getAccount().add(accnt);
		when(accountTransactionsFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(new HttpHeaders());
		when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(accounts);
		when(adapterFilterUtility.prevalidateAccounts(anyObject(), anyObject())).thenReturn(null);
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, new HashMap<>());
	}
	
	@Test(expected=AdapterException.class)
	public void testException_NO_ACCOUNT_ID_FOUND_RetrieveAccountTransaction() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("123");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts accounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accounts.getAccount().add(accnt);
		when(accountTransactionsFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(new HttpHeaders());
		when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(accounts);
		AccountMapping aMapping = new AccountMapping();
		AccountDetails accountDetails = new AccountDetails();
		aMapping.setAccountDetails(new ArrayList<AccountDetails>() );
		aMapping.getAccountDetails().add(accountDetails);
		when(adapterFilterUtility.prevalidateAccounts(anyObject(), anyObject())).thenReturn(aMapping);
		accountTransactionsFoundationServiceAdapter.retrieveAccountTransaction(accountMapping, new HashMap<>());
	}
	
	@Test
	public void testGetFoundationCustProfileUrl() {
		accountTransactionsFoundationServiceAdapter.getFoundationCustProfileUrl();
	}
}
