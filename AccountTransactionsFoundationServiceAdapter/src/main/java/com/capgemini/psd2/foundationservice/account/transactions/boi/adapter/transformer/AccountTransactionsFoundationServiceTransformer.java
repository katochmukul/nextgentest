/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.enumerator.CurrencyEnum;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.aisp.domain.Data3;
import com.capgemini.psd2.aisp.domain.Data3Amount;
import com.capgemini.psd2.aisp.domain.Data3Balance;
import com.capgemini.psd2.aisp.domain.Data3BalanceAmount;
import com.capgemini.psd2.aisp.domain.Data3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.transformer.AccountTransactionTransformer;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.GroupByDate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transaction;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transactions;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountTransactionsFoundationServiceTransformer.
 */
@Component
public class AccountTransactionsFoundationServiceTransformer implements AccountTransactionTransformer {

	/** The psd2 validator. */
	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator psd2Validator;
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public <T> AccountTransactionsGETResponse transformAccountTransaction(T input, Map<String, String> params) {
		Data3 data3 = new Data3();
		List<Data3Transaction> transactionList = new ArrayList<>();
		data3.setTransaction(transactionList);
		AccountTransactionsGETResponse accountTransactionsGETResponse = new AccountTransactionsGETResponse();
		accountTransactionsGETResponse.setData(data3);
		Accounts accounts = (Accounts) input;
		Accnt accnt = accounts.getAccount().get(0);
		Transactions transactions = accnt.getTransactions();
		for (GroupByDate groupByDate : transactions.getGroupByDate()) {
			for (Transaction transaction : groupByDate.getTransaction()) {
				Data3Transaction data = new Data3Transaction();
				data.setAccountId(params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID));
				if(transaction.getTransactionCode() != null && !transaction.getTransactionCode().isEmpty()) {
					Data3ProprietaryBankTransactionCode proprietaryBankTransactionCode = new Data3ProprietaryBankTransactionCode();
					proprietaryBankTransactionCode.setCode(transaction.getTransactionCode());
					psd2Validator.validate(proprietaryBankTransactionCode);
					data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
				}
				Data3Amount amount = new Data3Amount();
				String resAmount = null;
				if (transaction.getValue() != null) {
					resAmount = transaction.getValue().toString();
					if (!resAmount.contains("."))
						resAmount = resAmount + ".0";
					resAmount = resAmount.replaceFirst("-", "");
				}
				String currency = transaction.getCurrency();
				psd2Validator.validateEnum(CurrencyEnum.class, currency);
				amount.setAmount(resAmount);
				amount.setCurrency(transaction.getCurrency());
				psd2Validator.validate(amount);
				data.setAmount(amount);
				if (AccountTransactionsFoundationServiceConstants.CR
						.equalsIgnoreCase(transaction.getTransactionType().value())) {
					data.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum.CREDIT);
				} else if (AccountTransactionsFoundationServiceConstants.DR
						.equalsIgnoreCase(transaction.getTransactionType().value())) {
					data.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum.DEBIT);
				}
				if (AccountTransactionsFoundationServiceConstants.BOOKED
						.equalsIgnoreCase(transaction.getTransactionStatus().value())) {
					data.setStatus(com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum.BOOKED);
				} else if (AccountTransactionsFoundationServiceConstants.PENDING
						.equalsIgnoreCase(transaction.getTransactionStatus().value())) {
					data.setStatus(com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum.PENDING);
				}
				data.setTransactionInformation(transaction.getNarrative1());
				String dateStr = timeZoneDateTimeAdapter.parseDateCMA(groupByDate.getDatePosted());
				data.setBookingDateTime(dateStr);
				
				accountTransactionsGETResponse.getData().getTransaction().add(data);
				psd2Validator.validate(data);
			}
			Data3Balance balance = new Data3Balance();
			Data3BalanceAmount amount = new Data3BalanceAmount();
			String resAmount = null;
			if (groupByDate.getEodBalance() != null) {
				resAmount = groupByDate.getEodBalance().toString();
				if (!resAmount.contains("."))
					resAmount = resAmount + ".0";
				if (groupByDate.getEodBalance().compareTo(BigDecimal.ZERO) >= 0) {
					balance.setCreditDebitIndicator(
							com.capgemini.psd2.aisp.domain.Data3Balance.CreditDebitIndicatorEnum.CREDIT);
				} else {
					balance.setCreditDebitIndicator(
							com.capgemini.psd2.aisp.domain.Data3Balance.CreditDebitIndicatorEnum.DEBIT);
				}
				resAmount = resAmount.replaceFirst("-", "");
			}
			String currency = groupByDate.getCurrency();
			psd2Validator.validateEnum(CurrencyEnum.class, currency);
			amount.setAmount(resAmount);
			amount.setCurrency(currency);
			psd2Validator.validate(amount);
			balance.setAmount(amount);
			if (AccountTransactionsFoundationServiceConstants.AVAILABLE
					.equalsIgnoreCase(groupByDate.getBalanceType().value())) {
				balance.setType(com.capgemini.psd2.aisp.domain.Data3Balance.TypeEnum.EXPECTED);
			} else if (AccountTransactionsFoundationServiceConstants.POSTED
					.equalsIgnoreCase(groupByDate.getBalanceType().value())) {
				balance.setType(com.capgemini.psd2.aisp.domain.Data3Balance.TypeEnum.CLOSINGBOOKED);
			}
			accountTransactionsGETResponse.getData().getTransaction().get(accountTransactionsGETResponse.getData().getTransaction().size() - 1)
					.setBalance(balance);
		}

		// Prepare links based on the value of isHasMoreTxns
		Links links = new Links();
		if (transactions.getPageNumber() != 0) {
			if (transactions.isHasMoreTxns()) {
				links.setNext(Integer.toString(transactions.getPageNumber() + 1));
			}
			if (transactions.getPageNumber() > 1) {
				links.setPrev(Integer.toString(transactions.getPageNumber() - 1));
			}
			if(transactions.getTxnRetrievalKey() != null) {
				links.setSelf(Integer.toString(transactions.getPageNumber())+":"+transactions.getTxnRetrievalKey());
			} else {
				links.setSelf(Integer.toString(transactions.getPageNumber()));
			}
			if (!"1".equals(links.getSelf()))
				links.setFirst(AccountTransactionsFoundationServiceConstants.FIRST_PAGE);
			accountTransactionsGETResponse.setLinks(links);
		}
		psd2Validator.validate(accountTransactionsGETResponse);
		return accountTransactionsGETResponse;
	}

	@Override
	public <T> AccountTransactionsGETResponse transformAccountTransactions(T input, Map<String, String> params) {
		return null;
	}

}
