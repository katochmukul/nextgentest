package com.capgemini.psd2.security.consent.aisp.helpers;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AispConsentCreationDataHelperImpl implements AispConsentCreationDataHelper {

	@Autowired
	@Qualifier("CustomerAccountListAdapter")
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	@Qualifier("AccountRequestAdapter")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	private FraudSystemHelper fraudSystemHelper;
	
	@Autowired
	@Qualifier("aispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	

	@Override
	public String retrieveAccountRequestSetupData(String intentId) {

		AccountRequestPOSTResponse accountReq = accountRequestAdapter.getAccountRequestGETResponse(intentId);

		if (accountReq == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
		}
		Collections.sort(accountReq.getData().getPermissions());
		return JSONUtilities.getJSONOutPutFromObject(accountReq);
	}

	@Override
	public AccountGETResponse retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId) {
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CHANNEL_ID, channelId);
		paramsMap.put(PSD2Constants.USER_ID, userId);
		paramsMap.put(PSD2Constants.CORRELATION_ID, correlationId);
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
		AccountGETResponse accountGetResponse = customerAccountListAdapter.retrieveCustomerAccountList(userId,
				paramsMap);

		return accountGetResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void createConsent(List<Account> accountList, String userId, String cid, String intentId, String channelId,
			String headers, String tppAppName, Object tppInformation) throws NamingException {

		// Fraudnet Call
		Map<String, String> params = new HashMap<>();
		params.putAll(AispConsentCreationDataHelperImpl.captureFraudParam(headers));
		params.put(PSD2Constants.CHANNEL_NAME, channelId);
		fraudSystemHelper.captureFraudEvent(userId, (List<PSD2Account>) (List<?>) accountList, params);

		AccountDetails acctDetail = null;
		String legalEntityName = null;
		List<AccountDetails> accountDetails = new ArrayList<>();
		AispConsent consent = new AispConsent();

		for (Account custAcctInfo : accountList) {
			acctDetail = new AccountDetails();
			acctDetail.setHashValue(((PSD2Account)custAcctInfo).getHashedValue());
			if (custAcctInfo.getAccount().getSchemeName().equals(SchemeNameEnum.IBAN)) {
				if (custAcctInfo.getServicer() != null && custAcctInfo.getServicer().getIdentification() != null) {
					acctDetail.setAccountNSC(custAcctInfo.getServicer().getIdentification());
					acctDetail.setAccountNumber(custAcctInfo.getAccount().getIdentification());
					acctDetail.setAccountType(((PSD2Account)custAcctInfo).getAccountType());
					acctDetail.setAccount(custAcctInfo.getAccount());
					acctDetail.setServicer(custAcctInfo.getServicer());
					acctDetail.setCurrency(custAcctInfo.getCurrency());
					acctDetail.setNickname(custAcctInfo.getNickname());					
				} else {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR_OUTPUT);
				}
			} else {
				if (custAcctInfo.getAccount().getIdentification().length() == 14) {
					acctDetail.setAccountNSC(custAcctInfo.getAccount().getIdentification().substring(0, 6));
					acctDetail.setAccountNumber(custAcctInfo.getAccount().getIdentification().substring(6));
					acctDetail.setAccountType(((PSD2Account)custAcctInfo).getAccountType());
					acctDetail.setAccount(custAcctInfo.getAccount());
					acctDetail.setServicer(custAcctInfo.getServicer());
					acctDetail.setCurrency(custAcctInfo.getCurrency());
					acctDetail.setNickname(custAcctInfo.getNickname());
				} else {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR_OUTPUT);
				}
			}
			accountDetails.add(acctDetail);
		}
		if (tppInformation != null && tppInformation instanceof BasicAttributes) {
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformation;
			legalEntityName = returnLegalEntityName(tppInfoAttributes);
		}
		consent.setTppApplicationName(tppAppName);
		consent.setTppLegalEntityName(legalEntityName);
		consent.setChannelId(channelId);
		consent.setAccountDetails(accountDetails);
		consent.setPsuId(userId);
		consent.setTppCId(cid);
		consent.setAccountRequestId(intentId);
		ZonedDateTime startDate = ZonedDateTime.now();
		consent.setStartDate(DateUtilites.getCurrentDateInISOFormat(startDate));
		

		AccountRequestPOSTResponse accountRequestData = accountRequestAdapter.getAccountRequestGETResponse(intentId);

		if (!NullCheckUtils.isNullOrEmpty(accountRequestData.getData().getExpirationDateTime())) {
			String consentExpiry = accountRequestData.getData().getExpirationDateTime();
			consent.setEndDate(consentExpiry);
		}
	
		
		if (!NullCheckUtils.isNullOrEmpty(accountRequestData.getData().getTransactionFromDateTime())) {
			consent.setTransactionFromDateTime(accountRequestData.getData().getTransactionFromDateTime());
		}
		
		if (!NullCheckUtils.isNullOrEmpty(accountRequestData.getData().getTransactionToDateTime())) {
			consent.setTransactionToDateTime(accountRequestData.getData().getTransactionToDateTime());
		}
		
		aispConsentAdapter.createConsent(consent);

	}

	public static Map<String, String> captureFraudParam(String headers) {
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.FLOWTYPE, IntentTypeEnum.AISP_INTENT_TYPE.getIntentType());
		params.put(FraudSystemRequestMapping.FS_HEADERS, headers);
		return params;
	}

	@Override
	public void cancelAccountRequest(String intentId) {
		// The case is if the consent present but token has not been issued yet
		// and allowing the TPP go initate auth flow again with the same intent.
		AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId,
				ConsentStatusEnum.AWAITINGAUTHORISATION);
		if (consent != null) {
			aispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
		}
		accountRequestAdapter.updateAccountRequestResponse(intentId, StatusEnum.REJECTED);
	}

	@Override
	public AccountGETResponse findExistingConsentAccounts(String intentId) {
		List<Account> consentAccounts = null;
		AccountGETResponse accountGetResponse = new AccountGETResponse();
		Data2 data2 = new Data2();
		List<Account> accountList = new ArrayList<>();
		data2.setAccount(accountList);
		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId, ConsentStatusEnum.AUTHORISED);
		if (aispConsent != null) {			
			consentAccounts = consentAuthorizationHelper.populateAccountListFromAccountDetails(aispConsent);
			data2.getAccount().addAll(consentAccounts);
			accountGetResponse.setData(data2);
		}
		return accountGetResponse;
	}
	
	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
		return legalEntityName;
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

}