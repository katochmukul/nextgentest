package com.capgemini.psd2.security.consent.aisp.helpers;

import java.util.List;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;

public interface AispConsentCreationDataHelper {

	public String retrieveAccountRequestSetupData(String intentId);
	
	public AccountGETResponse retrieveCustomerAccountListInfo(String userId,String clientId,String flowType,String correlationId,String channelId);
	
	public void createConsent(List<Account> accountList, String userId, String cid, String intentId, String channelId, String headers, String tppAppName, Object tppInformation) throws NamingException;

	public void cancelAccountRequest(String intentId);
	
	public AccountGETResponse findExistingConsentAccounts(String intentId);
	
}
