package com.capgemini.psd2.security.consent.aisp.test.mock.data;
/*
import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.consent.domain.PSD2Account;
*/
public class AispConsentApplicationMockdata {/*

	public static AccountRequestPOSTResponse mockAccountRequestPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
	public static AccountRequestPOSTResponse getAccountRequestPOSTResponse() {
		mockAccountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();
		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2018-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2018-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2018-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2018-12-03T00:00:00-00:00");
		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}

	public static AccountRequestPOSTResponse getAccountRequestPOSTResponseInvalidStatus() {
		AccountRequestPOSTResponse mockAccountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();
		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}

	public static List<CustomerAccountInfo> getCustomerAccountInfoList() {
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

	public static List<PSD2Account> getCustomerAccounts() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		//customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		// customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static AccountGETResponse getCustomerAccountInfo() {
		AccountGETResponse mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		// acct.setHashedValue();
		acct.setAccountType("savings");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		Data2Account account = new Data2Account();
		account.setIdentification("12345");
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		acct.setAccount(account);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		// accnt.setHashedValue();
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockAccountGETResponse.setData(data2);

		return mockAccountGETResponse;
	}

	public static AccountGETResponse getCustomerAccountData() {
		AccountGETResponse mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		acct.setAccountType("savings");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		Data2Account account = new Data2Account();
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		account.setIdentification("12345");
		acct.setAccount(account);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		mockAccountGETResponse.setData(data2);

		return mockAccountGETResponse;
	}

	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static AccountGETResponse getCustomerAccountInfoNew() {
		AccountGETResponse mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		acct.setAccountType("savings");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		Data2Account account = new Data2Account();
		account.setIdentification("12345");
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		acct.setAccount(account);
		acct.setHashedValue();
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setHashedValue();
		accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockAccountGETResponse.setData(data2);

		return mockAccountGETResponse;
	}

*/}