package com.capgemini.psd2.pisp.payment.submission.platform.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionPlatformAdapter;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.repository.PaymentSubmissionPlatformRepository;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.utilities.DateUtilites;

@Component
public class PaymentSubmissionPlatformAdapterImpl implements PaymentSubmissionPlatformAdapter{

	@Autowired
	private PaymentSubmissionPlatformRepository submissionPlatformRepository;	
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	public PaymentSubmissionPlatformResource createPaymentSubmissionResource(PaymentSetupPlatformResource paymentLocalResource,
			PaymentResponseInfo params) {
		PaymentSubmissionPlatformResource submissionApiResource = new PaymentSubmissionPlatformResource();
		submissionApiResource.setPaymentId(paymentLocalResource.getPaymentId());			
		submissionApiResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		submissionApiResource.setStatus(params.getPaymentValidationStatus());		
		submissionApiResource.setIdempotencyRequest(params.getIdempotencyRequest());
		submissionApiResource.setPaymentSubmissionId(params.getPaymentSubmissionId());
		submissionApiResource.setTppCID(reqHeaderAtrributes.getTppCID()); 
		submissionApiResource.setIdempotencyKey(reqHeaderAtrributes.getIdempotencyKey());		
		try{
			return submissionPlatformRepository.save(submissionApiResource);
		}catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_SUBMISSION_CREATION);
		}
	}
	
	
	public PaymentSubmissionPlatformResource retrievePaymentSubmissionResource(String paymentSubmissionId) {
		
			try{
				return submissionPlatformRepository.findOneByPaymentSubmissionId(paymentSubmissionId);
			}catch(DataAccessResourceFailureException exception){
				throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_SUBMISSION_RETRIEVE_BY_PID);
			}
	}
	
	
	public PaymentSubmissionPlatformResource getIdempotentPaymentSubmissionResource(long idempotencyDuration) {
		try{
			//
			return submissionPlatformRepository.findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan
					( reqHeaderAtrributes.getTppCID(), 
					  reqHeaderAtrributes.getIdempotencyKey(),
					  String.valueOf(Boolean.TRUE), 
					  DateUtilites.formatMilisecondsToISODateFormat(System.currentTimeMillis() - idempotencyDuration)
					);
	    }catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_SUBMISSION_RETRIEVE_FOR_IDEMPOTENCY);
		}
		
	}

	
	public PaymentSubmissionPlatformResource retrievePaymentSubmissionResourceByPaymentId(String paymentId) {
		try{
			return submissionPlatformRepository.findOneByPaymentId(paymentId);
		}catch(DataAccessResourceFailureException exception){
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_SUBMISSION_RETRIEVE_BY_PID);
		}
	}


	public void updatePaymentSubmissionResource(
			PaymentSubmissionPlatformResource submissionPlatformResource) {
		try{
			submissionPlatformResource.setUpdatedAt(PispUtilities.getCurrentDateInISOFormat());
			submissionPlatformRepository.save(submissionPlatformResource);			
		}catch(DataAccessResourceFailureException exception){
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_SUBMISSION_UPDATION);
		}		
	}

}
