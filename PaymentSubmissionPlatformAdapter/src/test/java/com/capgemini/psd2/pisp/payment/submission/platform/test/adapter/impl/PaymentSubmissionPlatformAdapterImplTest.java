package com.capgemini.psd2.pisp.payment.submission.platform.test.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.impl.PaymentSubmissionPlatformAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.repository.PaymentSubmissionPlatformRepository;



@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionPlatformAdapterImplTest {
	
	@InjectMocks
	PaymentSubmissionPlatformAdapterImpl paymentSubmissionPlatformAdapterImpl;
	
	@Mock
	private PaymentSubmissionPlatformRepository submissionPlatformRepository;	
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testCreatePaymentSubmissionResource() {
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		PaymentSubmissionPlatformResource platformResource = new PaymentSubmissionPlatformResource();
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setTppDebtorDetails("test");
		PaymentResponseInfo paymentResponseInfo= new PaymentResponseInfo();
		paymentResponseInfo.setIdempotencyRequest("test");
		paymentResponseInfo.setPaymentValidationStatus("test");
		when(reqHeaderAtrributes.getTppCID()).thenReturn("test");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("test");
		when(submissionPlatformRepository.save(platformResource)).thenReturn(platformResource);
		
		PaymentSubmissionPlatformResource resource = paymentSubmissionPlatformAdapterImpl.createPaymentSubmissionResource(paymentSetupPlatformResource, paymentResponseInfo);
		assertNull(resource);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testExceptionCreatePaymentSubmissionResource() {
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setPaymentId("123");
		paymentSetupPlatformResource.setTppDebtorDetails("test");
		PaymentResponseInfo paymentResponseInfo= new PaymentResponseInfo();
		paymentResponseInfo.setIdempotencyRequest("test");
		paymentResponseInfo.setPaymentValidationStatus("test");
		when(reqHeaderAtrributes.getTppCID()).thenReturn("test");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("test");
		when(submissionPlatformRepository.save(any(PaymentSubmissionPlatformResource.class))).thenThrow(new DataAccessResourceFailureException("test"));
		paymentSubmissionPlatformAdapterImpl.createPaymentSubmissionResource(paymentSetupPlatformResource, paymentResponseInfo);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrievePaymentSubmissionResource() {
		PaymentSubmissionPlatformResource platformResource = new PaymentSubmissionPlatformResource();
		when(submissionPlatformRepository.findOneByPaymentSubmissionId(anyObject())).thenReturn(platformResource);
		PaymentSubmissionPlatformResource resource = paymentSubmissionPlatformAdapterImpl.retrievePaymentSubmissionResource("test");
		assertEquals(platformResource, resource);
		/*
		 * 
		 */
		when(submissionPlatformRepository.findOneByPaymentSubmissionId(anyObject())).thenThrow(new DataAccessResourceFailureException("test"));
		paymentSubmissionPlatformAdapterImpl.retrievePaymentSubmissionResource("test");
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetIdempotentPaymentSubmissionResource() {
		PaymentSubmissionPlatformResource platformResource = new PaymentSubmissionPlatformResource();
		when(submissionPlatformRepository.findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(platformResource);
		when(reqHeaderAtrributes.getTppCID()).thenReturn("test");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("key");
		PaymentSubmissionPlatformResource resource = paymentSubmissionPlatformAdapterImpl.getIdempotentPaymentSubmissionResource((long)0.001);
		assertNotNull(resource);
		/*
		 * 
		 */
		when(submissionPlatformRepository.findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new DataAccessResourceFailureException("test"));
		paymentSubmissionPlatformAdapterImpl.getIdempotentPaymentSubmissionResource((long)0.001);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrievePaymentSubmissionResourceByPaymentId() {
		PaymentSubmissionPlatformResource platformResource = new PaymentSubmissionPlatformResource();
		when(submissionPlatformRepository.findOneByPaymentId(anyString())).thenReturn(platformResource);
		PaymentSubmissionPlatformResource resource = paymentSubmissionPlatformAdapterImpl.retrievePaymentSubmissionResourceByPaymentId("123");
		assertNotNull(resource);
		/*
		 * 
		 */
		when(submissionPlatformRepository.findOneByPaymentId(anyString())).thenThrow(new DataAccessResourceFailureException("test"));
		paymentSubmissionPlatformAdapterImpl.retrievePaymentSubmissionResourceByPaymentId("123");
	}
	
	@Test(expected = PSD2Exception.class)
	public void testUpdatePaymentSubmissionResource() {
		PaymentSubmissionPlatformResource platformResource = new PaymentSubmissionPlatformResource();
		when(submissionPlatformRepository.save(any(PaymentSubmissionPlatformResource.class))).thenReturn(platformResource);
		paymentSubmissionPlatformAdapterImpl.updatePaymentSubmissionResource(platformResource);
		/*
		 * 
		 */
		PaymentSubmissionPlatformResource platformResource2 = new PaymentSubmissionPlatformResource();
		when(submissionPlatformRepository.save(any(PaymentSubmissionPlatformResource.class))).thenThrow(new DataAccessResourceFailureException("test"));
		paymentSubmissionPlatformAdapterImpl.updatePaymentSubmissionResource(platformResource2);
	}
	
}
