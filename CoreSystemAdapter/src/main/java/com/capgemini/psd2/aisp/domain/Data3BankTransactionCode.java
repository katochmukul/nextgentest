package com.capgemini.psd2.aisp.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Set of elements used to fully identify the type of underlying transaction resulting in an entry.
 */
@ApiModel(description = "Set of elements used to fully identify the type of underlying transaction resulting in an entry.")
public class Data3BankTransactionCode   {
  @JsonProperty("Code")
  private String code = null;

  @JsonProperty("SubCode")
  private String subCode = null;

  public Data3BankTransactionCode code(String code) {
    this.code = code;
    return this;
  }

   /**
   * Specifies the family within a domain.
   * @return code
  **/
  @ApiModelProperty(required = true, value = "Specifies the family within a domain.")
  @NotNull


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Data3BankTransactionCode subCode(String subCode) {
    this.subCode = subCode;
    return this;
  }

   /**
   * Specifies the sub-product family within a specific family.
   * @return subCode
  **/
  @ApiModelProperty(required = true, value = "Specifies the sub-product family within a specific family.")
  @NotNull


  public String getSubCode() {
    return subCode;
  }

  public void setSubCode(String subCode) {
    this.subCode = subCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data3BankTransactionCode data3BankTransactionCode = (Data3BankTransactionCode) o;
    return Objects.equals(this.code, data3BankTransactionCode.code) &&
        Objects.equals(this.subCode, data3BankTransactionCode.subCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, subCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data3BankTransactionCode {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    subCode: ").append(toIndentedString(subCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

