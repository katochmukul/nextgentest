/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.adapter;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;

/**
 * The Interface AccountRequestAdapter.
 */
public interface AccountRequestAdapter {

	/**
	 * Insert account request POST response.
	 *
	 * @param bankId the bank id
	 * @param accountRequestPOSTResponse the account request POST response
	 * @param params the params
	 * @return the account request POST response
	 */
	public AccountRequestPOSTResponse createAccountRequestPOSTResponse(Data1 accountRequestPOSTResponse);
	
	public AccountRequestPOSTResponse getAccountRequestGETResponse(String accountRequestId);
		
	public AccountRequestPOSTResponse updateAccountRequestResponse(String accountRequestId, StatusEnum statusEnum);
	
	public void removeAccountRequest(String accountRequestId,String cid);
}
