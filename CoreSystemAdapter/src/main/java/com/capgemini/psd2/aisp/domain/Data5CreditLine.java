package com.capgemini.psd2.aisp.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * Data5CreditLine
 */
public class Data5CreditLine   {
  @JsonProperty("Included")
  private Boolean included = null;

  @JsonProperty("Amount")
  private Data5Amount1 amount = null;

  /**
   * Limit type, in a coded form.
   */
  public enum TypeEnum {
    PRE_AGREED("Pre-Agreed"),
    
    EMERGENCY("Emergency"),
    
    TEMPORARY("Temporary");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("Type")
  private TypeEnum type = null;

  public Data5CreditLine included(Boolean included) {
    this.included = included;
    return this;
  }

   /**
   * Indicates whether or not the credit line is included in the balance of the account. Usage: If not present, credit line is not included in the balance amount of the account.
   * @return included
  **/
  @ApiModelProperty(required = true, value = "Indicates whether or not the credit line is included in the balance of the account. Usage: If not present, credit line is not included in the balance amount of the account.")
  @NotNull


  public Boolean getIncluded() {
    return included;
  }

  public void setIncluded(Boolean included) {
    this.included = included;
  }

  public Data5CreditLine amount(Data5Amount1 amount) {
    this.amount = amount;
    return this;
  }

   /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data5Amount1 getAmount() {
    return amount;
  }

  public void setAmount(Data5Amount1 amount) {
    this.amount = amount;
  }

  public Data5CreditLine type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * Limit type, in a coded form.
   * @return type
  **/
  @ApiModelProperty(value = "Limit type, in a coded form.")


  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data5CreditLine data5CreditLine = (Data5CreditLine) o;
    return Objects.equals(this.included, data5CreditLine.included) &&
        Objects.equals(this.amount, data5CreditLine.amount) &&
        Objects.equals(this.type, data5CreditLine.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(included, amount, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data5CreditLine {\n");
    
    sb.append("    included: ").append(toIndentedString(included)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

