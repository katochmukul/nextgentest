/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;

/**
 * The Interface AccountInformationTransformer.
 */
public interface AccountInformationTransformer {
	
	/**
	 * Transform account information.
	 *
	 * @param <T> the generic type
	 * @param source the source
	 * @param params the params
	 * @return the account GET response
	 */
	public <T> AccountGETResponse transformAccountInformation(T source, Map<String, String> params);
}
