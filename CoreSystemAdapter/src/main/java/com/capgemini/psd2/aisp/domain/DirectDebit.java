package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import com.capgemini.psd2.aisp.domain.Data6PreviousPaymentAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Direct Debit
 */
@ApiModel(description = "Direct Debit")
public class DirectDebit   {
  @JsonProperty("AccountId")
  private String accountId = null;

  @JsonProperty("DirectDebitId")
  private String directDebitId = null;

  @JsonProperty("MandateIdentification")
  private String mandateIdentification = null;

  /**
   * Specifies the status of the direct debit in code form.
   */
  public enum DirectDebitStatusCodeEnum {
    ACTIVE("Active"),
    
    INACTIVE("Inactive");

    private String value;

    DirectDebitStatusCodeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static DirectDebitStatusCodeEnum fromValue(String text) {
      for (DirectDebitStatusCodeEnum b : DirectDebitStatusCodeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("DirectDebitStatusCode")
  private DirectDebitStatusCodeEnum directDebitStatusCode = null;

  @JsonProperty("Name")
  private String name = null;

  @JsonProperty("PreviousPaymentDateTime")
  private String previousPaymentDateTime = null;

  @JsonProperty("PreviousPaymentAmount")
  private Data6PreviousPaymentAmount previousPaymentAmount = null;

  public DirectDebit accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

   /**
   * A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.
   * @return accountId
  **/
  @ApiModelProperty(required = true, value = "A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.")
  @NotNull

 @Size(min=1,max=40)
  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public DirectDebit directDebitId(String directDebitId) {
    this.directDebitId = directDebitId;
    return this;
  }

   /**
   * A unique and immutable identifier used to identify the direct debit resource. This identifier has no meaning to the account owner.
   * @return directDebitId
  **/
  @ApiModelProperty(value = "A unique and immutable identifier used to identify the direct debit resource. This identifier has no meaning to the account owner.")

 @Size(min=1,max=40)
  public String getDirectDebitId() {
    return directDebitId;
  }

  public void setDirectDebitId(String directDebitId) {
    this.directDebitId = directDebitId;
  }

  public DirectDebit mandateIdentification(String mandateIdentification) {
    this.mandateIdentification = mandateIdentification;
    return this;
  }

   /**
   * Direct Debit reference. For AUDDIS service users provide Core Reference. For non AUDDIS service users provide Core reference if possible or last used reference.
   * @return mandateIdentification
  **/
  @ApiModelProperty(required = true, value = "Direct Debit reference. For AUDDIS service users provide Core Reference. For non AUDDIS service users provide Core reference if possible or last used reference.")
  @NotNull

 @Size(min=1,max=35)
  public String getMandateIdentification() {
    return mandateIdentification;
  }

  public void setMandateIdentification(String mandateIdentification) {
    this.mandateIdentification = mandateIdentification;
  }

  public DirectDebit directDebitStatusCode(DirectDebitStatusCodeEnum directDebitStatusCode) {
    this.directDebitStatusCode = directDebitStatusCode;
    return this;
  }

   /**
   * Specifies the status of the direct debit in code form.
   * @return directDebitStatusCode
  **/
  @ApiModelProperty(value = "Specifies the status of the direct debit in code form.")


  public DirectDebitStatusCodeEnum getDirectDebitStatusCode() {
    return directDebitStatusCode;
  }

  public void setDirectDebitStatusCode(DirectDebitStatusCodeEnum directDebitStatusCode) {
    this.directDebitStatusCode = directDebitStatusCode;
  }

  public DirectDebit name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Name of Service User
   * @return name
  **/
  @ApiModelProperty(required = true, value = "Name of Service User")
  @NotNull

 @Size(min=1,max=70)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public DirectDebit previousPaymentDateTime(String previousPaymentDateTime) {
    this.previousPaymentDateTime = previousPaymentDateTime;
    return this;
  }

   /**
   * Date of most recent direct debit collection.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return previousPaymentDateTime
  **/
  @ApiModelProperty(value = "Date of most recent direct debit collection.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

  @Valid

  public String getPreviousPaymentDateTime() {
    return previousPaymentDateTime;
  }

  public void setPreviousPaymentDateTime(String previousPaymentDateTime) {
    this.previousPaymentDateTime = previousPaymentDateTime;
  }

  public DirectDebit previousPaymentAmount(Data6PreviousPaymentAmount previousPaymentAmount) {
    this.previousPaymentAmount = previousPaymentAmount;
    return this;
  }

   /**
   * Get previousPaymentAmount
   * @return previousPaymentAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data6PreviousPaymentAmount getPreviousPaymentAmount() {
    return previousPaymentAmount;
  }

  public void setPreviousPaymentAmount(Data6PreviousPaymentAmount previousPaymentAmount) {
    this.previousPaymentAmount = previousPaymentAmount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DirectDebit directDebit = (DirectDebit) o;
    return Objects.equals(this.accountId, directDebit.accountId) &&
        Objects.equals(this.directDebitId, directDebit.directDebitId) &&
        Objects.equals(this.mandateIdentification, directDebit.mandateIdentification) &&
        Objects.equals(this.directDebitStatusCode, directDebit.directDebitStatusCode) &&
        Objects.equals(this.name, directDebit.name) &&
        Objects.equals(this.previousPaymentDateTime, directDebit.previousPaymentDateTime) &&
        Objects.equals(this.previousPaymentAmount, directDebit.previousPaymentAmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, directDebitId, mandateIdentification, directDebitStatusCode, name, previousPaymentDateTime, previousPaymentAmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DirectDebit {\n");
    
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    directDebitId: ").append(toIndentedString(directDebitId)).append("\n");
    sb.append("    mandateIdentification: ").append(toIndentedString(mandateIdentification)).append("\n");
    sb.append("    directDebitStatusCode: ").append(toIndentedString(directDebitStatusCode)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    previousPaymentDateTime: ").append(toIndentedString(previousPaymentDateTime)).append("\n");
    sb.append("    previousPaymentAmount: ").append(toIndentedString(previousPaymentAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

