package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;

public interface AccountBeneficiariesTransformer {
	
	public <T> BeneficiariesGETResponse transformAccountBeneficiaries(T source, Map<String, String> params);

}
