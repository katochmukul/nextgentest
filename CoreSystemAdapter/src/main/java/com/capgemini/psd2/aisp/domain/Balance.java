package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Balance
 */
@ApiModel(description = "Balance")
public class Balance   {
  @JsonProperty("AccountId")
  private String accountId = null;

  @JsonProperty("Amount")
  private Data5Amount amount = null;

  /**
   * Indicates whether the balance is a credit or a debit balance. Usage: A zero balance is considered to be a credit balance.
   */
  public enum CreditDebitIndicatorEnum {
    CREDIT("Credit"),
    
    DEBIT("Debit");

    private String value;

    CreditDebitIndicatorEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static CreditDebitIndicatorEnum fromValue(String text) {
      for (CreditDebitIndicatorEnum b : CreditDebitIndicatorEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("CreditDebitIndicator")
  private CreditDebitIndicatorEnum creditDebitIndicator = null;

  /**
   * Balance type, in a coded form.
   */
  public enum TypeEnum {
    CLOSINGAVAILABLE("ClosingAvailable"),
    
    CLOSINGBOOKED("ClosingBooked"),
    
    EXPECTED("Expected"),
    
    FORWARDAVAILABLE("ForwardAvailable"),
    
    INFORMATION("Information"),
    
    INTERIMAVAILABLE("InterimAvailable"),
    
    INTERIMBOOKED("InterimBooked"),
    
    OPENINGAVAILABLE("OpeningAvailable"),
    
    OPENINGBOOKED("OpeningBooked"),
    
    PREVIOUSLYCLOSEDBOOKED("PreviouslyClosedBooked");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("Type")
  private TypeEnum type = null;

  @JsonProperty("DateTime")
  private String dateTime = null;

  @JsonProperty("CreditLine")
  private List<Data5CreditLine> creditLine = null;

  public Balance accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

   /**
   * A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.
   * @return accountId
  **/
  @ApiModelProperty(required = true, value = "A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.")
  @NotNull

 @Size(min=1,max=40)
  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public Balance amount(Data5Amount amount) {
    this.amount = amount;
    return this;
  }

   /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data5Amount getAmount() {
    return amount;
  }

  public void setAmount(Data5Amount amount) {
    this.amount = amount;
  }

  public Balance creditDebitIndicator(CreditDebitIndicatorEnum creditDebitIndicator) {
    this.creditDebitIndicator = creditDebitIndicator;
    return this;
  }

   /**
   * Indicates whether the balance is a credit or a debit balance. Usage: A zero balance is considered to be a credit balance.
   * @return creditDebitIndicator
  **/
  @ApiModelProperty(required = true, value = "Indicates whether the balance is a credit or a debit balance. Usage: A zero balance is considered to be a credit balance.")
  @NotNull


  public CreditDebitIndicatorEnum getCreditDebitIndicator() {
    return creditDebitIndicator;
  }

  public void setCreditDebitIndicator(CreditDebitIndicatorEnum creditDebitIndicator) {
    this.creditDebitIndicator = creditDebitIndicator;
  }

  public Balance type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * Balance type, in a coded form.
   * @return type
  **/
  @ApiModelProperty(required = true, value = "Balance type, in a coded form.")
  @NotNull


  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Balance dateTime(String dateTime) {
    this.dateTime = dateTime;
    return this;
  }

   /**
   * Indicates the date (and time) of the balance.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return dateTime
  **/
  @ApiModelProperty(required = true, value = "Indicates the date (and time) of the balance.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
  @NotNull

  @Valid

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  public Balance creditLine(List<Data5CreditLine> creditLine) {
    this.creditLine = creditLine;
    return this;
  }

  public Balance addCreditLineItem(Data5CreditLine creditLineItem) {
    if (this.creditLine == null) {
      this.creditLine = new ArrayList<Data5CreditLine>();
    }
    this.creditLine.add(creditLineItem);
    return this;
  }

   /**
   * Get creditLine
   * @return creditLine
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Data5CreditLine> getCreditLine() {
    return creditLine;
  }

  public void setCreditLine(List<Data5CreditLine> creditLine) {
    this.creditLine = creditLine;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Balance balance = (Balance) o;
    return Objects.equals(this.accountId, balance.accountId) &&
        Objects.equals(this.amount, balance.amount) &&
        Objects.equals(this.creditDebitIndicator, balance.creditDebitIndicator) &&
        Objects.equals(this.type, balance.type) &&
        Objects.equals(this.dateTime, balance.dateTime) &&
        Objects.equals(this.creditLine, balance.creditLine);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, amount, creditDebitIndicator, type, dateTime, creditLine);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Balance {\n");
    
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    creditDebitIndicator: ").append(toIndentedString(creditDebitIndicator)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    dateTime: ").append(toIndentedString(dateTime)).append("\n");
    sb.append("    creditLine: ").append(toIndentedString(creditLine)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

