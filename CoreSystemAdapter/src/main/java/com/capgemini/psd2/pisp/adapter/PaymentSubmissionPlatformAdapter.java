package com.capgemini.psd2.pisp.adapter;

import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;

public interface PaymentSubmissionPlatformAdapter {
	public PaymentSubmissionPlatformResource getIdempotentPaymentSubmissionResource(long idempotencyDuration);
	public PaymentSubmissionPlatformResource createPaymentSubmissionResource(PaymentSetupPlatformResource paymentLocalResource, PaymentResponseInfo params);
	public void updatePaymentSubmissionResource(PaymentSubmissionPlatformResource submissionPlatformResource);
	public PaymentSubmissionPlatformResource retrievePaymentSubmissionResource(String paymentSubmissionId);
	public PaymentSubmissionPlatformResource retrievePaymentSubmissionResourceByPaymentId(String paymentId);
}
