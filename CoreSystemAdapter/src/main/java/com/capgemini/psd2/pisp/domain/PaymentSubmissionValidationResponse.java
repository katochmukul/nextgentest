package com.capgemini.psd2.pisp.domain;

public class PaymentSubmissionValidationResponse {

	private String paymentSubmissionValidationStatus;

	public String getPaymentSubmissionValidationStatus() {
		return paymentSubmissionValidationStatus;
	}

	public void setPaymentSubmissionValidationStatus(String paymentSubmissionValidationStatus) {
		this.paymentSubmissionValidationStatus = paymentSubmissionValidationStatus;
	}
}
