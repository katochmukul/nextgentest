package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "paymentSubmissionPlatformResources")
public class PaymentSubmissionPlatformResource {
	@Id
	private String id;	
	private String paymentId;
	private String paymentSubmissionId;	
	private String idempotencyKey;
	private String tppCID;
	private String status;
	private String createdAt;
	private String updatedAt;	
	private String idempotencyRequest;
	
	public String getIdempotencyRequest() {
		return idempotencyRequest;
	}
	public void setIdempotencyRequest(String idempotencyRequest) {
		this.idempotencyRequest = idempotencyRequest;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}
	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}
	public String getIdempotencyKey() {
		return idempotencyKey;
	}
	public void setIdempotencyKey(String idempotencyKey) {
		this.idempotencyKey = idempotencyKey;
	}
	public String getTppCID() {
		return tppCID;
	}
	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}	
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public String toString() {
		return "PaymentSubmissionResource [id=" + id + ", paymentId=" + paymentId + ", paymentSubmissionId="
				+ paymentSubmissionId + ", idempotencyKey=" + idempotencyKey + ", tppCID=" + tppCID + ", status="
				+ status + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", idempotencyRequest=" + idempotencyRequest + "]";
	}
}
