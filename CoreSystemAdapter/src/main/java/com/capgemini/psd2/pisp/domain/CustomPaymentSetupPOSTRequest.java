package com.capgemini.psd2.pisp.domain;

import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;

public class CustomPaymentSetupPOSTRequest extends PaymentSetupPOSTRequest{
	private String paymentStatus;

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Override
	public String toString() {
		return "CustomPaymentSetupPOSTRequest [paymentStatus=" + paymentStatus + ", toString()=" + super.toString()	+  "]";
	}

	

	
}
