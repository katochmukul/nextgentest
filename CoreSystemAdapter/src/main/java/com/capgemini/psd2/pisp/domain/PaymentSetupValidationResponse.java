package com.capgemini.psd2.pisp.domain;

public class PaymentSetupValidationResponse {
 private String paymentSetupValidationStatus;
 private String paymentSubmissionId;

public String getPaymentSubmissionId() {
	return paymentSubmissionId;
}

public void setPaymentSubmissionId(String paymentSubmissionId) {
	this.paymentSubmissionId = paymentSubmissionId;
}
public String getPaymentSetupValidationStatus() {
	return paymentSetupValidationStatus;
}

public void setPaymentSetupValidationStatus(String paymentSetupValidationStatus) {
	this.paymentSetupValidationStatus = paymentSetupValidationStatus;
} 
}
