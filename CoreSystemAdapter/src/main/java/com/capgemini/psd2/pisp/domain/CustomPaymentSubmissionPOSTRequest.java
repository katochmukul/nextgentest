package com.capgemini.psd2.pisp.domain;

public class CustomPaymentSubmissionPOSTRequest extends PaymentSubmissionPOSTRequest{
	
	private String paymentStatus;
	private String createdOn;
	private String payerCurrency;
	private String payerJurisdiction;
	private Object fraudSystemResponse;
	
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getPayerCurrency() {
		return payerCurrency;
	}
	public void setPayerCurrency(String payerCurrency) {
		this.payerCurrency = payerCurrency;
	}
	public String getPayerJurisdiction() {
		return payerJurisdiction;
	}
	public void setPayerJurisdiction(String payerJurisdiction) {
		this.payerJurisdiction = payerJurisdiction;
	}
	@Override
	public String toString() {
		return "CustomPaymentSubmissionPOSTRequest [paymentStatus=" + paymentStatus + ", createdOn=" + createdOn + ", payerCurrency="
				+ payerCurrency + ", payerJurisdiction=" + payerJurisdiction + ", toString()=" + super.toString() + "]";
	}
	public Object getFraudSystemResponse() {
		return fraudSystemResponse;
	}
	public void setFraudSystemResponse(Object fraudSystemResponse) {
		this.fraudSystemResponse = fraudSystemResponse;
	}
	

}
