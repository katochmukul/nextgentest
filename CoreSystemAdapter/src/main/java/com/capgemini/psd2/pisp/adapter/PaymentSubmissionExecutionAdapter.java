package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;

public interface PaymentSubmissionExecutionAdapter {
	 public PaymentSubmissionExecutionResponse executePaymentSubmission(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest, Map<String, String> params);
}
