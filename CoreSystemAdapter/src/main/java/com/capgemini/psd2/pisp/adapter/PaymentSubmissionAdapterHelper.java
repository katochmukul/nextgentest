package com.capgemini.psd2.pisp.adapter;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;

public interface PaymentSubmissionAdapterHelper {
	public PaymentResponseInfo prePaymentSubmissionValidation(CustomPaymentSetupPOSTResponse paymentSetupResponse);
	public PaymentSubmissionExecutionResponse executePaymentSubmission(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest);
}

