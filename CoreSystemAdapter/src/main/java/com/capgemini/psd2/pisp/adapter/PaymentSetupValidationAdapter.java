package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public interface PaymentSetupValidationAdapter {
	public PaymentSetupValidationResponse validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest, Map<String, String> params);
}
