package com.capgemini.psd2.pisp.adapter;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public interface PaymentSetupAdapterHelper {
	
	public PaymentResponseInfo validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest);	
	public PaymentSetupStagingResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest);
	public PaymentSetupStagingResponse updateStagedPaymentSetup(CustomPaymentSetupPOSTResponse paymentSetupBankResource);	
	public CustomPaymentSetupPOSTResponse retrieveStagedPaymentSetup(String paymentId);
	public void updatePaymentSetupStatus(String paymentId, StatusEnum paymentSetupStatus, Map<String, String> paramsMap);
	public PaymentSetupValidationResponse preAuthorizationValidation(Account account, String paymentId,Map<String, String> mapParam);
	public void updatePaymentSetupDebtorDetails(Account account, String paymentId);
}
