package com.capgemini.psd2.pisp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SubmissionRetrieveGetRequest {
	private String paymentSubmissionId;

	@NotNull
	@Size(min=1,max=40)
	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}

	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}
	

	

}
