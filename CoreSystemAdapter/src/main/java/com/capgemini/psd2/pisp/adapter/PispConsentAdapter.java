package com.capgemini.psd2.pisp.adapter;

import java.util.List;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;

public interface PispConsentAdapter {

	public void createConsent(PispConsent pispConsent);
   
	public PispConsent retrieveConsentByPaymentId(String paymentId,ConsentStatusEnum status);
	
	public PispConsent retrieveConsent(String consentId);
	
	public void updateConsentStatus(String consentId,ConsentStatusEnum statusEnum);
	
	public PispConsent updateConsentStatusWithResponse(String consentId,ConsentStatusEnum statusEnum);
	
	public List<PispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum);	
	
}
