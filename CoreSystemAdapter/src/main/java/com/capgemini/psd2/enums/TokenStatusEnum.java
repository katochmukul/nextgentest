package com.capgemini.psd2.enums;

public enum TokenStatusEnum {
	
	ACTIVE("1","active"),
	REVOKED("2","revoked");
	
	private String statusId;
	private String statusCode;
	
	TokenStatusEnum(String statusId, String statusCode) {
		this.statusId = statusId;
		this.statusCode = statusCode;
	}
	
    public String getStatusId() {
			return statusId;
	}
	public String getStatusCode() {
			return statusCode;
	}
	
	public final static TokenStatusEnum findStatusFromEnum(String statusId){
		TokenStatusEnum currentStatus = null;
		TokenStatusEnum[] enums = TokenStatusEnum.values();
		for(TokenStatusEnum statusEnum : enums) {
			if(statusEnum.getStatusId().equalsIgnoreCase(statusId)) {
				currentStatus = statusEnum;
				break;
			}
		}
		return currentStatus;
	}
	
	public final static  boolean isActive(String statusId){
		TokenStatusEnum currentStatus=null;
		TokenStatusEnum[] enums = TokenStatusEnum.values();
		for(TokenStatusEnum status : enums) {
            if(status.getStatusId().equalsIgnoreCase(statusId)) {
            	currentStatus = status;
                  break;
            }
     }
     return currentStatus != null && currentStatus.equals(ACTIVE);	
	}
}
