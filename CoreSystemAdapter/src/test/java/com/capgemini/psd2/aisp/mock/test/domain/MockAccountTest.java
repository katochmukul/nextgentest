package com.capgemini.psd2.aisp.mock.test.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.psd2.aisp.mock.domain.MockAccount;

public class MockAccountTest {

	@Test
	public void test() {
		MockAccount mockAccount = new MockAccount();
		mockAccount.setPsuId("1234");
		assertEquals("1234", mockAccount.getPsuId());
	}

}
