package com.capgemini.psd2.aisp.mock.test.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.psd2.aisp.mock.domain.MockBalance;

public class MockBalanceTest {

	@Test
	public void test() {
		MockBalance mockBalance = new MockBalance();
		mockBalance.setPsuId("1234");
		mockBalance.setAccountNSC("Nsc123");
		mockBalance.setAccountNumber("1234");
		assertEquals("1234",mockBalance.getPsuId());
		assertEquals("Nsc123",mockBalance.getAccountNSC());
		assertEquals("1234",mockBalance.getAccountNumber());
	}

}
