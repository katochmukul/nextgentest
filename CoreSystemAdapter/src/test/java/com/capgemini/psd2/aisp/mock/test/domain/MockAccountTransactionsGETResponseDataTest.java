package com.capgemini.psd2.aisp.mock.test.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.psd2.aisp.mock.domain.MockAccountTransactionsGETResponseData;

public class MockAccountTransactionsGETResponseDataTest {

	@Test
	public void test() {
		MockAccountTransactionsGETResponseData mockAccountTransactionsGETResponseData = new MockAccountTransactionsGETResponseData();
		mockAccountTransactionsGETResponseData.setAccountNSC("Nsc123");
		mockAccountTransactionsGETResponseData.setPsuId("123");
		mockAccountTransactionsGETResponseData.setAccountNumber("123");
		assertEquals("123", mockAccountTransactionsGETResponseData.getAccountNumber());
		assertEquals("123", mockAccountTransactionsGETResponseData.getPsuId());
		assertEquals("Nsc123",mockAccountTransactionsGETResponseData.getAccountNSC());
	}

}
