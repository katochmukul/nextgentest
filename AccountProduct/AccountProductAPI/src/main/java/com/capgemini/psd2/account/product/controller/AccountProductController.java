package com.capgemini.psd2.account.product.controller;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.product.service.AccountProductService;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

@RestController
public class AccountProductController 
{
	/** The service. */
	@Autowired
	private AccountProductService service;

	/**
	 * Retrieve account product.
	 *
	 * @param accountId the account id
	 * @return the product GET response
	 */
	@RequestMapping(value ="/accounts/{accountId}/product", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ProductGETResponse retrieveAccountProduct(@PathVariable("accountId") String accountId) {

		if (NullCheckUtils.isNullOrEmpty(accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_ID_FOUND);
		if (!Pattern.matches("[a-zA-Z0-9-]{1,40}", accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		return service.retrieveAccountProduct(accountId);
	}
}
