package com.capgemini.psd2.account.product.test.controller;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.product.controller.AccountProductController;
import com.capgemini.psd2.account.product.service.AccountProductService;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductControllerTest 
{
	/** The service. */
	@Mock
	private AccountProductService service;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountProductController controller;

	/** The product response. */
	ProductGETResponse productGETResponse = null;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	/**
	 * Test null account id. Failure test
	 */
	@Test(expected = PSD2Exception.class)
	public void testNullAccountId() {
		controller.retrieveAccountProduct(null);
	}
	
	/**
	 * Test invalid account id length.
	 */
	@Test(expected = PSD2Exception.class)
	public void testInvalidAccountIdLength() {
		controller.retrieveAccountProduct("269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b");
	}

	/**
	 * Test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void successTest() throws Exception {
		when(service.retrieveAccountProduct(anyString())).thenReturn(productGETResponse);
		this.mockMvc.perform(get("/accounts/{accountId}/product", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		controller = null;
	}

	

}
