package com.capgemini.psd2.account.product.test.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.product.serviceimpl.AccountProductServiceImpl;
import com.capgemini.psd2.account.product.test.mock.data.AccountProductMockData;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductServiceImplTest 
{
	/** The adapter. */
	@Mock
	private AccountProductsAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	/** The service. */
	@InjectMocks
	private AccountProductServiceImpl service = new AccountProductServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountProductMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
		.thenReturn(AccountProductMockData.getMockAccountMapping());
		
	}

	/**
	 * Retrieve account product test (empty list).
	 */
	@Test
	public void retrieveAccountProductTest() {
		when(adapter.retrieveAccountProducts(anyObject(), anyObject()))
				.thenReturn(AccountProductMockData.getBlankResponse());
		ProductGETResponse productGETResponse = service.retrieveAccountProduct(
				"123");
		assertTrue(productGETResponse.getData().getProduct().isEmpty());
	}


	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
	

}

