package com.capgemini.psd2.account.product.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.product.routing.adapter.impl.AccountProductRoutingAdapter;
import com.capgemini.psd2.account.product.routing.adapter.routing.AccountProductAdapterFactory;
import com.capgemini.psd2.account.product.routing.adapter.test.adapter.AccountProductTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;

public class AccountProductRoutingAdapterTest 
{
	/** The account products adapter factory. */
	@Mock
	private AccountProductAdapterFactory accountProductAdapterFactory;

	/** The account products routing adapter. */
	@InjectMocks
	private AccountProductsAdapter accountProductRoutingAdapter = new AccountProductRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account balancetest.
	 */
	@Test
	public void retrieveAccountProductTest() {

		AccountProductsAdapter accountProductsAdapter = new AccountProductTestRoutingAdapter();
		Mockito.when(accountProductAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountProductsAdapter);

		ProductGETResponse productGETResponse = accountProductRoutingAdapter.retrieveAccountProducts(null,
				null);

		assertTrue(productGETResponse.getData().getProduct().isEmpty());

	}


}
