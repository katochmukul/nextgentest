package com.capgemini.psd2.account.product.routing.adapter.test.routing;


import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Data8;
import com.capgemini.psd2.aisp.domain.Product;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;

public class AccountProductRoutingAdapterTestMockData 
{
	
	
		public static ProductGETResponse getMockProductGETResponse() {
			ProductGETResponse productGETResponse = new ProductGETResponse();
			Data8 data = new Data8();
			List<Product> product = new ArrayList<>();
			data.setProduct(product);
			productGETResponse.setData(data);
			return productGETResponse;
		}

	}


