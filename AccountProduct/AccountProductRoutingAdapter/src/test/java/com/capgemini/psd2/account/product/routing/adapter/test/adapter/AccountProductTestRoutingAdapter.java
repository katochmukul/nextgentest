package com.capgemini.psd2.account.product.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.product.routing.adapter.test.routing.AccountProductRoutingAdapterTestMockData;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountProductTestRoutingAdapter implements AccountProductsAdapter
{

	@Override
	public ProductGETResponse retrieveAccountProducts(AccountMapping accountMapping, Map<String, String> params) {
		return AccountProductRoutingAdapterTestMockData.getMockProductGETResponse();
	}

}
