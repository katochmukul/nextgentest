package com.capgemini.psd2.product.mongo.db.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.product.mongo.db.adapter.impl.AccountProductMongoDbAdapterImpl;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;

@Configuration
public class AccountProductMongoDbAdapterConfig 
{
	
	@Bean(name="accountProductMongoDbAdapter")
	public AccountProductsAdapter mongoDBAdapter(){
		return new AccountProductMongoDbAdapterImpl();
	}

}
