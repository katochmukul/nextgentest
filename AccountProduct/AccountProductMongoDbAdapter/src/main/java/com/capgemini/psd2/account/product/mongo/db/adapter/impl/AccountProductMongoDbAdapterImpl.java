package com.capgemini.psd2.account.product.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.domain.Data8;
import com.capgemini.psd2.aisp.domain.Product;
import com.capgemini.psd2.aisp.domain.Product.ProductTypeEnum;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

@Component
public class AccountProductMongoDbAdapterImpl implements AccountProductsAdapter {

	@Override
	public ProductGETResponse retrieveAccountProducts(AccountMapping accountMapping, Map<String, String> params) {

		ProductGETResponse productResponse = new ProductGETResponse();
		Data8 data = new Data8();
		List<Product> productList = new ArrayList<>();
		Product product = new Product();
		product.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
		product.setProductIdentifier("51B");
		product.setProductName("321 Product");
		product.setProductType(ProductTypeEnum.PCA);
		product.setSecondaryProductIdentifier("Product Second Identifier");
		productList.add(product);
		data.setProduct(productList);
		productResponse.setData(data);
		return productResponse;
	}
}
