/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.product.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.client.AccountProductFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.constants.AccountProductFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate.AccountProductFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountProductFoundationServiceAdapter.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
public class AccountProductFoundationServiceAdapter implements AccountProductsAdapter {

	@Value("${foundationService.accountProductBaseURL}")
	private String productBaseURL;

	@Value("${foundationService.accountProductEndURL}")
	private String productEndURL;

	/** The account Product foundation service delegate. */
	@Autowired
	private AccountProductFoundationServiceDelegate accountProductFoundationServiceDelegate;

	/** The account Product foundation service client. */
	@Autowired
	private AccountProductFoundationServiceClient accountProductFoundationServiceClient;
	
	/** The AdapterFilterUtility object */
	@Autowired
	private AdapterFilterUtility adapterFilterUtility;
	
	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;
	
	@Override
	public ProductGETResponse retrieveAccountProducts(AccountMapping accountMapping, Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		
		/*For Filtering */
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility
				.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null
				|| filteredAccounts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}

		accountMapping = adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);
		
		
		RequestInfo requestinfo = new RequestInfo();

		AccountDetails accountDetails;
		if ( accountMapping.getAccountDetails() != null
				&& !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		ErrorInfo errorInfo = null; 
		HttpHeaders httpHeaders = accountProductFoundationServiceDelegate.createRequestHeaders(requestinfo,
				accountMapping, params);
		String finalURL = accountProductFoundationServiceDelegate.getFoundationServiceURL(productBaseURL,
				accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), productEndURL);
		requestinfo.setUrl(finalURL);
		Product product = null;
		params.put(AccountProductFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
		try{
			 product = accountProductFoundationServiceClient.restTransportForAccountProduct(requestinfo, Product.class, httpHeaders);
		}
		catch(AdapterException e){
			errorInfo  =  e.getErrorInfo();
			if(errorInfo.getActualErrorCode().equals(AdapterErrorCodeEnum.NO_PRODUCT_FOUND.getErrorCode()))
				product = new Product();
			else
				throw e;
		}catch (Exception e) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		
		return accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(product, params);

	}

}
