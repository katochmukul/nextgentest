/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.Data8;
import com.capgemini.psd2.aisp.domain.Product;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.aisp.transformer.AccountProductsTransformer;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.constants.AccountProductFoundationServiceConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountProductFoundationServiceTransformer.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Configuration
@EnableAutoConfiguration
public class AccountProductFoundationServiceTransformer implements AccountProductsTransformer {

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;

	@Override
	public <T> ProductGETResponse transformAccountProducts(T source, Map<String, String> params) {

		Data8 data8 = new Data8();
		com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product product = (com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product) source;
		ProductGETResponse productGETResponse = new ProductGETResponse();
		if (product.getProductType() == null && product.getProductIdentifier() == null) {
			data8.setProduct(new ArrayList<>());
			productGETResponse.setData(data8);
			return productGETResponse;
		}

		Product response = new Product();
		response.setAccountId(params.get(AccountProductFoundationServiceConstants.ACCOUNT_ID));
		if (!NullCheckUtils.isNullOrEmpty(product.getProductIdentifier())) {
			response.setProductIdentifier(product.getProductIdentifier());
		}
		if (Product.ProductTypeEnum.BCA.toString().equals(product.getProductType())) {
			response.setProductType(Product.ProductTypeEnum.BCA);
		} else if (Product.ProductTypeEnum.PCA.toString().equals(product.getProductType())) {
			response.setProductType(Product.ProductTypeEnum.PCA);
		}
		validator.validate(response);
		data8.addProductItem(response);
		validator.validate(data8);
		productGETResponse.setData(data8);
		return productGETResponse;

	}

}
