package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.client.AccountProductFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate.AccountProductFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer.AccountProductFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountInformationFoundationServiceDelegateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductFoundationServiceDelegateTransformationTest {

	/** The delegate. */
	@InjectMocks
	private AccountProductFoundationServiceDelegate delegate;

	/** The account information foundation service client. */
	@InjectMocks
	private AccountProductFoundationServiceClient accountProductFoundationServiceClient;

	/** The account information FS transformer. */
	@Mock
	private AccountProductFoundationServiceTransformer accountProductFSTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The adapterUtility. */
	@Mock
	private AdapterUtility adapterUtility;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test rest transport for single account information.
	 */
	@Test
	public void testRestTransportForSingleAccountInformation() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Product product = new Product();
	
		
		product.setProductIdentifier("test");
		product.setProductType("BCA");
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(product);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:9087/fs-abt-service/services/account");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		Product res = accountProductFoundationServiceClient.restTransportForAccountProduct(requestInfo, Product.class, httpHeaders);
		assertNotNull(res);
	}
	
	

	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {

		String payeeSortCode = "number";
		String payeeaccountNumber = "accountNumber";
		String endUrl = "product";
		String baseURL = "http://localhost:9087/fs-abt-service/services/account";
		String finalURL = "http://localhost:9087/fs-abt-service/services/account/number/accountNumber/product";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String testURL = delegate.getFoundationServiceURL(baseURL, payeeSortCode, payeeaccountNumber, endUrl);
		assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with invalid URL.
	 */
	@Test
	public void testGetFoundationServiceWithInvalidURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String endUrl = "product";
		String baseURL = "http://localhost:9087/fs-abt-service/services/account";
		String finalURL = "http://localhost:9087/fs-abt-service/services/account/number/accountNumber/account";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String testURL = delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber, endUrl);
		
		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String accountNSC = null;
		String accountNumber = "number";
		String baseURL = "http://localhost:9087/fs-abt-service/services/account";
		String endUrl = "product";
		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber, endUrl);
		
		fail("Invalid account NSC");
	}

	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {
		String accountNumber = "number";
		String accountNSC = "nsc";
		String baseURL = null;
		String endUrl = "product";
		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber, endUrl);

		fail("Invalid base URL");
	}

	/**
	 * Test get foundation service with base URL as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {

		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = "test";
		String endUrl = null;
		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn(null);
		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber, endUrl);

		fail("Invalid end URL");
	}
	/**
	 * Test get foundation service with base URL as null.
	 */

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithChannelIdAsNull() {
		String accountNumber = null;
		String accountNSC = "nsc";
		String baseURL = "http://localhost:9087/fs-abt-service/services/account";
		String endUrl = "product";
		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber, endUrl);

		fail("Invalid Account Number");
	}
	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "correlationReqHeader", "X-CORRELATION-ID");
		Map<String, String> params = new HashMap<>();
		params.put("channelId","channel123");
		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), accountMapping,params);

		assertNotNull(httpHeaders);
	}

	
	
	

	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		Product product = new Product();
		
		
		product.setProductIdentifier("test");
		product.setProductType("BCA");
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(anyObject(),anyObject())).thenReturn(new ProductGETResponse());
		ProductGETResponse res = delegate.transformResponseFromFDToAPI(product, new HashMap<String, String>());
		assertNotNull(res);

	}
	
	


}
