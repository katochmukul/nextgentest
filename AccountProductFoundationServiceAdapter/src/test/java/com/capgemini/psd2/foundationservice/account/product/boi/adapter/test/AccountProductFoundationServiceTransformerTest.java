package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer.AccountProductFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductFoundationServiceTransformerTest {
	@InjectMocks
	private AccountProductFoundationServiceTransformer accountProductFoundationServiceTransformer;
	
	@Mock
	@Qualifier("PSD2ResponseValidator")
	PSD2Validator validator;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		
	}
	@Test
	public void contextLoads() {
		
	}
	
	@Test
	public void testTransformAccountBeneficiaries(){
		Product product= new Product();
		
		product.setProductIdentifier("test");
		product.setProductType("BCA");
		
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");
		Mockito.doNothing().when(validator).validate(anyObject());
		ProductGETResponse response = accountProductFoundationServiceTransformer.transformAccountProducts(product, params);
		assertNotNull(response);
//		Passing ProductType null.
		product.setProductType(null);
		product.setProductIdentifier("test");
		response = accountProductFoundationServiceTransformer.transformAccountProducts(product, params);
		assertNotNull(response);
		// BCA as ProductType
		product.setProductType("BCA");
		response = accountProductFoundationServiceTransformer.transformAccountProducts(product, params);
		assertNotNull(response);
		//PCA as ProductType
		product.setProductType("PCA");
		response = accountProductFoundationServiceTransformer.transformAccountProducts(product, params);
		assertNotNull(response);
		//Product Identifier null
		product.setProductIdentifier(null);
		response = accountProductFoundationServiceTransformer.transformAccountProducts(product, params);
		assertNotNull(response);
		Product emptyProduct = new Product();
		response = accountProductFoundationServiceTransformer.transformAccountProducts(emptyProduct, params);
		assertNotNull(emptyProduct);
	}
	
	
	
	
	
}
