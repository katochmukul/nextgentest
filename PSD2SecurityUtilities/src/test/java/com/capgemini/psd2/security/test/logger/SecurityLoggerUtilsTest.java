package com.capgemini.psd2.security.test.logger;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.logger.SecurityLoggerUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityLoggerUtilsTest {

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private LoggerAttribute loggerAttribute;
	
	@InjectMocks
	private SecurityLoggerUtils securityLoggerUtils;
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetter(){
		//ReflectionTestUtils.setField(securityLoggerUtils, "apiId", "securityApi");
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(requestHeaderAttributes.getRequestId()).thenReturn(UUID.randomUUID());
		assertNotNull(securityLoggerUtils.populateLoggerData("SecurityMethods"));
	}

}
