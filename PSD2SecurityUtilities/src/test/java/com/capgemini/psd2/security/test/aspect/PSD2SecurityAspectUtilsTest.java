package com.capgemini.psd2.security.test.aspect;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.filteration.ResponseFilter;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.security.aspects.PSD2SecurityAspectUtils;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.security.logger.SecurityLoggerUtils;
import com.capgemini.psd2.utilities.JSONUtilities;

public class PSD2SecurityAspectUtilsTest {

	@Mock
	private LoggerAttribute loggerAttribute;

	@Mock
	private SecurityLoggerUtils securityLoggerUtils;

	@Mock
	private ResponseFilter responseFilterUtility;

	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	@Mock
	private DataMask dataMask ;

	@Mock
	private JoinPoint joinPoint;

	@Mock
	private MethodSignature signature;
	@Mock
	private RequestHeaderAttributes reqHeaderAttribute;

	@InjectMocks
	private PSD2SecurityAspectUtils aspect = new PSD2SecurityAspectUtils();

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		when(securityLoggerUtils.populateLoggerData(anyString())).thenReturn(loggerAttribute);
		when(proceedingJoinPoint.getSignature()).thenReturn(signature);
		when(signature.getName()).thenReturn("retrieveAccountBalance");
		when(signature.getDeclaringTypeName()).thenReturn("retrieveAccountBalance");
		//when(proceedingJoinPoint.getArgs()).thenReturn(new Object[1]);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(signature.getDeclaringType()).thenReturn(String.class);
	}

	@Test
	public void methodAdviceImplTest() {
		aspect.methodAdvice(proceedingJoinPoint);
	}

	@Test(expected = PSD2Exception.class)
	public void methodAdviceImplPSD2ExceptionFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodAdvice(proceedingJoinPoint);
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void methodAdviceImplPSD2SecurityExceptionTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2AuthenticationException.populateAuthenticationFailedException(SCAConsentErrorCodeEnum.INVALID_REQUEST));
		aspect.methodAdvice(proceedingJoinPoint);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void methodAdviceImplPSD2ExceptionTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.class);
		aspect.methodAdvice(proceedingJoinPoint);
	}

	@Test(expected = PSD2Exception.class)
	public void methodAdviceImplThrowableFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(new RuntimeException("Test"));
		aspect.methodAdvice(proceedingJoinPoint);
	}

	@Test(expected = PSD2Exception.class)
	public void methodPayloadAdviceThrowableFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(new RuntimeException("Test"));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}

	@Test(expected = PSD2Exception.class)
	public void methodPayloadAdvicePSD2ExceptionFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void methodPayloadAdvicePSD2AuthenticationExceptionTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2AuthenticationException.populateAuthenticationFailedException(SCAConsentErrorCodeEnum.INVALID_REQUEST));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void methodPayloadAdvicePSD2ExceptionTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.class);
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	

	/*@Test(expected = PSD2Exception.class)
	public void methodPayloadAdviceMaskPayloadLog() throws Throwable{
		Boolean maskPayloadLog = true;
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", maskPayloadLog );
		Boolean payloadLog = true;
		ReflectionTestUtils.setField(aspect, "payloadLog", payloadLog);
		when(dataMask.maskRequestLog(any(), any())).thenReturn(null);
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}*/
	
	/*@Test(expected = PSD2Exception.class)
	public void methodPayloadAdvicePayloadLogTrue() throws Throwable{
		Boolean payloadLog = true;
		ReflectionTestUtils.setField(aspect, "payloadLog", payloadLog);
		when(dataMask.maskRequestLog(any(), any())).thenReturn(null);
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}*/
	

	@Test(expected = PSD2Exception.class)
	public void methodPayloadAdviceMaskPayloadTrue() throws Throwable{
		Boolean maskPayload = true;
		ReflectionTestUtils.setField(aspect, "maskPayload", maskPayload );
		when(dataMask.maskRequestLog(any(), any())).thenReturn(null);
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	
	@Test
	public void methodPayloadAdviceTest() throws Throwable{
		Boolean payloadLog = true;
		ReflectionTestUtils.setField(aspect, "payloadLog", payloadLog);
		Object[] obj2 = {"123","456"};
		when(proceedingJoinPoint.getArgs()).thenReturn(obj2);
		Object[] obj = {"{\"tppInformation\":{\"tppLegalEntityName\":\"Moneywise Wealth\",\"tppRegisteredId\":\"123456\",\"tppRoles\":[\"PISP\",\"AISP\"]},\"scope\":[\"accounts\"],\"exp\":1507888967,\"client_id\":\"6795ee9ca8e3407694f866725303db37\"}"};
		when(dataMask.maskMRequestLog(any(), any())).thenReturn(obj);
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test
	public void methodPayloadAdviceMaskPayloadLogTrue() throws Throwable{
		Boolean maskPayloadLog = true;
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", maskPayloadLog);
		Boolean payloadLog = true;
		ReflectionTestUtils.setField(aspect, "payloadLog", payloadLog);
		Object[] obj2 = {"123","456"};
		when(proceedingJoinPoint.getArgs()).thenReturn(obj2);
		Object[] obj = {"{\"tppInformation\":{\"tppLegalEntityName\":\"Moneywise Wealth\",\"tppRegisteredId\":\"123456\",\"tppRoles\":[\"PISP\",\"AISP\"]},\"scope\":[\"accounts\"],\"exp\":1507888967,\"client_id\":\"6795ee9ca8e3407694f866725303db37\"}"};
		when(dataMask.maskRequestLog(any(), any())).thenReturn(obj);
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test
	public void methodPayloadAdviceMaskPayloadLogFalse() throws Throwable{
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
}
