/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.security.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Class PSD2Aspect.
 */
@Component
@Aspect
public class PSD2SecurityAspect {


	@Autowired
	private PSD2SecurityAspectUtils securityAspectUtils;

	/**
	 * Arround logger advice service.
	 *
	 * @param proceedingJoinPoint the proceeding join point
	 * @return the object
	 * @throws Throwable 
	 */
	@Around("(execution(* com.capgemini.psd2.security..handlers..*.* (..)))()")
	public Object arroundLoggerAdviceService(ProceedingJoinPoint proceedingJoinPoint) {
		return securityAspectUtils.methodAdvice(proceedingJoinPoint);
	}
	
	/*@Around("(execution(* com.capgemini.psd2.security..filters..*.* (..)))()")
	public Object arroundLoggerAdviceFilter(ProceedingJoinPoint proceedingJoinPoint) {
		return securityAspectUtils.methodAdvice(proceedingJoinPoint);
	}*/

	/**
	 * Arround logger advice controller.
	 *
	 * @param proceedingJoinPoint the proceeding join point
	 * @return the object
	 * @throws Throwable 
	 */
	@Around("(execution(* com.capgemini.psd2.security..controllers..* (..)))()")
	public Object arroundLoggerAdviceController(ProceedingJoinPoint proceedingJoinPoint) {
		return securityAspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	


}
