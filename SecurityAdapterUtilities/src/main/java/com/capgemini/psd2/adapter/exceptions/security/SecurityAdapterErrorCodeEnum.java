/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions.security;

/**
 * The Enum AdapterErrorCodeEnum.
 */
public enum SecurityAdapterErrorCodeEnum {

	
	/** The technical error. */
	TECHNICAL_ERROR("800","Technical Error. Please try again later","500"),

	/** The bad request. */
	BAD_REQUEST("801","Bad request.Please check your request","400"),
	
	NO_VALID_BRAND_ID("802","No valid Brand id provided in request","400"),
	
	NO_VALID_CHANNELS_FOUND("803","No valid channel found","400"),
	
	AUTHENTICATION_FAILURE_ERROR_LOGIN("804","Authentication failed","401"),
	
	INVALID_REQUEST_SETUP_ID("805","No such record exist against the requested request id","404"),
	
	SETUP_RECORD_WITH_INVALID_STATUS("806","Setup record is having invalid status & does not comply with current flow","400"),

	AUTHENTICATION_FAILURE_ERROR_HEADERS("807","You are not authorised to use this service", "500"),
	
	USER_TEMPORARILY_BLOCKED("808", "User temporarily blocked", "401"),
	
	USER_BLOCKED_ON_B365_OR_BOL("809", "User blocked on B365 or BOL", "401"),
	
	USER_BLOCKED_ON_HID("810", "User blocked on HID", "401"),
	
	USER_NOT_REGISTERED_HID("811", "User is not registered for HID", "401"),
	
	USER_DOES_NOT_EXIT_HID("812", "User does not exist on HID", "401"),
	
	USER_NAME_DOES_NOT_MATCH_HEADER("813", "username does not match header", "500"),
	
	GENERAL_ERROR("814", "General Error", "500"),
	
	USER_BLOCKED_ONE_HOUR("815","Authentication failed","401");

	
	/** The error code. */
	private String errorCode;
	
	/** The error message. */
	private String errorMessage;
	
	/** The status code. */
	private String statusCode;

	/**
	 * Instantiates a new adapter error code enum.
	 *
	 * @param errorCode the error code
	 * @param errorMesssage the error messsage
	 * @param statusCode the status code
	 */
	SecurityAdapterErrorCodeEnum(String errorCode,String errorMesssage,String statusCode){
		this.errorCode=errorCode;
		this.errorMessage=errorMesssage;
		this.statusCode = statusCode;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}

}
