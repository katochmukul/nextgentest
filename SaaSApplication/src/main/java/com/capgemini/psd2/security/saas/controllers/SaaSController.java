/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.saas.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
public class SaaSController {

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@RequestMapping("/errors")
	public String error(Model model, HttpServletRequest request) {
		//ErrorInfo errorInfo = (ErrorInfo) request.getAttribute(PSD2SecurityConstants.EXCEPTION);
		//if(errorInfo!=null)
			//errorInfo.setDetailErrorMessage(null); // Added as in case of technical exceptions thrown by java etc.., detailErorMessage contains double quote ("), which create issue in converting into json object
/*		model.addAttribute(PSD2SecurityConstants.EXCEPTION, errorInfo);
		model.addAttribute(PSD2SecurityConstants.OAUTH_URL_PARAM, request.getAttribute(PSD2SecurityConstants.OAUTH_URL_PARAM));
*/		model.addAttribute(PSD2Constants.CORRELATION_ID,requestHeaderAttributes.getCorrelationId());
		return "index";
	}
	
	@RequestMapping(value = "/signin")
	public String consentView(Model model, @RequestParam(value = "oAuthUrl", required = true) String oAuthUrl) {
		return "index";
	}
}