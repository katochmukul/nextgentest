/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*//*

package com.capgemini.psd2.security.saas.model;

import com.capgemini.psd2.security.models.JwtToken;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.jsonwebtoken.Claims;

public final class AccessJwtToken implements JwtToken {
    private final String rawToken;
    @JsonIgnore private Claims claims;

    public AccessJwtToken(final String token, Claims claims) {
        this.rawToken = token;
        this.claims = claims;
    }

    public String getToken() {
        return this.rawToken;
    }

    public Claims getClaims() {
        return claims;
    }
}*/