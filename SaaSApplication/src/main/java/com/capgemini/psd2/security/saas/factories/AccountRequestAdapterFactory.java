package com.capgemini.psd2.security.saas.factories;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

public interface AccountRequestAdapterFactory {
	public AccountRequestAdapter getAccountRequestAdapter(String requestAdapter);
}
