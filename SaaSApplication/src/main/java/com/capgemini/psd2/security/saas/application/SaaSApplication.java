/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*//*

package com.capgemini.psd2.security.saas.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableEurekaClient
public class SaaSApplication {
	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		context = SpringApplication.run(SaaSApplication.class, args);

	}
	@Bean
	public RequestContextListener requestContextListener(){
		return new RequestContextListener();
	}
	
	@Bean
	public FilterRegistrationBean saasFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();//buildPSD2SecurityFilter());
		filterRegBean.addUrlPatterns("/index.jsp");
		return filterRegBean;
	}	

	@Bean
	public PSD2SecurityFilter buildPSD2SecurityFilter(){
		return new PSD2SecurityFilter();
	}
	
	@Bean(name = "authAdapter")
	public AuthenticationAdapter authenticationAdapter() {
		return new SaaSMongoDbAdaptorImpl();
	}
}
*/