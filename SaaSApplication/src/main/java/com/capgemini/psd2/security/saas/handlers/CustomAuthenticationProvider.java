/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */

package com.capgemini.psd2.security.saas.handlers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.utilities.DateUtilites;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Autowired
	@Qualifier("authAdapter")
	private AuthenticationAdapter authenticaitonAdapter;
	
	@Value("${app.defaultRoutingAdapterAccountReq:#{null}}")
	private String accountRequestRoutingAdapter;
	
	@Value("${app.defaultAdapterAccountReq}")
	private String defaultAccountRequestAdapter;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	/**
	 * Its implemented method of AuthenticationProvider to perform MFA of the
	 * bank's customer, one by normal authentication with Ping Dir.(LDAP) & Ping
	 * ID (Mobile Notification).
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\",\"correlationId\":\"{}\"}","com.capgemini.psd2.security.alpha.saas.handlers.CustomAuthenticationProvider.authenticate()",DateUtilites.generateCurrentTimeStamp(), requestHeaderAttributes.getCorrelationId());
		try{
			
			//Map<String,String[]> paramMap = securityRequestAttributes.getParamMap();
			
	/*		if(NullCheckUtils.isNullOrEmpty(authentication.getPrincipal()) || NullCheckUtils.isNullOrEmpty(authentication.getCredentials())){
				throw PSD2AuthenticationException.populateAuthenticationFailedException(
						SecurityErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank.");
			}
*/			
			authenticateUser(authentication);

/*			if(ConsentTypeCodeEnum.isAISPFlow(securityRequestAttributes.getRequestIdParamName())){
				AccountRequestAdapter accountRequestAdapter = getRequestAdapter();
				AccountRequestPOSTResponse accountReq = getAccountRequestObject(accountRequestAdapter,paramMap);
				validateCurrentAccountRequestStatus(accountReq);
				authentication = authenticateUser(authentication,paramMap);
				accountRequestAdapter.updateAccountRequestResponse(accountReq.getData().getAccountRequestId(), Data1.StatusEnum.AWAITINGAUTHORISATION, paramMap);

			}
			else if(ConsentTypeCodeEnum.isPISPFlow(securityRequestAttributes.getRequestIdParamName())){
				validateCurrentPaymentRequestStatus();
				authentication = authenticateUser(authentication,paramMap);
			}*/
			
			/*else if(ConsentTypeCodeEnum.isCISPFlow(securityRequestAttributes.getRequestIdParamName())){
				// TODO
			}
			else if(ConsentTypeCodeEnum.isPISPFlow(securityRequestAttributes.getRequestIdParamName())){
				// TODO
			}*/			
		}
		catch(PSD2AuthenticationException e){
			throw e;
		}
		catch(PSD2Exception e){
			throw PSD2AuthenticationException.populateAuthenticationFailedException(e.getErrorInfo());
		}
		/*catch(Exception e){
			throw PSD2AuthenticationException.populateAuthenticationFailedException(SecurityErrorCodeEnum.TECHNICAL_ERROR,e.getMessage());
		}*/
		
		LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\":\"{}\"}","com.capgemini.psd2.security.alpha.saas.handlers.CustomAuthenticationProvider.authenticate()",DateUtilites.generateCurrentTimeStamp(), requestHeaderAttributes.getCorrelationId());
		return authentication;
	}

/*	private Authentication authenticateUser(Authentication authentication,Map<String,String> paramMap) {
		Map<String,String> headers = new HashMap<String,String>();
		headers.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
		return authenticaitonAdapter.authenticate(authentication,headers);
	}*/

	private Authentication authenticateUser(Authentication authentication) {
		Map<String,String> headers = new HashMap<String,String>();
		headers.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
		return authenticaitonAdapter.authenticate(authentication,headers);
	}
	
	/**
	 * 
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
	
/*	private void validateCurrentAccountRequestStatus(AccountRequestPOSTResponse accountReq){
		Data1.StatusEnum accountRequestStatus = accountReq.getData().getStatus();
		if (accountRequestStatus == null
				|| ( !accountRequestStatus.toString().equalsIgnoreCase(Data1.StatusEnum.AWAITINGAUTHORISATION.toString()))) {
			throw PSD2AuthenticationException.populateAuthenticationFailedException(
					SecurityErrorCodeEnum.SETUP_RECORD_WITH_INVALID_STATUS, "Account request status is not valid.");
		}
	}
	
	private AccountRequestPOSTResponse getAccountRequestObject(AccountRequestAdapter accountRequestAdapter, Map<String,String> paramMap){
		AccountRequestPOSTResponse accountReq = accountRequestAdapter.getAccountRequestGETResponse(securityRequestAttributes.getParamMap().get(securityRequestAttributes.getRequestIdParamName()),paramMap);
		if(accountReq == null || accountReq.getData() == null){
			throw PSD2AuthenticationException.populateAuthenticationFailedException(SecurityErrorCodeEnum.INVALID_REQUEST_SETUP_ID,"No such record exist against the requested request id");
		}
		return accountReq;
	}

	private AccountRequestAdapter getRequestAdapter() {
		//If Routing adapter is not mentioned in the configuration file, then direct adapter instance is returned.
		if(accountRequestRoutingAdapter == null){
			return accountRequestAdapterFactory.getAccountRequestAdapter(defaultAccountRequestAdapter);
		}
		else{
			return accountRequestAdapterFactory.getAccountRequestAdapter(accountRequestRoutingAdapter);
		}
	}
	
	private void validateCurrentPaymentRequestStatus() {
		PaymentSetupPOSTResponse paymentSetUpResponseData = paymentSetupAdapterHelper.retrieveStagedPaymentSetup(securityRequestAttributes.getParamMap().get(securityRequestAttributes.getRequestIdParamName()));
		
		if(paymentSetUpResponseData == null || paymentSetUpResponseData.getData() == null){
			throw PSD2AuthenticationException.populateAuthenticationFailedException(
					SecurityErrorCodeEnum.INVALID_REQUEST_SETUP_ID, "No such record exist against the requested request id.");
		}
		PaymentSetupResponse.StatusEnum paymentSetupStatus = paymentSetUpResponseData.getData().getStatus();
		if( paymentSetupStatus == null ||  !PaymentSetupResponse.StatusEnum.ACCEPTEDTECHNICALVALIDATION.toString().equalsIgnoreCase(paymentSetupStatus.toString())){
			throw PSD2SecurityException.populatePSD2SecurityException( "PaymentSetup status is not valid.",
					SecurityErrorCodeEnum.SETUP_RECORD_WITH_INVALID_STATUS);
		}
	}*/
}
