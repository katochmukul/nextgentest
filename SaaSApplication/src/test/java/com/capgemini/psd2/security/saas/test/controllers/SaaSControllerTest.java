/*package com.capgemini.psd2.security.saas.test.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.controllers.SaaSController;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSControllerTest {
	
	private MockMvc mockMvc;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes; 
	
	@InjectMocks
	private SaaSController SaaSController;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(SaaSController).dispatchOptions(true).build();
	}

	@Test
	public void testError() throws Exception {
		ErrorInfo errorInfo = mock(ErrorInfo.class);
		String encOAuthUrl = SaaSMockData.getMockEncOAuthUrl();
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		mockMvc.perform(get("/errors").contentType(MediaType.APPLICATION_JSON)
									  .requestAttr(PSD2SecurityConstants.EXCEPTION, errorInfo)
									  .requestAttr(PSD2SecurityConstants.OAUTH_URL_PARAM,encOAuthUrl)).andExpect(status().isOk())
																									.andExpect(model().attributeExists(PSD2SecurityConstants.EXCEPTION,PSD2SecurityConstants.OAUTH_URL_PARAM))
																									.andExpect(view().name("index"));
	}
	
	@Test
	public void consentViewWithNoOAuthUrlParam() throws Exception {
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}
	
	@Test
	public void consentViewWithOAuthUrlParam() throws Exception {
		String encOAuthUrl = SaaSMockData.getMockEncOAuthUrl();
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)
										 .param("oAuthUrl", encOAuthUrl)).andExpect(status().isOk())
												 						 .andExpect(view().name("index"));
	}

}
*/