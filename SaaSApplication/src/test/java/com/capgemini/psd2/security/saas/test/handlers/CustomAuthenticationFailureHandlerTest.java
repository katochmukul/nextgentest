/*package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SecurityErrorCodeEnum;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationFailureHandler;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomAuthenticationFailureHandlerTest {
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@InjectMocks
	private CustomAuthenticationFailureHandler failureHandler;
	
	private HttpServletRequest request = SaaSMockData.getMockHttpServletRequest();
	private HttpServletResponse response = SaaSMockData.getMockHttpServletResponse();
	
	private String correlationID = "12345";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void onAuthenticationFailureTest() throws ServletException,IOException{
		when(requestHeaderAttribute.getCorrelationId()).thenReturn(correlationID);
		when(request.getParameter(PSD2SecurityConstants.SAAS_URL_PARAM)).thenReturn(SaaSMockData.getSaaSUrl());
		when(request.getAttribute(PSD2SecurityConstants.OAUTH_URL_PARAM)).thenReturn(SaaSMockData.getMockEncOAuthUrl());
		when(request.getRequestDispatcher(anyString())).thenReturn(SaaSMockData.getMockRequestDispatcher());
		AuthenticationException exception = PSD2AuthenticationException.populateAuthenticationFailedException(SecurityErrorCodeEnum.TECHNICAL_ERROR);
		failureHandler.onAuthenticationFailure(request, response, exception);
		assert(true);
	}
	
	*//**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 *//*
	@After
	public void tearDown() throws Exception {
		failureHandler = null;
	}

}
*/