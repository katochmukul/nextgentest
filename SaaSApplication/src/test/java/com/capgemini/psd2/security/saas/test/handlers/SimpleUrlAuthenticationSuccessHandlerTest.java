/*package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.factories.JwtTokenFactory;
import com.capgemini.psd2.security.saas.handlers.SimpleUrlAuthenticationSuccessHandler;
import com.capgemini.psd2.security.saas.model.JwtSettings;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class SimpleUrlAuthenticationSuccessHandlerTest {
	
	HttpServletRequest request = SaaSMockData.getMockHttpServletRequest();
	HttpServletResponse response = SaaSMockData.getMockHttpServletResponse();
	Authentication authentication = SaaSMockData.getMockAuthentication();
	
	private JwtSettings settings = mock(JwtSettings.class);
	@Mock
	private RequestHeaderAttributes requestHeaderAttribute;
	private JwtTokenFactory tokenFactory = new JwtTokenFactory(settings);
	@InjectMocks
	private SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler(tokenFactory,settings);
	
	private String correlationID = "12345";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		SaaSMockData.setSecurityContextHolder();
	}
	
	@Test
	public void onAuthenticationSuccessTest() throws ServletException,IOException {
		when(requestHeaderAttribute.getCorrelationId()).thenReturn(correlationID);
		when(request.getParameter(PSD2SecurityConstants.OAUTH_URL_PARAM)).thenReturn(SaaSMockData.getMockEncOAuthUrl());
		when(settings.getTokenIssuer()).thenReturn("xyz");
		when(settings.getTokenExpirationTime()).thenReturn(15);
		when(settings.getTokenSigningKey()).thenReturn(SaaSMockData.tokenSigningKey);
		successHandler.onAuthenticationSuccess(request, response, authentication);
		assert(true);
	}
	
	*//**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 *//*
	@After
	public void tearDown() throws Exception {
		successHandler = null;
	}


}
*/