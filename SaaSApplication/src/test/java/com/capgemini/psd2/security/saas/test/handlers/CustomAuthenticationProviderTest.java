/*package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SecurityErrorCodeEnum;
import com.capgemini.psd2.security.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.saas.factories.AccountRequestAdapterFactory;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationProvider;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;
import com.capgemini.psd2.security.validation.ConsentTypeCodeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomAuthenticationProviderTest {
	
	@Mock
	private AuthenticationAdapter authenticaitonAdapter;
	
	@Mock
	private SecurityRequestAttributes securityRequestAttributes;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes; 
	
	@Mock
	private AccountRequestAdapterFactory accountRequestAdapterFactory;
	
	@Mock
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	
	@InjectMocks
	private CustomAuthenticationProvider authenticationProvider;
	
	private AccountRequestAdapter accountRequestAdapter = SaaSMockData.getMockAccountRequestAdapter();
	
	private Authentication mockAuthentication = SaaSMockData.getMockAuthentication();
	
	private String correlationId = "12345";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void authenticateTestForAISPFlow() {
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		when(accountRequestAdapterFactory.getAccountRequestAdapter(anyString())).thenReturn(accountRequestAdapter);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(),anyMapOf(String.class, String.class))).thenReturn(SaaSMockData.getMockAccountRequestPOSTResponse());
		when(authenticaitonAdapter.authenticate(anyObject(), anyMapOf(String.class, String.class))).thenReturn(mockAuthentication);
		when(accountRequestAdapter.updateAccountRequestResponse(anyString(), anyObject(), anyMapOf(String.class, String.class))).thenReturn(null);
		when(securityRequestAttributes.getParamMap()).thenReturn(SaaSMockData.getMockParamMap());
		
		authenticationProvider.authenticate(mockAuthentication);
		assert(true);
	}
	
	@Test
	public void authenticateTestForCISPFlow(){
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.FundsCheckRequestId.name());
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		authenticationProvider.authenticate(mockAuthentication);
		assert(true); //Will update the test when the code for CISP will be developed
	}
	
	@Test
	public void authenticateTestForPISPFlow(){
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		paymentSetupPOSTResponse.setData(data);
		
		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyString())).thenReturn(paymentSetupPOSTResponse);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.PaymentId.name());
		authenticationProvider.authenticate(mockAuthentication);
		assert(true); //Will update the test when the code for CISP will be developed
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void TestForNoAccountRequestFound(){
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(accountRequestAdapterFactory.getAccountRequestAdapter(anyString())).thenReturn(accountRequestAdapter);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(),anyMapOf(String.class, String.class))).thenReturn(null);
		authenticationProvider.authenticate(mockAuthentication);
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void TestForInvalidAccountStatus(){
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(accountRequestAdapterFactory.getAccountRequestAdapter(anyString())).thenReturn(accountRequestAdapter);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(),anyMapOf(String.class, String.class))).thenReturn(SaaSMockData.getMockAccountRequestPOSTResponseForRejectedStatus());
		authenticationProvider.authenticate(mockAuthentication);
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void TestForUnsuccessfulAuthentication(){
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(accountRequestAdapterFactory.getAccountRequestAdapter(anyString())).thenReturn(accountRequestAdapter);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(),anyMapOf(String.class, String.class))).thenReturn(SaaSMockData.getMockAccountRequestPOSTResponseForRejectedStatus());
		when(authenticaitonAdapter.authenticate(anyObject(), anyMapOf(String.class, String.class))).thenThrow(PSD2AuthenticationException.populateAuthenticationFailedException(SecurityErrorCodeEnum.USER_NOT_AUTHENTICATED,"Authentication failed."));
		authenticationProvider.authenticate(mockAuthentication);
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void TestForGenericExceptionDuringAuthentication(){
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(accountRequestAdapterFactory.getAccountRequestAdapter(anyString())).thenReturn(accountRequestAdapter);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(),anyMapOf(String.class, String.class))).thenReturn(SaaSMockData.getMockAccountRequestPOSTResponse());
		when(authenticaitonAdapter.authenticate(anyObject(), anyMapOf(String.class, String.class))).thenThrow(new RuntimeException("error message"));
		authenticationProvider.authenticate(mockAuthentication);
	}
	
	@Test(expected = PSD2AuthenticationException.class)
	public void TestForUnsuccessfulStatusUpdate(){
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(correlationId);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(accountRequestAdapterFactory.getAccountRequestAdapter(anyString())).thenReturn(accountRequestAdapter);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(),anyMapOf(String.class, String.class))).thenReturn(SaaSMockData.getMockAccountRequestPOSTResponse());
		when(authenticaitonAdapter.authenticate(anyObject(), anyMapOf(String.class, String.class))).thenReturn(mockAuthentication);
		when(accountRequestAdapter.updateAccountRequestResponse(anyString(), anyObject(), anyMapOf(String.class, String.class))).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		authenticationProvider.authenticate(mockAuthentication);
	}
	
	*//**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 *//*
	@After
	public void tearDown() throws Exception {
		authenticationProvider = null;
	}


}
*/