/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.routing.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.information.routing.adapter.impl.AccountInformationRoutingAdapter;
import com.capgemini.psd2.account.information.routing.adapter.routing.AccountInformationAdapterFactory;
import com.capgemini.psd2.account.information.routing.adapter.test.adapter.AccountInformationTestRoutingAdapter;
import com.capgemini.psd2.account.information.routing.adapter.test.mock.data.AccountInformationRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;

/**
 * The Class AccountInformationRoutingAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationRoutingAdapterTest {

	/** The account information adapter factory. */
	@Mock
	private AccountInformationAdapterFactory accountInformationAdapterFactory;

	/** The account information routing adapter. */
	@InjectMocks
	private AccountInformationAdapter accountInformationRoutingAdapter = new AccountInformationRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test retrieve account information success flow.
	 */
	@Test
	public void testRetrieveAccountInformationSuccessFlow() {
		AccountInformationAdapter accountInformationAdaptor = new AccountInformationTestRoutingAdapter();

		Mockito.when(accountInformationAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountInformationAdaptor);

		AccountGETResponse accountGETResponse = accountInformationRoutingAdapter
				.retrieveAccountInformation(AccountInformationRoutingAdapterTestMockData.getAccountMapping(), null);
		
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getAccountId(),
				accountGETResponse.getData().getAccount().get(0).getAccountId());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getAccount()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());

		assertEquals(AccountInformationRoutingAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getServicer()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getAccountGETResponse(), accountGETResponse);

	}
	
	
	/**
	 * Test retrieve multiple accounts information success flow.
	 */
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlow() {
		AccountInformationAdapter accountInformationAdaptor = new AccountInformationTestRoutingAdapter();

		Mockito.when(accountInformationAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountInformationAdaptor);

		AccountGETResponse accountGETResponse = accountInformationRoutingAdapter
				.retrieveMultipleAccountsInformation(AccountInformationRoutingAdapterTestMockData.getAccountMapping(), null);
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse().getData().getAccount().get(0).getAccountId(),
				accountGETResponse.getData().getAccount().get(0).getAccountId());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse().getData().getAccount().get(1).getAccountId(),
				accountGETResponse.getData().getAccount().get(1).getAccountId());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse().getData().getAccount().get(0).getAccount()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse().getData().getAccount().get(1).getAccount()
				.getIdentification(), accountGETResponse.getData().getAccount().get(1).getAccount().getIdentification());

		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse().getData().getAccount().get(0).getServicer()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse().getData().getAccount().get(1).getServicer()
				.getIdentification(), accountGETResponse.getData().getAccount().get(1).getServicer().getIdentification());
		
		assertEquals(AccountInformationRoutingAdapterTestMockData.getMultipleAccountGETResponse(), accountGETResponse);

	}
}
