/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.information.service.impl.AccountInformationServiceImpl;
import com.capgemini.psd2.account.information.test.mock.data.AccountInformationTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountInformationServiceImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationServiceImplTest {

	/** The account information adapter. */
	@Mock
	private AccountInformationAdapter accountInformationAdapter;

	/** The consent mapping adapter. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The request header attributes. */
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	/** The service. */
	@InjectMocks
	private AccountInformationServiceImpl service;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test retrieve account information success flow With Links And Data Null.
	 */
	@Test
	public void testRetrieveAccountInformationSuccessFlowWithLinksAndDataNull() {
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
				.thenReturn(AccountInformationTestMockData.getAccountMapping());
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());
		AccountGETResponse accountGETResponseMock = AccountInformationTestMockData.getAccountGETResponse();
		when(accountInformationAdapter.retrieveAccountInformation(any(AccountMapping.class), anyMap()))
				.thenReturn(accountGETResponseMock);
		AccountGETResponse accountGETResponse = service
				.retrieveAccountInformation("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccountId(),
				accountGETResponse.getData().getAccount().get(0).getAccountId());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccount().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getServicer().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());

	}

	/**
	 * Test retrieve account information success flow With Links And Data Not
	 * Null.
	 */
	@Test
	public void testRetrieveAccountInformationSuccessFlowWithLinksAndDataNotNull() {
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
				.thenReturn(AccountInformationTestMockData.getAccountMapping());
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());
		AccountGETResponse accountGETResponseMock = AccountInformationTestMockData.getAccountGETResponse();
		accountGETResponseMock.setLinks(new Links());
		accountGETResponseMock.setMeta(new MetaData());
		when(accountInformationAdapter.retrieveAccountInformation(any(AccountMapping.class), anyMap()))
				.thenReturn(accountGETResponseMock);
		AccountGETResponse accountGETResponse = service
				.retrieveAccountInformation("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccountId(),
				accountGETResponse.getData().getAccount().get(0).getAccountId());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccount().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getServicer().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());

	}

	/**
	 * Test retrieve account information null exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountInformationNullException() {

		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
				.thenReturn(AccountInformationTestMockData.getAccountMapping());
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());

		when(accountInformationAdapter.retrieveAccountInformation(any(AccountMapping.class), anyMap()))
				.thenReturn(null);
		service.retrieveAccountInformation("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
	}

	/**
	 * Test retrieve multiple accounts information success flow With Links And
	 * Data Null.
	 */
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlowWithLinksAndData() {
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountInformationTestMockData.getAccountMapping());
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());
		AccountGETResponse accountGETResponseMock = AccountInformationTestMockData.getMultipleAccountGETResponse();
		accountGETResponseMock.setLinks(new Links());
		accountGETResponseMock.setMeta(new MetaData());
		when(accountInformationAdapter.retrieveMultipleAccountsInformation(any(AccountMapping.class), anyMap()))
				.thenReturn(accountGETResponseMock);
		AccountGETResponse accountGETResponse = service.retrieveMultipleAccountsInformation();
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccountId(),
				accountGETResponse.getData().getAccount().get(0).getAccountId());
		assertEquals(accountGETResponseMock.getData().getAccount().get(1).getAccountId(),
				accountGETResponse.getData().getAccount().get(1).getAccountId());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccount().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(1).getAccount().getIdentification(),
				accountGETResponse.getData().getAccount().get(1).getAccount().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getServicer().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(1).getServicer().getIdentification(),
				accountGETResponse.getData().getAccount().get(1).getServicer().getIdentification());
	}

	/**
	 * Test retrieve multiple accounts information success flow With Links And
	 * Data Null.
	 */
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlowWithLinksAndDataNull() {
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountInformationTestMockData.getAccountMapping());
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());
		AccountGETResponse accountGETResponseMock = AccountInformationTestMockData.getMultipleAccountGETResponse();
		when(accountInformationAdapter.retrieveMultipleAccountsInformation(any(AccountMapping.class), anyMap()))
				.thenReturn(accountGETResponseMock);
		AccountGETResponse accountGETResponse = service.retrieveMultipleAccountsInformation();
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccountId(),
				accountGETResponse.getData().getAccount().get(0).getAccountId());
		assertEquals(accountGETResponseMock.getData().getAccount().get(1).getAccountId(),
				accountGETResponse.getData().getAccount().get(1).getAccountId());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getAccount().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(1).getAccount().getIdentification(),
				accountGETResponse.getData().getAccount().get(1).getAccount().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(0).getServicer().getIdentification(),
				accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());
		assertEquals(accountGETResponseMock.getData().getAccount().get(1).getServicer().getIdentification(),
				accountGETResponse.getData().getAccount().get(1).getServicer().getIdentification());
	}

	/**
	 * Test retrieve multiple accounts information null FS response.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationWithNullResponse() {

		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountInformationTestMockData.getAccountMapping());
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());

		when(accountInformationAdapter.retrieveMultipleAccountsInformation(any(AccountMapping.class), anyMap()))
				.thenReturn(null);
		try {
			service.retrieveMultipleAccountsInformation();
		} catch (PSD2Exception e) {
			assertEquals("No data found for the requested account", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
}
