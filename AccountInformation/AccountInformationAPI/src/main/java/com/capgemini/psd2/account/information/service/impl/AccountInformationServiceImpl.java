/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.information.service.AccountInformationService;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountInformationServiceImpl.
 */
@Service
public class AccountInformationServiceImpl implements AccountInformationService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account information adapter. */
	@Autowired
	@Qualifier("accountInformationAdapterImpl")
	private AccountInformationAdapter accountInformationAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.account.information.service.AccountInformationService#
	 * retrieveAccountInformation(java.lang.String)
	 */
	@Override
	public AccountGETResponse retrieveAccountInformation(String accountId) {
		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);
		AccountGETResponse accountGETResponse = accountInformationAdapter.retrieveAccountInformation(accountMapping,
				reqHeaderAtrributes.getToken().getSeviceParams());
		if (accountGETResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);
		if (accountGETResponse.getLinks() == null)
			accountGETResponse.setLinks(new Links());
		if (accountGETResponse.getMeta() == null)
			accountGETResponse.setMeta(new MetaData());
		accountGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		accountGETResponse.getMeta().setTotalPages(1);
		return accountGETResponse;
	}

	@Override
	public AccountGETResponse retrieveMultipleAccountsInformation() {
		AccountMapping accountMapping = aispConsentAdapter
				.retrieveAccountMapping(reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		AccountGETResponse accountGETResponse = accountInformationAdapter.retrieveMultipleAccountsInformation(
				accountMapping, reqHeaderAtrributes.getToken().getSeviceParams());

		if (accountGETResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		if (accountGETResponse.getLinks() == null)
			accountGETResponse.setLinks(new Links());
		if (accountGETResponse.getMeta() == null)
			accountGETResponse.setMeta(new MetaData());

		accountGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		if (!accountGETResponse.getData().getAccount().isEmpty())
			accountGETResponse.getMeta().setTotalPages(1);
		return accountGETResponse;
	}
}
