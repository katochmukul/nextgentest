/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mock.domain.MockAccount;

/**
 * The Interface AccountInformationRepository.
 */
public interface AccountInformationRepository extends MongoRepository<MockAccount, String> {
	
	/**
	 * Find by account number.
	 * Find by account nsc.
	 *
	 * @param accountId the account number
	 * @param accountId the account nsc
	 * @return the mock account GET response data
	 */
	public List<MockAccount> findByAccountIdentificationAndServicerIdentification(String accountNumber, String accountNsc);
	

	/**
	 * Find by psuId.
	 * @param <T>
	 *
	 * @param psuId the userId
	 * @return the mock account GET response data
	 */
	public List<MockAccount> findByPsuId(String userId);
}
					
