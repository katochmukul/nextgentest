package com.capgemini.psd2.authentication.application.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceAuthException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;



@RestController
public class AuthenticationApplicationController {

	@Autowired
	private AuthenticationApplicationService authenticationApplicationService;

	@RequestMapping(value = "/fs-authentication-service/services/business/login", method = RequestMethod.POST,consumes= {MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity authenticateBOLUserLogin(@RequestBody AuthenticationRequest authenticationRequest,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) {

		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_HEADER_AUTH);
		}

		String userName = authenticationRequest.getUserName();
		String password = authenticationRequest.getPassword();

		if (NullCheckUtils.isNullOrEmpty(userName) || NullCheckUtils.isNullOrEmpty(password)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.GENERAL_ERROR_AUTH);
		}

		authenticationApplicationService.retrieveBOLUserCredentials(userName, password);

		return new ResponseEntity(HttpStatus.NO_CONTENT);

	}
	
	
	@RequestMapping(value = "/fs-authentication-service/services/personal/login", method = RequestMethod.POST, consumes= {MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity authenticateB365UserLogin(@RequestBody AuthenticationRequest authenticationRequest,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) {

		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_HEADER_AUTH);
		}

		String userName = authenticationRequest.getUserName();
		String password = authenticationRequest.getPassword();

		if (NullCheckUtils.isNullOrEmpty(userName) || NullCheckUtils.isNullOrEmpty(password)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.GENERAL_ERROR_AUTH);
		}

		authenticationApplicationService.retrieveB365UserCredentials(userName, password);

		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
}
