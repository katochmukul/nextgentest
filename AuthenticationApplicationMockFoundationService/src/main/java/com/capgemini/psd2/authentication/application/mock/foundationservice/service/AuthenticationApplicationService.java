package com.capgemini.psd2.authentication.application.mock.foundationservice.service;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;

public interface AuthenticationApplicationService {

	public AuthenticationRequest retrieveBOLUserCredentials(String userName, String password);
	
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password);

}
