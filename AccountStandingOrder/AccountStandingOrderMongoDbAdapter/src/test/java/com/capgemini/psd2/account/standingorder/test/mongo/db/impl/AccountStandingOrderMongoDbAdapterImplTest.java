package com.capgemini.psd2.account.standingorder.test.mongo.db.impl;

import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.standingorder.mongo.db.adapter.impl.AccountStandingOrderMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderMongoDbAdapterImplTest
{
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/** The account standing order mongo DB adapter impl. */
	@InjectMocks
	private AccountStandingOrderMongoDbAdapterImpl accountStandingOrderMongoDbAdapterImpl;

	/**
	 * Test retrieve blank response account standing order success flow.
	 */

	
	@Test
	public void testRetrieveAccountStandingOrdersSuccessFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetailList = new ArrayList<>();
		AccountDetails accountDetails =new AccountDetails();
		accntDetailList.add(accountDetails);
		accountDetails.setAccountId("1234576");
		accountMapping.setAccountDetails(accntDetailList);
		Map<String, String> params = new HashMap<>();
		StandingOrdersGETResponse standingOrdersGETResponse = accountStandingOrderMongoDbAdapterImpl.retrieveAccountStandingOrders(accountMapping, params);
		StandingOrdersGETResponse x = new StandingOrdersGETResponse();
		assertNotNull(x);	
	}
	
	


}
