package com.capgemini.psd2.account.standingorder.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.domain.Data4CreditorAccount;
import com.capgemini.psd2.aisp.domain.Data4CreditorAccount.SchemeNameEnum;
import com.capgemini.psd2.aisp.domain.Data7;
import com.capgemini.psd2.aisp.domain.Data7FinalPaymentAmount;
import com.capgemini.psd2.aisp.domain.Data7FirstPaymentAmount;
import com.capgemini.psd2.aisp.domain.Data7NextPaymentAmount;
import com.capgemini.psd2.aisp.domain.Data7Servicer;
import com.capgemini.psd2.aisp.domain.StandingOrder;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

@Component
public class AccountStandingOrderMongoDbAdapterImpl implements AccountStandingOrdersAdapter {
	@Override
	public StandingOrdersGETResponse retrieveAccountStandingOrders(AccountMapping accountMapping,
			Map<String, String> params) {

		StandingOrdersGETResponse x = new StandingOrdersGETResponse();
		Data7 data = new Data7();
		List<StandingOrder> standingOrderList = new ArrayList<>();
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
		standingOrder.setStandingOrderId("Ben3");
		standingOrder.frequency("EvryWorkgDay");
		standingOrder.reference("Towbar Club 2 - We Love Towbars");
		
		standingOrder.setFirstPaymentDateTime("2017-08-12T00:00:00+00:00");		
		Data7FirstPaymentAmount firstPaymentAmount = new Data7FirstPaymentAmount();
		firstPaymentAmount.setAmount("1.57");
		firstPaymentAmount.setCurrency("GBP");		
		standingOrder.setFirstPaymentAmount(firstPaymentAmount);
		
		standingOrder.setNextPaymentDateTime("2017-08-13T00:00:00+00:00");
		Data7NextPaymentAmount nextPaymentAmount = new Data7NextPaymentAmount();
		nextPaymentAmount.setAmount("1.50");
		nextPaymentAmount.setCurrency("GBP");
		standingOrder.setNextPaymentAmount(nextPaymentAmount);
		
		standingOrder.setFinalPaymentDateTime("2017-08-12T00:00:00+00:00");
		Data7FinalPaymentAmount finalPaymentAmount = new Data7FinalPaymentAmount();
		finalPaymentAmount.setAmount("1.50");
		finalPaymentAmount.setCurrency("GBP");
		standingOrder.setFinalPaymentAmount(finalPaymentAmount);
		
		Data7Servicer servicer = new Data7Servicer();
		servicer.setIdentification("ServicerIdentification");
		servicer.setSchemeName(com.capgemini.psd2.aisp.domain.Data7Servicer.SchemeNameEnum.BICFI);
		standingOrder.setServicer(servicer);
		
		Data4CreditorAccount creditorAccount = new Data4CreditorAccount();
		creditorAccount.setSchemeName(SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		creditorAccount.setIdentification("80200112345678");
		creditorAccount.setName("Mrs Juniper");
		creditorAccount.setSecondaryIdentification("Secondary-Mrs Juniper");
		standingOrder.setCreditorAccount(creditorAccount);
		
		standingOrderList.add(standingOrder);
		data.setStandingOrder(standingOrderList);
		x.setData(data);
		return x;
	}
}