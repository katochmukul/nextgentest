package com.capgemini.psd2.account.standingorder.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


import com.capgemini.psd2.account.standingorder.routing.adapter.routing.AccountStandingOrderAdapterFactory;

import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountStandingOrderRoutingAdapter implements AccountStandingOrdersAdapter
{

	
	/** The account standing order adapter factory. */
	@Autowired
	private AccountStandingOrderAdapterFactory accountStandingOrderAdapterFactory;

	/** The default adapter. */
	@Value("${app.defaultAccountStandingOrdersAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public StandingOrdersGETResponse retrieveAccountStandingOrders(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountStandingOrdersAdapter accountStandingOrdersAdapter = accountStandingOrderAdapterFactory
				.getAdapterInstance(defaultAdapter);
		
		return accountStandingOrdersAdapter.retrieveAccountStandingOrders(accountMapping, params);
	}
	}
	
	


