package com.capgemini.psd2.account.standingorder.test.mock.data;


import java.util.ArrayList;
import java.util.List;


import com.capgemini.psd2.aisp.domain.Data6;
import com.capgemini.psd2.aisp.domain.Data7;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.aisp.domain.StandingOrder;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;

public class AccountStandingOrderRoutingAdapterTestMockData 
{
	public static StandingOrdersGETResponse getMockStandingOrdersGETResponse() {
		StandingOrdersGETResponse StandingOrdersGETResponse = new StandingOrdersGETResponse();
		Data7 data = new Data7();
		List<StandingOrder> standingOrder = new ArrayList<>();
		data.setStandingOrder(standingOrder);
		StandingOrdersGETResponse.setData(data);
		return StandingOrdersGETResponse;
	}
	

}
