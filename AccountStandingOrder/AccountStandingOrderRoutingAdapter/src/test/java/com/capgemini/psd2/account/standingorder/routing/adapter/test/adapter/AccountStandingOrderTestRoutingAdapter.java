package com.capgemini.psd2.account.standingorder.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.standingorder.test.mock.data.AccountStandingOrderRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountStandingOrderTestRoutingAdapter implements AccountStandingOrdersAdapter 
{

	@Override
	public StandingOrdersGETResponse retrieveAccountStandingOrders(AccountMapping accountMapping,
			Map<String, String> params) {
		return AccountStandingOrderRoutingAdapterTestMockData.getMockStandingOrdersGETResponse();
	}
	
	

}
