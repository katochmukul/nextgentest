package com.capgemini.psd2.account.standingorder.test.mockdata;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.Data6;
import com.capgemini.psd2.aisp.domain.Data7;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.aisp.domain.StandingOrder;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountStandingOrderMockData 
{
	
	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;

}
	public static StandingOrdersGETResponse getMockBlankResponse() {
		StandingOrdersGETResponse standingOrdersGETResponse = new StandingOrdersGETResponse();
		Data7 data = new Data7();
		List<StandingOrder> standingOrder = new ArrayList<>();
		data.setStandingOrder(standingOrder);
		standingOrdersGETResponse.setData(data);
		return standingOrdersGETResponse;
	}
}
