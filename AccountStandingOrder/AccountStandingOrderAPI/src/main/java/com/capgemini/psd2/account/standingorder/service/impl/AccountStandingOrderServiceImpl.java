package com.capgemini.psd2.account.standingorder.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.standingorder.service.AccountStandingOrderService;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class AccountStandingOrderServiceImpl implements AccountStandingOrderService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account standing order adapter. */
	@Autowired
	@Qualifier("accountStandingOrderAdapterImpl")
	private AccountStandingOrdersAdapter accountStandingOrderAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Override
	public StandingOrdersGETResponse retrieveAccountStandingOrders(String accountId) {
		
		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);
		
		StandingOrdersGETResponse standingOrdersGETResponse = accountStandingOrderAdapter.retrieveAccountStandingOrders(accountMapping,
				reqHeaderAtrributes.getToken().getSeviceParams());
		
		if (standingOrdersGETResponse.getLinks() == null)
			standingOrdersGETResponse.setLinks(new Links());
		if (standingOrdersGETResponse.getMeta() == null)
			standingOrdersGETResponse.setMeta(new MetaData());
		standingOrdersGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		standingOrdersGETResponse.getMeta().setTotalPages(1);
		return standingOrdersGETResponse;
	}
}
