/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.sync.test;

import java.nio.charset.Charset;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandlerImpl;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * The Class ExceptioHandlerTest.
 */
public class ExceptioHandlerTest {

	/** The resource access exception. */
	@Mock
	private ResourceAccessException resourceAccessException;

	/** The http client error exception. */
	@Mock
	private HttpClientErrorException httpClientErrorException;

	/** The exception handler impl. */
	@InjectMocks
	private ExceptionHandlerImpl exceptionHandlerImpl = new ExceptionHandlerImpl();

	/**
	 * Test resource access exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testResourceAccessException() {
		resourceAccessException = new ResourceAccessException("Connetion Time Out");
		exceptionHandlerImpl.handleException(resourceAccessException);
	}

	/**
	 * Test handle exception with client error.
	 */
	@Test(expected = PSD2Exception.class)
	public void testHandleExceptionWithClientError() {
		ErrorInfo info = new ErrorInfo("123", "message", HttpStatus.BAD_REQUEST.toString());
		/*info.setErrorCode("123");
		info.setErrorMessage("message");*/
		byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
		httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
				Charset.forName("UTF-8"));
		exceptionHandlerImpl.handleException(httpClientErrorException);
	}
}
