package com.capgemini.psd2.pisp.payment.submission.retrieve.service;

import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;

@FunctionalInterface
public interface PaymentSubmissionRetrieveService {
	
	public PaymentSubmitPOST201Response retrievePaymentSubmissionResource(
			SubmissionRetrieveGetRequest submissionRetrieveRequest);

}
