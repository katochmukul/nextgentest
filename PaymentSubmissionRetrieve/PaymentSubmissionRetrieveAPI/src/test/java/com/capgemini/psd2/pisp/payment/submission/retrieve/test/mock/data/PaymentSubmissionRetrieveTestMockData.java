package com.capgemini.psd2.pisp.payment.submission.retrieve.test.mock.data;

import org.joda.time.DateTime;

import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.PispUtilities;

public class PaymentSubmissionRetrieveTestMockData {

	public static PaymentSubmitPOST201Response getPaymentSubmissionResponse(
			SubmissionRetrieveGetRequest submissionRetrieveRequest) {
		
		PaymentSubmitPOST201Response paymentSubmitResponse = new PaymentSubmitPOST201Response();
		PaymentSetupResponse1 paymentSetupResponse = new PaymentSetupResponse1();
		paymentSetupResponse.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		links.self("https://localhost:8085/payment-submissions/" + submissionRetrieveRequest.getPaymentSubmissionId());
		paymentSubmitResponse.setLinks(links);
		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(1);
		paymentSubmitResponse.setMeta(meta);
		paymentSetupResponse.setPaymentSubmissionId(submissionRetrieveRequest.getPaymentSubmissionId());
		paymentSetupResponse.setPaymentId("56789");
		paymentSetupResponse.setStatus(com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum.ACCEPTEDSETTLEMENTCOMPLETED);
		paymentSubmitResponse.setData(paymentSetupResponse);
		return paymentSubmitResponse;
	}

	public static PaymentSubmissionPlatformResource getPaymentsubmissionPlatformResource() {
		PaymentSubmissionPlatformResource resource = new PaymentSubmissionPlatformResource();
		resource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		resource.setId("123");
		resource.setIdempotencyKey("1234");
		resource.setIdempotencyRequest("1234");
		resource.setPaymentId("56789");
		resource.setPaymentSubmissionId("12345");
		resource.setStatus(com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum.ACCEPTEDSETTLEMENTCOMPLETED.name());
		resource.setTppCID("MoneyWise.com");
		resource.setUpdatedAt(PispUtilities.getCurrentDateInISOFormat());
		return resource;
	}

}
