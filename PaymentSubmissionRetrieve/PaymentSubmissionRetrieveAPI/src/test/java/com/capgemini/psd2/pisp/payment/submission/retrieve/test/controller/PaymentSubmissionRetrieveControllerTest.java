package com.capgemini.psd2.pisp.payment.submission.retrieve.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.submission.retrieve.controller.PaymentSubmissionRetrieveController;
import com.capgemini.psd2.pisp.payment.submission.retrieve.service.PaymentSubmissionRetrieveService;
import com.capgemini.psd2.pisp.payment.submission.retrieve.test.mock.data.PaymentSubmissionRetrieveTestMockData;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionRetrieveControllerTest {

	@Mock
	private PaymentSubmissionRetrieveService service;

	@Mock
	private PaymentValidator paymentValidator;

	@InjectMocks
	private PaymentSubmissionRetrieveController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPaymentSubmissionRetrievePositive() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		PaymentSubmitPOST201Response paymentSubmissionResponse = PaymentSubmissionRetrieveTestMockData
				.getPaymentSubmissionResponse(submissionRetrieveRequest);
		Mockito.when(paymentValidator.validateSubmissionRetrieveRequest(submissionRetrieveRequest))
				.thenReturn(Boolean.TRUE);
		Mockito.when(service.retrievePaymentSubmissionResource(submissionRetrieveRequest))
				.thenReturn(paymentSubmissionResponse);

		ResponseEntity<PaymentSubmitPOST201Response> response = controller.retrievePaymentSubmissionResource(submissionRetrieveRequest);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
		assertEquals(response.getBody(), paymentSubmissionResponse);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveException() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(paymentValidator.validateSubmissionRetrieveRequest(submissionRetrieveRequest))
				.thenReturn(Boolean.TRUE);
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setDetailErrorMessage("Payment submission id is not found in system.");
		errorInfo.setErrorCode("942");
		Mockito.when(service.retrievePaymentSubmissionResource(submissionRetrieveRequest))
				.thenThrow(new PSD2Exception(errorInfo.getErrorCode(), errorInfo));

		controller.retrievePaymentSubmissionResource(submissionRetrieveRequest);
	}
	
	@Test
	public void testPaymentSubmissionRetrieveExceptionWithErrorMessage() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "false");
		Mockito.when(paymentValidator.validateSubmissionRetrieveRequest(submissionRetrieveRequest))
				.thenReturn(Boolean.TRUE);
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setDetailErrorMessage("Payment submission id is not found in system.");
		errorInfo.setErrorCode("942");
		errorInfo.setStatusCode("400");
		Mockito.when(service.retrievePaymentSubmissionResource(submissionRetrieveRequest))
				.thenThrow(new PSD2Exception(errorInfo.getErrorCode(), errorInfo));

		controller.retrievePaymentSubmissionResource(submissionRetrieveRequest);		
	}

	@After
	public void tearDown() throws Exception {
		controller = null;
		service = null;
		paymentValidator = null;
	}

}
