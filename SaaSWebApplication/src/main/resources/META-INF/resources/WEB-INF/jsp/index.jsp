<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE html>
<%@ page session="false" %>
<html lang="en" ng-app="loginApp" ng-cloak>
<head>
<meta charset="utf-8">
<base href="/${applicationName}<%=request.getContextPath() %>/">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Login - Bank of Ireland</title>

<link rel="stylesheet" type="text/css" href="${cdnBaseURL}/sca-ui/css/common.min.css" />
<link rel="stylesheet" type="text/css" href="${cdnBaseURL}/sca-ui/css/bank-of-ireland.css" />


<link rel="shortcut icon" type="image/x-icon" href="${cdnBaseURL}/sca-ui/img/favicon.ico?rev=16" />

<noscript>${JAVASCRIPT_ENABLE_MSG}</noscript>

<%-- <script   src="${cdnBaseURL}/sca-ui/js/libs.min.js"></script>
<script  src="${cdnBaseURL}/sca-ui/js/templates.js"></script>
<script   src="${cdnBaseURL}/sca-ui/js/app.js"></script>
 --%>
 
<script  integrity= "${libsHashCode}"  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/js/libs.min.js"></script>
<script  integrity="${templateHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/js/templates.min.js"></script>
<script  integrity="${appHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/js/app.min.js"></script>
 
</head>

<body class="next-gen-home" block-ui="main" >
 <div class="page-container">
	 
		<div ui-view autoscroll="false"></div>

		<div class="content" ui-view="modalContainer" autoscroll="false"></div>
		<!-- <div ui-view="main" class="main-container" block-ui="main" autoscroll="false"></div> -->
		</div>
		<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
			<input type="hidden" id="sec-error" name="secError" value="${fn:escapeXml(SPRING_SECURITY_LAST_EXCEPTION.errorInfo)}">
		</c:if>
		<c:if test="${not empty exception}">				
			<input type="hidden" id="error" name="error" value="${fn:escapeXml(exception)}">
		</c:if>
        <input type="hidden" id="resumePath" name="resumePath" value="${not empty redirectUri ? redirectUri : param.resumePath}"/>
        <input type="hidden" id="serverErrorFlag" name="serverErrorFlag" value="${serverErrorFlag}"/>
		<input type="hidden" id="correlationId" name="correlationId" value="${empty correlationId ? param.correlationId : correlationId}"/>
		<input type="hidden" id="staticContent" name="staticContent" value="${fn:escapeXml(UIContent)}"/>
		
<%-- 		<input type="hidden" id="allowInteraction" name="allowInteraction" value="${param.allowInteraction}"/>
		<input type="hidden" id="scope" name="scope" value="${param.scope}"/>
		<input type="hidden" id="connectionId" name="connectionId" value="${param.connectionId}"/>
		<input type="hidden" id="response_type" name="response_type" value="${param.response_type}"/>
		<input type="hidden" id="state" name="state" value="${param.state}"/>
 		<input type="hidden" id="REF" name="REF" value="${param.REF}"/>
		<input type="hidden" id="nonce" name="nonce" value="${param.nonce}"/>
		<input type="hidden" id="reauth" name="reauth" value="${param.reauth}"/>
 --%>		

		<%-- <input type="hidden" id="redirectUri" name="redirectUri" value="${redirectUri}"/>	   
 --%>		
<%-- 		<c:if test="${not empty param.oAuthUrl}">
			<input type="hidden" name="saasurl" id="saasurl" value="<%=javax.servlet.http.HttpUtils.getRequestURL(request).append("?resumePath=").append(request.getParameter("resumePath"))%>"/>
		</c:if>
 --%>		
 		<input type="hidden" id="brandId" name="brandId" value=""/>
		<input type = "hidden" id="applicationName" name="applicationName" value= "${applicationName}<%=request.getContextPath() %>"/>
		
		<input type="hidden" id="channelId" name="channelId" value=""/>

		<input type="hidden" id="jsc-file-path" name="jsc-file-path" value="${cdnBaseURL}/sca-ui/ext-libs/fraudnet/boiukprefs.js">

    	<input id="fraudHdmInfo" name="fraudHdmInfo" value='${fraudHdmInfo}' type="hidden"/>
		<input type="hidden" id="fsHeaders" name="fsHeaders" value='${fsHeaders}'/>
		<input type="hidden" id="cdnBaseURL" name="cdnBaseURL" value="${cdnBaseURL}">
</body>
		
</html>