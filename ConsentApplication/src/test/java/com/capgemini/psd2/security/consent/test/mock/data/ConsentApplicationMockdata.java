package com.capgemini.psd2.security.consent.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;

public class ConsentApplicationMockdata {

	public static AccountRequestPOSTResponse mockAccountRequestPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
/*	public static CustomClientDetails mockCustomClientDetails;
	
	public static CustomClientDetails getCustomClientDetails(){
		mockCustomClientDetails = new CustomClientDetails();
		mockCustomClientDetails.setLegalEntityName("MoneyWise Ltd.");
		return mockCustomClientDetails;
	}*/
	
	public static AccountRequestPOSTResponse getAccountRequestPOSTResponse() {
		mockAccountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();
		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	public static AccountRequestPOSTResponse getAccountRequestPOSTResponseInvalidStatus() {
		AccountRequestPOSTResponse mockAccountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();
		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

}
