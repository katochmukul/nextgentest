package com.capgemini.psd2.security.consent.mvc.test.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.controllers.ConsentApplicationController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ConsentApplicationControllerTest {

	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private HttpServletResponse httpServletResponse;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private UIStaticContentUtilityController uiController;
	
	private String applicationName = "PSD2";

	@InjectMocks
	private ConsentApplicationController consentApplicationController = new ConsentApplicationController();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(consentApplicationController, "applicationName", applicationName);
	}

	@Test
	public void testHealthUrl() {
		Model model = mock(Model.class);
		assertNotNull(consentApplicationController.hello(model));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testHandleSessionTimeOutOnCancel(){
		Model model = mock(Model.class);
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
		
	}
	
	/*@Test
	public void testSessionErrors() {
		assertNotNull(consentApplicationController.viewErrors(null));
	}*/
	
	@Test(expected=PSD2Exception.class)
	public void testErrors(){
		Model model = mock(Model.class);
	
		ErrorInfo errorInfo = new ErrorInfo("100", "Error message", "400");
		PickupDataModel pickUpDataModel = new PickupDataModel();
		pickUpDataModel.setIntentId("123");
		pickUpDataModel.setChannelId("BOI");
		pickUpDataModel.setClientId("6795ee9ca8e3407694f866725303db37");
		pickUpDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
				
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn("https://pfcluster.apiboidev.com");
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(null);
		when(httpServletRequest.getParameter(PSD2Constants.CO_RELATION_ID)).thenReturn("12345");
		assertNotNull(consentApplicationController.error(model));
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
		
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void testErrorNullCondition(){
		Model model = mock(Model.class);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(null);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(null);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(httpServletRequest.getParameter(PFConstants.RESUME_PATH)).thenReturn("https://pfcluster.apiboidev.com");
		when(httpServletRequest.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("oAuth.apidev.com");
		assertNotNull(consentApplicationController.error(model));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testErrorOAuthUrl(){
		Model model = mock(Model.class);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(null);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(null);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(httpServletRequest.getAttribute((PSD2Constants.CONSENT_FLOW_TYPE))).thenReturn("AISP");
		when(httpServletRequest.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("oAuth.apidev.com");
		assertNotNull(consentApplicationController.error(model));
	}

}
