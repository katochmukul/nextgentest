/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.pisp.consent.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.pisp.consent.adapter.test.mock.data.PispConsentAdapterMockData;


/**
 * The Class ConsentMappingAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PispConsentAdapterImplTest {

	/** The consent mapping repository. */
	@Mock
	private PispConsentMongoRepository pispConsentMongoRepository;

	/** The req header attributes. */
	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;

	/** The consent mapping adapter impl. */
	@InjectMocks
	private PispConsentAdapterImpl pispConsentAdapterImpl = new PispConsentAdapterImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}


	@Test 
	public void testCreateConsent(){
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(PispConsentAdapterMockData.getConsentMockData());
		PispConsent PispConsent=PispConsentAdapterMockData.getConsentMockData();
		pispConsentAdapterImpl.createConsent(PispConsent);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testConsentAlreadyExist(){
		when(pispConsentMongoRepository.findByPaymentIdAndStatus(anyString(),any())).thenReturn(PispConsentAdapterMockData.getConsentMockData());
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(PSD2Exception.class);
		PispConsent PispConsent=PispConsentAdapterMockData.getConsentMockData();
		pispConsentAdapterImpl.createConsent(PispConsent);
	}
	
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateConsentWithException(){
		when(pispConsentMongoRepository.findByPaymentIdAndStatus(anyString(),any())).thenReturn(PispConsentAdapterMockData.getConsentMockData());
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		pispConsentAdapterImpl.createConsent(PispConsentAdapterMockData.getConsentMockData());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails(){
		when(pispConsentMongoRepository.findByPaymentIdAndStatus(anyString(),any())).thenReturn(null);
		PispConsent PispConsent=PispConsentAdapterMockData.getConsentMockDataWithOutAccountDetails();
		pispConsentAdapterImpl.createConsent(PispConsent);
	}
	
	@Test
	public void testRetrieveConsentByPaymentRequestIdSuccessFlow(){
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByPaymentIdAndStatus(anyString(),any())).thenReturn(consent);
		assertEquals(consent.getPsuId(),pispConsentAdapterImpl.retrieveConsentByPaymentId("1234",ConsentStatusEnum.AWAITINGAUTHORISATION).getPsuId());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRetrieveConsentByPaymentRequestIdDataAccessResourceFailureException(){
		when(pispConsentMongoRepository.findByPaymentIdAndStatus(anyString(),any())).thenThrow(DataAccessResourceFailureException.class);
		pispConsentAdapterImpl.retrieveConsentByPaymentId("1234",ConsentStatusEnum.AWAITINGAUTHORISATION);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testUpdateConsentStatusPSD2Exception(){
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(PSD2Exception.class);
		pispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testUpdateConsentStatusDataResourceAccessExeption(){
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		pispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

	@Test
	public void testUpdateConsentStatus(){
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(consent);
		pispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}
	@Test(expected=PSD2Exception.class)
	@SuppressWarnings("unchecked")
	public void testRetrieveConsentByConsentIdStatus(){
		when(pispConsentMongoRepository.findByConsentId(anyString())).thenReturn(null);
		pispConsentAdapterImpl.retrieveConsent("123455");
	}
	@Test(expected=PSD2Exception.class)
	@SuppressWarnings("unchecked")
	public void testRetrieveConsentByConsentIdException(){
		when(pispConsentMongoRepository.findByConsentId(anyString())).thenThrow(PSD2Exception.class);
		pispConsentAdapterImpl.retrieveConsent("123455");
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testRetrieveConsentByConsentIdReturningConcent(){
	    PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentId(anyString())).thenReturn(consent);
		pispConsentAdapterImpl.retrieveConsent("123455");
	}
	
	@Test
	public void testRetrieveConsentByPsuId() {
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		List<PispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(pispConsentMongoRepository.findByPsuId(anyString())).thenReturn(consentList);
		pispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", null);
	}
	
	@Test
	public void testRetrieveConsentByPsuIdAndStatus() {
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		List<PispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(pispConsentMongoRepository.findByPsuIdAndStatus(anyString(), anyObject())).thenReturn(consentList);
		pispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", ConsentStatusEnum.AUTHORISED);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentByPsuIdAndStatusException() {
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		List<PispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(pispConsentMongoRepository.findByPsuIdAndStatus(anyString(), anyObject())).thenThrow(new DataAccessResourceFailureException("No Consents Found"));
		pispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", ConsentStatusEnum.AUTHORISED);
	}
}
