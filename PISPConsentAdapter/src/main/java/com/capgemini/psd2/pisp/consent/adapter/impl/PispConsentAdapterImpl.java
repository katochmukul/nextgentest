/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.pisp.consent.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class AispConsentAdapterImpl.
 */
@Component
public class PispConsentAdapterImpl implements PispConsentAdapter {

	/** The aisp consent repository. */
	@Autowired
	private PispConsentMongoRepository pispConsentMongoRepository;




	@Override
	public void createConsent(PispConsent pispConsent) {
		PispConsent consent = null;
		try{
			consent = retrieveConsentByPaymentId(pispConsent.getPaymentId(),ConsentStatusEnum.AWAITINGAUTHORISATION);
			if(consent != null){
				consent.setStatus(ConsentStatusEnum.REVOKED);				
				pispConsentMongoRepository.save(consent);
			}
			else{
				consent = retrieveConsentByPaymentId(pispConsent.getPaymentId(),ConsentStatusEnum.AUTHORISED);
				if(consent != null){
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}
			if(pispConsent.getAccountDetails() == null ){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND);
			}
			pispConsent.setConsentId(GenerateUniqueIdUtilities.generateRandomUniqueID());
			pispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
			pispConsentMongoRepository.save(pispConsent);
		}catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
	}

	@Override
	public PispConsent retrieveConsentByPaymentId(String paymentRequestId,ConsentStatusEnum status) {
		PispConsent consent = null;
		try{
			consent = pispConsentMongoRepository.findByPaymentIdAndStatus(paymentRequestId,status);
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
		return consent;
	}

	@Override
	public void updateConsentStatus(String consentId,ConsentStatusEnum statusEnum) {
		PispConsent pispConsent = null;
		try{
			pispConsent = pispConsentMongoRepository.findByConsentId(consentId);
			pispConsent.setStatus(statusEnum);
			pispConsentMongoRepository.save(pispConsent);
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
	}
	
	@Override
	public PispConsent retrieveConsent(String consentId) {
		PispConsent consent =null;
		try{
			consent = pispConsentMongoRepository.findByConsentId(consentId);
		}catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
		if (consent == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
		return consent;
	}

	@Override
	public List<PispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum) {

		
		List<PispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = pispConsentMongoRepository.findByPsuId(psuId);
			} else {
				consentList = pispConsentMongoRepository.findByPsuIdAndStatus(psuId, statusEnum);
			}
			
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR);
		}
		
		
		return consentList;
	
	}
	
	@Override
	public PispConsent updateConsentStatusWithResponse(String consentId,ConsentStatusEnum statusEnum) {
		PispConsent pispConsent = null;
		try{
			pispConsent = pispConsentMongoRepository.findByConsentId(consentId);
			pispConsent.setStatus(statusEnum);
			return pispConsentMongoRepository.save(pispConsent);
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
	}
	
}
