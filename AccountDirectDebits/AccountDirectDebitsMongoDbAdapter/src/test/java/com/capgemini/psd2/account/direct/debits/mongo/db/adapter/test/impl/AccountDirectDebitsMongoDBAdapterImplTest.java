/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.direct.debits.mongo.db.adapter.impl.AccountDirectDebitsMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountDirectDebitsMongoDBAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsMongoDBAdapterImplTest {

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/** The account direct debits mongo DB adapter impl. */
	@InjectMocks
	private AccountDirectDebitsMongoDbAdapterImpl accountDirectDebitsMongoDbAdapterImpl;

	/**
	 * Test retrieve blank response account direct debits success flow.
	 */
	@Test
	public void testRetrieveAccountDirectDebitsSuccessFlow() {
		AccountGETResponse1 accountGETResponse1 = accountDirectDebitsMongoDbAdapterImpl.retrieveAccountDirectDebits(null, null);
		assertTrue(accountGETResponse1.getData().getDirectDebit().isEmpty());
		
	}
	
	@Test
	public void testRetrieveAccountDirectDebitsSuccessFlowWithAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		List<AccountDetails> accountDetails = new ArrayList<>();
		mapping.setAccountDetails(accountDetails);
		AccountDetails details = new AccountDetails();
		details.setAccountId("bef6ac83-bb2f-48b6-bdcf-fee4d84ff1db");
		accountDetails.add(details);
		AccountGETResponse1 accountGETResponse1 = accountDirectDebitsMongoDbAdapterImpl.retrieveAccountDirectDebits(mapping, null);
		assertTrue(accountGETResponse1.getData().getDirectDebit().size() > 0);
		
	}
}
