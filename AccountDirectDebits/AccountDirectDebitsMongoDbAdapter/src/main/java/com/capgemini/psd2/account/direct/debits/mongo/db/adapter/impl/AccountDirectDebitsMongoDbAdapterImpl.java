package com.capgemini.psd2.account.direct.debits.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.Data6;
import com.capgemini.psd2.aisp.domain.Data6PreviousPaymentAmount;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.aisp.domain.DirectDebit.DirectDebitStatusCodeEnum;
import com.capgemini.psd2.consent.domain.AccountMapping;

@Component
public class AccountDirectDebitsMongoDbAdapterImpl implements AccountDirectDebitsAdapter {

	@Override
	public AccountGETResponse1 retrieveAccountDirectDebits(AccountMapping accountMapping, Map<String, String> params) {
		AccountGETResponse1 accountGETResponse1 = new AccountGETResponse1();
		Data6 data = new Data6();
		List<DirectDebit> directDebitList = new ArrayList<>();
		DirectDebit directDebit = new DirectDebit();
		if(accountMapping != null){
			directDebit.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
			directDebit.setDirectDebitId("DD03");
			directDebit.setDirectDebitStatusCode(DirectDebitStatusCodeEnum.ACTIVE);
			directDebit.setMandateIdentification("Caravanners");
			directDebit.setName("Towbar Club 3 - We Love Towbars");
			directDebit.setPreviousPaymentDateTime("2017-04-05T10:43:07+00:00");
			Data6PreviousPaymentAmount previousPaymentAmount= new Data6PreviousPaymentAmount();
			previousPaymentAmount.setAmount("1.88");
			previousPaymentAmount.setCurrency("GBP");
			directDebit.setPreviousPaymentAmount(previousPaymentAmount);
			directDebitList.add(directDebit);
		}
		data.setDirectDebit(directDebitList);
		accountGETResponse1.setData(data);
		return accountGETResponse1;
	}
}