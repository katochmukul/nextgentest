package com.capgemini.psd2.account.direct.debits;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.account.direct.debits.routing.adapter.impl.AccountDirectDebitsRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.logger.PSD2Filter;

@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableDiscoveryClient
public class AccountDirectDebitsApplication {

	/** The context. */
	static ConfigurableApplicationContext context = null;

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		context = SpringApplication.run(AccountDirectDebitsApplication.class, args);
	}

	/**
	 * Gets the account direct debits adapter.
	 *
	 * @return the account direct debits adapter
	 */
	@Bean(name = "accountDirectDebitsAdapterImpl")
	public AccountDirectDebitsAdapter getAccountDirectDebitsAdapter() {
		return new AccountDirectDebitsRoutingAdapter();
	}
	
	@Bean(name="psd2Filter")
	public Filter psd2Filter(){
		return new PSD2Filter(); 
	}	
	
}
