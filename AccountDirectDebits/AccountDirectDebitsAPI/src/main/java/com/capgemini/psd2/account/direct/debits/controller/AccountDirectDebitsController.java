/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.controller;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.direct.debits.service.AccountDirectDebitsService;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
/**
 * The Class AccountDirectDebitsController.
 */
@RestController
public class AccountDirectDebitsController {

	/** The service. */
	@Autowired
	private AccountDirectDebitsService service;

	/**
	 * Retrieve account direct debits.
	 *
	 * @param accountId the account id
	 * @return the account GET response
	 */
	@RequestMapping(value ="/accounts/{accountId}/direct-debits", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public AccountGETResponse1 retrieveAccountDirectDebits(@PathVariable("accountId") String accountId) {
		
		if (NullCheckUtils.isNullOrEmpty(accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_ID_FOUND);
		
		if (!Pattern.matches("[a-zA-Z0-9-]{1,40}", accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		
		return service.retrieveAccountDirectDebits(accountId);
	}
}
