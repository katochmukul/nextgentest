/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.direct.debits.service.AccountDirectDebitsService;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountDirectDebitsServiceImpl.
 */
@Service
public class AccountDirectDebitsServiceImpl implements AccountDirectDebitsService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account direct debits adapter. */
	@Autowired
	@Qualifier("accountDirectDebitsAdapterImpl")
	private AccountDirectDebitsAdapter accountDirectDebitsAdapter;
	
	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.account.direct.debits.service.AccountDirectDebitsService#
	 * retrieveAccountDirectDebits(java.lang.String)
	 */
	@Override
	public AccountGETResponse1 retrieveAccountDirectDebits(String accountId) {
		
		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);
		
		AccountGETResponse1 accountGETResponse1 = accountDirectDebitsAdapter.retrieveAccountDirectDebits(accountMapping,
				reqHeaderAtrributes.getToken().getSeviceParams());
		
		if (accountGETResponse1.getLinks() == null)
			accountGETResponse1.setLinks(new Links());
		if (accountGETResponse1.getMeta() == null)
			accountGETResponse1.setMeta(new MetaData());
		accountGETResponse1.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		accountGETResponse1.getMeta().setTotalPages(1);
		return accountGETResponse1;
	}
}
