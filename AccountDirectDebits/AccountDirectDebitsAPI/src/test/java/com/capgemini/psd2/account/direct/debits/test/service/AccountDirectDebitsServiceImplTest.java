/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.test.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.direct.debits.service.impl.AccountDirectDebitsServiceImpl;
import com.capgemini.psd2.account.direct.debits.test.mock.data.AccountDirectDebitsMockData;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountDirectDebitsServiceImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsServiceImplTest {

	/** The adapter. */
	@Mock
	private AccountDirectDebitsAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	/** The service. */
	@InjectMocks
	private AccountDirectDebitsServiceImpl service = new AccountDirectDebitsServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountDirectDebitsMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
		.thenReturn(AccountDirectDebitsMockData.getMockAccountMapping());
		
	}

	/**
	 * Retrieve account direct debits test (empty list).
	 */
	@Test
	public void retrieveAccountDirectDebitsTest() {
		when(adapter.retrieveAccountDirectDebits(anyObject(), anyObject()))
				.thenReturn(AccountDirectDebitsMockData.getMockBlankResponse());
		AccountGETResponse1 accountGETResponse1 = service.retrieveAccountDirectDebits(
				"123");
		assertTrue(accountGETResponse1.getData().getDirectDebit().isEmpty());
	}


	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
}
