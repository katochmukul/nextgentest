package com.capgemini.psd2.account.direct.debits.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.Data6;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountDirectDebitsMockData.
 */
public class AccountDirectDebitsMockData {


	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static AccountGETResponse1 getMockBlankResponse() {
		AccountGETResponse1 accountGETResponse1 = new AccountGETResponse1();
		Data6 data = new Data6();
		List<DirectDebit> directDebit = new ArrayList<>();
		data.setDirectDebit(directDebit);
		accountGETResponse1.setData(data);
		return accountGETResponse1;
	}
}
