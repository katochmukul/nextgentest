/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.direct.debits.routing.adapter.impl.AccountDirectDebitsRoutingAdapter;
import com.capgemini.psd2.account.direct.debits.routing.adapter.routing.AccountDirectDebitsAdapterFactory;
import com.capgemini.psd2.account.direct.debits.routing.adapter.test.adapter.AccountDirectDebitsTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;

/**
 * The Class AccountDirectDebitsRoutingAdapterTest.
 */
public class AccountDirectDebitsRoutingAdapterTest {

	/** The account direct debits adapter factory. */
	@Mock
	private AccountDirectDebitsAdapterFactory accountDirectDebitsAdapterFactory;

	/** The account direct debits routing adapter. */
	@InjectMocks
	private AccountDirectDebitsAdapter accountDirectDebitsRoutingAdapter = new AccountDirectDebitsRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account balancetest.
	 */
	@Test
	public void retrieveAccountDirectDebitstest() {

		AccountDirectDebitsAdapter accountDirectDebitsAdapter = new AccountDirectDebitsTestRoutingAdapter();
		Mockito.when(accountDirectDebitsAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountDirectDebitsAdapter);

		AccountGETResponse1 accountGETResponse1 = accountDirectDebitsRoutingAdapter.retrieveAccountDirectDebits(null,
				null);

		assertTrue(accountGETResponse1.getData().getDirectDebit().isEmpty());

	}

}
