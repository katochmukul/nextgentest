package com.capgemini.psd2.account.direct.debits.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.Data6;
import com.capgemini.psd2.aisp.domain.DirectDebit;

public class AccountDirectDebitsRoutingAdapterTestMockData {

	public static AccountGETResponse1 getMockAccountGETResponse1() {
		AccountGETResponse1 accountGETResponse1 = new AccountGETResponse1();
		Data6 data = new Data6();
		List<DirectDebit> directDebit = new ArrayList();
		data.setDirectDebit(directDebit);
		accountGETResponse1.setData(data);
		return accountGETResponse1;
	}

}
