/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.direct.debits.routing.adapter.routing.AccountDirectDebitsAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountDirectDebitsRoutingAdapter.
 */
public class AccountDirectDebitsRoutingAdapter implements AccountDirectDebitsAdapter {

	/** The account direct debits adapter factory. */
	@Autowired
	private AccountDirectDebitsAdapterFactory accountDirectDebitsAdapterFactory;

	/** The default adapter. */
	@Value("${app.defaultAccountDirectDebitsAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter#
	 * retrieveAccountDirectDebits(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */

	@Override
	public AccountGETResponse1 retrieveAccountDirectDebits(AccountMapping accountMapping, Map<String, String> params) {
		
		AccountDirectDebitsAdapter accountDirectDebitsAdapter = accountDirectDebitsAdapterFactory
				.getAdapterInstance(defaultAdapter);
		
		return accountDirectDebitsAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
}