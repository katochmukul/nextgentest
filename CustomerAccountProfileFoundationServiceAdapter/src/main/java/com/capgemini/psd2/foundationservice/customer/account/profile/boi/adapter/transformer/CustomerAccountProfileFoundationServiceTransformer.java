package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.enumerator.CurrencyEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.aisp.transformer.CustomerAccountListTransformer;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants.CustomerAccountProfileFoundationServiceConstants;
import com.capgemini.psd2.fraudsystem.domain.Address;
import com.capgemini.psd2.fraudsystem.domain.Emails;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.domain.PhoneNumbers;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class CustomerAccountProfileFoundationServiceTransformer
 *
 */

@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class CustomerAccountProfileFoundationServiceTransformer implements CustomerAccountListTransformer {

	/** The Account filters */
	private Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();

	/** The Jurisdiction filter */
	private Map<String, Map<String, String>> jurisdiction = new HashMap<>();

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;
	
	@Value("${foundationService.accuntNumLength:8}")
	private String accuntNumLength;
	@Value("${foundationService.accountNSCLength:6}")
	private String accountNSCLength;

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Override
	public <T> AccountGETResponse transformCustomerAccountListAdapter(T inputResObject, Map<String, String> params) {
		Accnts accounts = (Accnts) inputResObject;
		Data2 data2 = new Data2();
		List<Account> accountList = new ArrayList<>();
		AccountGETResponse finalAIResponseObj = new AccountGETResponse();
		
		getJurisdictionMap(params);
		
		for (Accnt accnt : accounts.getAccount()) {
			if(accnt.getAccountNumber().length()!=Integer.parseInt(accuntNumLength)|| accnt.getAccountNSC().length()!=Integer.parseInt(accountNSCLength))
			{
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.INVALID_ACCOUNT_NUMBER_LENGTH);	
			}
			if (accnt.getAccountPermission() != null) {
				PSD2Account responseObj = new PSD2Account();
				Data2Account acc = new Data2Account();
				Data2Servicer servicer = new Data2Servicer();
				if (!NullCheckUtils.isNullOrEmpty(accnt.getJurisdiction())) {
					HashMap<String, String> valuesMap = (HashMap) jurisdiction.get(accnt.getJurisdiction().value());
					acc.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum
							.valueOf(valuesMap.get(CustomerAccountProfileFoundationServiceConstants.SCHEMENAME)));
					String identification = valuesMap
							.get(CustomerAccountProfileFoundationServiceConstants.IDENTIFICATION);
					if (!NullCheckUtils.isNullOrEmpty(identification)) {
						if (identification
								.equals(CustomerAccountProfileFoundationServiceConstants.SORTCODEACCOUNTNUMBER)) {
							acc.setIdentification(accnt.getAccountNSC() + accnt.getAccountNumber());
						} else if (identification.equals(CustomerAccountProfileFoundationServiceConstants.IBAN)) {
							acc.setIdentification(accnt.getIban());
						}
					}
					if (!NullCheckUtils.isNullOrEmpty(
							valuesMap.get(CustomerAccountProfileFoundationServiceConstants.SERVICERSCHEMENAME))) {
						if (valuesMap.get(CustomerAccountProfileFoundationServiceConstants.SERVICERSCHEMENAME)
								.equals(CustomerAccountProfileFoundationServiceConstants.BICFI)) {
							servicer.setSchemeName(
									com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum.valueOf(valuesMap
											.get(CustomerAccountProfileFoundationServiceConstants.SERVICERSCHEMENAME)));
						}
					}
					String serIden = valuesMap
							.get(CustomerAccountProfileFoundationServiceConstants.SERVICERIDENTIFICATION);
					if (!NullCheckUtils.isNullOrEmpty(serIden)) {
						if (serIden.equals(CustomerAccountProfileFoundationServiceConstants.BIC)) {
							servicer.setIdentification(accnt.getBic());
						}
					}
				}
				validator.validate(acc);
				responseObj.setAccount(acc);
				validator.validateEnum(CurrencyEnum.class, accnt.getCurrency());
				validator.validate(accnt);
				responseObj.setCurrency(accnt.getCurrency());
				responseObj.setNickname(accnt.getAccountName());
				responseObj.setAccountId("NA");
				responseObj.setAccountType(accnt.getAccountType());
				responseObj.setHashedValue();
				Map<String, String> additionalInformation = new HashMap<String, String>();
				additionalInformation.put(PSD2Constants.PAYER_JURISDICTION, accnt.getJurisdiction().value());
				additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, accnt.getAccountNumber());
				additionalInformation.put(PSD2Constants.ACCOUNT_NSC, accnt.getAccountNSC());
				additionalInformation.put(PSD2Constants.ACCOUNT_PERMISSION, accnt.getAccountPermission());
				if (!NullCheckUtils.isNullOrEmpty(accnt.getIban()))
					additionalInformation.put(PSD2Constants.IBAN, accnt.getIban());
				if (!NullCheckUtils.isNullOrEmpty(accnt.getBic()))
					additionalInformation.put(PSD2Constants.BIC, accnt.getBic());
				responseObj.setAdditionalInformation(additionalInformation);

				if (servicer.getIdentification() != null && servicer.getSchemeName() != null) {
					validator.validate(servicer);
					responseObj.setServicer(servicer);
				}
				validator.validate(responseObj);
				data2.setAccount(accountList);
				finalAIResponseObj.setData(data2);
				finalAIResponseObj.getData().getAccount().add(responseObj);
			}
		}

		return finalAIResponseObj;
	}



	/**
	 * @param params
	 */
	protected void getJurisdictionMap(Map<String, String> params) {
		Set<String> regions = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION)
				.get(params.get(PSD2Constants.CONSENT_FLOW_TYPE)).stream().map(u -> u.substring(0, u.indexOf(".")))
				.collect(Collectors.toSet());
		regions.stream().forEach(x -> {
			List<String> filterLst = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION)
					.get(params.get(PSD2Constants.CONSENT_FLOW_TYPE)).stream().filter(u -> u.startsWith(x))
					.collect(Collectors.toList());
			Map<String, String> data = filterLst.stream().collect(Collectors.toMap(
					s1 -> s1.substring(x.length() + 1, s1.indexOf("=")), s1 -> s1.substring(s1.indexOf("=") + 1)));
			jurisdiction.put(x, data);
		});
	}

	

	@Override
	public <T> PSD2CustomerInfo transformCustomerInfo(T source, Map<String, String> params) {
		PSD2CustomerInfo psd2CustomerInfo=new PSD2CustomerInfo();
		ChannelProfile channelProfile=(ChannelProfile)source;
		if(!NullCheckUtils.isNullOrEmpty(channelProfile.getCustomer()))
		{
			 psd2CustomerInfo.setId(CustomerAccountProfileFoundationServiceConstants.CONTACT_ID);
			 psd2CustomerInfo.setName(channelProfile.getCustomer().getTitle()+" "+channelProfile.getCustomer().getFirstName()+" "+channelProfile.getCustomer().getSurName());
			 psd2CustomerInfo.setClientType(channelProfile.getCustomer().getClientType());
			 /*setting Phone Number*/
			 PhoneNumbers phoneNumber=new PhoneNumbers();
		     phoneNumber.setNumber(channelProfile.getCustomer().getContactNumber());
			 List<PhoneNumbers> phoneList=new ArrayList<>();
			 phoneList.add(phoneNumber);
			 psd2CustomerInfo.setPhoneNumbers(phoneList);
			 /*setting Email*/
			 Emails email =new Emails();
			 email.setEmail(channelProfile.getCustomer().getEmail());
			 List<Emails> emails=new ArrayList<>();
			 emails.add(email);
			 psd2CustomerInfo.setEmails(emails);
			 /*setting Address*/
			 Address address=new Address();
			 address.setStreetLine1(channelProfile.getCustomer().getAddressLine1());
			 address.setStreetLine2(channelProfile.getCustomer().getAddressLine2());
			 address.setCity(channelProfile.getCustomer().getAddressLine3());
			 address.setPostal(channelProfile.getCustomer().getAddressLine4());
			 psd2CustomerInfo.setAddress(address);
			 /*setting Company name*/ 
			 psd2CustomerInfo.setCompany(channelProfile.getCustomer().getTradingAsName());
			 /*setting DOB*/
			 if(!NullCheckUtils.isNullOrEmpty(channelProfile.getCustomer().getDateOfBirth()))
			 {
			 psd2CustomerInfo.setDob(channelProfile.getCustomer().getDateOfBirth().toString());
			 }
		}
		AccountGETResponse accountGETResponse=transformCustomerAccountListAdapter(channelProfile.getAccounts(), params);
		psd2CustomerInfo.setData(accountGETResponse.getData());
		return psd2CustomerInfo;
	}

	public Map<String, Map<String, List<String>>> getAccountFiltering() {
		return accountFiltering;
	}

	public void setAccountFiltering(Map<String, Map<String, List<String>>> accountFiltering) {
		this.accountFiltering = accountFiltering;
	}
	
}