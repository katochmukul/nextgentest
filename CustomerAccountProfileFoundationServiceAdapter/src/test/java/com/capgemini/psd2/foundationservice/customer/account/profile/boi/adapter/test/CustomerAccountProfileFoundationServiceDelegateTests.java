package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
//import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.config.test.CustomerAccountProfileFoundationServiceAdapterTestConfig;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants.CustomerAccountProfileFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;


@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceDelegateTests {

	@InjectMocks
	private CustomerAccountProfileFoundationServiceDelegate cusAccProFoundServiceDelegate;

	@Mock
	private CustomerAccountProfileFoundationServiceTransformer cusAccProFoundServiceTransformer;

	@Mock
	private CustomerAccountProfileFoundationServiceConstants CusAccProFoundServiceConstants;
	
	@InjectMocks
	private CustomerAccountProfileFoundationServiceClient CusAccProFounSerClient;

	@Mock
	private Environment env;
	
    @Mock
    private AdapterUtility adapterUtility;

	@Mock
	private RestClientSyncImpl restClient;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}


	
	@Test
    public void testTransformResponseFromFDToAPI() {
           Accnts accounts = new Accnts();
           Accnt acc = new Accnt();
           acc.setAccountNumber("acct1234");
           acc.setAccountNSC("nsc1234");
           accounts.getAccount().add(acc);
           AccountMapping accountMapping = new AccountMapping();
           List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
           AccountDetails accDet = new AccountDetails();
           accDet.setAccountId("12345");
           accDet.setAccountNSC("nsc1234");
           accDet.setAccountNumber("acct1234");
           accDetList.add(accDet);
           accountMapping.setAccountDetails(accDetList);
           accountMapping.setTppCID("test");
           accountMapping.setPsuId("test");
           Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
   	      com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts acc1= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();

           AccountGETResponse res = cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(acc1, new HashMap<String, String>());
           assertNotNull(res);

    }
	
	/*@Test
    public void testTransformResponseCustomerInfoFromFDToAPI() {
           Accounts accounts = new Accounts();
           Accnt acc = new Accnt();
           acc.setAccountNumber("acct1234");
           acc.setAccountNSC("nsc1234");
           accounts.getAccount().add(acc);
           AccountMapping accountMapping = new AccountMapping();
           List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
           AccountDetails accDet = new AccountDetails();
           accDet.setAccountId("12345");
           accDet.setAccountNSC("nsc1234");
           accDet.setAccountNumber("acct1234");
           accDetList.add(accDet);
           accountMapping.setAccountDetails(accDetList);
           accountMapping.setTppCID("test");
           accountMapping.setPsuId("test");
           Mockito.when(cusAccProFoundServiceTransformer.transformCustomerInfo(any(), any())).thenReturn(new PSD2CustomerInfo());
   	      com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts acc1= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();

 //  	   PSD2CustomerInfo res = cusAccProFoundServiceDelegate.transformCustomerInfoResponseFromFDToAPI(acc1, new HashMap<String, String>());
           assertNotNull(res);

    }*/

	@Test
	public void testCreateRequestHeadersAISP(){
		
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "userInReqHeader", "user");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "channelInReqHeader", "channel");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "platformInReqHeader", "platform");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "correlationReqHeader", "correlation");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "platform", "platform");
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		
		HttpHeaders headers = cusAccProFoundServiceDelegate.createRequestHeaders(params);
		assertNotNull(headers);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersUserIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, null);
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		cusAccProFoundServiceDelegate.createRequestHeaders(params);
	}

	
	@Test
	public void testGetFoundationServiceUrlAISP(){
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURL(anyObject())).thenReturn("test");
		String result = cusAccProFoundServiceDelegate.getFoundationServiceURLAISP(new HashMap<>(), "test");
		assertNotNull(result);
	}

	
	@Test
	public void testCustomerAccountProfileClient(){
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		ChannelProfile channelProfile=new ChannelProfile();
		RequestInfo requestInfo = new RequestInfo();
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(channelProfile);
		channelProfile = CusAccProFounSerClient.restTransportForCustomerAccountProfile(requestInfo, ChannelProfile.class, httpHeaders);
		assertNotNull(channelProfile);
	}
}
