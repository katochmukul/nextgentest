package com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;

@Component
public class PaymentSetupPlatformAdapterImpl implements PaymentSetupPlatformAdapter {
	@Autowired
	private PaymentSetupPlatformRepository paymentSetupRepository;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	public PaymentSetupPlatformResource createPaymentSetupResource(CustomPaymentSetupPOSTRequest paymentSetupRequest,
			PaymentResponseInfo params) {

		PaymentSetupPlatformResource paymentPlatformResource = new PaymentSetupPlatformResource();

		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (paymentSetupRequest.getData() != null && paymentSetupRequest.getData().getInitiation() != null) {
			if (paymentSetupRequest.getData().getInitiation().getDebtorAccount() != null){
				tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
				if (! NullCheckUtils.isNullOrEmpty(paymentSetupRequest.getData().getInitiation().getDebtorAccount().getName()))
					tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
			}

			paymentPlatformResource.setTppDebtorDetails(tppDebtorDetailsStatus);
			paymentPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
			paymentPlatformResource.setEndToEndIdentification(
					paymentSetupRequest.getData().getInitiation().getEndToEndIdentification());
			paymentPlatformResource.setInstructionIdentification(
					paymentSetupRequest.getData().getInitiation().getInstructionIdentification());
		}
		paymentPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentPlatformResource.setStatus(params.getPaymentValidationStatus());
		paymentPlatformResource.setIdempotencyRequest(params.getIdempotencyRequest());
		paymentPlatformResource.setTppCID(reqHeaderAtrributes.getTppCID());
		paymentPlatformResource.setIdempotencyKey(reqHeaderAtrributes.getIdempotencyKey());	
		paymentPlatformResource.setTppLegalEntityName(reqHeaderAtrributes.getToken().getTppInformation().getTppLegalEntityName());

		try {
			return paymentSetupRepository.save(paymentPlatformResource);

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_PAYMENT_CREATION);
		}
	}

	public PaymentSetupPlatformResource retrievePaymentSetupResource(String paymentId) {
		try {
			return paymentSetupRepository.findOneByPaymentId(paymentId);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_PAYMENT_RETRIEVE);
		}
	}

	public PaymentSetupPlatformResource getIdempotentPaymentSetupResource(long idempotencyDuration) {
		try {
			Long l = System.currentTimeMillis() - idempotencyDuration;
			return paymentSetupRepository.findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(
					reqHeaderAtrributes.getTppCID(), reqHeaderAtrributes.getIdempotencyKey(),
					String.valueOf(Boolean.TRUE), DateUtilites.formatMilisecondsToISODateFormat(l));
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_PAYMENT_RETRIEVE_FOR_IDEMPOTENCY);
		}

	}

	public void updatePaymentSetupResource(PaymentSetupPlatformResource paymentSetupPlatformResource) {
		try {
			paymentSetupPlatformResource.setUpdatedAt(PispUtilities.getCurrentDateInISOFormat());
			paymentSetupRepository.save(paymentSetupPlatformResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_PLATFORM_PAYMENT_UPDATION);
		}

	}
}
