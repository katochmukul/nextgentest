package com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;

public interface PaymentSetupPlatformRepository extends MongoRepository<PaymentSetupPlatformResource, String>{

	 public PaymentSetupPlatformResource findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(String tppCID, String idempotencyKey, String idempotencyRequest, String date);
	 public PaymentSetupPlatformResource findOneByPaymentId(String paymentId );
		
}
