package com.capgemini.psd2.adapter.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.logger.AdapterLoggerUtils;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.filteration.ResponseFilter;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * The Class PSD2Aspect.
 */
@Component
@Aspect
public class PSD2AdapterAspect{

	private static final Logger LOGGER = LoggerFactory.getLogger(PSD2AdapterAspect.class);

	@Autowired
	private AdapterLoggerUtils loggerUtils;

	@Autowired
	private LoggerAttribute loggerAttribute;

	@Value("${foundationService.payloadLog:#{false}}")
	private boolean payloadLog;

	/** The mask payload log. */
	@Value("${foundationService.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog;

	/** The mask payload. */
	@Value("${foundationService.maskPayload:#{false}}")
	private boolean maskPayload;

	@Autowired
	private DataMask dataMask;

	@Autowired
	private ResponseFilter responseFilterUtility;

	/**
	 * Arround logger advice controller.
	 *
	 * @param proceedingJoinPoint the proceeding join point
	 * @return the object
	 */
	@Around("((execution(* com.capgemini.psd2.foundationservice..client..* (..))) || (execution(* com.capgemini.psd2.fraudnet..client..* (..))))  && !(execution(* com.capgemini.psd2.foundationservice.authentication.application..client..* (..)))")
	public Object arroundLoggerAdviceClient(ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerStartData(proceedingJoinPoint.getSignature().getName());
		logRequest(proceedingJoinPoint);
		try {

			Object result = proceedingJoinPoint.proceed();
			loggerUtils.populateLoggerEndData(proceedingJoinPoint.getSignature().getName());
			result = responseFilterUtility.filterResponse(result, proceedingJoinPoint.getSignature().getName());

			if(maskPayload)
				dataMask.maskResponse(result,  proceedingJoinPoint.getSignature().getName());
			else
				dataMask.maskMResponse(result,  proceedingJoinPoint.getSignature().getName());

			logResponse(proceedingJoinPoint, result);
			return result;
		} catch(AdapterException e){
			loggerUtils.populateLoggerEndData(proceedingJoinPoint.getSignature().getName());
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{},\"FoundationErrorDetails\":{}}" ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo(), JSONUtilities.getJSONOutPutFromObject(e.getFoundationError()));
			if(LOGGER.isDebugEnabled()){
				LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw e;
		}
		catch (Throwable e) {
			loggerUtils.populateLoggerEndData(proceedingJoinPoint.getSignature().getName());
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}" ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, psd2Exception.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw psd2Exception;
		}
	}

	private void logRequest(ProceedingJoinPoint proceedingJoinPoint){
		if(maskPayloadLog && payloadLog){
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute, JSONUtilities.getJSONOutPutFromObject(dataMask.maskRequestLog(proceedingJoinPoint.getArgs(), proceedingJoinPoint.getSignature().getName())));

		}else if(!maskPayloadLog && payloadLog){
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute, JSONUtilities.getJSONOutPutFromObject(dataMask.maskMRequestLog(proceedingJoinPoint.getArgs(), proceedingJoinPoint.getSignature().getName())));

		}else{
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute);

		}
	}

	private void logResponse(ProceedingJoinPoint proceedingJoinPoint, Object result){
		if(maskPayloadLog && payloadLog){
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute,JSONUtilities.getJSONOutPutFromObject(dataMask.maskResponseLog(result, proceedingJoinPoint.getSignature().getName())));
		}else if(!maskPayloadLog && payloadLog){
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute,JSONUtilities.getJSONOutPutFromObject(dataMask.maskMResponseLog(result, proceedingJoinPoint.getSignature().getName())));
		}else{
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute);
		}
	}
}