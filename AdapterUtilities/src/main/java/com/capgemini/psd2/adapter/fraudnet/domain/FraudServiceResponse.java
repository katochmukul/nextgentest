package com.capgemini.psd2.adapter.fraudnet.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "decisionScore", "decisionType", "riskLevel", "deviceIdentifier", "assessmentRules" })
public class FraudServiceResponse {

	@JsonProperty("decisionScore")
	private Integer decisionScore;
	@JsonProperty("decisionType")
	private String decisionType;
	@JsonProperty("riskLevel")
	private String riskLevel;
	@JsonProperty("deviceIdentifier")
	private String deviceIdentifier;
	@JsonProperty("assessmentRules")
	private List<AssessmentRule> assessmentRules = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("decisionScore")
	public Integer getDecisionScore() {
		return decisionScore;
	}

	@JsonProperty("decisionScore")
	public void setDecisionScore(Integer decisionScore) {
		this.decisionScore = decisionScore;
	}

	@JsonProperty("decisionType")
	public String getDecisionType() {
		return decisionType;
	}

	@JsonProperty("decisionType")
	public void setDecisionType(String decisionType) {
		this.decisionType = decisionType;
	}

	@JsonProperty("riskLevel")
	public String getRiskLevel() {
		return riskLevel;
	}

	@JsonProperty("riskLevel")
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}

	@JsonProperty("deviceIdentifier")
	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}

	@JsonProperty("deviceIdentifier")
	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	@JsonProperty("assessmentRules")
	public List<AssessmentRule> getAssessmentRules() {
		return assessmentRules;
	}

	@JsonProperty("assessmentRules")
	public void setAssessmentRules(List<AssessmentRule> assessmentRules) {
		this.assessmentRules = assessmentRules;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
