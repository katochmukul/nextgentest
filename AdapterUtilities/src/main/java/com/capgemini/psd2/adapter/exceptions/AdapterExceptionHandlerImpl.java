/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;

/**
 * The Class AdapterExceptionHandlerImpl.
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="foundationService")
public class AdapterExceptionHandlerImpl implements ExceptionHandler{

	/** The errormap. */
	private Map<String, String> errormap = new HashMap<>();	

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpServerErrorException)
	 */
	@Override
	public void handleException(HttpServerErrorException e) {
		String responseBody = e.getResponseBodyAsString().trim();
		if (responseBody != null && responseBody.contains("errorCode") && responseBody.contains("errorText")) {
			JAXBContext jaxbContext;
			Unmarshaller unmarshaller;
			StringReader reader;
			ValidationViolations errorInfoObj;
			try {
				jaxbContext = JAXBContext.newInstance(ValidationViolations.class);
				unmarshaller = jaxbContext.createUnmarshaller();
				reader = new StringReader(responseBody);
				errorInfoObj = (ValidationViolations) unmarshaller.unmarshal(reader);
			} catch (JAXBException e1) {
				throw AdapterException.populatePSD2Exception(e1.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			String errorMapping = errormap.get(errorInfoObj.getValidationViolation().get(0).getErrorCode());
			AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
			throw AdapterException.populatePSD2Exception(errorInfoObj.getValidationViolation().get(0).getErrorText(), errorCodeEnum, errorInfoObj);
		} else if (responseBody != null) {
			int rawStatusCode = e.getRawStatusCode();
			String statusCode = Integer.toString(rawStatusCode);
			throw AdapterException.populatePSD2Exception(e.getMessage(), responseBody, statusCode);
		}
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpClientErrorException)
	 */
	@Override
	public void handleException(HttpClientErrorException e) {
		String responseBody  = e.getResponseBodyAsString();
		if(responseBody!=null && responseBody.contains("errorCode") && responseBody.contains("errorText")){
			JAXBContext jaxbContext;
			Unmarshaller unmarshaller;
			StringReader reader;
			ValidationViolations errorInfoObj;
			try {
				jaxbContext = JAXBContext.newInstance(ValidationViolations.class);
				unmarshaller = jaxbContext.createUnmarshaller();
				reader = new StringReader(responseBody);
				errorInfoObj = (ValidationViolations) unmarshaller.unmarshal(reader);
			} catch (JAXBException e1) {
				throw AdapterException.populatePSD2Exception(e1.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			String errorMapping = errormap.get(errorInfoObj.getValidationViolation().get(0).getErrorCode());
			AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
			throw AdapterException.populatePSD2Exception(errorInfoObj.getValidationViolation().get(0).getErrorText(),errorCodeEnum,errorInfoObj);
		}
		else if (responseBody != null) {
			int rawStatusCode = e.getRawStatusCode();
			String statusCode = Integer.toString(rawStatusCode);
			throw AdapterException.populatePSD2Exception(e.getMessage(), responseBody, statusCode);
		}
		throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.ResourceAccessException)
	 */
	@Override
	public void handleException(ResourceAccessException e) {
		throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
	}

	/**
	 * Gets the errormap.
	 *
	 * @return the errormap
	 */
	public Map<String, String> getErrormap() {
		return errormap;
	}

	/**
	 * Sets the errormap.
	 *
	 * @param errormap the errormap
	 */
	public void setErrormap(Map<String, String> errormap) {
		this.errormap = errormap;
	}	
}
