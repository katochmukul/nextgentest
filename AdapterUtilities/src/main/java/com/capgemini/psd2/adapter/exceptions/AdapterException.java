/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class AdapterException.
 */
public class AdapterException extends PSD2Exception{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private transient Object foundationError;
	/**
	 * Instantiates a new adapter exception.
	 *
	 * @param message the message
	 * @param errorInfo the error info
	 */
	public AdapterException(String message, ErrorInfo errorInfo){
		super(message,errorInfo);
	}		
	
	
	public Object getFoundationError() {
		return foundationError;
	}

	public AdapterException(String message, ErrorInfo errorInfo, Object foundationError){
		super(message,errorInfo);
		this.foundationError = foundationError;
	}
	/**
	 * Populate PSD 2 exception.
	 *
	 * @param detailErrorMessage the detail error message
	 * @param errorCodeEnum the error code enum
	 * @return the adapter exception
	 */
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage, String reponceBody, String status){
		ErrorInfo errorInfo = new ErrorInfo(status, detailErrorMessage, reponceBody, status);
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new AdapterException(detailErrorMessage, errorInfo);
	}
	
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage,AdapterErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), detailErrorMessage, errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new AdapterException(detailErrorMessage, errorInfo);
	}
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage,AdapterErrorCodeEnum errorCodeEnum, Object foundationError){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), detailErrorMessage, errorCodeEnum.getStatusCode());
		return new AdapterException(detailErrorMessage, errorInfo, foundationError);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum the error code enum
	 * @return the adapter exception
	 */
	public static AdapterException populatePSD2Exception(AdapterErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new AdapterException(errorCodeEnum.getErrorMessage(), errorInfo);
	}
}
