/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions;

/**
 * The Enum AdapterErrorCodeEnum.
 */
public enum AdapterErrorCodeEnum {

	/** The authentication failure error. */
	AUTHENTICATION_FAILURE_ERROR("500","You are not authorised to use this service","500"),
	
	/** The account not found. */
	ACCOUNT_NOT_FOUND("501","The request can not be processed,Account not found","400"),
	
	/** The account details not found. */
	ACCOUNT_DETAILS_NOT_FOUND("502","No account details found for the requested account(s)","400"),
	
	/** The technical error. */
	TECHNICAL_ERROR("503","Technical Error. Please try again later","500"),
	
	/** The bad request. */
	BAD_REQUEST("504","Bad request.Please check your request","500"),
	
	/** The no account data found. */
	NO_ACCOUNT_DATA_FOUND("505","No Account data found","404"),
	
	/** The no account data found foundation service. */
	NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE("506","No Account data found in foundation service","400"),
	
	/** The invalid amount. */
	INVALID_AMOUNT("507","Invalid amount found in foundation service","500"),
	
	/** The invalid currency. */
	INVALID_CURRENCY("508","Invalid currency found in foundation service","500"),
	
	/** The invalid nickname. */
	INVALID_NICKNAME("509","Invalid nick name found in foundation service","500"),
	
	/** The invalid identification. */
	INVALID_IDENTIFICATION("510","Invalid identification found in foundation service","500"),
	
	/** The no account id found. */
	NO_ACCOUNT_ID_FOUND("511","No Account ID found","404"),
	
	/** The no channel id in request. */
	NO_CHANNEL_ID_IN_REQUEST("512","No channel present in the request","400"),
	
	/** The no account nsc in request. */
	NO_ACCOUNT_NSC_IN_REQUEST("513","No account nsc present in the request","400"),
	
	/*
	 * PISP Error Code
	 */
	
	CONNECTION_ERROR("514", "Connection error", "500"),
	
	GENERAL_ERROR("515", "General error", "500" ),
	
	INVALID_IBAN_LENGTH("516", "IBAN provided/length is invalid", "400"),
	
	INVALID_BIC("517", "BIC provided is invalid", "400"),
	
	INVALID_IBAN_CHECKSUM("518", "Invalid IBAN checksum", "400"),
	
	ACCOUNT_NOT_FOUND_OR_CLOSED("519", "Account not found or closed", "400"),
	
	BIC_IS_NOT_REACHABLE("520", "BIC is not reachable", "400"),
	
	BIC_AND_IBAN_DO_NOT_MATCH("521", "BIC and IBAN do not match", "400"),
	
	NOT_A_VALID_NSC("522", "NSC provided is invalid", "400"),
	
	NOT_A_VALID_ACCOUNT_NUMBER("523", "Account number provided is invalid", "400"),
	
	NOT_A_VALID_CURRENCY("524", "Transaction currency code is invalid", "400"),	
	
	BANK_DOES_NOT_PROCESS_COUNTRY("525", "Bank does not process payments to the requested beneficiary country", "400"),	
	
	BANK_DOES_NOT_PROCESS_CURRENCY("526", "Bank does not deal in the requested transaction currency",  "400"),
	
	COUNTRY_CODE_IS_INVALID("527","Country code is invalid", "400"),
	
	PAYER_ACCOUNT_BLOCKED_TO_DEBITS("528","Account is blocked to debits",  "400"),
	
	PAYER_ACCOUNT_BLOCKED_TO_CREDITS("529","Account is blocked to credits", "400"),
	
	PAYER_ACCOUNT_BLOCKED_TO_ALL("530","Account is blocked to All",  "400"),
	
	CREDIT_GRADE_NOT_6_OR_7("531","Credit grade cannot be 6 or 7",  "400"),
	
	ACCOUNT_CANNOT_BE_DORMANT("532","Account cannot be dormant", "400"),
	
	ACCOUNT_CANNOT_BE_LIEN("533","Account cannot be lien",  "400"),
	
	JOINT_ACCOUNT_MUST_BE_PERMITTED("534","Joint account must be permitted by all the customer associated",  "400"),
	
	FUNDS_NOT_AVAILABLE("535","Account does not have enough funds",  "400"),
	
	LIMIT_EXCEEDED_FOR_CUSTOMER("536","Limit exceeded for this customer",  "400"),
	
	LIMIT_EXCEEDED_FOR_TRANSACTION_TYPE("537","Limit exceeded for this payment type for the customer",  "400"),
	
	LIMIT_EXCEEDED_FOR_AGENT("538","Limit exceeded for this agent", "400"),
	
	INVALID_PAYMENT_REQUEST("539","Invalid Payment Request",  "400"),
	
	/*
	 *SaaS and ConsentApplication Error Code Mapping for UI Specific message. The below error code is specific for SaaS and ConsentApplication YML mapping.
	 */
	
	AUTHENTICATION_FAILURE_ERROR_PMV("540","You are not authorized to use this service","500"),
	
	AUTHENTICATION_FAILURE_ERROR_PMR("541","You are not authorized to use this service","500"),
	
	NO_PAYMENT_INSTRUCTION_FOUND_PMV("542", "No Payment instruction found for the requested information", "500"),
	
	NO_TRANSACTION_DETAILS_FOUND_PMU("543", "No transaction details found for the requested account(s)", "400"),
	
	TECHNICAL_ERROR_PMR("544","Technical Error.Please try again later","500"),
	
	TECHNICAL_ERROR_PMU("545","Technical Error.Please try again later","500"),
	
	TECHNICAL_ERROR_CAP("546","Technical Error.Please try again later","500"),
	
	ACCOUNT_DETAILS_NOT_FOUND_CAP("547","No account details found for the requested account(s)","400"),
	
	GENERAL_ERROR_PMV("548", "General error", "500" ),
	
	TECHNICAL_ERROR_PMV("549","Technical Error.Please try again later","500"),
	
	
	/*CR028 error code added*/
	
	NO_PAYMENT_INSTRUCTION_FOUND("550", "No Payment instruction found for the requested information", "500"),
	
	NO_TRANSACTION_DETAILS_FOUND("551", "No transaction details found for the requested account(s)", "400"),
	
	NOT_A_SEPA_IBAN("552", "IBAN does not belong to SEPA zone", "400"),
	
	NO_SUCH_SEPA_BIC("553", "BIC does not belong to SEPA Zone", "400"),
	
	NO_OPEN_ACCOUNT_EXISTS("554", "Account does not exists", "400"),
	
	ACCOUNT_STATUS_MUST_BE_ACTIVE("555", "Account status must be active", "400"),
	
	INVALID_ACCOUNT_TYPE("556", "Account Type is invalid", "400"),
	
	SWIFT_ADDRESS_IS_INVALID("557","Swift address is invalid", "400"),
	
	ABA_CODE_IS_INVALID("558","ABA code is invalid", "400"),
	
	COUNTRY_IS_INVALID("559","Country is invalid", "400"),

	COUNTRY_RULES_LIMIT_EXCEEDED("560","Country rules limit exceeded", "400"),

	USER_ID_INVALID("561","User id is invalid", "400"),

	JURISDICTION_CODE_INVALID("562","Jurisdiction code is invalid", "400"),

	CIS_ID_INVALID("563","CIS id is invalid","400"),

	PAYER_PAYEE_ACCOUNT_SAME("564","The payer and the payee account cannot be the same", "400"),

	BIC_PROVIDER_IS_INVALID("565","BIC provided is invalid", "400"),
	
	PAYMENT_REJECTED("566","Payment rejected due to the suspicion of fraud", "400"),
	
	INVALID_ACCOUNT_NUMBER_LENGTH("567","Invalid Account Number or NSC","500"),
	
	USER_IS_BLOCKED("568","The Customer Account Profile is blocked","400"),
	
	/*For AccountBeneficiaryAdapter*/
	
	NO_BENEFICIARY_FOUND("569","This request cannot be processed. No benificiary found","200"),
	
	/*For AccountStandingOrdersAdapter*/
	
	NO_STANDINGORDER_FOUND("570","This request cannot be processed. No StandingOrder found" ,"200"),
	
	/*For AccountProductAdapter*/ 
	
	NO_PRODUCT_FOUND("571","This request cannot be processed. No Product found" ,"200"),
	
	
	/*For AccountTransactionAdapter*/
	
	CONSENT_EXPIRED("572","Consent has expired","403"),
	
	INVALID_FILTER("573","Invalid transaction filter passed" ,"400"),
	
	/*CR046 error code added*/
	
	BAD_REQUEST_PISP("574","Bad request.Please check your request","400");
	
	
	
	
	/** The error code. */
	private String errorCode;
	
	/** The error message. */
	private String errorMessage;
	
	/** The status code. */
	private String statusCode;

	/**
	 * Instantiates a new adapter error code enum.
	 *
	 * @param errorCode the error code
	 * @param errorMesssage the error messsage
	 * @param statusCode the status code
	 */
	AdapterErrorCodeEnum(String errorCode,String errorMesssage,String statusCode){
		this.errorCode=errorCode;
		this.errorMessage=errorMesssage;
		this.statusCode = statusCode;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}

}
