package com.capgemini.psd2.adapter.fraudnet.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ruleType", "ruleDescription" })
public class AssessmentRule {

	@JsonProperty("ruleType")
	private String ruleType;
	@JsonProperty("ruleDescription")
	private String ruleDescription;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("ruleType")
	public String getRuleType() {
		return ruleType;
	}

	@JsonProperty("ruleType")
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	@JsonProperty("ruleDescription")
	public String getRuleDescription() {
		return ruleDescription;
	}

	@JsonProperty("ruleDescription")
	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
