package com.capgemini.psd2.adapter.utility;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AdapterUtility {

	private Map<String, String> foundationUrl = new HashMap<>();

	private Map<String, String> foundationCustProfileUrl = new HashMap<>();

	private Map<String, String> throwableError = new HashMap<>();

	public String retriveFoundationServiceURL(String channelId) {
		return foundationUrl.get(channelId);
	}

	public String retriveCustProfileFoundationServiceURL(String channelId) {
		return foundationCustProfileUrl.get(channelId);
	}

	public Map<String, String> getThrowableError() {
		return throwableError;
	}

	public void setThrowableError(Map<String, String> throwableError) {
		this.throwableError = throwableError;
	}

	public Map<String, String> getFoundationUrl() {
		return foundationUrl;
	}

	public void setFoundationUrl(Map<String, String> foundationUrl) {
		this.foundationUrl = foundationUrl;
	}

	public Map<String, String> getFoundationCustProfileUrl() {
		return foundationCustProfileUrl;
	}

	public void setFoundationCustProfileUrl(Map<String, String> foundationCustProfileUrl) {
		this.foundationCustProfileUrl = foundationCustProfileUrl;
	}

}
