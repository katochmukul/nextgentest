package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationFailureHandler;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomAuthenticationFailureHandlerTest {
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@Mock
	private FraudSystemHelper fraudSystemHelper;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private PSD2AuthenticationException exception;
	
	@InjectMocks
	private CustomAuthenticationFailureHandler failureHandler;
	
	private String correlationID = "12345";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	
	
	@Test
	public void onAuthenticationFailureTest() throws ServletException,IOException{
		PickupDataModel pickUpDataModel = new PickupDataModel();
		pickUpDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(SCAConsentHelper.populatePickupDataModel(request)).thenReturn(pickUpDataModel);
		ErrorInfo errorInfo = mock(ErrorInfo.class);
		when(((PSD2AuthenticationException) exception).getErrorInfo()).thenReturn(errorInfo);
		
		
		String saasUrl = "https://localhost:8096/SaaS/saaslogin?oAuthUrl=ZeFd1fUQy82i5SUjwDb6/U/0CR5Epj+hxQJLNnqyF13+TJ61bj/0+GSAEy0CAwlJdC9+CbujbHcIdSnuNaR0sMIpqrKMzkcOCarWasWfjZE=";
		when(requestHeaderAttribute.getCorrelationId()).thenReturn(correlationID);
		when(request.getParameter(AdapterSecurityConstants.SAAS_URL_PARAM)).thenReturn(saasUrl);
		//when(request.getAttribute(PSD2SecurityConstants.OAUTH_URL_PARAM)).thenReturn(SaaSMockData.getMockEncOAuthUrl());
		when(request.getRequestDispatcher(anyString())).thenReturn(SaaSMockData.getMockRequestDispatcher());
		//AuthenticationException exception = AdapterAuthenticationException.populateAuthenticationFailedException(SecurityErrorCodeEnum.TECHNICAL_ERROR);
		when(errorInfo.getStatusCode()).thenReturn("200");
		failureHandler.onAuthenticationFailure(request, response, exception);
		assert(true);
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		failureHandler = null;
	}

}
