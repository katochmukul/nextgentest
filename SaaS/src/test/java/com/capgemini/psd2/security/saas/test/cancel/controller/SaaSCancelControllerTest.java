package com.capgemini.psd2.security.saas.test.cancel.controller;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.saas.cancel.controller.SaaSCancelController;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSCancelControllerTest {
	
	@Mock
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	
	@Mock
	private SCAConsentHelperService helperService;

	@Mock
	private AccountRequestAdapter accountRequestAdapter;

	@Mock
	private PFConfig pfConfig;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private AispConsentAdapter aispConsentAdapter;
	
	@InjectMocks
	private SaaSCancelController controller = new SaaSCancelController();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void cancelSetUpTestWithAISPIntentTypeServerErrorFlagNotNull() throws ParseException{
		AispConsent aispConsent=new AispConsent();
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(), anyObject())).thenReturn(aispConsent);
		AccountRequestPOSTResponse accountRequestPOSTResponse=new AccountRequestPOSTResponse();
		Data1 obj=new Data1();
		obj.setStatus(StatusEnum.AUTHORISED);
		accountRequestPOSTResponse.setData(obj);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountRequestPOSTResponse);
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId);
	}
	
	@Test
	public void cancelSetUpTestWithAISPIntentTypeServerErrorFlagNull() throws ParseException{
		AispConsent aispConsent=new AispConsent();
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(), anyObject())).thenReturn(aispConsent);
		AccountRequestPOSTResponse accountRequestPOSTResponse=new AccountRequestPOSTResponse();
		Data1 obj=new Data1();
		obj.setStatus(StatusEnum.AUTHORISED);
		accountRequestPOSTResponse.setData(obj);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountRequestPOSTResponse);
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId);
	}
	
	@Test
	public void cancelSetUpTestWithPISPIntentTypeServerErrorFlagNotNull() throws ParseException{
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.PISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId);
	}
	
	@Test
	public void cancelSetUpTestWithPISPIntentTypeServerErrorFlagNull() throws ParseException{
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = null;
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.PISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId);
	}
}
