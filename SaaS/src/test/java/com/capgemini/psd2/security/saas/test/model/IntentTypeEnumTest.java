package com.capgemini.psd2.security.saas.test.model;

import static org.junit.Assert.assertEquals;

import javax.accessibility.AccessibleRelation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.security.saas.model.IntentTypeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
public class IntentTypeEnumTest {
		
	private IntentTypeEnum intentType;
	
	@Before
	public void setUp() throws Exception {	
		MockitoAnnotations.initMocks(this);
		intentType = IntentTypeEnum.AISP_INTENT_TYPE;
	}
	
	@Test
	public void testGetter(){
		assertEquals(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType(),intentType.getIntentType());
		assertEquals(IntentTypeEnum.AISP_INTENT_TYPE.getScope(),intentType.getScope());
	}
	
	@Test
	public void testSetters(){
		intentType.setScope("accounts");
		intentType.setIntentType("AISP");
	}
	
	@Test
	public void findCurrentIntentTypeTest(){
		assertEquals(IntentTypeEnum.AISP_INTENT_TYPE.findCurrentIntentType("accounts"),intentType.findCurrentIntentType("accounts"));
	}
}
