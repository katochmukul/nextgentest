package com.capgemini.psd2.security.saas.services;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.MainPickupDataService;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

@Service
public class SaaSPickupDataService extends MainPickupDataService{
	

	public PickupDataModel populateIntentData(String jsonResponse) throws ParseException {

		PickupDataModel intentData = new PickupDataModel();

		JSONParser parser = new JSONParser();

		String intentId = null;

		JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
		String claims = (String) jsonObject.get(OIDCConstants.REQUEST);
		
		String correlationId =  (String) jsonObject.get(PSD2Constants.CORRELATION_ID);
		
		if(correlationId == null || correlationId.trim().length() == 0){
			correlationId =  (String) jsonObject.get(PSD2Constants.CO_RELATION_ID);
		}
				
		
		Jwt jwt = JwtHelper.decode(claims);
		JSONObject claimsJsonObject = (JSONObject) parser.parse(jwt.getClaims());
		String scopes = (String) claimsJsonObject.get(OIDCConstants.SCOPE);
		String clientId = (String) claimsJsonObject.get(OIDCConstants.CLIENT_ID);
		
		if(clientId == null) {
			clientId = (String)jsonObject.get(OIDCConstants.CLIENT_ID);
		}
		
		if (scopes != null && !scopes.isEmpty() && scopes.contains(OIDCConstants.OPENID)) {
			scopes = scopes.replace(OIDCConstants.OPENID, StringUtils.EMPTY).trim();
		}
		JSONObject intentJsonObject = (JSONObject) claimsJsonObject.get(OIDCConstants.CLAIMS);
		
/*		Pattern openBankingPattern = Pattern.compile("\"userinfo\":\\{\"openbanking_intent_id\":\\{\"value\":(.*?),");
		Pattern idTokenPattern = Pattern.compile("\"id_token\":\\{\"openbanking_intent_id\":\\{\"value\":(.*?),");

		
		Matcher openBankingMatcher = openBankingPattern.matcher(intentJsonObject.toJSONString());
		Matcher idTokenMatcher = idTokenPattern.matcher(intentJsonObject.toJSONString());
		
		if(openBankingMatcher.matches()){
			intentId = openBankingMatcher.group(1);
		}
		else if(idTokenMatcher.matches()){
			intentId = idTokenMatcher.group(1);
		}*/
		
		if (intentJsonObject.get(OIDCConstants.ID_TOKEN) != null) {
			JSONObject intentObj = (JSONObject) ((JSONObject) intentJsonObject.get(OIDCConstants.ID_TOKEN))
					.get(OIDCConstants.OPENBANKING_INTENT_ID);
			if (intentObj != null) {
				if (intentObj.get(OIDCConstants.VALUE) != null) {
					intentId = ((String) intentObj.get(OIDCConstants.VALUE)).substring(
							((String) intentObj.get(OIDCConstants.VALUE)).lastIndexOf(":") + 1,
							((String) intentObj.get(OIDCConstants.VALUE)).length());
				}
			}
		}
/*		if (intentId == null || intentId.isEmpty()) {
			if (intentJsonObject.get(OIDCConstants.USER_INFO) != null) {
				JSONObject intentObj = (JSONObject) ((JSONObject) intentJsonObject.get(OIDCConstants.USER_INFO))
						.get(OIDCConstants.OPENBANKING_INTENT_ID);
				if (intentObj != null) {
					if (intentObj.get(OIDCConstants.VALUE) != null) {
						intentId = ((String) intentObj.get(OIDCConstants.VALUE)).substring(
								((String) intentObj.get(OIDCConstants.VALUE)).lastIndexOf(":") + 1,
								((String) intentObj.get(OIDCConstants.VALUE)).length());
					}
				}
			}
		}*/

		intentData.setClientId(clientId);
		intentData.setScope(scopes);
		intentData.setIntentId(intentId);
		intentData.setCorrelationId(correlationId);
		return intentData;
	}
}
