package com.capgemini.psd2.security.saas.handlers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;

@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	//private static final Logger LOGGER = LoggerFactory.getLogger(CustomAuthenticationFailureHandler.class);
	
/*	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;
*/	
	@Autowired
	private FraudSystemHelper fraudSystemHelper;
	
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		//LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}", "com.capgemini.psd2.security.alpha.saas.handlers.CustomAuthenticationFailureHandler.onAuthenticationFailure()",DateUtilites.generateCurrentTimeStamp(),reqHeaderAttribute.getCorrelationId());
		//LOGGER.info("Error" + exception.getMessage() );
		//LOGGER.info("Error" + exception.getCause() );

		// Fraudnet Call
		PickupDataModel pickUpDataModel = SCAConsentHelper.populatePickupDataModel(request);
		Map<String, String> fraudParams = new HashMap<>();
		fraudParams.put(PSD2Constants.FLOWTYPE, pickUpDataModel.getIntentTypeEnum().getIntentType());
		fraudSystemHelper.captureFraudEvent(fraudParams);			
		
		this.setUseForward(Boolean.TRUE);
		if(exception instanceof PSD2AuthenticationException)
		{
			ErrorInfo errorInfo = ((PSD2AuthenticationException) exception).getErrorInfo();
			if(errorInfo!=null){
				errorInfo.setDetailErrorMessage(null);
				exception = PSD2AuthenticationException.populateAuthenticationFailedException(errorInfo);
				response.setContentType("application/json");
				response.setStatus(Integer.parseInt(errorInfo.getStatusCode()));
			}
		}
		saveException(request, exception);
		/*if(LOGGER.isDebugEnabled()){
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":\"{}\"}", "com.capgemini.psd2.security.alpha.saas.handlers.CustomAuthenticationFailureHandler.onAuthenticationFailure()",DateUtilites.generateCurrentTimeStamp(),reqHeaderAttribute.getCorrelationId(),exception.getStackTrace());
		}*/
		request.getRequestDispatcher("/errors").forward(request, response);
	}
}
