package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.Beneficiaries;

public interface AccountBeneficiariesService {
	
	public Beneficiaries getBeneficiaries(String userId, String nsc, String accountNumber);

}
