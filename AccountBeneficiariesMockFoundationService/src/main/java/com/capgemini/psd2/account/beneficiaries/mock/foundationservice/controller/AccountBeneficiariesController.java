package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.Beneficiaries;
import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service.AccountBeneficiariesService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;


@RestController
@RequestMapping("/fs-abt-service/services")
public class AccountBeneficiariesController {

	@Autowired
	private AccountBeneficiariesService accountBeneficiariesService; 	

	@RequestMapping(value = "/user/{userId}/beneficiaries", method = RequestMethod.GET, produces = {MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity<Beneficiaries> retrieveBeneficiaries(
			@PathVariable("userId") String userId,
			@RequestParam(required = false, value = "nsc") String nsc,
			@RequestParam(required = false, value = "accountNumber") String accountNumber,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) {

		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_ESBE);
		}
		if(NullCheckUtils.isNullOrEmpty(nsc) || NullCheckUtils.isNullOrEmpty(accountNumber)){
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_ESBE);
		}
		Beneficiaries beneficiaries = accountBeneficiariesService.getBeneficiaries(userId, nsc, accountNumber);
		return new ResponseEntity(beneficiaries, HttpStatus.OK);
	}


}
