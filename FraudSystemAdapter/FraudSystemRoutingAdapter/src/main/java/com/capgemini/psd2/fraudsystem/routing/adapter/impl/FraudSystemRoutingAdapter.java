package com.capgemini.psd2.fraudsystem.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.routing.adapter.routing.FraudSystemAdapterFactory;

public class FraudSystemRoutingAdapter implements FraudSystemAdapter {

	@Autowired
	private FraudSystemAdapterFactory fraudSystemAdapterFactory;

	@Value("${app.defaultFraudSystemAdapter}")
	private String defaultAdapter;


	@Override
	public <T> T retrieveFraudScore(Map<String, Map<String, Object>> fraudSystemRequest) {
		 FraudSystemAdapter fraudSysteAdapter = fraudSystemAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return fraudSysteAdapter.retrieveFraudScore(fraudSystemRequest);
	}

}