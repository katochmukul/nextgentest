package com.capgemini.psd2.consent.revocation.test.data;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.revocation.data.Consent;
import com.capgemini.psd2.consent.revocation.data.ConsentListResponse;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentListResponseTest {

	@Test
	public void test() {
		ConsentListResponse response = new ConsentListResponse();
		List<Consent> data = new ArrayList<>();
		Consent consent = new Consent();

		consent.setConsentId("19910488");
		consent.setPsuId("boi123");
		consent.setTppLegalEntityName("Moneywise-Wealth");
		consent.setStartDate("2017-07-24T13:45:44.269Z");
		consent.setEndDate("2017-05-02T00:00:00.875");
		consent.setConsentCreationDate("2017-07-24T13:45:44.269Z");
		consent.setStatusName("AWAITINGAUTHENTICATION");

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		consent.setPermissions(permissions);

		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails detail1 = new AccountDetails();
		detail1.setAccountNumber("76528776");
		detail1.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail1.setAccountNSC("903779");
		accountDetails.add(detail1);
		AccountDetails detail2 = new AccountDetails();
		detail2.setAccountNumber("76528776");
		detail2.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail2.setAccountNSC("903779");
		accountDetails.add(detail2);
		AccountDetails detail3 = new AccountDetails();
		detail3.setAccountNumber("76528776");
		detail3.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail3.setAccountNSC("903779");
		accountDetails.add(detail3);
		consent.setAccountDetails(accountDetails);

		consent.setTppCId("Moneywise-Wealth");

		consent.setTransactionFromDateTime("2015-05-02T00:00:00.875");

		consent.setTransactionToDateTime("2019-05-02T00:00:00.875");

		consent.setAccountRequestId("55559717-5ae1-48c2-95bb-c27d8be1df9d");

		data.add(consent);
		response.setData(data);
		assertTrue(response.getData().equals(data));

		Links links = new Links();
		response.setLinks(links);
		assertTrue(response.getLinks().equals(links));

		MetaData meta = new MetaData();
		response.setMeta(meta);
		assertTrue(response.getMeta().equals(meta));

		response.data(data);
		response.links(links);
		response.meta(meta);
		
		response.hashCode();
		response.equals(response);
		response.toString();
		response.setData(null);
		response.toString();
		response.equals(null);
		response.equals(consent);
		
	}
	
}
