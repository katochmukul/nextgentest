package com.capgemini.psd2.consent.revocation.test.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.revocation.controller.ConsentRevocationController;
import com.capgemini.psd2.consent.revocation.data.ConsentListResponse;
import com.capgemini.psd2.consent.revocation.service.ConsentRevocationService;
import com.capgemini.psd2.consent.revocation.test.mock.data.ConsentRevocationMockData;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentRevocationControllerTest {
	
	@Mock
	private ConsentRevocationService service;
	
	
	@InjectMocks
	private ConsentRevocationController controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetConsentListSuccess() {
		ConsentListResponse consentListResponse = ConsentRevocationMockData.getConsentListResponseMockData();
		String userId = "boi123";
		String status = "EXPIRED";
		Mockito.when(service.getConsentList(userId, status)).thenReturn(consentListResponse);
		
		ConsentListResponse response = controller.getConsentList(userId, status);
		assertNotNull(response.getData());
		assertTrue(response.getData().size() != 0);
		assertNotNull(response.getLinks());
		assertNotNull(response.getMeta());
		assertTrue(consentListResponse.getData().get(0).getConsentId().equals(response.getData().get(0).getConsentId()));		
	}
		
	@Test
	public void testGetConsentListNullStatusSuccess() {
		ConsentListResponse consentListResponse = ConsentRevocationMockData.getConsentListResponseMockData();
		String userId = "boi123";
		Mockito.when(service.getConsentList(userId, null)).thenReturn(consentListResponse);
		ConsentListResponse response = controller.getConsentList(userId, null);
		assertNotNull(response.getData());
		assertTrue(response.getData().size() != 0);
		assertNotNull(response.getLinks());
		assertNotNull(response.getMeta());
		assertTrue(consentListResponse.getData().get(0).getConsentId().equals(response.getData().get(0).getConsentId()));		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListNullOrEmptyUserIdException() {
		String userId = "";
		controller.getConsentList(userId, null);		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListInvalidStatusException() {
		String userId = "boi123";
		String status = "abc";
		controller.getConsentList(userId, status);		
	}
	
	@After
	public void tearDown() throws Exception {
		controller = null;
		service = null;
	}
	
}
