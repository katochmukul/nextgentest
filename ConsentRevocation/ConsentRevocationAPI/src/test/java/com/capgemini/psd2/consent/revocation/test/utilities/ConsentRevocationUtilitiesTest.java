package com.capgemini.psd2.consent.revocation.test.utilities;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.revocation.utilities.ConsentRevocationUtilities;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentRevocationUtilitiesTest {

	@Test(expected = PSD2Exception.class)
	public void testGetConsentListNullOrEmptyUserIdException() {
		String userId = "";
		ConsentRevocationUtilities.validateInputs(userId, "test");
	}

	@Test(expected = PSD2Exception.class)
	public void testGetConsentListInvalidStatusException() {
		String userId = "boi123";
		String status = "abc";
		ConsentRevocationUtilities.validateInputs(userId, status);
	}
}
