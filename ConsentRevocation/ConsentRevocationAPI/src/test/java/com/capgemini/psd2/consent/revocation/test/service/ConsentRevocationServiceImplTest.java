package com.capgemini.psd2.consent.revocation.test.service;

import static org.mockito.Matchers.anyString;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.consent.revocation.service.impl.ConsentRevocationServiceImpl;
import com.capgemini.psd2.consent.revocation.test.mock.data.ConsentRevocationMockData;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentRevocationServiceImplTest {

	@Mock
	private AispConsentAdapterImpl aispConsentAdapter;
	
	@Mock
	private PispConsentAdapterImpl pispConsentAdapter;

	@Mock
	private AccountRequestAdapter accountRequestAdapter;
	
	@Mock
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Mock
	private HttpServletRequest httpServletRequest;

	@InjectMocks
	private ConsentRevocationServiceImpl service;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void test() {

	}

	@Test
	public void testGetConsentListSuccessNullStatus() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null))
				.thenReturn(ConsentRevocationMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentRevocationMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				null)).thenReturn(ConsentRevocationMockData.getPispConsentsList());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentId(anyString())).thenReturn(ConsentRevocationMockData.getPaymentSetupPlatformRepository());
		Mockito.when(httpServletRequest.getQueryString()).thenReturn(null);
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, null);
	}

	@Test
	public void testGetConsentListSuccess() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(ConsentRevocationMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentRevocationMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(ConsentRevocationMockData.getPispConsentsList());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentId(anyString())).thenReturn(ConsentRevocationMockData.getPaymentSetupPlatformRepository());
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode().toUpperCase());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListNoConsentsFoundException() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(new ArrayList<AispConsent>());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentRevocationMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(new ArrayList<PispConsent>());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentId(anyString())).thenReturn(ConsentRevocationMockData.getPaymentSetupPlatformRepository());
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode().toUpperCase());
	}

	@Test
	public void testGetConsentListSuccessEmptyAccountRequestResponse() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(ConsentRevocationMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(null);
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode().toUpperCase());
	}

	@After
	public void tearDown() throws Exception {
		aispConsentAdapter = null;
		accountRequestAdapter = null;
		httpServletRequest = null;
	}

}