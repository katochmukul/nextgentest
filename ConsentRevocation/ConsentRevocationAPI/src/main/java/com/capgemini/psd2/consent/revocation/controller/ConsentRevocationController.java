package com.capgemini.psd2.consent.revocation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.consent.revocation.data.ConsentListResponse;
import com.capgemini.psd2.consent.revocation.service.ConsentRevocationService;
import com.capgemini.psd2.consent.revocation.utilities.ConsentRevocationUtilities;

@RestController
public class ConsentRevocationController {

	@Autowired
	private ConsentRevocationService consentRevocationService;

	@RequestMapping(value = "/consents/{userid}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ConsentListResponse getConsentList(@PathVariable("userid") String userId,
			@RequestParam(value = "status", required = false) String status) {

		ConsentRevocationUtilities.validateInputs(userId, status);
		
		return consentRevocationService.getConsentList(userId, status);
	}
}
