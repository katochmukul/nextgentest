package com.capgemini.psd2.aspsp.proxy.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.springframework.cloud.netflix.ribbon.ServerIntrospector;
import org.springframework.cloud.netflix.ribbon.apache.RibbonLoadBalancingHttpClient;
import org.springframework.cloud.netflix.zuul.util.ZuulRuntimeException;

import com.netflix.client.config.CommonClientConfigKey;
import com.netflix.client.config.IClientConfig;

public class CustomHttpClient extends RibbonLoadBalancingHttpClient{


	private KeyStore populateTrustStore() {
		KeyStore ks = null;
		String trustStoreFile = System.getProperty("javax.net.ssl.trustStore");
		String trustStorePwd = System.getProperty("javax.net.ssl.trustStorePassword");
		if(trustStoreFile !=null && !trustStoreFile.isEmpty() && trustStorePwd != null && !trustStorePwd.isEmpty()) {
			try (FileInputStream fi = new FileInputStream(new File(trustStoreFile))) {
				ks= KeyStore.getInstance("jks");
				ks.load(fi,trustStorePwd.toCharArray());
			} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
				throw new ZuulRuntimeException(e);
			}		
		}
		return ks;
	}

	/**
	 * @param config
	 * @param serverIntrospector
	 */
	public CustomHttpClient(IClientConfig config, ServerIntrospector serverIntrospector) {
		super(config, serverIntrospector);
	}

	@Override
	protected CloseableHttpClient createDelegate(IClientConfig config) {
		SSLContext sslcontext = null;
		KeyStore trustStore = populateTrustStore();
		try {
			sslcontext = SSLContexts.custom()
					.loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
					.build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			throw new ZuulRuntimeException(e);
		}
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,NoopHostnameVerifier.INSTANCE);
		return HttpClientBuilder.create()
				.setSSLSocketFactory(sslsf).setMaxConnPerRoute(config.getPropertyAsInteger(CommonClientConfigKey.MaxConnectionsPerHost, 0))
				.setMaxConnTotal(config.getPropertyAsInteger(CommonClientConfigKey.MaxTotalConnections, 0))
				.disableCookieManagement()
				.useSystemProperties()
				.build();

	}

}