package com.capgemini.psd2.account.request.routing.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.request.routing.adapter.routing.AccountRequestAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;

public class AccountRequestRoutingAdapter implements AccountRequestAdapter {
	
	/** The default adapter. */
	@Value("${app.defaultAdapterAccountReq}")
	private String defaultAdapter;
	
	@Autowired
	private AccountRequestAdapterFactory accountRequestAdapterFactory;

	@Override
	public AccountRequestPOSTResponse createAccountRequestPOSTResponse(Data1 data) {

		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		return accountRequestAdapter.createAccountRequestPOSTResponse(data);
	}

	@Override
	public AccountRequestPOSTResponse getAccountRequestGETResponse(String accountRequestId) {

		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		return accountRequestAdapter.getAccountRequestGETResponse(accountRequestId);
	}

	@Override
	public AccountRequestPOSTResponse updateAccountRequestResponse(String accountRequestId, StatusEnum statusEnum) {

		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		return accountRequestAdapter.updateAccountRequestResponse(accountRequestId, statusEnum);
	}
	
	@Override
	public void removeAccountRequest(String accountRequestId,String cid) {
		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		accountRequestAdapter.removeAccountRequest(accountRequestId,cid);
	}
}
