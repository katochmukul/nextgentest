package com.capgemini.psd2.adapter.routing.test.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.joda.time.LocalDateTime;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;

public class AccountRequestRoutingAdapterTestMockData {

	public static AccountRequestPOSTResponse postAccountRequestPOSTResponse(Data1 data) {

		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);
		return accountRequestPOSTResponse;
	}

	public static AccountRequestPOSTResponse getAccountRequestGETResponse(String accountRequestId) {
		
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 data = createData();
		
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);		
		return accountRequestPOSTResponse;
	}

	public static AccountRequestPOSTResponse updateAccountRequestResponse(String accountRequestId, StatusEnum statusEnum) {

		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 data = createData();
		data.setStatus(statusEnum);
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);		
		return accountRequestPOSTResponse;
	}

	public static Data1 createData() {
		
		Data1 data = new Data1();
		data.setAccountRequestId(UUID.randomUUID().toString());
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		data.setPermissions(permissions);

		data.setCreationDateTime(LocalDateTime.now().toString());
		data.setExpirationDateTime("2017-08-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		return data;
	}

}
