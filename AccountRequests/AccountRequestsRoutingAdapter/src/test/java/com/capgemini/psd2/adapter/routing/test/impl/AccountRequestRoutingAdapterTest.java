package com.capgemini.psd2.adapter.routing.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.account.request.routing.adapter.routing.AccountRequestAdapterFactory;
import com.capgemini.psd2.adapter.routing.test.adapter.AccountRequestTestRoutingAdapter;
import com.capgemini.psd2.adapter.routing.test.data.AccountRequestRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestRoutingAdapterTest {
	
	@Mock
	private AccountRequestAdapterFactory accountRequestAdapterFactory;
	
	@InjectMocks
	private AccountRequestAdapter accountRequestRountingAdapter = new AccountRequestRoutingAdapter();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void testAccountRequestPOSTRequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		Data1  data = AccountRequestRoutingAdapterTestMockData.createData();
		AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestRountingAdapter.createAccountRequestPOSTResponse(data);
		
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(StatusEnum.AWAITINGAUTHORISATION));
	}

	
	@Test
	public void testAccountRequestGETRequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestRountingAdapter.getAccountRequestGETResponse(anyString());
		
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(StatusEnum.AWAITINGAUTHORISATION));
	}
	
	@Test
	public void testAccountRequestUPDATERequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestRountingAdapter.updateAccountRequestResponse(anyString(), StatusEnum.AUTHORISED);
		
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(StatusEnum.AUTHORISED));
	}
	
	@Test
	public void testAccountRequestDELETERequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		accountRequestRountingAdapter.removeAccountRequest(anyString(), null);
		
	}
}
