package com.capgemini.psd2.account.request.validation;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.Data.PermissionsEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.DateUtilites;

@Component
public class AccountRequestValidationUtilities {

	@Autowired
	private PermissionsValidationRules permissionsValidationRules;

	@Value("${app.validAccountRequestIdChars}")
	private String validAccountRequestIdChars;

	public AccountRequestPOSTRequest validatePermissions(AccountRequestPOSTRequest accountRequestPOSTRequest) {

		List<PermissionsEnum> permissions;
		Set<PermissionsEnum> setOfPermissions;

		if (null == accountRequestPOSTRequest.getData().getPermissions()
				|| accountRequestPOSTRequest.getData().getPermissions().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PERMISSIONS_VALIDATION_ERROR);
		} else {
			permissions = accountRequestPOSTRequest.getData().getPermissions();
			if (permissions.contains(null)) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PERMISSIONS_VALIDATION_ERROR);
			}

			setOfPermissions = validateMandatoryRules(permissions);
			validateOrConditionRules(setOfPermissions);

		}
		permissions = new ArrayList<>(setOfPermissions);
		accountRequestPOSTRequest.getData().setPermissions(permissions);
		return accountRequestPOSTRequest;
	}

	private void validateOrConditionRules(Set<PermissionsEnum> setOfPermissions) {
		Map<String, List<PermissionsEnum>> orConditonRules = permissionsValidationRules.getOrConditonRules();
		for (Map.Entry<String, List<PermissionsEnum>> entry : orConditonRules.entrySet()) {
			if (setOfPermissions.contains(PermissionsEnum.valueOf(entry.getKey()))) {
				validateOrConditionPermissions(setOfPermissions, entry.getValue());
			}
		}
	}

	private void validateOrConditionPermissions(Set<PermissionsEnum> setOfPermissions,
			List<PermissionsEnum> permissions) {

		boolean flag = Boolean.FALSE;
		for (PermissionsEnum orPermission : permissions) {
			if (setOfPermissions.contains(orPermission)) {
				flag = true;
				break;
			}
		}

		if (Boolean.FALSE == flag) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PERMISSIONS_VALIDATION_ERROR);
		}
	}

	private Set<PermissionsEnum> validateMandatoryRules(List<PermissionsEnum> permissions) {

		Set<PermissionsEnum> setOfPermissions = new LinkedHashSet<>(permissions);
		Map<String, List<PermissionsEnum>> mandatoryRules = permissionsValidationRules.getMandatoryRules();
		for (Map.Entry<String, List<PermissionsEnum>> entry : mandatoryRules.entrySet()) {
			if (setOfPermissions.contains(PermissionsEnum.valueOf(entry.getKey()))) {
				setOfPermissions.addAll(entry.getValue());
			}
		}
		return setOfPermissions;
	}

	public AccountRequestPOSTRequest validateAccountRequestInputs(AccountRequestPOSTRequest accountRequestPOSTRequest) {

		AccountRequestPOSTRequest validatedAccountRequestPOSTRequest;

		if (null == accountRequestPOSTRequest || null == accountRequestPOSTRequest.getData()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		} else {
			String expirationDateTime = DateUtilites
					.validateAndUpdateDateTime(accountRequestPOSTRequest.getData().getExpirationDateTime());
			String transactionFromDateTime = DateUtilites
					.validateAndUpdateDateTime(accountRequestPOSTRequest.getData().getTransactionFromDateTime());
			String transactionToDateTime = DateUtilites
					.validateAndUpdateDateTime(accountRequestPOSTRequest.getData().getTransactionToDateTime());

			boolean expirationDateTimeCheck = false;

			if (null != expirationDateTime) {
				accountRequestPOSTRequest.getData().setExpirationDateTime(expirationDateTime);
				expirationDateTimeCheck = DateUtilites.isDateComparisonPassed(expirationDateTime,
						DateUtilites.getCurrentDateInISOFormat());
			}

			AccountRequestPOSTRequest validFromDateTimeUpdatedRequest = updateTransactionFromDateTime(
					accountRequestPOSTRequest, transactionFromDateTime);
			AccountRequestPOSTRequest validToDateTimeUpdatedRequest = updateTransactionToDateTime(
					validFromDateTimeUpdatedRequest, transactionToDateTime);

			if (null != transactionFromDateTime && null != transactionToDateTime) {
				boolean fromAndTodateTimeComparison;
				fromAndTodateTimeComparison = DateUtilites.isDateComparisonPassed(transactionFromDateTime,
						transactionToDateTime);
				if (!fromAndTodateTimeComparison) {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
				}
			}

			if (expirationDateTimeCheck)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);

			validatedAccountRequestPOSTRequest = validatePermissions(validToDateTimeUpdatedRequest);
			
		}
		return validatedAccountRequestPOSTRequest;
	}


	private AccountRequestPOSTRequest updateTransactionFromDateTime(AccountRequestPOSTRequest accountRequestPOSTRequest,
			String transactionFromDateTime) {

		if (null != transactionFromDateTime) {
			accountRequestPOSTRequest.getData().setTransactionFromDateTime(transactionFromDateTime);
		}

		return accountRequestPOSTRequest;
	}

	private AccountRequestPOSTRequest updateTransactionToDateTime(AccountRequestPOSTRequest accountRequestPOSTRequest,
			String transactionToDateTime) {

		if (null != transactionToDateTime) {
			accountRequestPOSTRequest.getData().setTransactionToDateTime(transactionToDateTime);
		}

		return accountRequestPOSTRequest;
	}

	public void validateAccountRequestId(String accountRequestId) {

		if (accountRequestId.length() < 1 || accountRequestId.length() > 128) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}

		Pattern p = Pattern.compile(validAccountRequestIdChars);
		Matcher m = p.matcher(accountRequestId);
		if (m.find()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}
	}
}
