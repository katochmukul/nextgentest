package com.capgemini.psd2.account.request.validation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.Data.PermissionsEnum;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app.permissionValidationRules")
public class PermissionsValidationRules {
	
	/** The mandatoryRules. */
	private Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();

	/** The orConditonRules */
	private Map<String, List<PermissionsEnum>> orConditonRules = new HashMap<>();

	public Map<String, List<PermissionsEnum>> getMandatoryRules() {
		return mandatoryRules;
	}

	public void setMandatoryRules(Map<String, List<PermissionsEnum>> mandatoryRules) {
		this.mandatoryRules = mandatoryRules;
	}

	public Map<String, List<PermissionsEnum>> getOrConditonRules() {
		return orConditonRules;
	}

	public void setOrConditonRules(Map<String, List<PermissionsEnum>> orConditonRules) {
		this.orConditonRules = orConditonRules;
	}

}
