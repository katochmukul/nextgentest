package com.capgemini.psd2.account.request.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.request.data.Risk;
import com.capgemini.psd2.account.request.service.AccountRequestService;
import com.capgemini.psd2.account.request.validation.AccountReqeustConfig;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

@Service
public class AccountRequestServiceImpl implements AccountRequestService {
	
	@Autowired
	@Qualifier("accountRequestRoutingAdapter")
	private AccountRequestAdapter accountRequestAdapter;
	
	@Autowired 
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private AccountReqeustConfig bypassTPPRoles;
	
	@Override
	public AccountRequestPOSTResponse createAccountRequest(AccountRequestPOSTRequest accountRequestPOSTRequest) {
		
		AccountRequestPOSTResponse accountRequestPOSTResponse = JSONUtilities.getObjectFromJSONString(
				JSONUtilities.getJSONOutPutFromObject(accountRequestPOSTRequest), AccountRequestPOSTResponse.class);

		Data1 data = accountRequestPOSTResponse.getData();
		data.setAccountRequestId(UUID.randomUUID().toString());
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);	
		data.setCreationDateTime(DateUtilites.getCurrentDateInISOFormat());
		
		data.setTppCID(reqHeaderAtrributes.getTppCID());	
		Token token = reqHeaderAtrributes.getToken();
		TppInformationTokenData tppInformationTokenData = token.getTppInformation();
		data.setTppLegalEntityName(tppInformationTokenData.getTppLegalEntityName());
		
		accountRequestPOSTResponse = accountRequestAdapter.createAccountRequestPOSTResponse(data);
		
		if (accountRequestPOSTResponse.getRisk() == null)
			accountRequestPOSTResponse.setRisk(new Risk());
		if (accountRequestPOSTResponse.getLinks() == null)
			accountRequestPOSTResponse.setLinks(new Links());
		if (accountRequestPOSTResponse.getMeta() == null)
			accountRequestPOSTResponse.setMeta(new MetaData());
		accountRequestPOSTResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl() + PSD2Constants.SLASH
				+ accountRequestPOSTResponse.getData().getAccountRequestId());
		accountRequestPOSTResponse.getMeta().setTotalPages(1);
		return accountRequestPOSTResponse;
	}

	@Override
	public AccountRequestPOSTResponse retrieveAccountRequest(String accountRequestId) {
		
    	AccountRequestPOSTResponse accountRequestGETResponse = accountRequestAdapter.getAccountRequestGETResponse(accountRequestId);
		
		validateTppCIDUserAccess(accountRequestGETResponse.getData().getTppCID());
		
    	if (accountRequestGETResponse.getRisk() == null)
    		accountRequestGETResponse.setRisk(new Risk());
		if (accountRequestGETResponse.getLinks() == null)
			accountRequestGETResponse.setLinks(new Links());
		if (accountRequestGETResponse.getMeta() == null)
			accountRequestGETResponse.setMeta(new MetaData());
		accountRequestGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		accountRequestGETResponse.getMeta().setTotalPages(1);
		return accountRequestGETResponse;
	}

	@Override
	public void removeAccountRequest(String accountRequestId) {
		
		
		accountRequestAdapter.removeAccountRequest(accountRequestId,reqHeaderAtrributes.getTppCID());
	}
		
	private void validateTppCIDUserAccess(String tppCID) {
		if (!reqHeaderAtrributes.getTppCID().equals(tppCID)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
	}
}
