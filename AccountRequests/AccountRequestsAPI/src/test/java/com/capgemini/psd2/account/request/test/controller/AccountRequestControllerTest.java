package com.capgemini.psd2.account.request.test.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.request.controller.AccountRequestController;
import com.capgemini.psd2.account.request.service.AccountRequestService;
import com.capgemini.psd2.account.request.test.mock.data.AccountRequestMockData;
import com.capgemini.psd2.account.request.validation.AccountRequestValidationUtilities;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestControllerTest {

	@Mock
	private AccountRequestService service;

	private MockMvc mockMvc;

	@Mock
	private AccountRequestValidationUtilities accountRequestValidationUtilities;

	@InjectMocks
	private AccountRequestController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	@Test
	public void accountRequestPOSTRequestTestPositive() throws Exception {

		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestMockData
				.getAccountRequestsParamatersMockData();
		AccountRequestPOSTResponse accountRequestsResponse = AccountRequestMockData
				.getAccountRequestsMockResponseData();

		Mockito.when(accountRequestValidationUtilities.validateAccountRequestInputs(accountRequestPOSTRequest))
				.thenReturn(accountRequestPOSTRequest);
		Mockito.when(service.createAccountRequest(accountRequestPOSTRequest)).thenReturn(accountRequestsResponse);
		this.mockMvc
				.perform(post("/account-requests").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(AccountRequestMockData.asJsonString(accountRequestPOSTRequest)))
				.andExpect(status().isCreated());
	}

	@Test
	public void accountRequestGETRequestTestPositive() throws Exception {
		AccountRequestPOSTResponse accountRequestsResponse = AccountRequestMockData
				.getAccountRequestsMockResponseData();
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		Mockito.when(service.retrieveAccountRequest(accountRequestId)).thenReturn(accountRequestsResponse);
		AccountRequestPOSTResponse response = controller.getAccountRequest(accountRequestId);

		assertTrue(accountRequestsResponse.equals(response));
	}

	@Test(expected = PSD2Exception.class)
	public void accountRequestGETRequestTestNullRequestId() throws Exception {
		controller.getAccountRequest(null);
	}

	@Test
	public void accountRequestDELETERequestPositiveTest() throws Exception {
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		Mockito.doNothing().when(service).removeAccountRequest(accountRequestId);
		controller.deleteAccountRequest(accountRequestId);
	}

	@Test(expected = PSD2Exception.class)
	public void accountRequestDELETERequestExceptionTest() throws Exception {
		Mockito.doNothing().when(service).removeAccountRequest(null);
		controller.deleteAccountRequest(null);

	}

	@After
	public void tearDown() throws Exception {
		controller = null;
		mockMvc = null;
		service = null;
	}

}
