package com.capgemini.psd2.account.request.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.account.request.data.Risk;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data;
import com.capgemini.psd2.aisp.domain.Data.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountRequestMockData {

	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockData() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
		
	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockDataOrConditions() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		permissions.add(PermissionsEnum.READTRANSACTIONSDEBITS);
		permissions.add(PermissionsEnum.READTRANSACTIONSDETAIL);
		permissions.add(PermissionsEnum.READTRANSACTIONSBASIC);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static AccountRequestPOSTRequest getAccountRequestsParamatersExceptionMockData() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		
		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static AccountRequestPOSTResponse getAccountRequestsMockResponseData() {

		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();

		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("2017-05-02T00:00:00+00:00");
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00+00:00");

		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");

		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);

		return accountRequestPOSTResponse;
	}
	
	public static AccountRequestPOSTResponse getAccountRequestsBranchCoverageMockResponseData() {

		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();

		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("2017-05-02T00:00:00+00:00");
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00+00:00");

		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);

		data.setTppCID("6443e15975554bce8099e35b88b40465");
		
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(new Risk());
		Links links = new Links();
		links.setSelf("http://localhost:8989/account-requests");
		accountRequestPOSTResponse.setLinks(links);
		MetaData metaData = new MetaData();
		metaData.setTotalPages(1);
		accountRequestPOSTResponse.setMeta(metaData);

		return accountRequestPOSTResponse;
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}