package com.capgemini.psd2.account.request.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.request.mongo.db.adapter.impl.AccountRequestMongoDbAdaptorImpl;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

@Component
public class AccountRequestMongoDbAdapterConfig {

	@Bean(name="accountRequestMongoDbAdaptor")
	public AccountRequestAdapter accountRequestMongoDBAdapter(){
		return new AccountRequestMongoDbAdaptorImpl();
	}
}
