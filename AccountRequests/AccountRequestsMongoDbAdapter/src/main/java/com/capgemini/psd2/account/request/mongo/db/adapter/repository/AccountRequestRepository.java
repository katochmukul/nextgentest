package com.capgemini.psd2.account.request.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.aisp.domain.Data1;

public interface AccountRequestRepository extends MongoRepository<Data1, String> {

	public Data1 findByAccountRequestId(String accountrequestid);
	
}
