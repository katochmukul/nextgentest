package com.capgemini.psd2.account.request.mongo.db.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

public class AccountRequestMongoDbAdaptorImpl implements AccountRequestAdapter {

	@Autowired
	private AccountRequestRepository accountRequestRepository;

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;

	@Override
	public AccountRequestPOSTResponse createAccountRequestPOSTResponse(Data1 data) {

		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 responseData;
		try {
			responseData = accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		accountRequestPOSTResponse.setData(responseData);
		return accountRequestPOSTResponse;
	}

	@Override
	public AccountRequestPOSTResponse getAccountRequestGETResponse(String accountRequestId) {

		AccountRequestPOSTResponse accountRequestGETResponse = new AccountRequestPOSTResponse();
		Data1 data = null;

		try {
			data = accountRequestRepository.findByAccountRequestId(accountRequestId);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		if (null == data) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
		}

		accountRequestGETResponse.setData(data);
		return accountRequestGETResponse;
	}

	@Override
	public AccountRequestPOSTResponse updateAccountRequestResponse(String accountRequestId, StatusEnum statusEnum) {

		AccountRequestPOSTResponse updateAccountRequestResponse = new AccountRequestPOSTResponse();
		Data1 data = null;
		try {
			data = accountRequestRepository.findByAccountRequestId(accountRequestId);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		if (null == data) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
		}

		data.setStatus(statusEnum);
		try {
			data = accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		updateAccountRequestResponse.setData(data);
		return updateAccountRequestResponse;
	}

	@Override
	public void removeAccountRequest(String accountRequestId, String cid) {
		Data1 data = null;
		try {
			data = accountRequestRepository.findByAccountRequestId(accountRequestId);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		if (null == data) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
		}

		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}

		if (data.getStatus().equals(StatusEnum.AWAITINGAUTHORISATION)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_REVOKE_CONSENT_STATUS);
		}

		data.setStatus(StatusEnum.REVOKED);
		try {
			revokeConsentAndToken(accountRequestId);
			accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	private void revokeConsentAndToken(String accountRequestId) {

		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(accountRequestId,
				ConsentStatusEnum.AUTHORISED);
		if (aispConsent == null)
			return;
		aispConsentAdapter.updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.REVOKED);
	}
}
