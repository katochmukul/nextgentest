package com.capgemini.psd2.account.request.mongo.db.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;

public class AccountRequestMongoDBAdapterTestMockData {

	public static Data1 getData() {
		Data1 data = new Data1();
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setAccountRequestId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);

		return data;
	}
	
	public static Data1 getDataInvalidTppAccess() {
		Data1 data = new Data1();
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setAccountRequestId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("6443e15975554bce8099e3asgruw541b40465");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);

		return data;
	}

}
