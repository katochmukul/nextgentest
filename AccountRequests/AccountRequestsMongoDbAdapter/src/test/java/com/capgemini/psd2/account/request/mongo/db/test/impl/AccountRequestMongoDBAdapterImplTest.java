package com.capgemini.psd2.account.request.mongo.db.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.request.mongo.db.adapter.impl.AccountRequestMongoDbAdaptorImpl;
import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.account.request.mongo.db.test.mock.data.AccountRequestMongoDBAdapterTestMockData;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestMongoDBAdapterImplTest {

	@Mock
	private AccountRequestRepository accountRequestRepository;

	@InjectMocks
	private AccountRequestMongoDbAdaptorImpl accountRequestMongoDbAdaptorImpl;

	@Mock
	private AispConsentAdapterImpl aispConsentAdapter;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAccountRequestPostRequestMongoDBAdapterImplSuccess() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestMongoDbAdaptorImpl
				.createAccountRequestPOSTResponse(data);
		assertNotNull(accountRequestPOSTResponse);
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(data.getStatus()));
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestPostRequestMongoDBAdapterImplException() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.save(data)).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.createAccountRequestPOSTResponse(data);
	}

	@Test
	public void testAccountRequestGetRequestMongoDBAdapterImplSuccess() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestMongoDbAdaptorImpl
				.getAccountRequestGETResponse(anyString());
		assertNotNull(accountRequestPOSTResponse);
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestGetRequestMongoDBAdapterImplException() {

		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString()))
				.thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.getAccountRequestGETResponse(anyString());
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestGetRequestMongoDBAdapterImplNoDataFoundError() {

		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString()))
				.thenReturn(null);
		accountRequestMongoDbAdaptorImpl.getAccountRequestGETResponse(anyString());
	}
	
	@Test
	public void testAccountRequestUpateRequestMongoDBAdapterImplSuccess() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		Data1 updatedData = AccountRequestMongoDBAdapterTestMockData.getData();
		updatedData.setStatus(StatusEnum.AUTHORISED);
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(updatedData);
		AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestMongoDbAdaptorImpl
				.updateAccountRequestResponse(anyString(), StatusEnum.AUTHORISED);
		assertNotNull(accountRequestPOSTResponse);
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(updatedData.getStatus()));
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestUpdateRequestMongoDBAdapterImplException() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		Mockito.when(accountRequestRepository.save(data)).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.updateAccountRequestResponse(anyString(), StatusEnum.AUTHORISED);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestUpdateRequestMongoDBAdapterImplException2() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenThrow(new DataAccessResourceFailureException("error"));
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		accountRequestMongoDbAdaptorImpl.updateAccountRequestResponse(anyString(), StatusEnum.AUTHORISED);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestUpdateRequestMongoDBAdapterImplNoDataFoundException() {

		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(null);
		accountRequestMongoDbAdaptorImpl.updateAccountRequestResponse(anyString(), StatusEnum.AUTHORISED);
	}

	@Test
	public void testAccountRequestDeleteRequestMongoDBAdapterImplSuccess() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		data.setStatus(StatusEnum.AUTHORISED);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(data.getAccountRequestId(), ConsentStatusEnum.AUTHORISED)).thenReturn(new AispConsent());
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(any(),any());
		
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("623e9088-6855-4ce6-8fe6-a7ff906ce477");
		aispConsent.setConsentId("44483470");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(), any())).thenReturn(aispConsent);
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.REVOKED);
		

		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(), data.getTppCID());
	}
	
	@Test
	public void testAccountRequestDeleteRequestMongoDBAdapterImplSuccessNullAccessToken() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		data.setStatus(StatusEnum.AUTHORISED);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(data.getAccountRequestId(), ConsentStatusEnum.AUTHORISED)).thenReturn(new AispConsent());
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(any(),any());
		
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("623e9088-6855-4ce6-8fe6-a7ff906ce477");
		aispConsent.setConsentId("44483470");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(), any())).thenReturn(aispConsent);
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.REVOKED);
		
		
		
		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(), data.getTppCID());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestDeleteRequestMongoDBAdapterImplRevokeAccessTokenFailure() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(data.getAccountRequestId(), ConsentStatusEnum.AUTHORISED)).thenReturn(new AispConsent());
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(any(),any());
		
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("623e9088-6855-4ce6-8fe6-a7ff906ce477");
		aispConsent.setConsentId("44483470");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(), any())).thenReturn(aispConsent);
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.REVOKED);
		
		
		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(), data.getTppCID());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterImplNoDataFoundException() {

		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(),null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterImplException() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		data.setStatus(StatusEnum.REVOKED);
		Mockito.when(accountRequestRepository.save(data)).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(), data.getTppCID());		
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterImplNullDataException() {
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(null);
		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(), null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterInvalidTppAccess() {

		Data1 data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data);
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, AccountRequestMongoDBAdapterTestMockData.getDataInvalidTppAccess().getTppCID());
		accountRequestMongoDbAdaptorImpl.removeAccountRequest(anyString(), data.getTppCID());
	}
	
}
