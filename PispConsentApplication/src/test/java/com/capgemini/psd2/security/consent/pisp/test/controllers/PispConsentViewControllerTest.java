package com.capgemini.psd2.security.consent.pisp.test.controllers;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.pisp.test.mock.data.PispConsentApplicationMockdata;
import com.capgemini.psd2.security.consent.pisp.view.controllers.PispConsentViewController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PispConsentViewControllerTest {

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Mock
	private UIStaticContentUtilityController uiController;
	
	@Mock
	private RequestHeaderAttributes requestHeaders;

	@Mock
	private PispConsentCreationDataHelper consentCreationDataHelper;
	

	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private DataMask dataMask;
	
	@Mock
	private HttpServletResponse response;

	@InjectMocks
	private PispConsentViewController viewController;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".jsp");
	}

	@Test
	public void testHomePage() {
		Map<String,Object> map=new HashMap<>();
		when(httpServletRequest.getRequestURI()).thenReturn("/home");
		ReflectionTestUtils.setField(viewController, "edgeserverhost", "");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		when(httpServletRequest.getQueryString()).thenReturn("key=value");
		viewController.homePage(map);
	}
	
	@Test
	public void testCustomerConsentViewException() throws Exception {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("cahnnelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj=new PickupDataModel();
		pickupobj.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),anyString(),anyString(),anyString(),anyString())).thenReturn(PispConsentApplicationMockdata.getCustomerAccountList());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrievePaymentSetupData(anyString())).thenReturn(PispConsentApplicationMockdata.getPaymentSetupPOSTResponsewithAccounts());
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		Map<String,Object> model=new HashMap<>();
		Enumeration<String> fsHeader =  new Enumeration<String>() {
			
			@Override
			public String nextElement() {
				return null;
			}
			
			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		viewController.consentView(model);
	}

}
