package com.capgemini.psd2.security.consent.pisp.helpers;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;

public class PispConsentAuthorizationHelper extends ConsentAuthorizationHelper {

	@Override
	public PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask) {
		Map<String,String> additionalInfo = accountwithoutMask.getAdditionalInformation();
		Map<String,String> additionalInfotobeUnmasked = accountwithMask.getAdditionalInformation();
		
		if(accountwithoutMask.getAccount() != null){
			if(accountwithMask.getAccount().getSchemeName().equals(SchemeNameEnum.IBAN)){
				accountwithMask.getAccount().setIdentification(accountwithoutMask.getAdditionalInformation().get(PSD2Constants.IBAN));
			}else{
				accountwithMask.getAccount().setIdentification(accountwithoutMask.getAccount().getIdentification());
			}
		}
		
		if(additionalInfo != null && additionalInfotobeUnmasked != null){
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NUMBER, additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
			additionalInfotobeUnmasked.put(PSD2Constants.IBAN, additionalInfo.get(PSD2Constants.IBAN));
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NSC, additionalInfo.get(PSD2Constants.ACCOUNT_NSC));
			
		}
		
		return accountwithMask;
	}

}
