package com.capgemini.psd2.security.consent.pisp.helpers;

import java.util.Map;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public interface PispConsentCreationDataHelper {

	public void cancelPaymentSetup(String paymentId,Map<String, String> paramsMap);
	
	public PaymentSetupValidationResponse validatePreAuthorisation(Account account,String paymentId,Map<String,String> paramsMap);
	
	public AccountGETResponse retrieveCustomerAccountListInfo(String userId,String clientId,String flowType,String correlationId,String channelId);

	public void createConsent(Account account, String userId,String cid, String intentId, String channelId, String header, String tppAppName, Object tppInformation) throws NamingException;
		
	public PaymentSetupPOSTResponse retrievePaymentSetupData(String intentId);		
}
