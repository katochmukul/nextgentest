package com.capgemini.psd2.security.consent.pisp.helpers;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.DateUtilites;

@Component
public class PispConsentCreationDataHelperImpl implements PispConsentCreationDataHelper {


	@Autowired
	@Qualifier("CustomerAccountListAdapter") 
	private CustomerAccountListAdapter customerAccountListAdapter;
	 
	@Autowired
	private PispConsentAdapter pispConsentAdapter;

	@Autowired
	private FraudSystemHelper fraudSystemHelper;

	@Autowired
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	

	@Value("${app.paymentSetupExpiryTime:#{24}}")
	private Integer paymentSetupExpiryTime;

	@Value("${app.consentExpiryTime:#{24}}")
	private Integer consentExpiryTime;

	private static final long TIMEINSECONDS = 3600;

	@Override
	public PaymentSetupPOSTResponse retrievePaymentSetupData(String paymentId) {
		PaymentSetupPOSTResponse paymentSetUpResponse = paymentSetupAdapterHelper.retrieveStagedPaymentSetup(paymentId);
		if (paymentSetUpResponse == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND);
		}
		return paymentSetUpResponse;
	}

	@Override
	public AccountGETResponse retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId) {
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CHANNEL_ID, channelId);
		paramsMap.put(PSD2Constants.USER_ID, userId);
		paramsMap.put(PSD2Constants.CORRELATION_ID, correlationId);
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
		AccountGETResponse accountGetResponse = customerAccountListAdapter.retrieveCustomerAccountList(userId,
				paramsMap);

		return accountGetResponse;
	}

	@Override
	public void createConsent(Account customerAccount, String userId, String cid, String intentId, String channelId,
			String headers, String tppApplicaitonName, Object tppInformationObj) throws NamingException {
		AccountDetails acctDetail = new AccountDetails();
		PispConsent consent = new PispConsent();
		String legalEntityName = null;
		if (SchemeNameEnum.IBAN.equals(customerAccount.getAccount().getSchemeName())) {
			if (customerAccount.getServicer() != null) {
				acctDetail.setAccountNSC(customerAccount.getServicer().getIdentification());
			} else {
				// Fraudnet Call
				throw fraudSystemHelper.throwFraudSystemException(ErrorCodeEnum.VALIDATION_ERROR_OUTPUT,
						captureFraudParam(headers));
			}
			acctDetail.setAccountNumber(customerAccount.getAccount().getIdentification());
		} else if (SchemeNameEnum.SORTCODEACCOUNTNUMBER.equals(customerAccount.getAccount().getSchemeName())) {
			acctDetail.setAccountNSC(customerAccount.getAccount().getIdentification().substring(0, 6));
			acctDetail.setAccountNumber(customerAccount.getAccount().getIdentification().substring(6));
		}
		if(tppInformationObj != null && tppInformationObj instanceof BasicAttributes){
			BasicAttributes tppInfoAttributes = (BasicAttributes)tppInformationObj;
			legalEntityName = returnLegalEntityName(tppInfoAttributes);			
		}
		consent.setTppApplicationName(tppApplicaitonName);
		consent.setTppLegalEntityName(legalEntityName);;
		consent.setChannelId(channelId);
		consent.setAccountDetails(acctDetail);
		consent.setPsuId(userId);
		consent.setTppCId(cid);
		consent.setPaymentId(intentId);

		ZonedDateTime startDate = ZonedDateTime.now();
		consent.setStartDate(DateUtilites.getCurrentDateInISOFormat(startDate));
		ZonedDateTime endDate = startDate.plusSeconds(consentExpiryTime * TIMEINSECONDS);
		consent.setEndDate(DateUtilites.getCurrentDateInISOFormat(endDate));

		PaymentSetupPOSTResponse paymentSetUpResponseData = paymentSetupAdapterHelper
				.retrieveStagedPaymentSetup(consent.getPaymentId());

		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.putAll(captureFraudParam(headers));
		paramsMap.put(PSD2Constants.CHANNEL_NAME, channelId);
		paramsMap.put(FraudSystemConstants.PAYMENT_ID, intentId);

		Object fraudResponse = fraudSystemHelper.captureFraudEvent(userId, customerAccount, paymentSetUpResponseData,
				paramsMap);
		((PSD2Account) customerAccount).setFraudResponse(fraudResponse);

		pispConsentAdapter.createConsent(consent);

		// update fraud response
		paymentSetupAdapterHelper.updatePaymentSetupDebtorDetails(customerAccount, intentId);
	}

	private Map<String, String> captureFraudParam(String headers) {
		Map<String, String> param = new HashMap<>();
		param.put(PSD2Constants.FLOWTYPE, IntentTypeEnum.PISP_INTENT_TYPE.getIntentType());
		param.put(FraudSystemRequestMapping.FS_HEADERS, headers);
		return param;
	}

	@Override
	public void cancelPaymentSetup(String intentId, Map<String, String> paramsMap) {
		PispConsent consent = pispConsentAdapter.retrieveConsentByPaymentId(intentId,
				ConsentStatusEnum.AWAITINGAUTHORISATION);
		if (consent != null) {
			pispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
		}
		paymentSetupAdapterHelper.updatePaymentSetupStatus(intentId, StatusEnum.REJECTED, paramsMap);
	}

	@Override
	public PaymentSetupValidationResponse validatePreAuthorisation(Account account, String paymentId,
			Map<String, String> paramsMap) {
		return paymentSetupAdapterHelper.preAuthorizationValidation(account, paymentId, paramsMap);
	}
	
	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException{
		String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
		return legalEntityName;
	}
	
	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}
}
