package com.capgemini.psd2.identitymanagement.test.models;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.identitymanagement.models.TppAuthorities;

public class TppAuthoritiesTest {

	private TppAuthorities tppAuthorities;
	@Before
	public void setUp() throws Exception {
		tppAuthorities = new TppAuthorities("TppAuthorities");
	}
	
	@Test
	public void test(){
		assertEquals("TppAuthorities",tppAuthorities.getAuthority());
	}
}
