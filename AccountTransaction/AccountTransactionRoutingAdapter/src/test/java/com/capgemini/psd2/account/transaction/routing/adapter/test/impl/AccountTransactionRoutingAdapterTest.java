/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.transaction.routing.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.transaction.routing.adapter.impl.AccountTransactionRoutingAdapter;
import com.capgemini.psd2.account.transaction.routing.adapter.routing.AccountTransactionAdapterFactory;
import com.capgemini.psd2.account.transaction.routing.adapter.test.mock.data.AccountTransactionTestData;
import com.capgemini.psd2.account.transcation.routing.adapter.test.adapter.AccountTransactionTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;

/**
 * The Class AccountTransactionRoutingAdapterTest.
 */
public class AccountTransactionRoutingAdapterTest {

	/** The account transaction adapter factory. */
	@Mock
	private AccountTransactionAdapterFactory accountTransactionAdapterFactory;

	/** The account transaction routing adapter. */
	@InjectMocks
	private AccountTransactionAdapter accountTransactionRoutingAdapter = new AccountTransactionRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account transaction test.
	 */
	@Test
	public void retrieveAccountTransactiontest() {

		AccountTransactionAdapter accountTransactionAdapter = new AccountTransactionTestRoutingAdapter();
		Mockito.when(accountTransactionAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountTransactionAdapter);
		
		AccountTransactionsGETResponse accountTransactionsGETResponse = accountTransactionRoutingAdapter.retrieveAccountTransaction(AccountTransactionTestData.getMockAccountMapping(), null);
		
		assertEquals("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", accountTransactionsGETResponse.getData().getTransaction().get(0).getAccountId());

	}

}
