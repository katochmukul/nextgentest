package com.capgemini.psd2.account.transaction.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.aisp.domain.Data3;
import com.capgemini.psd2.aisp.domain.Data3Amount;
import com.capgemini.psd2.aisp.domain.Data3Balance;
import com.capgemini.psd2.aisp.domain.Data3BalanceAmount;
import com.capgemini.psd2.aisp.domain.Data3BankTransactionCode;
import com.capgemini.psd2.aisp.domain.Data3MerchantDetails;
import com.capgemini.psd2.aisp.domain.Data3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountTransactionTestData {

	static Data3Transaction data = new Data3Transaction();
	static Data3Amount amount = new Data3Amount();
	static Data3Balance balance = new Data3Balance();
	static Data3BalanceAmount amount2 = new Data3BalanceAmount();
	static Data3BankTransactionCode bankTransactionCode = new Data3BankTransactionCode();
	static Data3ProprietaryBankTransactionCode proprietaryBankTransactionCode = new Data3ProprietaryBankTransactionCode();
	static Data3MerchantDetails merchantDetails = new Data3MerchantDetails();
	
	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}

	public static AccountTransactionsGETResponse getAccountTransactionsGETResponse() {
		AccountTransactionsGETResponse resp = new AccountTransactionsGETResponse();
		List<Data3Transaction> dataList = new ArrayList<Data3Transaction>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(
				com.capgemini.psd2.aisp.domain.Data3Balance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(com.capgemini.psd2.aisp.domain.Data3Balance.TypeEnum.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		Data3 data3 = new Data3();
		data3.setTransaction(dataList);
		resp.setData(data3);
		return resp;
	}

	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

}
