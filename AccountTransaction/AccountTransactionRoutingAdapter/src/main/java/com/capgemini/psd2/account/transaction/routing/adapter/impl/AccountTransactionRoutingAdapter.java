package com.capgemini.psd2.account.transaction.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.transaction.routing.adapter.routing.AccountTransactionAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountTransactionRoutingAdapter implements AccountTransactionAdapter {

	@Autowired
	private AccountTransactionAdapterFactory adapters;

	@Value("${app.defaultAdapterAccountTransaction}")
	private String defaultAdapter;

	@Override
	public AccountTransactionsGETResponse retrieveAccountTransaction(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountTransactionAdapter accountTransactionAdapter = adapters.getAdapterInstance(defaultAdapter);
		return accountTransactionAdapter.retrieveAccountTransaction(accountMapping, params);
	}

}