package com.capgemini.psd2.account.transaction.mongo.db.adapter.constants;

public class AccountTransactionMongoDbAdapterConstants {

	public static final String REQUESTED_FROM_DATETIME = "RequestedFromDateTime";
	
	public static final String REQUESTED_TO_DATETIME = "RequestedToDateTime";
	
	public static final String REQUESTED_TO_CONSENT_DATETIME = "RequestedToConsentDateTime";
	
	public static final String REQUESTED_FROM_CONSENT_DATETIME = "RequestedFromConsentDateTime";
	
	public static final String CONSENT_EXPIRATION_DATETIME = "ConsentExpirationDateTime";
	
	public static final String REQUESTED_TXN_FILTER = "RequestedTxnFilter";
	
	public static final String REQUESTED_TXN_KEY = "RequestedTxnKey";
	
}
