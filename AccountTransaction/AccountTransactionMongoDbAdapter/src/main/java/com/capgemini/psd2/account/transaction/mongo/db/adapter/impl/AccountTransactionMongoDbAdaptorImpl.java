package com.capgemini.psd2.account.transaction.mongo.db.adapter.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.tansaction.mongo.db.adapter.utility.AccountTransactionMongoDbAdaptorUtility;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.constants.AccountTransactionMongoDbAdapterConstants;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.domain.TransactionDateRange;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.repository.AccountTransactionRepository;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.aisp.domain.Data3;
import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.mock.domain.MockAccountTransactionsGETResponseData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class AccountTransactionMongoDbAdaptorImpl implements AccountTransactionAdapter {

	private static final Integer FIRST = Integer.valueOf(1);

	@Autowired
	private AccountTransactionRepository accountTransRepo;
	
	/** The psd2 validator. */
	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator psd2Validator;
	
	@Autowired
	private AccountTransactionMongoDbAdaptorUtility accountTransactionMongoDbAdaptorUtility;

	@Override
	public AccountTransactionsGETResponse retrieveAccountTransaction(AccountMapping accountMapping,
			Map<String, String> params) {
		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNsc = accountMapping.getAccountDetails().get(0).getAccountNSC();
		
		
		/*TransactionDateRange Check*/
		
		LocalDateTime fromLocalDateTime = null;
		LocalDateTime toLocalDateTime = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		TransactionDateRange transactionDateRange = accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
		TransactionDateRange decision = accountTransactionMongoDbAdaptorUtility.fsCallFilter(transactionDateRange);
		if (decision.isEmptyResponse()) {
			return new AccountTransactionsGETResponse();
		} else {
			
			fromLocalDateTime = decision.getNewFilterFromDate();
			toLocalDateTime = decision.getNewFilterToDate();
		}

		LocalDateTime mongoFromDt = fromLocalDateTime.minusNanos(1);
		LocalDateTime mongoToDt = toLocalDateTime.plusNanos(1000000);
		
		String filter = params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER);
		Integer pageNo = Integer.parseInt(params.get("RequestedPageNumber"));

		final PageRequest pageReq = new PageRequest(pageNo - 1, 2, Direction.ASC, "bookingDateTime");

		Page<MockAccountTransactionsGETResponseData> pageRecords;
		if ("ALL".equals(filter)) {
			try {
				pageRecords = accountTransRepo.findByAccountNumberAndAccountNSCAndBookingDateTimeCopyBetween(
						accountNumber, accountNsc, mongoFromDt, mongoToDt, pageReq);

			} catch (DataAccessResourceFailureException e) {
				throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
			}
		} else {
			try {
				pageRecords = accountTransRepo
						.findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(
								accountNumber, accountNsc, filter, mongoFromDt, mongoToDt, pageReq);
			} catch (DataAccessResourceFailureException e) {
				throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
			}
		}

		String txnKey = null;
		if (!pageRecords.getContent().isEmpty()) {
			MockAccountTransactionsGETResponseData mockResponseData1 = pageRecords.getContent().get(0);
			mockResponseData1.setAccountId(accountId);
			MockAccountTransactionsGETResponseData mockResponseData = validateFields(mockResponseData1);
			
			txnKey = mockResponseData.getTxnRetrievalKey();
		}
		
		AccountTransactionsGETResponse transactionResponsePreValidation = populateIdAndLinks(pageRecords, accountId, txnKey);
		return transactionResponsePreValidation;
	}

	private AccountTransactionsGETResponse populateIdAndLinks(Page<MockAccountTransactionsGETResponseData> pageRecords,
			String accountId, String txnKey) {
		AccountTransactionsGETResponse transactionResponse = new AccountTransactionsGETResponse();

		if (!pageRecords.getContent().isEmpty())
			pageRecords.getContent().get(0).setAccountId(accountId);
		List<MockAccountTransactionsGETResponseData> mockResponseData = pageRecords.getContent();
		
		List<Data3Transaction> responseDataTransaction = new ArrayList<>();
		responseDataTransaction.addAll( mockResponseData);
		Data3 responseData = new Data3();
		responseData.setTransaction(responseDataTransaction);
		transactionResponse.setData(responseData);

		transactionResponse.setLinks(new Links());
		transactionResponse.getLinks().setFirst(String.valueOf(FIRST));
		if (txnKey != null)
			transactionResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1) + ":" + txnKey);
		else
			transactionResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1));
		transactionResponse.getLinks().setLast(String.valueOf(pageRecords.getTotalPages()));
		if (pageRecords.hasNext())
			transactionResponse.getLinks().setNext(String.valueOf(pageRecords.getNumber() + 2));
		if (pageRecords.hasPrevious())
			transactionResponse.getLinks().setPrev(String.valueOf(pageRecords.getNumber()));
		return transactionResponse;
	}
	
	private MockAccountTransactionsGETResponseData validateFields(MockAccountTransactionsGETResponseData accountTransactionsGETResponse) {
		psd2Validator.validate(accountTransactionsGETResponse.getBankTransactionCode());
		psd2Validator.validateEnum(CurrencyEnum.class,accountTransactionsGETResponse.getAmount().getCurrency());
		psd2Validator.validate(accountTransactionsGETResponse.getAmount());
		psd2Validator.validateEnum(CurrencyEnum.class,accountTransactionsGETResponse.getBalance().getAmount().getCurrency());
		psd2Validator.validate(accountTransactionsGETResponse.getBalance().getAmount());
		psd2Validator.validate(accountTransactionsGETResponse);
		return accountTransactionsGETResponse;
	}
	
}
enum CurrencyEnum {
	GBP, EUR;
}
