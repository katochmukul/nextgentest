package com.capgemini.psd2.test.utilities.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.transaction.utilities.AccountTransactionValidationUtilities;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(Parameterized.class)
public class AccountTransactionValidationUtilitiesTest {

	@InjectMocks
	static AccountTransactionValidationUtilities utility = new AccountTransactionValidationUtilities();

	private String inputID, expectedOutput;

	Integer inputPageNo,inputPageSize = 5;

	public AccountTransactionValidationUtilitiesTest(String inputID, Integer inputPageNo, String expectedOutput) {
		super();
		this.inputID = inputID;
		this.inputPageNo = inputPageNo;
		this.expectedOutput = expectedOutput;
	}

	@Before
	public void setUp() throws Exception {
	}

	@Parameters
	public static List<Object[]> conditionsToTest() {
		String validID = "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
		Integer validPageNo = 2;
		String above40ID = "269c3ff5-d7f8-419b-a3b9-7136c5b4611e-13344bff-1242565XXXXXXXX";
		Integer invalidPageNo = -2;

		Object expectedScenarios[][] = {
				// Test Scenario 1) When ID is null
				{ null, validPageNo, "No Account id found in the request" },

				// Test Scenario 2) When ID is blank
				{ "", validPageNo, "No Account id found in the request" },

				// Test Scenario 3) When ID is greater than 40 characters.
				{ above40ID, validPageNo, "Validation error found with the provided input" },

				// Test Scenario 4) When pageNo is invalid
				{ validID, invalidPageNo, "Validation error found with the provided input" },

				// Test Scenario 5) When pageNo is null - success case
				{ validID, null, "Passed" },

				// Test Scenario 6) When pageNo is valid - success case
				{ validID, validPageNo, "Passed" } };

		return (List<Object[]>) Arrays.asList(expectedScenarios);

	}
	
	@Test
	public void retrieveTransactionTestWithDiffParams() {
		try {
			ReflectionTestUtils.setField(utility, "minPageSize", "5");
			ReflectionTestUtils.setField(utility, "maxPageSize", "25");
			utility.validateParams(inputID, inputPageNo,inputPageSize);
			
		} catch (PSD2Exception e) {
			assertEquals(expectedOutput, e.getErrorInfo().getErrorMessage());
		}
	}

}
