package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.client.AccountStandingOrderFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderFoundationServiceClientTest {
	@InjectMocks
	private AccountStandingOrderFoundationServiceClient accountStandingOrderFoundationServiceClient;
	@Mock
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Mock
	private DataMask dataMask;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRestTransportForAccountStandingOrdersFS(){
		RequestInfo requestInfo = new RequestInfo();
		StandingOrders standingOrders = new StandingOrders();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		requestInfo.setUrl("http://localhost:9087/fs-abt-service/services/user/12345678/beneficiaries");
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(standingOrders);
		Mockito.when(dataMask.maskResponse(anyObject(), anyObject())).thenReturn(standingOrders);

		//		maskResponse is true
		StandingOrders response = accountStandingOrderFoundationServiceClient.restTransportForAccountStandingOrder(requestInfo, StandingOrders.class, httpHeaders);
		assertThat(response).isEqualTo(standingOrders);
		//	maskResponse is false
			ReflectionTestUtils.setField(accountStandingOrderFoundationServiceClient, "maskStandingOrdersResponse", true);
			response = accountStandingOrderFoundationServiceClient.restTransportForAccountStandingOrder(requestInfo, StandingOrders.class, httpHeaders);
			assertThat(response).isEqualTo(standingOrders);
		
		
}
}