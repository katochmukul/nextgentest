/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

/**
 * The Class AccountStandingOrderFoundationServiceClient.
 */
@Component
public class AccountStandingOrderFoundationServiceClient {
	
	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Autowired
	private DataMask dataMask;
	
	@Value("${foundationService.maskStandingOrdersResponse:#{false}}")
	private boolean maskStandingOrdersResponse;
	
	/**
	 * Rest transport for single account balance.
	 *
	 * @param reqInfo the req info
	 * @param responseType the response type
	 * @param headers the headers
	 * @return the accounts
	 */
	public StandingOrders restTransportForAccountStandingOrder(RequestInfo reqInfo, Class <StandingOrders> responseType, HttpHeaders headers) {
		StandingOrders standingOrders=restClient.callForGet(reqInfo, responseType, headers);
		if(maskStandingOrdersResponse){
			standingOrders=dataMask.maskResponse(standingOrders,"restTransportForAccountStandingOrder");
		}
		return standingOrders;
		
	}
	

}
