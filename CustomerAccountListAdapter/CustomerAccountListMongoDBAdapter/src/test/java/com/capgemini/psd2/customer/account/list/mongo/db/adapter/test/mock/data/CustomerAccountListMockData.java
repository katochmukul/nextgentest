package com.capgemini.psd2.customer.account.list.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.PSD2Account;

public class CustomerAccountListMockData {

	public static List<Account> customerAccountInfoList;

	public static Data2  getCustomerAccountInfoList(){
		List<Account> customerAccountInfoList = new ArrayList<>();
		Data2 data2 = new Data2();
		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("John Doe");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setAccountType("checking");
		Data2Account account = new Data2Account();
		account.setIdentification("23344455");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("23344455");
		accnt.setServicer(servicer );
		accnt.setAccount(account );

		PSD2Account acct = new PSD2Account();
		acct.setAccountId("John Does");
		acct.setCurrency("EUR");
		acct.setNickname("Johns");
		acct.setAccountType("Checking");
		acct.setServicer(servicer );
		accnt.setAccount(account );
		customerAccountInfoList.add(acct);
		customerAccountInfoList.add(accnt);
		data2.setAccount(customerAccountInfoList);
		return data2;
	}
}
