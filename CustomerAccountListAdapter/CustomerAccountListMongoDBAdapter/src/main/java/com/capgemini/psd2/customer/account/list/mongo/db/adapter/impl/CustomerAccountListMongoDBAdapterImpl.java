package com.capgemini.psd2.customer.account.list.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository.CustomerAccountListMongoDBAdapterRepository;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public class CustomerAccountListMongoDBAdapterImpl implements CustomerAccountListAdapter {

	@Autowired
	private CustomerAccountListMongoDBAdapterRepository customerAccountListMongoDBAdapterRepository;

	@Override
	public AccountGETResponse retrieveCustomerAccountList(String userId, Map<String, String> params) {
		AccountGETResponse accountGETResponse = new AccountGETResponse();
		try{
			List<Account> accounts = customerAccountListMongoDBAdapterRepository.findByPsuId(userId);
			Data2 data2= new Data2();
			data2.setAccount(accounts);
			accountGETResponse.setData(data2);
		}catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
		return  accountGETResponse;
	}

	
	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		// TODO Auto-generated method stub
		return null;
	}
}
