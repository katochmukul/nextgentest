package com.capgemini.psd2.customer.account.list.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.impl.CustomerAccountListMongoDBAdapterImpl;

@Configuration
public class CustomerAccountListMongoDBAdapterConfig {

	@Bean
	public CustomerAccountListAdapter customerAccountsMongoDBAdapter(){
		return new CustomerAccountListMongoDBAdapterImpl();
	}
}
