package com.capgemini.psd2.customer.account.list.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.customer.account.list.routing.adapter.test.mock.data.CustomerAccountListRoutingAdapterMockData;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public class CustomerAccountListRoutingTestAdapter implements CustomerAccountListAdapter{

	@Override
	public AccountGETResponse retrieveCustomerAccountList(String userId, Map<String, String> params) {
		return CustomerAccountListRoutingAdapterMockData.getCustomerAccountInfo();
	}

	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		return null;
	}

}