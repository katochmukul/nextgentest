package com.capgemini.psd2.fraudsystem.test.helper;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.service.FraudSystemService;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;





public class FraudSystemHelperTest {
	
	
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private FraudSystemService fraudSystemService;
	
	
//	public enum ErrorCodeEnum {
//
//		/** The technical error. */
//		TECHNICAL_ERROR("110","Technical Error.","Technical Error.","500"),
//		
//		/** The header missing. */
//		HEADER_MISSING("111","Mandatory header missing.","Mandatory header missing.","400"),
//		
//		/** The connection error. */
//		CONNECTION_ERROR("112","Error while establishing connection,please try again..",null,"500");
//		
//		private String errorCode;
//		
//		/** The error message. */
//		private String errorMessage;
//		
//		/** The detail error message. */
//		private String detailErrorMessage;
//		
//		/** The status code. */
//		private String statusCode;
//
//		/**
//		 * Instantiates a new error code enum.
//		 *
//		 * @param errorCode the error code
//		 * @param errorMesssage the error messsage
//		 * @param detailErrorMessage the detail error message
//		 * @param statusCode the status code
//		 */
//		ErrorCodeEnum(String errorCode,String errorMesssage,String detailErrorMessage,String statusCode){
//			this.errorCode=errorCode;
//			this.errorMessage=errorMesssage;
//			this.detailErrorMessage = detailErrorMessage;
//			this.statusCode = statusCode;
//		}
//		
//}

	@InjectMocks
	FraudSystemHelper fraudSystemHelper=new FraudSystemHelper(); 
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);	
	}
	
	@Test(expected=Exception.class)
	public void testCaptureFraudEvent() {
		//when(fraudSystemHelper.captureFraudEvent(anyString(), anyObject(), anyObject(), anyMap()).thenReturn
		String dummyUserId="12345678";
		
		Account customerAccount=new Account();
		PaymentSetupPOSTResponse paymentSetupPOSTResponse=new PaymentSetupPOSTResponse();
		PaymentSetupResponse data=new PaymentSetupResponse();
		PaymentSetupResponseInitiation initiation=new PaymentSetupResponseInitiation(); 
		PaymentSetupInitiationInstructedAmount instructedAmount=new PaymentSetupInitiationInstructedAmount();
		RemittanceInformation remittanceInformation=new RemittanceInformation();
		remittanceInformation.setReference("dummyref");
		instructedAmount.setAmount("99999");
		instructedAmount.setCurrency("EUR");
		
		initiation.setInstructedAmount(instructedAmount);
		initiation.setRemittanceInformation(remittanceInformation);
		data.setInitiation(initiation);
		data.setCreationDateTime("2017-10-05");
		paymentSetupPOSTResponse.setData(data);
		
		Map<String, String> paramsMap=new HashMap<>();
		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccount, paymentSetupPOSTResponse, paramsMap);
	}
	
	@Test
	public void testCaptureFraudEvent2() {
        String dummyUserId="12345678";
        PSD2Account customerAccount=new PSD2Account();
		Data2Account account=new Data2Account();
		account.setIdentification("99999");
		customerAccount.setAccount(account);
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put("flowType", "AISP");
		paramsMap.put("channelId", "B365");
		paramsMap.put("paymentId", "123456");
		paramsMap.put(FraudSystemRequestMapping.FS_HEADERS,"dummy");
		Map<String, Object> fraudSystemParams=new HashMap<>();
		when(fraudSystemService.callFraudSystemService(anyMap())).thenReturn(new Object());
		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccount, paramsMap, fraudSystemParams);
	}
	
	@Test
	public void testCaptureFraudEvent3() {
		String dummyUserId="12345678";
		List<PSD2Account> customerAccountList=new ArrayList<>();
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put("flowType", "AISP");
		paramsMap.put("channelId","B365");
		paramsMap.put(FraudSystemRequestMapping.FS_HEADERS,"dummy");
		
		//doNothing().when(fraudSystemService).callFraudSystemService(anyMap());
		
		PSD2Account accObj=new PSD2Account();
		Data2Account account=new Data2Account();
		account.setIdentification("99999");
		accObj.setAccount(account);
		
		customerAccountList.add(accObj);
		
		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccountList, paramsMap);
	}
	
	@Test
	public void testCaptureFraudEvent4() {
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put("flowType","AISP");
		paramsMap.put("userName","12345678");
		when(fraudSystemService.callFraudSystemService(anyMap())).thenReturn(new Object());
		fraudSystemHelper.captureFraudEvent(paramsMap);
	}

}
