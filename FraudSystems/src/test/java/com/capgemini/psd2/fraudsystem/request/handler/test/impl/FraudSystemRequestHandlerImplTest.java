package com.capgemini.psd2.fraudsystem.request.handler.test.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestAttributes;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(value = BlockJUnit4ClassRunner.class)
public class FraudSystemRequestHandlerImplTest {

	@Mock
	private CustomerAccountListAdapter custAccountAdapter;

	@Mock
	private ServletRequestAttributes requestAttributes;

	@Mock
	MockHttpServletRequest request;

	@Mock
	private FraudSystemRequestAttributes fraudSystemRequestAttributes;

	@Mock
	private FraudSystemRequestHandlerImpl fraudSystemRequestHandlerImplException;

	@InjectMocks
	PSD2CustomerInfo custInfoTest = new PSD2CustomerInfo();

	@InjectMocks
	Data2 data2Test = new Data2();

	@InjectMocks
	PSD2Account accountClassTest = new PSD2Account();

	@InjectMocks
	Data2Account data2AccountTest = new Data2Account();

	@InjectMocks
	private FraudSystemRequestHandlerImpl fraudSystemRequestHandlerImpl = new FraudSystemRequestHandlerImpl();

	private Map<String, Object> paramsTest = new HashMap<>();

	private Map<String, String> fraudSystemTest = new HashMap<>();

	private Map<String, Map<String, String>> fraudSystemMapTest = new HashMap<>();

	private Map<String, Map<String, Object>> fraudSystemRequestTest = new HashMap<>();

	private Map<String, String> additionalInformationTest = new HashMap<>();

	private List<String> accNumsTest = new ArrayList<>();

	private List<Account> accountTest = new ArrayList<>();

	Map<String, Map<String, Object>> fraudSystemRequestMaps = new HashMap<>();
	Map<String, String> serviceParams = new HashMap<>();
	Map<String, Object> map = new HashMap<>();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		paramsTest.put(FraudSystemConstants.FLOWTYPE_KEY, FraudSystemConstants.FLOWTYPE_KEY);
		paramsTest.put(FraudSystemConstants.PAGETYPE_KEY, FraudSystemConstants.PAGETYPE_KEY);
		paramsTest.put("authenticationStatus", FraudSystemConstants.LOGIN_SUCCESS);
		paramsTest.put(FraudSystemConstants.PAYMENT_ID, FraudSystemConstants.PAYMENT_ID);
		paramsTest.put(FraudSystemRequestMapping.TRANSFER_CURRENCY, FraudSystemRequestMapping.TRANSFER_CURRENCY);
		paramsTest.put(FraudSystemRequestMapping.TRANSFER_AMNT, FraudSystemRequestMapping.TRANSFER_AMNT);
		accNumsTest.add("identification");
		paramsTest.put("identificationList", accNumsTest);

		fraudSystemTest.put("blockingPolicy", "FALSE");
		fraudSystemTest.put("orgCode", "BOICMA");
		fraudSystemTest.put("source", "WEB");
		fraudSystemTest.put("sourceSystem", "API Platform");
		fraudSystemTest.put("channelCode", "Online");
		fraudSystemTest.put("multiFactorAuthenticated", "TRUE");
		fraudSystemTest.put("finAccountsType", "BANK");
		fraudSystemTest.put("transferType", "P2P");

		Map<String, String> modelMap = new HashMap<String, String>();
		Map<String, String> pageMap = new HashMap<String, String>();

		modelMap.put("aispLogin", "AISPL");
		modelMap.put("pispLogin", "PISPL");
		modelMap.put("aispConsent", "AISPC");
		modelMap.put("pispConsent", "PISPC");

		pageMap.put("aispLogin", "AISP-LOGIN");
		pageMap.put("pispLogin", "PISP-LOGIN");
		pageMap.put("aispConsent", "AISP-CONSENT");
		pageMap.put("pispConsent", "PISP-CONSENT");

		fraudSystemMapTest.put("modelCode", modelMap);
		fraudSystemMapTest.put("pageCode", pageMap);

		fraudSystemRequestTest.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, paramsTest);
		fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestTest);

		ReflectionTestUtils.setField(fraudSystemRequestHandlerImpl, "fraudSystem", fraudSystemTest);
		ReflectionTestUtils.setField(fraudSystemRequestHandlerImpl, "fraudSystemMap", fraudSystemMapTest);

		map.put(FraudSystemConstants.FLOWTYPE_KEY, "dummyflowtype");
		map.put(FraudSystemConstants.PSUID_KEY, "dummy psuid");
		map.put(FraudSystemConstants.CHANNEL_ID, "dummychannelid");
		map.put(FraudSystemConstants.CORRELATION_ID, "dummy correlation id");
	}

	@Test
	public void captureDeviceEventInfoTest() {
		Cookie cookie = new Cookie("test", "test");
		Cookie[] cookieArray = { cookie };
		Mockito.when(request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER)).thenReturn(null);
		Mockito.when(request.getCookies()).thenReturn(cookieArray);
		fraudSystemRequestHandlerImpl.captureDeviceEventInfo(paramsTest);
	}

	@Test(expected = Exception.class)
	public void captureDeviceEventInfoExceptionTest() {
		Mockito.doThrow(new Exception()).when(fraudSystemRequestHandlerImplException)
				.captureDeviceEventInfo(paramsTest);
	}

	@Test
	public void captureFinanacialAccountsInfoTest() {
		PSD2CustomerInfo custInfoTestObj = new PSD2CustomerInfo();
		Map<String, Object> paramsTestMap = new HashMap<>();
		paramsTestMap.put("flowType", "AISP");
		List<String> hashedAccountList=new ArrayList<>();
		hashedAccountList.add("fd5f56b40a79a385708428e7b32ab996a681080a166a2206e750eb4819186145");
		
		
		paramsTestMap.put("hashedIdentificationList", hashedAccountList);
		
		
		data2AccountTest.setIdentification("99999");
		accountClassTest.setAccount(data2AccountTest);
		additionalInformationTest.put(PSD2Constants.ACCOUNT_NUMBER, "10254520");
		accountClassTest.setAdditionalInformation(additionalInformationTest);
		accountTest.add(accountClassTest);
		data2Test.setAccount(accountTest);
		custInfoTestObj.setData(data2Test);
		Mockito.when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestTest);
		fraudSystemRequestHandlerImpl.captureFinanacialAccountsInfo(custInfoTestObj, paramsTestMap);
	}
	
	@Test
	public void captureFinanacialAccountsInfoPISPTest() {
		PSD2CustomerInfo custInfoTestObj = new PSD2CustomerInfo();
		Map<String, Object> paramsTestMap = new HashMap<>();
		paramsTestMap.put("flowType", "PISP");
		List<String> hashedAccountList=new ArrayList<>();
		hashedAccountList.add("fd5f56b40a79a385708428e7b32ab996a681080a166a2206e750eb4819186145");
		
		List<String> identificationAccountList=new ArrayList<>();
		identificationAccountList.add("99999");
		
		paramsTestMap.put("hashedIdentificationList", hashedAccountList);
		paramsTestMap.put("identificationList", identificationAccountList);
		
		data2AccountTest.setIdentification("99999");
		accountClassTest.setAccount(data2AccountTest);
		additionalInformationTest.put(PSD2Constants.ACCOUNT_NUMBER, "10254520");
		accountClassTest.setAdditionalInformation(additionalInformationTest);
		accountTest.add(accountClassTest);
		data2Test.setAccount(accountTest);
		custInfoTestObj.setData(data2Test);
		Mockito.when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestTest);
		fraudSystemRequestHandlerImpl.captureFinanacialAccountsInfo(custInfoTestObj, paramsTestMap);
	}

	@Test(expected = Exception.class)
	public void captureFinanacialAccountsInfoExceptionTest() {
		Mockito.doThrow(new Exception()).when(fraudSystemRequestHandlerImplException)
				.captureFinanacialAccountsInfo(custInfoTest, paramsTest);
	}

	@Test
	public void captureTransferInfoTest() {
		Mockito.when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestTest);
		fraudSystemRequestHandlerImpl.captureTransferInfo(paramsTest);
	}

	@Test(expected = Exception.class)
	public void captureTransferInfoExceptionTest() {
		Mockito.doThrow(new Exception()).when(fraudSystemRequestHandlerImplException).captureTransferInfo(paramsTest);
	}

	@Test
	public void testCaptureContactInfo() {
		PSD2CustomerInfo customerObject = new PSD2CustomerInfo();
		when(custAccountAdapter.retrieveCustomerInfo(anyString(), anyMap())).thenReturn(customerObject);
		when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestMaps);
		assertEquals(fraudSystemRequestHandlerImpl.captureContactInfo(map), customerObject);
	}

	@Test
	public void testBranchesOfCaptureContactInfo() {
		PSD2CustomerInfo customerObject = new PSD2CustomerInfo();
		customerObject.setId("dummyuid");
		when(custAccountAdapter.retrieveCustomerInfo(anyString(), anyMap())).thenReturn(customerObject);
		when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestMaps);
		assertEquals(fraudSystemRequestHandlerImpl.captureContactInfo(map), customerObject);
	}

	@Test
	public void testForExceptionInCaptureContactInfo() {
		PSD2CustomerInfo customerObject = null;
		when(custAccountAdapter.retrieveCustomerInfo(anyString(), anyMap())).thenReturn(customerObject);
		when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestMaps);
		assertNull(fraudSystemRequestHandlerImpl.captureContactInfo(map));
	}

	@Test
	public void getFraudSystemMapTest() {
		Map<String, Map<String, String>> map = fraudSystemRequestHandlerImpl.getFraudSystemMap();
	}

	@Test
	public void setFraudSystemMapTest() {
		fraudSystemRequestHandlerImpl.setFraudSystemMap(new HashMap<String, Map<String, String>>());
	}

	@Test
	public void getFraudSystemTest() {
		Map<String, String> map = fraudSystemRequestHandlerImpl.getFraudSystem();
	}

	@Test
	public void setFraudSystemTest() {
		fraudSystemRequestHandlerImpl.setFraudSystem(new HashMap<String, String>());
	}
	
	@Test
	public void testCaptureContactInfoTest() {
		Map<String, Object> params=null;
		fraudSystemRequestHandlerImpl.captureTransferInfo(params);
	}
	
	@Test
	public void testCaptureDeviceEventInfo() {
		fraudSystemRequestHandlerImpl.captureDeviceEventInfo(null);
	}
	
	@Test
	public void testDeviceEventInfo() {
		Cookie cookie = new Cookie("test", "test");
		Cookie[] cookieArray = { cookie };
		Mockito.when(request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER)).thenReturn(null);
		Mockito.when(request.getCookies()).thenReturn(cookieArray);
		when(request.getHeader(anyString())).thenReturn("abc,def");
		fraudSystemRequestHandlerImpl.captureDeviceEventInfo(paramsTest);	
	}
	
	@Test
	public void testCaptureTransferInfo() {
		Map<String,Object> map=new HashMap<>();
		map.put(FraudSystemConstants.PAYMENT_ID,"dummypaymentId");
		map.put(FraudSystemRequestMapping.TRANSFER_CURRENCY, "EUR");
		map.put(FraudSystemRequestMapping.TRANSFER_AMNT, "10000");
		map.put(FraudSystemRequestMapping.TRANSFER_MEMO, "memo");
		
		
		
		  String date = "2017-12-03T10:15:30+01:00";
		
		map.put(FraudSystemRequestMapping.TRANSFER_TIME,date);
		Map<String, Map<String, Object>> fraudSystemRequestMaps=new HashMap<>();
		Map<String, Object> fraudSystemEventMap=new HashMap<>();
		when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudSystemRequestMaps);
		fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP,fraudSystemEventMap);
		fraudSystemRequestHandlerImpl.captureTransferInfo(map);
	}
	
	@Test
	public void testFinancialAccountsInfo() {
		fraudSystemRequestHandlerImpl.captureFinanacialAccountsInfo(null, null);
	}
	
	@Test
	public void testDeviceEventInfoRequestParam() {
		when(request.getParameter(anyString())).thenReturn("DUMMY");
		Cookie cookie = new Cookie("test", "test");
		Cookie[] cookieArray = { cookie };
		Mockito.when(request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER)).thenReturn(null);
		Mockito.when(request.getCookies()).thenReturn(cookieArray);
		when(request.getHeader(anyString())).thenReturn("abc,def");
		fraudSystemRequestHandlerImpl.captureDeviceEventInfo(paramsTest);	
	}
}
