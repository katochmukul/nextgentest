package com.capgemini.psd2.fraudsystem.request.test.handler;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Test;

import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;

public class FraudSystemRequestMappingTest {

	@Test
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<FraudSystemRequestMapping> constructor = FraudSystemRequestMapping.class.getDeclaredConstructor();
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	}
}
