package com.capgemini.psd2.fraudsystem.request.handler.impl;

public final class  FraudSystemRequestMapping {

	// Please do not modify any of the below values. Its being used in Boi Adapter.
	// Event Map - 1
	// basic event info
	public static final String FRAUDREQUEST_EVENT_MAP = "fraudRequstEventMap";
	public static final String EVENT_ORG_CODE = "orgCode";
	public static final String EVENT_MODEL_CODE = "modelCode";
	public static final String EVENT_ID = "eventId";
	public static final String EVENT_TIME = "eventTime";
	public static final String EVENT_SOURCE = "eventSource";
	public static final String EVENT_SOURCE_SYSTEM = "eventSourceSystem";
	public static final String EVENT_SOURCE_SUBSYSTEM = "eventSourceSubSystem";
	public static final String EVENT_TYPE = "eventType";
	public static final String EVENT_CHANNEL = "channelCode";
	// fin acc info
	public static final String FIN_ACCNTS = "finAccnts";
	// trans info
	public static final String TRANSFER_TYPE = "transType";
	public static final String TRANSFER_ID = "transId";
	public static final String TRANSFER_AMNT = "transAmount";
	public static final String TRANSFER_CURRENCY = "transCurrency";
	public static final String TRANSFER_TIME = "transTime";
	public static final String TRANSFER_FROM = "transFrom";
	public static final String TRANSFER_MEMO = "transMemo";

	// Device Map -2
	public static final String FRAUDREQUEST_DEVICE_MAP = "fraudRequestDeviceMap";
	public static final String DEVICE_IP = "deviceIp";
	public static final String DEVICE_HEADERS = "deviceHeaders";
	public static final String DEVICE_COOKIES = "deviceCookies";
	public static final String DEVICE_JSC = "jsc";
	public static final String DEVICE_PAYLOAD = "payload";
	public static final String SESSION_ID = "sessionId";
	public static final String SESSION_PAGE_CODE = "sessionPageCode";
	public static final String SESSION_LOGGEDIN = "sessionLoggedIn";
	public static final String SESSION_MULTIFACTORED = "sessionMultiFactored";

	// Customer Map -3
	public static final String FRAUDREQUEST_CUSTOMER_MAP = "fraudRequestCustomerMap";
	public static final String CONTACT_DETAILS = "custDetails";
	// account info
	public static final String ACCOUNT_ID = "accountId";
	public static final String ACCOUNT_HOLDER = "accountHolder";

	public static final String USER_IP_ADDRESS = "x-forwarded-for";
	public static final String X_API_CLIENT_IP = "x-api-client-IP";
	public static final String FS_HEADERS = "fsHeaders";

	private FraudSystemRequestMapping() {
	}

}