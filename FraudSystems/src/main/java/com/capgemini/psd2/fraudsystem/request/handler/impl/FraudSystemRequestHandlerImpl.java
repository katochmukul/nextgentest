package com.capgemini.psd2.fraudsystem.request.handler.impl;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.domain.PSD2FinancialAccount;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestAttributes;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestHandler;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.StringUtils;

@Component
@ConfigurationProperties(prefix = "app")
@Configuration
@EnableAutoConfiguration
public class FraudSystemRequestHandlerImpl implements FraudSystemRequestHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(FraudSystemRequestHandlerImpl.class);

	@Autowired
	private FraudSystemRequestAttributes fraudSystemRequestAttributes;

	@Autowired
	@Qualifier("CustomerAccountListFraudSystemAdapter")
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Autowired
	private HttpServletRequest request;

	/** The fraudSystemMap request values */
	private Map<String, Map<String, String>> fraudSystemMap = new HashMap<>();

	/** The fraudSystem request values */
	private Map<String, String> fraudSystem = new HashMap<>();

	@Override
	public void captureDeviceEventInfo(Map<String, Object> params) {
		try {
			Map<String, Map<String, Object>> fraudSystemRequestMaps = new HashMap<>();
			Map<String, Object> fraudSystemEventMap = new HashMap<>();

			// EventInfo
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_ORG_CODE, fraudSystem.get("orgCode"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_MODEL_CODE,
					fraudSystemMap.get("modelCode")
							.get(params.get(FraudSystemConstants.FLOWTYPE_KEY).toString().toLowerCase()
									+ params.get(FraudSystemConstants.PAGETYPE_KEY)));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_ID,
					request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER) != null
							? request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER)
							: params.get(FraudSystemConstants.CORRELATION_ID));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_TIME,
					request.getParameter(FraudSystemConstants.EVENT_TIME_PARAMETER) != null
							? request.getParameter(FraudSystemConstants.EVENT_TIME_PARAMETER)
							: request.getHeader(FraudSystemConstants.TRANS_TIME_PARAMETER));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_SOURCE, fraudSystem.get("source"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_SOURCE_SYSTEM, fraudSystem.get("sourceSystem"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_SOURCE_SUBSYSTEM,
					params.get(FraudSystemConstants.FLOWTYPE_KEY).toString().toUpperCase());
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_TYPE,
					params.get(FraudSystemConstants.PAGETYPE_KEY));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_CHANNEL, fraudSystem.get("channelCode"));
			Map<String, Object> fraudSystemDeviceMap = new HashMap<>();

			// DeviceInfo
			setClientIPAddress(fraudSystemDeviceMap);

			fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_JSC,
					(request.getParameter(FraudSystemConstants.DEVICE_JSC_PARAMETER) != null)
							? request.getParameter(FraudSystemConstants.DEVICE_JSC_PARAMETER)
							: request.getHeader(FraudSystemConstants.TRANS_JSC_PARAMETER));
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_PAYLOAD,
					(request.getParameter(FraudSystemConstants.DEVICE_PAYLOAD_PARAMETER) != null)
							? request.getParameter(FraudSystemConstants.DEVICE_PAYLOAD_PARAMETER)
							: request.getHeader(FraudSystemConstants.TRANS_HDIM_PARAMETER));

			// SessionInfo
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_ID,
					getCookieValue(request, FraudSystemConstants.SESSION_ID_PARAMETER));
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_PAGE_CODE,
					fraudSystemMap.get("pageCode")
							.get(params.get(FraudSystemConstants.FLOWTYPE_KEY).toString().toLowerCase()
									+ params.get(FraudSystemConstants.PAGETYPE_KEY)));
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_LOGGEDIN,
					params.get("authenticationStatus").equals(FraudSystemConstants.LOGIN_SUCCESS) ? Boolean.TRUE
							: Boolean.FALSE);
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_MULTIFACTORED,
					fraudSystem.get("multiFactorAuthenticated"));

			if (request.getParameter(FraudSystemRequestMapping.FS_HEADERS) != null) {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_HEADERS,
						request.getParameter(FraudSystemRequestMapping.FS_HEADERS));
			} else if (!NullCheckUtils.isNullOrEmpty((String) (params.get(FraudSystemRequestMapping.DEVICE_HEADERS)))) {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_HEADERS,
						(String) (params.get(FraudSystemRequestMapping.DEVICE_HEADERS)));
			}

			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, fraudSystemEventMap);
			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_DEVICE_MAP, fraudSystemDeviceMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureDeviceEventInfo(Map<String, Object> params)",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());

		}
	}

	private String getCookieValue(HttpServletRequest req, String cookieName) {
		return Arrays.stream(req.getCookies()).filter(c -> c.getName().equals(cookieName)).findFirst()
				.map(Cookie::getValue).orElse(null);
	}

	@Override
	public PSD2CustomerInfo captureContactInfo(Map<String, Object> params) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			Map<String, String> serviceParams = new HashMap<>();
			serviceParams.put(FraudSystemConstants.FLOW_TYPE, params.get(FraudSystemConstants.FLOWTYPE_KEY).toString());
			serviceParams.put(FraudSystemConstants.USERID, params.get(FraudSystemConstants.PSUID_KEY).toString());
			serviceParams.put(FraudSystemConstants.CORRELATION_ID,
					(request.getParameter("correlationId") != null) ? request.getParameter("correlationId")
							: params.get(FraudSystemConstants.CORRELATION_ID).toString());
			serviceParams.put(FraudSystemConstants.CHANNEL_ID,
					(request.getParameter("channelId") != null) ? request.getParameter("channelId")
							: params.get(FraudSystemConstants.CHANNEL_ID).toString());
			PSD2CustomerInfo custInfo = customerAccountListAdapter
					.retrieveCustomerInfo(params.get(FraudSystemConstants.PSUID_KEY).toString(), serviceParams);
			Map<String, Map<String, Object>> fraudSystemRequestMaps = fraudSystemRequestAttributes
					.getFraudSystemRequest();

			Map<String, Object> fraudSystemCustomerMap = new HashMap<>();
			fraudSystemCustomerMap.put(FraudSystemRequestMapping.CONTACT_DETAILS, custInfo);
			fraudSystemCustomerMap.put(FraudSystemRequestMapping.ACCOUNT_ID,
					params.get(FraudSystemConstants.PSUID_KEY));
			fraudSystemCustomerMap.put(FraudSystemRequestMapping.ACCOUNT_HOLDER,
					(custInfo.getId() != null) ? custInfo.getId() : FraudSystemConstants.CONTACT_ID);

			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_CUSTOMER_MAP, fraudSystemCustomerMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
			return custInfo;
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureContactInfo(Map<String, Object> params)",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void captureFinanacialAccountsInfo(PSD2CustomerInfo custInfo, Map<String, Object> params) {
		try {
			List<PSD2FinancialAccount> finAccntList = new ArrayList<>();
			List<String> hashedAccNums = new ArrayList<>();

			hashedAccNums.addAll((List<String>) params.get("hashedIdentificationList"));
			custInfo.getData().getAccount().forEach(account -> {
				if (hashedAccNums.contains(convertStringToHash(account.getAccount().getIdentification()))) {
					PSD2FinancialAccount finAccnt = new PSD2FinancialAccount();
					finAccnt.setId(GenerateUniqueIdUtilities.generateRandomUniqueID());
					finAccnt.setType(fraudSystem.get("finAccountsType"));
					finAccnt.setSubType(((PSD2Account) (account)).getAccountType());
					finAccnt.setName(custInfo.getName());
					// Hashed
					finAccnt.setHashedAccountNumber(convertStringToHash(account.getAccount().getIdentification()));
					finAccnt.setAccountHolder(
							(custInfo.getId() != null) ? custInfo.getId() : FraudSystemConstants.CONTACT_ID);
					finAccnt.setRoutingNumber(
							((PSD2Account) (account)).getAdditionalInformation().get(PSD2Constants.ACCOUNT_NSC));
					finAccnt.setCurrency(account.getCurrency());
					finAccntList.add(finAccnt);
				}
			});
			Map<String, Map<String, Object>> fraudSystemRequestMaps = fraudSystemRequestAttributes
					.getFraudSystemRequest();
			Map<String, Object> fraudSystemEventMap = fraudSystemRequestMaps
					.get(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP);
			fraudSystemEventMap.put(FraudSystemRequestMapping.FIN_ACCNTS, finAccntList);
			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, fraudSystemEventMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureFinanacialAccountsInfo(PSD2CustomerInfo custInfo, Map<String, Object> params)",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
		}
	}

	@Override
	public void captureTransferInfo(Map<String, Object> params) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			request.getHeaderNames();
			Map<String, Map<String, Object>> fraudSystemRequestMaps = fraudSystemRequestAttributes
					.getFraudSystemRequest();
			Map<String, Object> fraudSystemEventMap = fraudSystemRequestMaps
					.get(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP);
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_ID, params.get(FraudSystemConstants.PAYMENT_ID));
			OffsetDateTime transDateTime = OffsetDateTime.parse(
					params.get(FraudSystemRequestMapping.TRANSFER_TIME).toString(), DateTimeFormatter.ISO_DATE_TIME);
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_TIME,
					transDateTime.plusNanos(1000000).toString().replace("1Z", "0Z"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_CURRENCY,
					params.get(FraudSystemRequestMapping.TRANSFER_CURRENCY).toString());
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_AMNT,
					params.get(FraudSystemRequestMapping.TRANSFER_AMNT).toString());
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_MEMO,
					params.get(FraudSystemRequestMapping.TRANSFER_MEMO));
			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, fraudSystemEventMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureTransferInfo(Map<String, Object> params)\r\n"
							+ "",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
		}
	}

	private String convertStringToHash(String input) {
		return StringUtils.generateHashedValue(input);
	}

	/*
	 * setClientIPAddress method will try to find out client IP address from
	 * x-forwarded-for and will check total IP address avialable in it, if it more
	 * then 2 or as per configuration then it will pick 1st value for client-IP, if
	 * value is not present then in x-forwarded-for , it will try to fetch the value
	 * from x-api-client-IP from the request header
	 * 
	 */
	private void setClientIPAddress(Map<String, Object> fraudSystemDeviceMap) {
		String xForwardedForHeader = request.getHeader(FraudSystemRequestMapping.USER_IP_ADDRESS);
		String xApiClientIP = request.getHeader(FraudSystemRequestMapping.X_API_CLIENT_IP);

		if (xForwardedForHeader == null) {
			if (NullCheckUtils.isNullOrEmpty(xApiClientIP)) {
				LOGGER.error("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
						"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureDeviceEventInfo(Map<String, Object> params)",
						DateUtilites.generateCurrentTimeStamp(),
						"No Client IP Addess Found in header-{x-forwarded-for or x-api-client-IP}");
			} else {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_IP, xApiClientIP);
			}
		} else {

			String IPAddress[] = xForwardedForHeader.split(",");
			if (IPAddress.length > 2) {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_IP, IPAddress[0].trim());
			} else
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_IP, xApiClientIP);

			/*
			 * fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_IP, new
			 * StringTokenizer(xForwardedForHeader, ",").nextToken().trim());
			 */
		}

	}

	public Map<String, Map<String, String>> getFraudSystemMap() {
		return fraudSystemMap;
	}

	public void setFraudSystemMap(Map<String, Map<String, String>> fraudSystemMap) {
		this.fraudSystemMap = fraudSystemMap;
	}

	public Map<String, String> getFraudSystem() {
		return fraudSystem;
	}

	public void setFraudSystem(Map<String, String> fraudSystem) {
		this.fraudSystem = fraudSystem;
	}
}
