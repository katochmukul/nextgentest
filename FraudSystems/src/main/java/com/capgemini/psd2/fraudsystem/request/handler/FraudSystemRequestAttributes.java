/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.fraudsystem.request.handler;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * The Class RequestHOseaderAttributes.
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class FraudSystemRequestAttributes {

	private Map<String, Map<String,Object>> fraudSystemRequest;

	public Map<String, Map<String, Object>> getFraudSystemRequest() {
		return fraudSystemRequest;
	}

	public void setFraudSystemRequest(Map<String,  Map<String,Object>> fraudSystemRequest) {
		this.fraudSystemRequest = fraudSystemRequest;
	}

}
