package com.capgemini.psd2.fraudsystem.request.handler;

import java.util.Map;

import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public interface FraudSystemRequestHandler {
	public void captureDeviceEventInfo(Map<String, Object> params);
	public PSD2CustomerInfo captureContactInfo(Map<String, Object> params);
	public void captureFinanacialAccountsInfo(PSD2CustomerInfo custInfo, Map<String, Object> params);
	public void captureTransferInfo(Map<String, Object> params);
} 