/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.routing.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.balance.routing.adapter.impl.AccounBalanceRoutingAdapter;
import com.capgemini.psd2.account.balance.routing.adapter.routing.AccountBalanceAdapterFactory;
import com.capgemini.psd2.account.balance.routing.adapter.test.adapter.AccountBalanceTestRoutingAdapter;
import com.capgemini.psd2.account.balance.routing.adapter.test.mock.data.AccountBalanceRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;

/**
 * The Class AccountBalanceRoutingAdapterTest.
 */
public class AccountBalanceRoutingAdapterTest {

	/** The account balance adapter factory. */
	@Mock
	private AccountBalanceAdapterFactory accountBalanceAdapterFactory;

	/** The account balance routing adapter. */
	@InjectMocks
	private AccountBalanceAdapter accountBalanceRoutingAdapter = new AccounBalanceRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account balancetest.
	 */
	@Test
	public void retrieveAccountBalancetest() {

		AccountBalanceAdapter accountBalanceAdapter = new AccountBalanceTestRoutingAdapter();
		Mockito.when(accountBalanceAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountBalanceAdapter);
		
		BalancesGETResponse balancesGETResponse = accountBalanceRoutingAdapter.retrieveAccountBalance(AccountBalanceRoutingAdapterTestMockData.getMockAccountMapping(), null);
		
		assertEquals("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", balancesGETResponse.getData().getBalance().get(0).getAccountId());

	}

}
