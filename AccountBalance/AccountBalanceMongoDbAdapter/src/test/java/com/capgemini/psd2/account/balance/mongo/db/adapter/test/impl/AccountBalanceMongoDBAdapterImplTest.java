/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.balance.mongo.db.adapter.impl.AccountBalanceMongoDbAdaptorImpl;
import com.capgemini.psd2.account.balance.mongo.db.adapter.repository.AccountBalanceRepository;
import com.capgemini.psd2.account.balance.mongo.db.adapter.test.mock.data.AccountBalanceAdapterMockData;
import com.capgemini.psd2.aisp.domain.Balance;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.mock.domain.MockBalance;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class AccountBalanceMongoDBAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceMongoDBAdapterImplTest {

	/** The account balance repo. */
	@Mock
	private AccountBalanceRepository accountBalanceRepo;
	
	/** The account balance mongo db adapter. */
	@InjectMocks
	private AccountBalanceMongoDbAdaptorImpl accountBalanceMongoDbAdapter;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account balance test.
	 */
	@Test
	public void retrieveAccountBalanceTest() {
		AccountMapping accountMapping = AccountBalanceAdapterMockData.getMockAccountMapping();
		List<Balance> balanceList = AccountBalanceAdapterMockData.getBalancesList();
		List<? extends Balance> mockBalanceList=balanceList;
		
		when(accountBalanceRepo.findByAccountNumberAndAccountNSC(anyString(),anyString())).thenReturn((List<MockBalance>) mockBalanceList);
		BalancesGETResponse balancesGETResponse = accountBalanceMongoDbAdapter.retrieveAccountBalance(accountMapping, null);
		assertEquals("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", balancesGETResponse.getData().getBalance().get(0).getAccountId());
	}
	
	/**
	 * Retrieve account balance failure test.
	 */
	@Test(expected = PSD2Exception.class)
	public void retrieveAccountBalanceFailureTest() {
		AccountMapping accountMapping = AccountBalanceAdapterMockData.getMockAccountMapping();
		when(accountBalanceRepo.findByAccountNumberAndAccountNSC(anyString(),anyString())).thenReturn(null);
		accountBalanceMongoDbAdapter.retrieveAccountBalance(accountMapping, null);
	}
	
	/**
	 * Retrieve account balance data access resource failure exception test.
	 */
	@Test(expected = PSD2Exception.class)
	public void retrieveAccountBalanceDataAccessResourceFailureExceptionTest() {
		AccountMapping accountMapping = AccountBalanceAdapterMockData.getMockAccountMapping();
		when(accountBalanceRepo.findByAccountNumberAndAccountNSC(anyString(),anyString())).thenThrow(DataAccessResourceFailureException.class);
		accountBalanceMongoDbAdapter.retrieveAccountBalance(accountMapping, null);
	}

}
