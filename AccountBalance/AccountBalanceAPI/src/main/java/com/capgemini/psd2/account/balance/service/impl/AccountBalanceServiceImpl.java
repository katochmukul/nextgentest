/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.balance.service.AccountBalanceService;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountBalanceServiceImpl.
 */
@Service
public class AccountBalanceServiceImpl implements AccountBalanceService {

	/** The adapter. */
	@Autowired
	@Qualifier("accountBalanceAdapter")
	private AccountBalanceAdapter adapter;

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The consent mapping adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.account.balance.service.AccountBalanceService#
	 * retrieveAccountBalance(java.lang.String)
	 */
	@Override
	public BalancesGETResponse retrieveAccountBalance(String accountId) {
		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);
		BalancesGETResponse balancesGETResponse = adapter.retrieveAccountBalance(accountMapping,
				reqHeaderAtrributes.getToken().getSeviceParams());
		if (balancesGETResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);
		if (balancesGETResponse.getLinks() == null)
			balancesGETResponse.setLinks(new Links());
		if (balancesGETResponse.getMeta() == null)
			balancesGETResponse.setMeta(new MetaData());
		balancesGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		balancesGETResponse.getMeta().setTotalPages(1);
		return balancesGETResponse;
	}

}
