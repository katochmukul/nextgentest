/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.balance.service.impl.AccountBalanceServiceImpl;
import com.capgemini.psd2.account.balance.test.mock.data.AccountBalanceMockData;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountBalanceServiceImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceServiceImplTest {

	/** The adapter. */
	@Mock
	private AccountBalanceAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	/** The service. */
	@InjectMocks
	private AccountBalanceServiceImpl service = new AccountBalanceServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
		.thenReturn(AccountBalanceMockData.getMockAccountMapping());
		when(headerAtttributes.getToken()).thenReturn(AccountBalanceMockData.getToken());
	}

	/**
	 * Retrieve account balance test.
	 */
	@Test
	public void retrieveAccountBalanceTest() {
		when(adapter.retrieveAccountBalance(anyObject(), anyObject()))
				.thenReturn(AccountBalanceMockData.getBalancesGETResponse());
		BalancesGETResponse balance = service.retrieveAccountBalance(
				AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getAccountId());
		
		assertEquals(AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getAccountId(),
				balance.getData().getBalance().get(0).getAccountId());
		assertEquals(AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getAmount().getAmount(),
				balance.getData().getBalance().get(0).getAmount().getAmount());
		assertEquals(AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getAmount().getCurrency(),
				balance.getData().getBalance().get(0).getAmount().getCurrency());
		assertEquals(AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getCreditLine().get(0).getAmount()
				.getAmount(), balance.getData().getBalance().get(0).getCreditLine().get(0).getAmount().getAmount());
		assertEquals(AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getCreditLine().get(0).getAmount()
				.getCurrency(), balance.getData().getBalance().get(0).getCreditLine().get(0).getAmount().getCurrency());
		assertEquals(
				AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getCreditLine().get(0).getIncluded(),
				balance.getData().getBalance().get(0).getCreditLine().get(0).getIncluded());
	}

	/**
	 * Retrieve account balance failure test.
	 */
	@Test(expected = PSD2Exception.class)
	public void retrieveAccountBalanceFailureTest() {
		when(adapter.retrieveAccountBalance(anyObject(), anyObject())).thenReturn(null);
		service.retrieveAccountBalance(AccountBalanceMockData.getBalancesGETResponse().getData().getBalance().get(0).getAccountId());
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
}
